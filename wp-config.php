<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'democracy' );

/** MySQL database username */
define( 'DB_USER', 'democracy2' );

/** MySQL database password */
define( 'DB_PASSWORD', 'D3m0cr@c9' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FS_METHOD', 'direct');
/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '!+g))5pmit*ILC!$d%n]*3y$A}7EY*Er>zE5*[wySq(1< Skr9DR5Ks!!Z>z%qkx' );
define( 'SECURE_AUTH_KEY',  'p#PKXAw`>vloK%[V|4d91c:pqfr>k1?C>r:I@M%L.m0#]&W(_)m#3c.OolS7GzA!' );
define( 'LOGGED_IN_KEY',    '/bkEovmL--G)iZ)v:gEy,`]-Ii,6rp%r]8}&;wiTeuV(.<B%,Fs mo)t4bFua`&O' );
define( 'NONCE_KEY',        '8wq&1)8X> Bo>6sqg@;tuO/<uvxTLyanov]o8?Idhg2y`.|TK;#}=,yIw!2Uh>9 ' );
define( 'AUTH_SALT',        'to`!)s_ m_XtGU1tA4Km5B*l$NP%5ayu1g z:J~QJz^[TPHWIFWg@vGN H4r@A~t' );
define( 'SECURE_AUTH_SALT', '+qW$b lZe-efL}gf>E]$E/x>1Tc&G~)`P7|yjik+qV4l4_Y(3[(.iF,Fr|dN]}jo' );
define( 'LOGGED_IN_SALT',   '/J8YpC-E!HtVT6(5#m=cbh6{wl(0D4}N-}l-3x:u[*%hCS+BCYtS/JlSsI:_7*{d' );
define( 'NONCE_SALT',       'k8rxLcER^2H+b,4Y7/mjcj*`u> &;3*Cx#SA}gI~2hTUQZ)JT`g:0o_1i]V;}T5X' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_DISPLAY', true );
define( 'WP_DEBUG_LOG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}


/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
