<?php
/**
 * Plugin Name:       Multiple Shipping Addresses for WooCommerce (Address Book) - PRO
 * Plugin URI:        https://themehigh.com/product/woocommerce-multiple-addresses-pro/
 * Description:       Lets you save multiple addresses, as well as ship different products to as many multiple addresses in a single purchase.
 * Version:           2.0.0
 * Author:            ThemeHigh
 * Author URI:        https://themehigh.com/
 *
 * Text Domain:       woocommerce-multiple-addresses-pro
 * Domain Path:       /languages
 * Update URI:          https://www.themehigh.com/product/woocommerce-multiple-addresses-pro/
 *
 * WC requires at least: 3.0.0
 * WC tested up to: 5.7.1
 */

if(!defined('WPINC')){	die; }

if (!function_exists('is_woocommerce_active')){
	function is_woocommerce_active(){
	    $active_plugins = (array) get_option('active_plugins', array());
	    if(is_multisite()){
		   $active_plugins = array_merge($active_plugins, get_site_option('active_sitewide_plugins', array()));
	    }
	    return in_array('woocommerce/woocommerce.php', $active_plugins) || array_key_exists('woocommerce/woocommerce.php', $active_plugins);
	}
}

if(is_woocommerce_active()) {
	define('THWMA_VERSION', '2.0.0');
	!defined('THWMA_SOFTWARE_TITLE') && define('THWMA_SOFTWARE_TITLE', 'WooCommerce Multiple Addresses');
	!defined('THWMA_FILE') && define('THWMA_FILE', __FILE__);
	!defined('THWMA_PATH') && define('THWMA_PATH', plugin_dir_path( __FILE__ ));
	!defined('THWMA_URL') && define('THWMA_URL', plugins_url( '/', __FILE__ ));
	!defined('THWMA_BASE_NAME') && define('THWMA_BASE_NAME', plugin_basename( __FILE__ ));
	
	/**
	 * The code that runs during plugin activation.
	 */
	function activate_thwma() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-thwma-activator.php';
		THWMA_Activator::activate();
	}
	
	/**
	 * The code that runs during plugin deactivation.
	 */
	function deactivate_thwma() {
		require_once plugin_dir_path( __FILE__ ) . 'includes/class-thwma-deactivator.php';
		THWMA_Deactivator::deactivate();
	}
	
	register_activation_hook( __FILE__, 'activate_thwma' );
	register_deactivation_hook( __FILE__, 'deactivate_thwma' );

	function thwma_license_form_title_note($title_note){
        $help_doc_url = 'https://www.themehigh.com/help-guides/general-guides/download-purchased-plugin-file';

        $title_note .= ' Find out how to <a href="%s" target="_blank">get your license key</a>.';
        $title_note  = sprintf($title_note, $help_doc_url);
        return $title_note;
    }

	function thwma_license_page_url($url, $prefix){
		$url = 'admin.php?page=th_multiple_addresses_pro&tab=license_settings';
		return admin_url($url);
	}
	
	/*function init_auto_updater_thwma(){
		if(!class_exists('THWMA_License_Manager') ) {
			add_filter('thlm_license_form_title_note_woocommerce_multiple_addresses', 'thwma_license_form_title_note');
			add_filter('thlm_license_page_url_woocommerce_multiple_addresses', 'thwma_license_page_url', 10, 2);
			add_filter('thlm_enable_default_license_page', '__return_false');

			require_once( plugin_dir_path( __FILE__ ) . 'class-thwma-license-manager.php' );
			$api_url = 'https://themehigh.com/';
			THWMA_License_Manager::instance(__FILE__, $api_url, 'plugin', THWMA_SOFTWARE_TITLE);
		}
	}
	init_auto_updater_thwma();
	*/

	function init_edd_updater_thwma(){
		if(!class_exists('THWMA_License_Manager') ) {

			require_once( plugin_dir_path( __FILE__ ) . 'class-thwma-license-manager.php' );
			$helper_data = array(
				'api_url' => 'https://www.themehigh.com', // API URL
				'product_id' => 26, // Product ID in store
				'product_name' => 'Multiple Addresses for WooCommerce', // Product name in store. This must be unique.
				'license_page_url' => admin_url('plugins.php?page=my-plugin-license'), // ;icense page URL
			);

			THWMA_License_Manager::instance(__FILE__, $helper_data);
		}
	}
	init_edd_updater_thwma();
	
	/**
	 * The core plugin class that is used to define internationalization,
	 * admin-specific hooks, and public-facing site hooks.
	 */
	require plugin_dir_path( __FILE__ ) . 'includes/class-thwma.php';
	
	/**
	 * Begins execution of the plugin.
	 */
	function run_thwma() {
		$plugin = new THWMA();
		$plugin->run();
	}
	run_thwma();

	function thwma_lm_to_edd_license_migration() {
		$edd_license_key = 'th_multiple_addresses_for_woocommerce_license_data';
		$edd_license_data = get_option($edd_license_key, array());
		if(empty($edd_license_data)){
			$lm_software_title = "WooCommerce Multiple Addresses";
			$lm_prefix = str_ireplace(array( ' ', '_', '&', '?', '-' ), '_', strtolower($lm_software_title));
			$lm_license_key = $lm_prefix . '_thlmdata';
			$lm_license_data = thwma_get_thlm_saved_license_data($lm_license_key);
			if($lm_license_data){
				$status = isset($lm_license_data['status']) ? $lm_license_data['status'] : '';
				if($status = 'active'){
					$new_data = array(
						'license_key' => isset($lm_license_data['license_key']) ? $lm_license_data['license_key'] : '',
						'expiry' => isset($lm_license_data['expiry_date']) ? $lm_license_data['expiry_date'] : '',
						'status' => 'valid',
					);
					$result = thwma_update_edd_license_data($edd_license_key, $new_data);
					if($result){
						thwma_delete_lm_license_data($lm_license_key);
					}
				}
			}
		}
		
	}
	add_action( 'admin_init', 'thwma_lm_to_edd_license_migration' );

	function thwma_get_thlm_saved_license_data($key){
		$license_data = '';
		if(is_multisite()){
			$license_data = get_site_option($key);
		}else{
			$license_data = get_option($key);
		}
		return $license_data;
	}

	function thwma_update_edd_license_data($edd_license_key, $data){
		$result = false;
		if(is_multisite()){
			$result = update_site_option($edd_license_key, $data, 'no');
		}else{
			$result = update_option($edd_license_key, $data, 'no');
		}
		return $result;
	}

	function thwma_delete_lm_license_data($key){
		if(is_multisite()){
			delete_site_option($key);
		}else{
			delete_option($key);
		}
	}
}