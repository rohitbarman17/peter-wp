<?php
/**
 * The admin order page functionality of the plugin
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    woocommerce-multiple-addresses-pro
 * @subpackage woocommerce-multiple-addresses-pro/admin
 */
if(!defined('WPINC')) { 
	die; 
}

if(!class_exists('THWMA_Admin_Order_Settings')) :

	/**
     * The Admin order settings class.
     */ 
	class THWMA_Admin_Order_Settings extends THWMA_Admin_Settings {
		public $email_delivery_date;
		public $product_name;
		public $product_qty;
		public $product_price;
		public function __construct() {
			$this->define_order_page_hook();
			$this->email_delivery_date ='';
			$this->product_name = ''; 
			$this->product_qty = '';
			$this->product_price = '';
			$this->delivery_time = '';
		}

		/**
         * Function for define order page hook.
         */
		public function define_order_page_hook() {
			//add_action('woocommerce_after_order_itemmeta', array($this. 'thwma_multi_addresses_after_order_itemmeta'), 10, 3); 
			add_action('woocommerce_admin_order_data_after_shipping_address', array($this, 'thwma_update_woo_order_status'),10,1);
			add_action('woocommerce_admin_order_preview_line_items', array($this, 'thwma_admin_order_preview_line_items'),10,2);
		}

		/**
         * Function for update woocommerce order status.
         * 
         * @param array $order The order details
         */
		function thwma_update_woo_order_status($order) {
			$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping']:'';
			$user_id = get_current_user_id();
			if($cart_shipping_enabled == 'yes') {
				$enable_multi_ship_data = '';
		        if (is_user_logged_in()) {
		        	$enable_multi_ship_data = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
		        } else {
		        	//$enable_multi_ship_data = get_transient(THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP);
		        	$enable_multi_ship_data = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP])?$_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]:'';
		        }

		        if($enable_multi_ship_data == 'yes') {
					echo '<input type="hidden" name="multi_ship_enabled" value="yes" class="multi_ship_enabled">';
				} else {
					echo '<input type="hidden" name="multi_ship_enabled" value="" class="multi_ship_enabled">';
				}
			} else {
				echo '<input type="hidden" name="multi_ship_enabled" value="" class="multi_ship_enabled">';
			}
		}

		function thwma_admin_order_preview_line_items($order_items, $order) {
			return $order_items;
		}
		// public static function thwma_multi_addresses_after_order_itemmeta($item_id, $item, $product) {

		// 	$shipping_address_data = wc_get_order_item_meta($item_id, THWMA_Utils::ORDER_KEY_SHIPPING_ADDR, 'true');
		// }
	}
endif;