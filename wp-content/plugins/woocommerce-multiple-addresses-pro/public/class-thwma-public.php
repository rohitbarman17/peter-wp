<?php
/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://themehigh.com
 * @since      1.0.0
 *
 * @package    woocommerce-multiple-addresses-pro
 * @subpackage woocommerce-multiple-addresses-pro/public
 */
if(!defined('WPINC')) {	
	die; 
}

if(!class_exists('THWMA_Public')) :
 
 	/**
     * Public class.
     */
	class THWMA_Public {	
		const DEFAULT_SHIPPING_ADDRESS_KEY = 'thwma-shipping-alt';
		const DEFAULT_BILLING_ADDRESS_KEY  = 'thwma-billing-alt';
		const THWMA_TEXT_DOMAIN = 'thwma';	
		private $plugin_name;
		private $version;
		public static $is_creating_suborder = false;

		/**
         * Constructor.
         *
         * @param string $plugin_name The plugin name
         * @param string $version The plugin version number
         */
		public function __construct($plugin_name, $version) {	
			$this->plugin_name = $plugin_name;
			$this->version = $version;
			add_action('after_setup_theme', array($this, 'define_public_hooks'));
		
		}

		/**
         * Enqueue script and style.
         */
		public function enqueue_styles_and_scripts() { 
			global $wp_scripts;
			if(is_wc_endpoint_url('edit-address') || (is_checkout()) || is_cart() || is_wc_endpoint_url('view-order') || apply_filters('thwma_force_enqueue_public_scripts', false)) {
				$debug_mode = apply_filters('thwma_debug_mode', false);
				$suffix = $debug_mode ? '' : '.min';
				
				$jquery_version = isset($wp_scripts->registered['jquery-ui-core']->ver) ? $wp_scripts->registered['jquery-ui-core']->ver : '1.9.2';
				
				$this->enqueue_styles($suffix, $jquery_version);
				$this->enqueue_scripts($suffix, $jquery_version);
			}
		}
		
		/**
         * Function for enqueue style.
         *
         * @param string $suffix The suffix of the style sheet file
         * @param string $jquery_version The passed jquery version
         */
		private function enqueue_styles($suffix, $jquery_version) {
			wp_register_style('select2', THWMA_WOO_ASSETS_URL.'/css/select2.css');
			wp_enqueue_style('woocommerce_frontend_styles');
			wp_enqueue_style('select2');
			wp_enqueue_style('dashicons');
			//wp_enqueue_style('jquery-ui-style', '//ajax.googleapis.com/ajax/libs/jqueryui/'. $jquery_version .'/themes/smoothness/jquery-ui.css');		
			//wp_enqueue_style('FontAwesome', 'https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css');
			wp_enqueue_style('jquery-ui-style', THWMA_ASSETS_URL_PUBLIC . 'css/jquery-ui.min.css', 'v1.12.1');
			wp_enqueue_style('font-awesome-style', THWMA_ASSETS_URL_PUBLIC . 'css/font-awesome.min.css', '3.0.2');	
			wp_enqueue_style('thwma-public-style', THWMA_ASSETS_URL_PUBLIC . 'css/thwma-public'. $suffix .'.css', $this->version);			
		}

		/**
         * Function for enqueue script.
         *
         * @param string $suffix The suffix of the style sheet file
         * @param string $jquery_version The passed jquery version 
         * @param string $is_quick_view string for check quick view of the theme flatsome
         */
		private function enqueue_scripts($suffix, $jquery_version) {
			wp_register_script('thwma-public-script', THWMA_ASSETS_URL_PUBLIC . 'js/thwma-public'. $suffix .'.js', array('jquery', 'jquery-ui-dialog', 'jquery-ui-accordion', 'select2',), $this->version, true);
			wp_enqueue_script('thwma-public-script');

			if(THWMA_Utils:: get_advanced_settings_value('enable_autofill') == 'yes') {
				$api_key = THWMA_Utils:: get_advanced_settings_value('autofill_apikey');
				if($api_key) {
					wp_enqueue_script('google-autocomplete', 'https://maps.googleapis.com/maps/api/js?v=3&libraries=places&key='.$api_key);
				}
			}

			$select_options = apply_filters('thwma_checkout_select_options', false);	

			$address_fields_billing = THWMA_Utils::get_address_fields_by_address_key('billing_');
			$address_fields_shipping = THWMA_Utils::get_address_fields_by_address_key('shipping_');

			$section_settings = THWMA_Utils::get_custom_section_settings();
			$section_settings = $section_settings ? $section_settings : '';

			$store_country = WC()->countries->get_base_country();	
			$sell_countries =  WC()->countries->get_allowed_countries();
			$specific_coutries =  array();

			// convert auto fill state code.
			// $state_code_array = array(
			// 	'Gro.' => 'GR',
			// 	'Jal.' => 'JA',
			//);

			// Filter for state list.
			$state_code_convert_data = apply_filters('thwma_state_code_list', $state_code_array=null);
			$state_code_convert = json_encode($state_code_convert_data);
			// Filter for console.log.
			$enable_console_log = apply_filters('enable_consolelog_to_display_state_code_from_google_api', false);
		
			$thwcfe_is_active = THWMA_Utils::check_thwcfe_plugin_is_active();
			if(is_checkout()) {
				$flag = true;
			} else{
				$flag = false;
			}

			global $current_user;
   			$current_user_id = get_current_user_id();

   			// Address limit.
			$address_limit_bil = '';
			$address_limit_ship = '';
            $address_limit_bil = THWMA_Utils::get_setting_value('settings_billing' , 'billing_address_limit');
            $address_limit_ship = THWMA_Utils::get_setting_value('settings_shipping' , 'shipping_address_limit');

            if (!is_numeric($address_limit_bil)) {
                $address_limit_bil = 0;
            }

            if (!is_numeric($address_limit_ship)) {
                $address_limit_ship = 0;
            }

            // Default address.
            $customer_id = get_current_user_id();
            $default_set_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_billing');
			$same_address = THWMA_Utils::is_same_address_exists($customer_id, 'billing');
			$default_bil_address = 	$same_address ? $same_address : $default_set_address;

            $default_set_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_shipping');
			$same_address = THWMA_Utils::is_same_address_exists($customer_id, 'shipping');
			$default_ship_address = 	$same_address ? $same_address : $default_set_address;


			$script_var = array(
				'ajax_url'    => admin_url('admin-ajax.php'),
				'address_fields_billing'	=> $address_fields_billing,
				'address_fields_shipping'	=> $address_fields_shipping,
				'current_user_id'			=> $current_user_id,
				
				'enable_autofill' 	=> THWMA_Utils:: get_advanced_settings_value('enable_autofill'),
				'store_country' 	=> $store_country,
				'sell_countries' 	=> $sell_countries,
				'specific_country' 	=> apply_filters('thwma_auto_fill_specific_country', $specific_coutries),		
				'custom_sections' 	=> $section_settings,
				'select_options' 	=> $select_options,
				'billing_address' 	=> esc_html__('Billing Addresses', 'woocommerce-multiple-addresses-pro'),
				'shipping_address'  => esc_html__('Shipping Addresses', 'woocommerce-multiple-addresses-pro'),
				'addresses'			=> esc_html__('Addresses', 'woocommerce-multiple-addresses-pro'),
				'state_code_convert'=> $state_code_convert,
				'enable_console_log'=> $enable_console_log,
				'thwcfe_is_active'  => $thwcfe_is_active,
				'is_checkout_page'  => $flag,
				'billing_adr_limit' => $address_limit_bil,
				'shipping_adr_limit'=> $address_limit_ship,
				'default_bil_address'=> $default_bil_address,
				'default_ship_address'=> $default_ship_address,
				'get_address_with_id_nonce' 					=> wp_create_nonce( 'get-address-with-id' ),
				'delete_address_with_id_nonce' 					=> wp_create_nonce( 'delete-address-with-id' ),	
				'set_default_address_nonce' 					=> wp_create_nonce( 'set-default-address' ),	
				'delete_address_with_id_cart_nonce' 			=> wp_create_nonce( 'delete-address-with-id-cart' ),				
				'set_default_address_cart_nonce' 				=> wp_create_nonce( 'set-default-address-cart' ),	
				'enable_ship_to_multi_address_nonce' 			=> wp_create_nonce( 'enable-ship-to-multi-address' ),	
				'add_new_shipping_address_nonce' 				=> wp_create_nonce( 'add-new-shipping-address' ),	
				'thwma_save_address_nonce' 						=> wp_create_nonce( 'thwma-save-address' ),	
				'guest_users_add_new_shipping_address_nonce' 	=> wp_create_nonce( 'guest-users-add-new-shipping-address' ),	
				'thwma_save_guest_address_nonce' 				=> wp_create_nonce( 'thwma-save-guest-address' ),	
				'delete_address_with_id_cart_guest_nonce' 		=> wp_create_nonce( 'delete-address-with-id-cart-guest' ),	
				'save_multi_selected_shipping_nonce' 			=> wp_create_nonce( 'save-multi-selected-shipping' ),	
				'save_shipping_method_details_nonce' 			=> wp_create_nonce( 'save-shipping-method-details' ),	
				'additional_address_management_nonce' 			=> wp_create_nonce( 'additional-address-management' ),	
				'remove_multi_shipping_row_nonce' 				=> wp_create_nonce( 'remove-multi-shipping-row' ),	
				'update_multi_shipping_qty_field_nonce' 		=> wp_create_nonce( 'update-multi-shipping-qty-field' ),	
			);
			wp_localize_script('thwma-public-script', 'thwma_public_var', $script_var);
		}
		
		/**
         * Function for enqueue public hooks.
         */
		public function define_public_hooks() {
			if (THWMA_Utils::check_thwcfe_plugin_is_active()) {	
				add_filter('thwcfe_force_enqueue_checkout_public_scripts', '__return_true');
			}

			// Detects "create suborders" button click.
			add_action('save_post', array($this, 'thwma_create_suborders_call_on_btn_click'));

			// add_action('all', array($this, 'th_show_all_hooks')); 
			add_filter('woocommerce_address_to_edit', array($this, 'change_edit_form'), 10, 2);	
			add_action('woocommerce_after_save_address_validation', array($this, 'save_address'), 10, 3);
				
			// My-account.
			add_action('thwma_after_address_display', array($this, 'display_custom_addresses'));		

			add_action('woocommerce_before_edit_account_address_form', array($this, 'delete_address'));
			add_action('woocommerce_before_edit_account_address_form', array($this, 'set_default_billing_address'));
			add_action('woocommerce_before_edit_account_address_form', array($this, 'set_default_shipping_address'));
			add_action('woocommerce_before_checkout_billing_form', array($this, 'session_update_billing'));
			add_action('woocommerce_before_checkout_shipping_form', array($this, 'session_update_shipping'));
			add_action('woocommerce_before_checkout_billing_form', array($this, 'address_above_billing_form'));
			add_action('woocommerce_after_checkout_billing_form', array($this, 'address_below_billing_form'));

			if(!is_admin()) {
				add_filter('woocommerce_checkout_fields', array($this, 'add_hidden_field_to_checkout_fields'), 9);
			}
			add_filter('woocommerce_form_field_hidden', array($this, 'add_hidden_field'), 5, 4);
			add_action('woocommerce_checkout_order_processed', array($this, 'update_custom_billing_address_from_checkout'), 10, 3);
			add_action('woocommerce_before_checkout_shipping_form', array($this, 'address_above_shipping_form'));
			add_action('woocommerce_after_checkout_shipping_form', array($this, 'address_below_shipping_form'));
			add_action('woocommerce_checkout_order_processed', array($this, 'update_custom_shipping_address_from_checkout'), 10, 3);

			add_action('wp_ajax_get_address_with_id', array($this, 'get_addresses_by_id'));
	    	add_action('wp_ajax_nopriv_get_address_with_id', array($this, 'get_addresses_by_id'));

			add_action('wp_ajax_delete_address_with_id', array($this, 'delete_address_from_checkout'));
	    	add_action('wp_ajax_nopriv_delete_address_with_id', array($this, 'delete_address_from_checkout'));

			add_action('wp_ajax_set_default_address', array($this, 'default_address_from_checkout'));
	    	add_action('wp_ajax_nopriv_set_default_address', array($this, 'default_address_from_checkout'));

	    	$atype = true;
	    	if(apply_filters('disable_additional_address_field_on_default_address', true)){
	    		$atype = isset($_GET['atype']);
	    	}
			//if(isset($_GET['atype'])) {
	    	if($atype) {
				add_filter('woocommerce_billing_fields', array($this, 'add_additional_billing_field'), 1000, 2);
				add_filter('woocommerce_shipping_fields', array($this, 'add_additional_shipping_field'), 1000, 2);
			}

			add_action('woocommerce_after_checkout_validation', array($this, 'add_address_from_checkout'), 30, 2);		
			add_filter('woocommerce_billing_fields', array($this, 'prepare_address_fields_before_billing'), 1500, 2);
			add_filter('woocommerce_shipping_fields', array($this, 'prepare_address_fields_before_shipping'), 1500, 2);		
		
			add_filter('woocommerce_localisation_address_formats', array($this, 'localisation_address_formats'));

			$section_settings = THWMA_Utils::get_custom_section_settings();
			if(is_user_logged_in()) {
				if($section_settings) {
					if(!empty($section_settings) && is_array($section_settings)){
						foreach ($section_settings as $section => $props) {						
							if($props['enable_section'] == 'yes') {
								
								add_action('thwcfe_before_section_fields_'.$section, array($this, 'add_additional_fields'), 10, 1);
							}							
						}
					}					
				}
			}

			$shipping_section = isset($_POST['ship_to_different_address']) ? sanitize_key($_POST['ship_to_different_address']) : '';

			//if($shipping_section == true){
	    	$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping']:'';
			$enable_product_disticty = isset($settings['enable_product_disticty']) ? $settings['enable_product_disticty']:'';
			$user_id = get_current_user_id();
	        $enable_multi_ship = '';

	        // Enable multi-shipping on checkout page.
	        if (is_user_logged_in()) {
	        	$enable_multi_ship = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
	        } else {
	        	$enable_multi_ship = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]) ? $_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]:'';
	        }

			// Checkout page- multi-shipping.
			add_action('wp_ajax_enable_ship_to_multi_address', array($this, 'enable_ship_to_multi_address'),10);
	    	add_action('wp_ajax_nopriv_enable_ship_to_multi_address', array($this, 'enable_ship_to_multi_address'),10);

	    	// multi-shipping delete address.
	    	add_action('wp_ajax_delete_address_with_id_cart', array($this, 'delete_address_from_cart'));
	    	add_action('wp_ajax_nopriv_delete_address_with_id_cart', array($this, 'delete_address_from_cart'));

	    	// multi-shipping set to default address.
			add_action('wp_ajax_set_default_address_cart', array($this, 'default_address_from_cart'));
	    	add_action('wp_ajax_nopriv_set_default_address_cart', array($this, 'default_address_from_cart'));

	    	if($enable_multi_ship == 'yes'){
				// Save seleted addresses-multi shipping.
		    	add_action('wp_ajax_save_multi_selected_shipping', array($this, 'save_multi_selected_shipping'));
		    	add_action('wp_ajax_nopriv_save_multi_selected_shipping', array($this, 'save_multi_selected_shipping'));
		    }
	        			
	        if($cart_shipping_enabled == 'yes') {
	        	if($enable_multi_ship == 'yes') {
					add_filter('woocommerce_shipping_show_shipping_calculator', '__return_false');
					add_action('woocommerce_cart_shipping_packages', array($this, 'thwma_get_shipping_packages'), 10, 1);
					//add_action('woocommerce_package_rates', array($this, 'thwma_woocommerce_package_rates'), 10, 2);

					// fill shipping fields by using default address.
					add_filter( 'woocommerce_checkout_get_value', array($this, 'thwma_fill_shipping_fields_values'), 99, 2 );			        		
	        	} else {
					add_action('woocommerce_cart_shipping_packages', array($this, 'thwma_get_shipping_packages_default'), 10, 1);
				}

				if($enable_product_disticty == 'yes') {
		    		add_filter('woocommerce_add_cart_item_data', array($this,'thwma_namespace_force_individual_cart_items'), 10, 3);
		    	}
	    	}
	    	
	    	if($enable_multi_ship == 'yes'){
		    	// Multi-addresses add to order meta.
		  		if(THWMA_Utils::woo_version_check('3.3')) {
		  			add_action('woocommerce_new_order_item', array($this, 'thwma_add_addrs_to_new_order_item'), 1, 3);						
				} else {
		    		add_action('woocommerce_add_order_item_meta', array($this, 'thwma_add_addrs_to_order_item_meta'), 1, 3);	
				}


				// Shipping method disaply on order edit page.
				add_action('woocommerce_checkout_create_order_shipping_item', array($this, 'order_shipping_item'), 300, 4);
			}
			//cmnt.
			// Display Thankyou page.
			add_filter('woocommerce_order_item_get_formatted_meta_data', array($this, 'thwma_shipping_addresses_display_on_thankyou_page'), 10, 2);
			add_filter('woocommerce_order_get_formatted_shipping_address', array($this, 'thwma_overrides_shipping_address_section_on_thankyou_page'), 10, 3);

			////////////////////////////////////////////

			//Order again.
			add_filter('woocommerce_order_again_cart_item_data', array($this, 'thwma_filter_order_again_cart_item_data'), 10, 3); 

			// Shipping method saving.			
	    	add_action('wp_ajax_save_shipping_method_details', array($this, 'save_shipping_method_details'));
			add_action('wp_ajax_nopriv_save_shipping_method_details', array($this, 'save_shipping_method_details'));

			add_action('wp_ajax_additional_address_management', array($this, 'additional_address_management'));
			add_action('wp_ajax_nopriv_additional_address_management', array($this, 'additional_address_management'));

			add_action('wp_ajax_remove_multi_shipping_row', array($this, 'remove_multi_shipping_row'));
			add_action('wp_ajax_nopriv_remove_multi_shipping_row', array($this, 'remove_multi_shipping_row'));

			
			add_action('wp_ajax_update_multi_shipping_qty_field', array($this, 'update_multi_shipping_qty_field'));
			add_action('wp_ajax_nopriv_update_multi_shipping_qty_field', array($this, 'update_multi_shipping_qty_field'));

			//cmnt.
			if($enable_multi_ship == 'yes'){

				// Set sub orders.
				add_action( 'woocommerce_thankyou_order_id', array( $this, 'thwma_set_sub_order_id' ),10 );

				// Remove main order data.
				//add_action( 'woocommerce_thankyou', array( $this, 'thwma_remove_main_order' ),10 );
				
				add_action( 'woocommerce_order_details_after_order_table', array( $this, 'thwma_before_thankyou_account' ),10 );
				add_action( 'admin_footer', array( $this, 'admin_dashboard_subscriptions_filter_callback' ),1000 );		
				//}
			}

			//Add new shipping address.
			add_action('wp_ajax_add_new_shipping_address', array($this, 'add_new_shipping_address'));
	    	add_action('wp_ajax_nopriv_add_new_shipping_address', array($this, 'add_new_shipping_address'));

	    	add_action('wp_ajax_thwma_save_address', array($this, 'thwma_save_address'),999);
	    	add_action('wp_ajax_nopriv_thwma_save_address', array($this, 'thwma_save_address'),999);

	    	// Multi-shipping (Guest user).
	    	add_action('wp_ajax_guest_users_add_new_shipping_address', array($this, 'guest_users_add_new_shipping_address'));
	    	add_action('wp_ajax_nopriv_guest_users_add_new_shipping_address', array($this, 'guest_users_add_new_shipping_address'));

	    	add_action('wp_ajax_thwma_save_guest_address', array($this, 'thwma_save_guest_address'));
	    	add_action('wp_ajax_nopriv_thwma_save_guest_address', array($this, 'thwma_save_guest_address'));

	    	// Delete guest user.
	    	add_action('wp_ajax_delete_address_with_id_cart_guest', array($this, 'delete_guest_address_from_cart'));
	    	add_action('wp_ajax_nopriv_delete_address_with_id_cart_guest', array($this, 'delete_guest_address_from_cart'));
		}	

		/**
         * Function for locate template cart-shipping.php
         * 
         * @param array $template The template datas
         * @param string $template_name The template name
         * @param string $template_path The template path link
         *
         * @return string
         */ 
	 	public function address_template($template, $template_name, $template_path) {
			$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping']:'';

			if('myaccount/my-address.php' == $template_name) {           	
	        	$template = THWMA_TEMPLATE_URL_PUBLIC.'myaccount/my-address.php';   
	        }
	        // if('cart/cart.php' == $template_name) {           	
	        // 	$template = THWMA_TEMPLATE_URL_PUBLIC.'cart/cart.php';   
	        // }
	        // if('order/order-details.php' == $template_name) {           	
	        // 	$template = THWMA_TEMPLATE_URL_PUBLIC.'order/order-details.php';   
	        // }
	        // if('order/order-details-item.php' == $template_name) {           	
	        // 	$template = THWMA_TEMPLATE_URL_PUBLIC.'order/order-details-item.php';   
	        // }
	        $user_id = get_current_user_id();
	        $enable_multi_ship = '';
	        if (is_user_logged_in()) {
	        	$enable_multi_ship = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
	        } else {
	        	$enable_multi_ship = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]) ? $_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP] : '';
	        }

	    	if($enable_multi_ship == 'yes') {
		        if($cart_shipping_enabled == 'yes') {
			        if('cart/cart-shipping.php' == $template_name) {           	
			        	$template = THWMA_TEMPLATE_URL_PUBLIC.'cart/cart-shipping.php';   
			        }
			    }
			}			
	        return $template;
	    }

	    /**
	     * Function for change the address edit form on my-account page.
	     *
	     * @param array $address The existing address
	     * @param string $load_address The loaded address type
	     *
	     * @return array
	     */
		public function change_edit_form($address, $load_address) {			
			if($load_address) {
				if(isset($_GET['atype'])) {
					$url = $_GET['atype'];
					$type = str_replace('/', '', $url);
					if($type == 'add-address') {
						$sell_countries =  WC()->countries->get_allowed_countries();
						$sell_countries_size = count($sell_countries);	
						if(!empty($address) && is_array($address)){				
							foreach ($address as $key => $value) {
								$skip = ($sell_countries_size === 1 && $key == $load_address.'_country') ? true : false;
								if (!$skip) {
									$address[$key]['value']='';		
								}
							}
						}
					} else {
						$address = $this->get_address_to_edit($load_address, $address, $type);
					}
				}
			}
			return $address;	
		}

		/**
	     * Function for add shipping heading.
	     *
	     * @param array $address_fields The existing address fields
	     * @param string $country The country details
	     *
	     * @return array
	     */
		public function add_additional_billing_field($address_fields, $country) {
			if( is_wc_endpoint_url('edit-address') ){
				if(apply_filters('enable_additional_address_field', true)){
					$label = apply_filters('additional_billing_label','Address Type');
					$placeholder = apply_filters('additional_billing_placeholder','Home/Office/Other');
					$address_fields['billing_heading'] = array(
						'label'        => esc_html__($label, 'woocommerce-multiple-addresses-pro'),
						'required'     => false,
						'class'        => array('form-row-wide', 'address-field'),
						'autocomplete' => '',
						'placeholder'  => esc_html__($placeholder, 'woocommerce-multiple-addresses-pro'),
						'priority'     => 0.5,
					);
				}
			}
			return $address_fields;
		}

		/**
	     * Function for add billing heading.
	     *
	     * @param array $address_fields The existing address fileds
	     * @param string $country The country details
	     *
	     * @return array
	     */
		public function add_additional_shipping_field($address_fields, $country) {
			if( is_wc_endpoint_url('edit-address') ){
				if(apply_filters('enable_additional_address_field', true)){
					$label = apply_filters('additional_shipping_label','Address Type');
					$placeholder = apply_filters('additional_shipping_placeholder','Home/Office/Other');
					$address_fields['shipping_heading'] = array(
							'label'        => esc_html__($label, 'woocommerce-multiple-addresses-pro'),
							'required'     => false,
							'class'        => array('form-row-wide', 'address-field'),
							'autocomplete' => '',
							'placeholder'  => esc_html__($placeholder, 'woocommerce-multiple-addresses-pro'),
							'priority'     => 0.5,
						);
				}
			}
			return $address_fields;
		}

		/**
	     * Function for Save Address from my-account section.
	     *
	     * @param integer $user_id The user id
	     * @param string $load_address The loaded address type
	     * @param array $address The addresses
	     *
	     * @return array
	     */
		public function save_address($user_id, $load_address, $address) {
			if($load_address != '') {
				if(isset($_GET['atype'])) {
					$url = isset($_GET['atype'])?sanitize_key($_GET['atype']):'';
					$type = str_replace('/', '', $url);	
					if (0 === wc_notice_count('error')) {
						if($type =='add-address') {
							if($load_address == 'billing') {
								$new_address = $this->prepare_posted_address($user_id, $address, 'billing');
								THWMA_Utils::save_address_to_user($user_id, $new_address, 'billing');	
							} elseif($load_address == 'shipping') {
								$custom_address=$this->prepare_posted_address($user_id, $address, 'shipping');
								THWMA_Utils::save_address_to_user($user_id, $custom_address, 'shipping');
							}
						} else {
							$this->update_address($user_id, $load_address, $address, $type);
						}

						if($type == 'add-address') {
							wc_add_notice(esc_html__('Address Added Successfully.', 'woocommerce-multiple-addresses-pro'));
						} else {
							wc_add_notice(esc_html__('Address Changed Successfully.', 'woocommerce-multiple-addresses-pro'));
						}
						wp_safe_redirect(wc_get_endpoint_url('edit-address', '', wc_get_page_permalink('myaccount')));
	 					exit;
					}		
				} else {
					$exist_address = get_user_meta($user_id,THWMA_Utils::ADDRESS_KEY, true);
					if(is_array($exist_address)) {
						if (0 === wc_notice_count('error')) {
							$default_key = THWMA_Utils::get_custom_addresses($user_id, 'default_'.$load_address);
							$same_address_key = THWMA_Utils::is_same_address_exists($user_id, $load_address);
							$address_key = ($default_key) ? $default_key : $same_address_key;
							
							if($address_key) {
								$this->update_address($user_id, $load_address, $address, $address_key);
							} else {

								$new_address = $this->prepare_posted_address($user_id, $address, $load_address);
								THWMA_Utils::save_address_to_user($user_id, $new_address, $load_address);
							}
						}	
					}
				}
			}
		}

		/**
	     * Function prepare for saving Address on my-account section.
	     *
	     * @param integer $user_id The user id
	     * @param string $load_address The loaded address type
	     * @param array $address The addresses
	     *
	     * @return array
	     */
		private function prepare_posted_address($user_id, $address, $type) {
			$address_new = array();
			if(!empty($address) && is_array($address)){	
				foreach ($address as $key => $value) {
					if(isset($_POST[ $key ])){
						$address_value = is_array($_POST[ $key ]) ? implode(', ', sanitize_key(wc_clean($_POST[ $key ]))): wc_clean($_POST[ $key ]);
					}
					$address_new[$key] = $address_value;
				}
			}
			$default_heading = apply_filters('thwma_default_heading', false);
			if($default_heading) {
				if($type=='billing') {
					if(isset($address_new['billing_heading']) && ($address_new['billing_heading'] == '')) {
						$address_new['billing_heading'] = esc_html__('Home', 'woocommerce-multiple-addresses-pro');
					}
				} elseif($type=='shipping') {
					if(isset($address_new['shipping_heading']) && ($address_new['shipping_heading'] == '')) {
						$address_new['shipping_heading'] = esc_html__('Home', 'woocommerce-multiple-addresses-pro');
					}
				}
			}			
			return $address_new;
		}

		/**
	     * Function for display custom addresses on my-account addresses section.
	     *
	     * @param integer $customer_id The customer id
	     */
		public function display_custom_addresses($customer_id) {
			$enable_billing = THWMA_Utils::get_setting_value('settings_billing', 'enable_billing');
			$enable_shipping = THWMA_Utils::get_setting_value('settings_shipping', 'enable_shipping');
			$custom_addresses_billing	= THWMA_Utils::get_addresses($customer_id, 'billing');			
			if(is_array($custom_addresses_billing)) {
				$billing_addresses = $this->get_account_addresses($customer_id, 'billing', $custom_addresses_billing);
			}

			$custom_addresses_shipping	= THWMA_Utils::get_addresses($customer_id, 'shipping');
			if(is_array($custom_addresses_shipping)) {
				$shipping_addresses = $this->get_account_addresses($customer_id, 'shipping', $custom_addresses_shipping);
			}

			$disable_adr_mngmnt = self::disable_address_mnagement();
			if($disable_adr_mngmnt == true) {
				$disable_mngt_class = 'thwma_disable_adr_mngt';					
			} else {
				$disable_mngt_class = '';			
			}

			$theme_class_name = $this->check_current_theme();
			$theme_class =  $theme_class_name.'_acnt'; 			 
            $additional_billing_addresses = apply_filters('additional_billing_address_label','Additional billing addresses');
            $additional_shipping_addresses = apply_filters('additional_shipping_address_label','Additional shipping addresses');
			?> 
			<div class= 'th-custom thwma_my_acnt <?php echo esc_attr($theme_class); ?>'>
				<?php if($enable_billing == 'yes') {
					if(empty($custom_addresses_billing) && $disable_adr_mngmnt == true) {
						$div_hide = 'thwma_hide_div';
					} else {
						$div_hide = '';
					} ?>					
					<div class='thwma-acnt-cus-addr th-custom-address <?php echo esc_attr($div_hide); ?>' >
						<div class = 'th-head'><h3><?php esc_html_e($additional_billing_addresses, 'woocommerce-multiple-addresses-pro'); ?> </h3></div>
						<?php if($custom_addresses_billing) {
							echo $billing_addresses; 
						} else {
							$query_arg_add_adr = add_query_arg('atype', 'add-address', wc_get_endpoint_url('edit-address', 'billing')); ?>
							<div class="add-acnt-adrs thwma-add-acnt-adrs new-adrs <?php echo esc_attr($disable_mngt_class); ?>">
		               		<?php /*<a href="<?php echo esc_url(wc_get_endpoint_url('edit-address', 'billing?atype=add-address')); ?>" class="button primary is-outline"> <i class="fa fa-plus"></i><?php  esc_html_e(' Add new address', 'woocommerce-multiple-addresses-pro'); ?> </a>*/ ?>
		               		<a href="<?php echo esc_attr($query_arg_add_adr); ?>" class="button primary is-outline"> <i class="fa fa-plus"></i><?php  esc_html_e(' Add new address', 'woocommerce-multiple-addresses-pro'); ?> </a>						 
		            		</div>
		            	<?php } ?>
					</div>
				<?php }
				if (! wc_ship_to_billing_address_only() && wc_shipping_enabled()) {
					if($enable_shipping == 'yes') {
					if(empty($custom_addresses_shipping) && $disable_adr_mngmnt == true) {
						$div_hide = 'thwma_hide_div';
					} else {
						$div_hide = '';
					} ?>					
						<div class='thwma-acnt-cus-addr th-custom-address <?php echo esc_attr($div_hide); ?>'>
							<div class = 'th-head'><h3><?php esc_html_e($additional_shipping_addresses, 'woocommerce-multiple-addresses-pro'); ?> </h3></div>
							<?php if($custom_addresses_shipping) {
								 	echo $shipping_addresses;  
							} else {
								$query_arg_add_adr = add_query_arg('atype', 'add-address', wc_get_endpoint_url('edit-address', 'shipping')); ?>
								<div class="add-acnt-adrs thwma-add-acnt-adrs new-adrs <?php echo esc_attr($disable_mngt_class); ?>">
			               			<a href="<?php echo esc_url_raw($query_arg_add_adr); ?>" class="button primary is-outline"> <i class="fa fa-plus"></i> <?php esc_html_e(" Add new address", 'woocommerce-multiple-addresses-pro'); ?></a>  
			            		</div> 
			            	<?php } ?>
						</div>
					<?php }
				} ?>
			</div>		
		<?php }

		/**
	     * Function for get account addresses (my-account page addresses section).
	     *
	     * @param integer $customer_id The user id
	     * @param string $type The loaded address type (billing/shipping)
	     * @param array $custom_addresses The custom addresses
	     *
	     * @return string
	     */
		public function get_account_addresses($customer_id,$type,$custom_addresses) {		
			$return_html = '';
			$add_class='';
			$address_type = "'$type'";
			$address_count = count(THWMA_Utils::get_custom_addresses($customer_id, $type));

			// Address limit.
			$address_limit = '';
            if($type) {
                $address_limit = THWMA_Utils::get_setting_value('settings_'.$type , $type.'_address_limit');
            }
            if (!is_numeric($address_limit)) {
                $address_limit = 0;
            }
			$disable_adr_mngmnt = self::disable_address_mnagement();
			if($disable_adr_mngmnt == true) {
				$disable_mngt_class = 'thwma_disable_adr_mngt';
				$disable_acnt_sec = 'disable_acnt_sec';
			} else {
				$disable_mngt_class = '';
				$disable_acnt_sec = '';
			}

			$default_set_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_'.$type);
			$same_address = THWMA_Utils::is_same_address_exists($customer_id, $type);
			//$default_address = $default_set_address ? $default_set_address : $same_address;	
			$default_address = 	$same_address ? $same_address : $default_set_address;

			$no_of_tile_display = '';
			if(!empty($default_address)) {
				$no_of_tile_display = 1;
			} else {
				$no_of_tile_display = 0;
			}

			if($address_limit > $no_of_tile_display) {
                if(!empty($custom_addresses)) {
					if(is_array($custom_addresses)) {
						$add_list_class  = ($type == 'billing') ? " thwma-thslider-list bill " : " thwma-thslider-list ship";
						$return_html .= '<div class="thwma-thslider">';
				           	$return_html .= '<div class="thwma-thslider-box">';
					           	$return_html .= '<div class="thwma-thslider-viewport '.esc_attr($type).'">';
						           	$return_html .= '<ul id="thwma-th-list" class="'.esc_attr($add_list_class).'">';
										$i = 0;
										if(!empty($custom_addresses)) {	
											foreach ($custom_addresses as $name => $title) {
												$default_heading = apply_filters('thwma_default_heading', false);
												if($default_heading) {
													$heading = !empty($title) ? $title : esc_html__('Home', 'woocommerce-multiple-addresses-pro') ;
												} else {
													$heading = !empty($title) ? $title : esc_html__('', 'woocommerce-multiple-addresses-pro') ;
												}				

												$address = THWMA_Utils::get_all_addresses($customer_id,$name);
												$address_key = substr($name, strpos($name,"=") + 1);
												$action_row_html = '';
												$action_def_html = '';
												$delete_msg = esc_html__('Are you sure you want to delete this address?', 'woocommerce-multiple-addresses-pro');
												//$confirm = "return confirm(". $delete_msg .");";
												$str_arr = preg_split ("/\?/", $name);
												$str_arr1 = $str_arr[0];
												$str_arr2 = $str_arr[1];
												$str_arr_sec = preg_split ("/\=/", $str_arr2);
												$str_arr_sec1 = $str_arr_sec[0];
												$str_arr_sec2 = $str_arr_sec[1];
												$query_arg = add_query_arg($str_arr_sec1, $str_arr_sec2, wc_get_endpoint_url('edit-address', $str_arr1));
												$action_row_html .= '<div class="thwma-acnt-adr-footer acnt-address-footer '.$disable_acnt_sec.'">';
													$action_row_html .= '<div class="btn-acnt-edit '.esc_attr($disable_mngt_class).'"> <a href=" '.esc_url_raw($query_arg).'" class="" title="Edit"> <span> '.esc_html__('Edit', 'woocommerce-multiple-addresses-pro').' </span> </a></div>';
													$action_row_html .= '<form action="" method="post" name="thwma_account_adr_delete_action">';
													 $action_row_html.=  '<input type="hidden" name="acnt_adr_delete_action" value="'.wp_create_nonce('thwma_account_adr_delete_action').'"> ';

														$action_row_html .=' <button type="submit" name="thwma_del_addr"  class="thwma-del-acnt th-del-acnt '.esc_attr($disable_mngt_class).'" title="Delete"  onclick="return confirm(\''. $delete_msg .'\');">'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'</button>';
														$action_row_html .=	'<input type="hidden" name="thwma_deleteby" value="'.esc_attr($type).'_'. esc_attr($address_key).'"/>';
													$action_row_html .= '</form>';
												$action_row_html .= '</div>';				 
												if($type == "billing") {
													$action_def_html.=  ' <form action="" method="post" name="thwma_bil_adr_default_action">';

													$action_def_html.=  '<button type="submit" name="thwma_default_bil_addr" id="submit-billing" class="primary button account-default thwma-acnt-dflt"  >'.esc_html__('Set as default', 'woocommerce-multiple-addresses-pro').' </button> 
													<input type="hidden" name="thwma_bil_defaultby" value="'.esc_attr($type
													).'_'. esc_attr($address_key).'"/>';

													$action_def_html.=  '<input type="hidden" name="bil_adr_default_action" value="'.wp_create_nonce('thwma_bil_adr_default_action').'"> '; 
													$action_def_html.=  '</form>';
												} else { 
													$action_def_html.= '<form action="" method="post" name="thwma_ship_adr_default_action">';

													$action_def_html.=  '<button type="submit" name="thwma_default_ship_addr" id="submit-shipping" class="primary button account-default thwma-acnt-dflt" >'.esc_html__('Set as default', 'woocommerce-multiple-addresses-pro').'</button>
													<input type="hidden" name="thwma_ship_defaultby" value="'.esc_attr($type).'_'. esc_attr($address_key).'"/>';

													$action_def_html.=  '<input type="hidden" name="ship_adr_default_action" value="'.wp_create_nonce('thwma_ship_adr_default_action').'"> ';
													$action_def_html.= '</form>';									
												}
												$query_arg_add_adr = add_query_arg('atype', 'add-address', wc_get_endpoint_url('edit-address', $type));
									            $add_address_btn = '<div class="add-acnt-adrs thwma-add-acnt-adrs '.esc_attr($disable_mngt_class).' '.esc_attr($disable_acnt_sec).'">
									               	<a href=" '.$query_arg_add_adr.'" class="button primary is-outline" >
									                	<i class="fa fa-plus"></i> '.esc_html__(' Add new address', 'woocommerce-multiple-addresses-pro').' </a>   
									            </div>';
												$add_class  = "thwma-thslider-item $type " ;
												$add_class .= $i == 0 ? ' first' : '';

												if(isset($heading) && $heading != '') {
													$show_heading = '<div class="address-type-wrapper row"> 
														<div title="'.esc_attr($heading).'" class="address-type">'.esc_attr($heading).'</div>  
														</div>
														<div class="acnt-adrr-text thwma-adr-text address-text address-wrapper">'.$address.'</div>' ;
												} else {
													$show_heading = '<div class="acnt-adrr-text thwma-adr-text address-text address-wrapper wrapper-only">'.$address.'</div>';
												}
												$return_html .= '<li class="'.esc_attr($add_class).'" value="'. esc_attr($address_key).'" >
													<div class="thwma-adr-box address-box" data-index="'.esc_attr($i).'" data-address-id=""> 
														<div class="thwma-main-content"> 
															<div class="complete-aaddress">  
																'.$show_heading.'								
															</div>						
															<div class="btn-continue address-wrapper"> 								
																'.$action_def_html.'	 								
															</div> 
														</div>
															'.$action_row_html.'
													</div>
												</li>';
												// if($i >= $address_limit-2) {
		                                        //     break;
		                                        // }

		                                        // My-account tiles.
		                                        if($default_address) {
													if($i >= $address_limit-2) {
			                                            break;
			                                        }
			                                    } else {
			                                    	if($i >= $address_limit-1) {
			                                            break;
			                                        }
			                                    }
												$i++;
											}
										}
									$return_html .= '</ul>';
								$return_html .= '</div>';
							$return_html .= '</div>';
							$return_html .= '<div class="control-buttons control-buttons-'.esc_attr($type).'">';
								$return_html .= '<input type="hidden" value="'.esc_attr($address_count).'" class="get_addr_count">';
								if($address_count) {
									if($default_address) {
										$slider_limit = 3;
									} else {
										$slider_limit = 2;
									}
									//if($address_limit > $slider_limit) {
							  			$return_html .= '<div class="prev thwma-thslider-prev '.esc_attr($type).'"><i class="fa fa-angle-left fa-3x"></i></div>';
							  			$return_html .= '<div class="next thwma-thslider-next '.esc_attr($type).'"><i class="fa fa-angle-right fa-3x"></i></div>';
							  		//}
								}
			           		$return_html .= '</div>';
				           	if($address_limit > $address_count) {

				           		$return_html .= $add_address_btn;
				           	}
			           	$return_html .= '</div>';
					}
				}
			}
			return $return_html;
		}

		/**
         * Function for get address to edit(my-account page).
         * 
         * @param string $load_address The loaded adress.
         * @param array $address  The address data.
         * @param string $type  The address type.
         *
         * @return array.
         */
		public function get_address_to_edit($load_address, $address, $type) {		
			$user_id = get_current_user_id();	
			$custom_address = THWMA_Utils::get_custom_addresses($user_id, $load_address, $type);
			if(!empty($address) && is_array($address)) {
				foreach ($address as $key => $value) {
				 	if(isset($custom_address[$key])) {					
						$address[$key]['value'] = $custom_address[$key];
						if(($key == 'shipping_state') || ($key == 'billing_state')) {
							if(!empty($custom_address) && is_array($custom_address)) {
								foreach ($custom_address as $k => $data) {
									if(($k == 'shipping_country') || ($k == 'billing_country')) {
										$address[$key]['country'] = $custom_address[$k];	
									}
								}
							}					
						}
					}
				}
			}
			return $address;
		}

		/**
         * Function for update address(my-account page).
         * 
         * @param integer $user_id The user id.
         * @param string $address_type  The type of the address.
         * @param array $address  The address data.
         * @param string $type  The address type.
         */
		public function update_address($user_id, $address_type, $address, $type) {		
			$edited_address = $this->prepare_posted_address($user_id, $address, $address_type);
			THWMA_Utils::update_address_to_user($user_id, $edited_address, $address_type, $type);	
		}
		
		/**
         * Function for Delete Address (my-account page).
         */
		public function delete_address() {
			if(isset($_POST['thwma_del_addr'])) {
				if (! isset($_POST['acnt_adr_delete_action']) || ! wp_verify_nonce($_POST['acnt_adr_delete_action'], 'thwma_account_adr_delete_action')) {
                   echo $responce = '<div class="error"><p>'.esc_html__('Sorry, your nonce did not verify.').'</p></div>';
                   exit;
                }else {
					$user_id = get_current_user_id();
					$buton_id = isset($_POST['thwma_deleteby']) ? sanitize_key($_POST['thwma_deleteby']) : '';
					$type = substr($buton_id.'_', 0, strpos($buton_id, '_'));
					$address_key = substr($buton_id, strpos($buton_id, "_") + 1);
					THWMA_Utils::delete($user_id, $type, $address_key);
				}
			}
		}

		/**
         * Function for set default billing address (my-account page).
         */
		public function set_default_billing_address() {
			$customer_id = get_current_user_id();
			if(isset($_POST['thwma_default_bil_addr'])) {
                if (! isset($_POST['bil_adr_default_action']) || ! wp_verify_nonce($_POST['bil_adr_default_action'], 'thwma_bil_adr_default_action')) {
                   echo $responce = '<div class="error"><p>'.esc_html__('Sorry, your nonce did not verify.').'</p></div>';
                   exit;
                } else {
					$address_key = isset($_POST['thwma_bil_defaultby']) ? sanitize_key($_POST['thwma_bil_defaultby']) : '';
					$address_key = str_replace(' ', '', $address_key);
					$type = substr($address_key.'_', 0, strpos($address_key, '_'));
					$custom_key = substr($address_key, strpos($address_key, "_") + 1);
					$this->change_default_address($customer_id, $type, $custom_key);
				}
			}
		}

		/**
         * Function for set default shipping address (my-account page).
         */
		public function set_default_shipping_address() {
			$customer_id = get_current_user_id();
			if(isset($_POST['thwma_default_ship_addr'])) {
				if (! isset($_POST['ship_adr_default_action']) || ! wp_verify_nonce($_POST['ship_adr_default_action'], 'thwma_ship_adr_default_action')) {
                   echo $responce = '<div class="error"><p>'.esc_html__('Sorry, your nonce did not verify.').'</p></div>';
                   exit;
                }else {
					$address_key = isset($_POST['thwma_ship_defaultby']) ? sanitize_key($_POST['thwma_ship_defaultby']) : '';
					$type = substr($address_key.'_', 0, strpos($address_key, '_'));
					$custom_key = substr($address_key, strpos($address_key, "_") + 1);
					$this->change_default_address($customer_id, $type, $custom_key);
				}
			}
		}

		/**
         * Function for change default address (my-account page).
         * 
         * @param integer $customer_id The user id.
         * @param string $type  The type of the address.
         * @param string $custom_key  The custom key.
         */
		public function change_default_address($customer_id, $type, $custom_key) {
			$default_address = THWMA_Utils::get_custom_addresses($customer_id, $type, $custom_key);
			$current_address = THWMA_Utils::get_default_address($customer_id, $type);
			$custom_addresses = get_user_meta($customer_id, THWMA_Utils::ADDRESS_KEY, true);
			if(is_array($custom_addresses)) {
	        	$all_addresses = $custom_addresses;
	        } else {
				$all_addresses = array();
				$def_address = THWMA_Utils::get_default_address($customer_id, $type);
				if(array_filter($def_address) && (count(array_filter($def_address)) > 2)) {
					$all_addresses ['selected_address'] = $def_address;
				}
			}

			if(!empty($default_address)) {
				if(!empty($current_address) && is_array($current_address)) {
					foreach ($current_address as $key => $value) {
						$new_address = (isset($default_address[$key])) ? $default_address[$key] : '';
						
						update_user_meta($customer_id, $key, $new_address, $current_address[$key]);	
					}
				}
			}
			if(isset($custom_addresses[$type]) && is_array($custom_addresses[$type])) {
				$total_address_count = count($custom_addresses[$type]);
			} else {
				$total_address_count = 0;
			}
			if($total_address_count == 0) {
				$custom_addresses = array(
					$type => array(
						'selected_address' => $current_address 
					),
					'default_'.$type => 'selected_address'
				);				
			} else {	
				$custom_addresses['default_'.$type] = $custom_key;
			}
			update_user_meta($customer_id, THWMA_Utils::ADDRESS_KEY, $custom_addresses);
			$custom_addresses = get_user_meta($customer_id, THWMA_Utils::ADDRESS_KEY, true);
			$current_address = THWMA_Utils::get_default_address($customer_id, $type);
		}
		
		/**
         * Function for add hidden fieds to checkout form (Checkout page)
         *
         * @param array $fields The field informations.
         *
         * @return array.
         */
		public function add_hidden_field_to_checkout_fields($fields) {
			$user_id = get_current_user_id();
			$default_bil_key = THWMA_Utils::get_custom_addresses($user_id, 'default_billing');
			$same_bil_key = THWMA_Utils:: is_same_address_exists($user_id, 'billing'); 
			$default_value = $default_bil_key ? $default_bil_key : $same_bil_key;
			$fields['billing']['thwma_hidden_field_billing'] = array(
				'label'    => esc_html__('hidden value',''),
				'required' => false,
				'class'    => array('form-row'),
				'clear'    => true,
				'default'  => $default_value,
				'type'     => 'hidden',
				'priority' => '',
			);

			$default_ship_key = THWMA_Utils::get_custom_addresses($user_id, 'default_shipping');
			$same_ship_key = THWMA_Utils:: is_same_address_exists($user_id, 'shipping'); 
			$default_value_ship = $default_ship_key ? $default_ship_key : $same_ship_key;
			$fields['billing']['thwma_checkbox_shipping'] = array(
				'label'    => esc_html__('hidden value', ''),
				'required' => false,
				'class'    => array('form-row'),
				'clear'    => true,
				'default'	=> $default_value_ship,
				'type'     => 'hidden',
				'priority' => '', 
			);		
			$fields['shipping']['thwma_hidden_field_shipping'] = array(
				'label'    => esc_html__('hidden value', ''),
				'required' => false,
				'class'    => array('form-row'),
				'clear'    => true,
				'default'	=> $default_value_ship,
				'type'     => 'hidden',
				'priority' => '', 
			);
			return $fields;
		}

		/**
         * Function for create hidden fields (Checkout page)
         *
         * @param array $fields The field informations.
         * @param array $key The key value of hidden field.
         * @param array $args The hidden field arguments.
         * @param array $value The hidden field value.
         *
         * @return string.
         */
		public function add_hidden_field($field, $key, $args, $value) {
			 return '<input type="hidden" name="'.esc_attr($key).'" id="'.esc_attr($args['id']).'" value="'.esc_attr($args['default']).'" />';
		}

		/**
         * Function for update billing session (Checkout page)
         *
         * @param array $checkout The checkout informations.
         *
         * @return void.
         */
		public function session_update_billing($checkout) {
			$customer_id = get_current_user_id();			
			if(is_user_logged_in()) {	
				$default_address = THWMA_Utils::get_default_address($customer_id, 'billing');
				$addresfields = array('first_name', 'last_name', 'company','address_1', 'address_2', 'city', 'state', 'postcode', 'country', 'phone', 'email');
				if($default_address && array_filter($default_address) && (count(array_filter($default_address)) > 2)) {
					if(!empty($addresfields) && is_array($addresfields)){
						foreach ($addresfields as $key) {
							if(isset($default_address['billing_'.$key])) {
								$temp_value = isset($default_address['billing_'.$key]) ? $default_address['billing_'.$key] : '';
								WC()->customer->{"set_billing_"."{$key}"}($temp_value);
					 			WC()->customer->save();
					 		}
						}
					}
				}				

				// Fix for deactivate & activate.
				$default_set_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_billing');
				if($default_set_address) {
					$address_key = THWMA_Utils::is_same_address_exists($customer_id, 'billing');
					if(!$address_key) {						
						THWMA_Utils::update_address_to_user($customer_id, $default_address, 'billing', $default_set_address);
					}
				}
			}
		}

		/**
         * Function for update shipping session (Checkout page)
         *
         * @param array $checkout The checkout informations.
         *
         * @return void.
         */
		public function session_update_shipping($checkout) {
			$customer_id = get_current_user_id();
			if (is_user_logged_in()) {				
				$default_address = THWMA_Utils::get_default_address($customer_id, 'shipping');
				$addresfields = array('first_name', 'last_name', 'company', 'address_1', 'address_2', 'city', 'state', 'postcode', 'country');
				if($default_address && array_filter($default_address) && (count(array_filter($default_address)) > 2)) {
					if(!empty($addresfields) && is_array($addresfields)) {
						foreach ($addresfields as $key) {
							if(isset($default_address['shipping_'.$key])) {
								$temp_value = isset($default_address['shipping_'.$key]) ? $default_address['shipping_'.$key] : '';
								WC()->customer->{"set_shipping_"."{$key}"}($temp_value);
					 			WC()->customer->save();
					 		}
						}
					}
				}
				// Fix for deactivate & activate.
				$default_set_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_shipping');
				if($default_set_address) {
					$address_key = THWMA_Utils::is_same_address_exists($customer_id, 'shipping');
					if(!$address_key) {						
						THWMA_Utils::update_address_to_user($customer_id, $default_address, 'shipping', $default_set_address);
					}
				}
			}
		}

		/**
         * Function for set address above billing form (Checkout page - address display position)
         *
         * @return void.
         */
		public function address_above_billing_form() {		
			$settings = THWMA_Utils::get_setting_value('settings_billing');
			if (is_user_logged_in()) {
				if($settings && !empty($settings)) {
					if($settings['enable_billing'] == 'yes') {
						if($settings['billing_display_position'] == 'above') {
							if($settings['billing_display'] == 'popup_display') {
								$this->add_tile_to_checkout_billing_fields();
							}
							elseif($settings['billing_display'] == 'dropdown_display') {
								$this->add_dd_to_checkout_billing();
							}
							else {
								//$this->add_accordion_to_checkout_billing();
							}							
						}
					}
				}
			}
		}

		/**
         * Function for set address below billing form (Checkout page - address display position)
         *
         * @param array $checkout The checkout informations.
         *
         * @return void.
         */
		public function address_below_billing_form($checkout) {		
			$settings = THWMA_Utils::get_setting_value('settings_billing');
			if (is_user_logged_in()) {				
				if($settings && !empty($settings)) {
					if($settings['enable_billing'] == 'yes') {
						if($settings['billing_display_position']=='below') {
							if($settings['billing_display']=='popup_display') {
								$this->add_tile_to_checkout_billing_fields();
							}
							elseif($settings['billing_display']=='dropdown_display') {
								$this->add_dd_to_checkout_billing();
							}
							else {
								//$this->add_accordion_to_checkout_billing();
							}
							
						}
					}
				}			
			}
		}

		/**
         * Function for set address above shipping form (Checkout page - address display position)
         *
         * @return void.
         */
		public function address_above_shipping_form() {
			$settings=THWMA_Utils::get_setting_value('settings_shipping');
			if (is_user_logged_in()) {
				if (! wc_ship_to_billing_address_only() && wc_shipping_enabled()) {
					if($settings && !empty($settings)) {
						if($settings['enable_shipping'] == 'yes') {

							// Checkout page Multi-shipping.
							$settings_multi_ship = THWMA_Utils::get_setting_value('settings_multiple_shipping');
							$multi_shipping_enabled = isset($settings_multi_ship['enable_cart_shipping']) ? $settings_multi_ship['enable_cart_shipping']:'';
							//$this->add_checkbox_for_choose_different_address();
							if($multi_shipping_enabled == 'yes') {
								$this->add_checkbox_for_set_multi_shipping();
							}

							// Set display position of normal address book.
							if($settings['shipping_display_position'] == 'above') {
								if($settings['shipping_display'] =='popup_display') {
									$this->add_tile_to_checkout_shipping_fields();
								}
								elseif($settings['shipping_display']=='dropdown_display') {
									$this->add_dd_to_checkout_shipping();
								}
								else {
									//$this->add_accordion_to_checkout_shipping();
								}
								
							}
						}
					}
				}
			} else{
				// Checkout page Multi-shipping.
				if($settings && !empty($settings)) {
					if(array_key_exists('enable_shipping', $settings)) {
						if($settings['enable_shipping'] == 'yes') {

							// Check Multi-shipping is enabled.
							$settings_multi_ship = THWMA_Utils::get_setting_value('settings_multiple_shipping');
							$multi_shipping_enabled = isset($settings_multi_ship['enable_cart_shipping']) ? $settings_multi_ship['enable_cart_shipping']:'';
							if($multi_shipping_enabled == 'yes') {
								$settings_guest_urs = THWMA_Utils::get_setting_value('settings_guest_users');
								$enabled_guest_user = isset($settings_guest_urs['enable_guest_shipping']) ? $settings_guest_urs['enable_guest_shipping']:'';
								if($enabled_guest_user == 'yes') {
									$this->add_checkbox_for_set_multi_shipping();
								}
							}
						}
					}
				}
			}
		}

		/**
         * Function for set address below shipping form (Checkout page - address display position)
         *
         * @return void.
         */
		public function address_below_shipping_form() {
			$settings = THWMA_Utils::get_setting_value('settings_shipping');
			if (is_user_logged_in()) {
				if (! wc_ship_to_billing_address_only() && wc_shipping_enabled()) {
					if($settings && !empty($settings)) {
						if($settings['enable_shipping'] == 'yes') {
							if($settings['shipping_display_position'] == 'below') {
								if($settings['shipping_display'] == 'popup_display') {
									$this->add_tile_to_checkout_shipping_fields();
								}
								elseif($settings['shipping_display'] == 'dropdown_display') {
									$this->add_dd_to_checkout_shipping();
								}
								else {
									//$this->add_accordion_to_checkout_shipping();
								}
								
							}
						}
					}
				}
			}
		}

		/**
         * Function for set checkout popup billing tiles (Checkout page - address display style(popup))
         *
         * @return void.
         */
		public function add_tile_to_checkout_billing_fields() {
			$customer_id = get_current_user_id();
			if (is_user_logged_in()) {
				$settings = THWMA_Utils::get_setting_value('settings_billing'); 				
				//$button_color = THWMA_Utils::get_setting_value('button_color'); 
				$theme_class_name = $this->check_current_theme(); 
				$theme_class = $theme_class_name.'_tile_field';
				$disable_adr_mngmnt = self::disable_address_mnagement();
				$billing_display_text = htmlspecialchars($settings['billing_display_text']); ?>
				<div id="billing_tiles" class="<?php echo $theme_class; ?>"> 
					<?php if($settings['billing_display_title'] == 'button') { ?>
						<div class = "add-address thwma-add-adr btn-checkout ">	
							<a id = "thwma-popup-show-billing" class="btn-add-adrs-checkout button primary is-outline"  onclick="thwma_show_billing_popup(event)">
							<!-- <button type="button" id = "thwma-popup-show-billing" class="btn-add-adrs-checkout"  onclick="thwma_show_billing_popup(event)"> -->
							<?php esc_html_e($billing_display_text, 'woocommerce-multiple-addresses-pro'); ?>
							</a>
							<!-- </button> -->
						</div>
					<?php } else { ?>
						<a href='#' id="thma-popup-show-billing_link" class='th-popup-billing th-pop-link' onclick="thwma_show_billing_popup(event)">
						<?php esc_html_e($billing_display_text, 'woocommerce-multiple-addresses-pro'); ?>
						</a>
					<?php }
					$all_address=''; 
					$html_address = $this->get_tile_field($customer_id, 'billing');
						$theme_class_name = $this->check_current_theme(); 
						$theme_class = $theme_class_name.'_tile_field';
						$all_address.= '<div id="thwma-billing-tile-field" class="'.esc_attr($theme_class).'">
							<div>'. $html_address.'</div>
						</div>' ;?>
					<div class="u-columns woocommerce-Addresses col2-set addresses  ">
						<?php echo $all_address; ?>
					</div>	
				</div>
			<?php }
		}

		/**
         * Function for set checkout popup shipping tiles (Checkout page - address display style(popup))
         *
         * @return void.
         */
		public function add_tile_to_checkout_shipping_fields() {			
			if (is_user_logged_in()) {
				if (! wc_ship_to_billing_address_only() && wc_shipping_enabled()) {
					$customer_id = get_current_user_id();
					$settings = THWMA_Utils::get_setting_value('settings_shipping'); 
					$theme_class_name = $this->check_current_theme(); 
					$theme_class = $theme_class_name.'_tile_field';
					$disable_adr_mngmnt = self::disable_address_mnagement();
					$shipping_display_text = htmlspecialchars($settings['shipping_display_text']); ?>	
					<div id="shipping_tiles" class="<?php echo $theme_class; ?>"> 
						<?php if($settings['shipping_display_title']=='button') { ?>
							<div class = "add-address thwma-add-adr btn-checkout">
								<a id="thwma-popup-show-shipping" class="btn-add-adrs-checkout button primary is-outline"  onclick="thwma_show_shipping_popup(event)">
							<!-- <button type="button" id="thwma-popup-show-shipping" class="btn-add-adrs-checkout"  onclick="thwma_show_shipping_popup(event)"> -->
							<?php esc_html_e($shipping_display_text, 'woocommerce-multiple-addresses-pro'); ?>
								</a>
							<!-- </button>  -->
							</div>
						<?php } 
						else { ?>
							<a href='#' id="thwma-popup-show-shipping_link" class='th-popup-shipping th-pop-link' onclick="thwma_show_shipping_popup(event)">
							<?php esc_html_e($shipping_display_text, 'woocommerce-multiple-addresses-pro'); ?>
							</a>
						<?php }
						$all_address=''; 
						$html_address = $this->get_tile_field($customer_id, 'shipping');
						$theme_class_name = $this->check_current_theme(); 
						$theme_class = $theme_class_name.'_tile_field';
						$all_address.= '<div id="thwma-shipping-tile-field" class="'.esc_attr($theme_class).'">'. $html_address.'</div>' ?>
						<div class="u-columns woocommerce-Addresses col2-set addresses billing-addresses ">
							<?php echo $all_address; ?>
						</div>
					</div>
				<?php }
			}
		}

		/**
         * Function for get tile fields (Checkout page) .
         * 
         * @param integer $customer_id The user id.
         * @param string $type  The address type.
         *
         * @return string.
         */
		public function get_tile_field($customer_id, $type) {
			$oldcols = 1;
			$cols    = 1;
			// $custom_address = THWMA_Utils::get_custom_addresses($customer_id, $type) ? THWMA_Utils::get_custom_addresses($customer_id, $type) : array();
			$disable_adr_mngmnt = self::disable_address_mnagement();
			if($disable_adr_mngmnt != true){
				$disable_adr_mngmnt = apply_filters('disable_address_management_on_checkout', false);
			}
			if($disable_adr_mngmnt == true) {
				$disable_mngt_class = 'thwma_disable_adr_mngt';
				$disable_acnt_sec = 'disable_acnt_sec';
			} else {
				$disable_mngt_class = '';
				$disable_acnt_sec = '';
			}
			$custom_address = THWMA_Utils::get_custom_addresses($customer_id, $type);
			$default_set_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_'.$type);
			$same_address = THWMA_Utils::is_same_address_exists($customer_id, $type);
			$default_address = $default_set_address ? $default_set_address : $same_address;	
			$default_address = $same_address;			
			$address_count = is_array($custom_address) ? count($custom_address) : 0 ;

			// Address limit.
			$address_limit = '';
            if($type) {
                $address_limit = THWMA_Utils::get_setting_value('settings_'.$type , $type.'_address_limit');
            }
            if (!is_numeric($address_limit)) {
                $address_limit = 0;
            }

			$return_html = '';
			$add_class = '';
			$address_type = "'$type'";		
			$add_list_class  = ($type == 'billing') ? " thwma-thslider-list bill " : " thwma-thslider-list ship"; 	
			$add_address_btn = '<div class="add-address thwma-add-adr '.esc_attr($disable_acnt_sec).'">
	            <button class="btn-add-address primary button '.esc_attr($disable_mngt_class).'" onclick="thwma_add_new_address(event, this, '.$address_type.')">
	                <i class="fa fa-plus"></i> '.esc_html__(' Add new address', 'woocommerce-multiple-addresses-pro').'
	            </button>
	        </div>';
	        // if(is_cart()) { // Check is Cart page.
	        // 	$add_address_btn = '<div class="add-address thwma-add-adr '.esc_attr($disable_acnt_sec).'">
		       //      <button class="btn-add-address primary button '.esc_attr($disable_mngt_class).'" onclick="thwma_add_new_shipping_address(event, this, '.$address_type.')">
		       //          <i class="fa fa-plus"></i> '.esc_html__(' Add new address', 'woocommerce-multiple-addresses-pro').'
		       //      </button>
		       //  </div>';
	        // }	

	        if(is_array($custom_address)) {
	        	$all_addresses = $custom_address;
	        } else {
				$all_addresses = array();
				$def_address = THWMA_Utils::get_default_address($customer_id, $type);

				if(array_filter($def_address) && (count(array_filter($def_address)) > 2)) {
					$all_addresses ['selected_address'] = $def_address;
				}
			}
			$total_address_count = count($all_addresses);
			// if($address_limit && ($total_address_count > 1)) {			
			$return_html .= '<div class="thwma-thslider">';
				if($address_limit && ($total_address_count >= 1)) {
					if($all_addresses && is_array($all_addresses)) {
			           	$return_html .= '<div class="thwma-thslider-box">';
				           	$return_html .= '<div class="thwma-thslider-viewport '.esc_attr($type).'">';
					           	$return_html .= '<ul class=" '.esc_attr($add_list_class).'">';
									$i = 1;

									// Default address.
									$action_row_html = '';
									$new_address = isset($all_addresses[$default_address]) ? $all_addresses[$default_address] : '';
									$def_new_address = $new_address;
									if(!empty($def_new_address)) {
										$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
										$options_arr = WC()->countries->get_formatted_address($new_address_format);
										$address_key_param = "'".$default_address."'";	
										$address_type_css = 'default';
										$heading = sprintf(esc_html__('Default', 'woocommerce-multiple-addresses-pro'));
										$action_row_html .= '<div class="thwma-adr-footer address-footer '.$address_type_css.'"> 
											<div class="th-btn btn-delete '.$disable_mngt_class.'"><span>'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'</span></div> 
											<div class="th-btn btn-default"><span>'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'</span></div>   
										</div>';
										if(isset($heading) && $heading != '') {
											$heading_css = '<div class="address-type-wrapper row"> 
												<div title="'.$heading.'" class="address-type '.$address_type_css.'">'.$heading.'</div>  
												</div>       
												<div class="tile-adrr-text thwma-adr-text address-text address-wrapper">'.$options_arr.'</div>'; 
										} else {
											$heading_css = '<div class="tile-adrr-text thwma-adr-text address-text address-wrapper wrapper-only">'.$options_arr.'</div>'; 
										}
										$add_class  = "thwma-thslider-item $type " ;
										$return_html .= '<li class="'.$add_class.'" value="'. $default_address.'" >
											<div class="thwma-adr-box address-box" data-index="0" data-address-id=""> 
												<div class="thwma-main-content"> 
													<div class="complete-aaddress">  
														'.$heading_css.'

													</div> 
													<div class="btn-continue address-wrapper"> 
														<a class="th-btn button primary is-outline '.$default_address.'" onclick="thwma_populate_selected_address(event, this, '.$address_type.', '.$address_key_param.')"> 
															<span>'.esc_html__('Choose This Address', 'woocommerce-multiple-addresses-pro').'</span> 
														</a> 
													</div> 
												</div>'.$action_row_html.'</div>';
										$return_html .= '</li>';
									}

									$no_of_tile_display = '';
									if(!empty($def_new_address)) {
										$no_of_tile_display = 2;
									} else {
										$no_of_tile_display = 1;
									}
									
									if($address_limit >= $no_of_tile_display) {
										foreach ($all_addresses as $address_key => $value) {
											// if(isset($value[$type.'_heading'])) {
											// 	if(!is_array($value[$type.'_heading'])) {
											// 		$new_address = $all_addresses[$address_key];
											// 		$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
											// 	}
											// }
											$new_address = $all_addresses[$address_key];
											$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
											$options_arr = WC()->countries->get_formatted_address($new_address_format);
											$address_key_param = "'".$address_key."'";					
											$heading = !empty($new_address[$type.'_heading']) ? $new_address[$type.'_heading'] : esc_html__('', 'woocommerce-multiple-addresses-pro') ;
											
											if($default_address) {
												$is_default = ($default_address == $address_key) ? true : false;
											} else {
												$is_default = false;
											}
											$address_type_css = '';
											$action_row_html = '';
											if(!$is_default || empty($def_new_address)) {				
												if($total_address_count>=1) {
													if(!empty($custom_address))	{				
														$action_row_html .= '<div class="thwma-adr-footer address-footer"> 
															<div class="btn-delete '.$disable_mngt_class.'" data-index="0" data-address-id="" onclick="thwma_delete_selected_address(this, '.$address_type.', '.$address_key_param.')" title="'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'"> 
																<span>'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'</span> 
															</div> 
															<div class="btn-default" data-index="0" data-address-id="" onclick="thwma_set_default_address(this, '.$address_type.', '.$address_key_param.')" title="'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'"> 
																<span>'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'</span>
															</div>   
														</div>';
													}						
												} else{
													// $action_row_html .= '<div class="thwma-adr-footer address-footer '.$address_type_css.'"> 
													// 	<div class="th-btn btn-delete '.$disable_mngt_class.'"><span>'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'</span></div> 
													// 	<div class="btn-default" data-index="0" data-address-id="" onclick="thwma_set_default_address(this, '.$address_type.', '.$address_key_param.')" title="'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'"> 
													// 		<span>'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'</span>
													// 	</div>    
													// </div>';						
												}
												if(empty($def_new_address)) {
													if(empty($custom_address)) {
														$address_type_css = 'default';
														$heading = sprintf(esc_html__('Default', 'woocommerce-multiple-addresses-pro'));
														$action_row_html .= '<div class="thwma-adr-footer address-footer '.$address_type_css.'"> 
															<div class="th-btn btn-delete '.$disable_mngt_class.'"><span>'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'</span></div> 
															<div class="th-btn btn-default"><span>'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'</span></div>   
														</div>';
													}
												}
											//if((isset($heading)) && ($heading != '') && (!is_array($heading))) {
												if(isset($heading) && $heading != '') {
													$heading_css = '<div class="address-type-wrapper row"> 
														<div title="'.$heading.'" class="address-type '.$address_type_css.'">'.$heading.'</div>  
														</div>       
														<div class="tile-adrr-text thwma-adr-text address-text address-wrapper">'.$options_arr.'</div>'; 
												} else {
													$heading_css = '<div class="tile-adrr-text thwma-adr-text address-text address-wrapper wrapper-only">'.$options_arr.'</div>'; 
												}			
												//$add_class  = "u-columns address-single  $type " ;
												$add_class  = "thwma-thslider-item $type " ;
												// $add_class .= $is_default ? ' first' : '';
												$add_class .= $i == 1 ? ' first' : '';
												$return_html .= '<li class="'.$add_class.'" value="'. $address_key.'" >
													<div class="thwma-adr-box address-box" data-index="'.$i.'" data-address-id=""> 
														<div class="thwma-main-content"> 
															<div class="complete-aaddress">  
																'.$heading_css.'

															</div> 
															<div class="btn-continue address-wrapper"> 
																<a class="th-btn button primary is-outline '.$address_key.'" onclick="thwma_populate_selected_address(event, this, '.$address_type.', '.$address_key_param.')"> 
																	<span>'.esc_html__('Choose This Address', 'woocommerce-multiple-addresses-pro').'</span> 
																</a> 
															</div> 
														</div>'.$action_row_html.'</div>';
												$return_html .= '</li>';
												// if($i >= $address_limit-1) {
		          //                                   break;
		          //                               }
												// checkout tile fields.
												if(!empty($def_new_address)){
													if($i >= $address_limit-1) {
			                                            break;
			                                        }
			                                    } else {
			                                    	if($i >= $address_limit) {
			                                            break;
			                                        }
			                                    }
												$i++;
											}
										}
									}
								$return_html .= '</ul>';
							$return_html .= '</div>';
						$return_html .= '</div>';
						$return_html .= '<div class="control-buttons control-buttons-'.$type.'">';
						if($address_count && $address_count > 3) {
							if($address_limit>3) {
				            	$return_html .= '<div class="prev thwma-thslider-prev '.$type.'"><i class="fa fa-angle-left fa-3x"></i></div>';
				            	$return_html .= '<div class="next thwma-thslider-next '.$type.'"><i class="fa fa-angle-right fa-3x"></i></div>';
				            }
			            }

			           	$return_html .= '</div>'; 
			           		if(((int)($address_limit)) > $address_count) {
			           		$return_html .= $add_address_btn;
			           	}
			        }
		        } else {
		        	$return_html .= '<div class="th-no-address-msg"  >	<span>'.esc_html__('No saved addresses found', 'woocommerce-multiple-addresses-pro').'</span>  </div>';
		        	//if(is_cart()) {
		        		$return_html .= $add_address_btn;
		        	//}
		        }		    	           
	        $return_html .= '</div>';	        	        
			return $return_html;
		}

		/**
         * Function for add additional fields (Additional drop-down fields  sections-checkout page) .
         * 
         * @param integer $section The section data.
         *
         * @return string.
         */
		public function add_additional_fields($section) {
			$section_name = $section->name;
			//$section_settings = get_option('thwma_sectionz', true);
			//$mapped_section = $section_settings[$section_name]['maped_section'];
			$enable_billing = THWMA_Utils::get_setting_value('settings_billing', 'enable_billing');
			$enable_shipping = THWMA_Utils::get_setting_value('settings_shipping', 'enable_shipping');
			$mapped_section = THWMA_Utils::get_maped_sections_settings_value($section_name, 'maped_section');
			$disable_adr_mngmnt = self::disable_address_mnagement();
			$customer_id = get_current_user_id();
			$same_address = THWMA_Utils::is_same_address_exists($customer_id, $mapped_section);
			//$default_address = $default_bil_address ? $default_bil_address : $same_address ;
			$default_address = $same_address;

			// Address limit.
			$address_limit = '';
            if($mapped_section) {
                $address_limit = THWMA_Utils::get_setting_value('settings_'.$mapped_section , $mapped_section.'_address_limit');
            }
            if (!is_numeric($address_limit)) {
                $address_limit = 0;
            }
		
			if(!($mapped_section == 'shipping' &&  (wc_ship_to_billing_address_only()  || !wc_shipping_enabled()))) {
				$customer_id = get_current_user_id();
				$settings = THWMA_Utils::get_setting_value('settings_'.$mapped_section);
				$display_type = $settings[$mapped_section.'_display'];
				$display_title = $settings[$mapped_section.'_display_title'];
				if(($mapped_section == 'shipping')&&($enable_shipping == 'yes') ||($mapped_section == 'billing')&&($enable_billing == 'yes')) {
					if($display_type == 'popup_display') {
						if($display_title == 'link') { ?>
							<a href='#' class='thma-popup-show-custom_link th-pop-link' onclick="thwma_show_custom_popup(event,'<?php echo $section_name; ?>', '<?php echo $mapped_section; ?>')" > <?php esc_html_e('Choose Different Address', 'woocommerce-multiple-addresses-pro') ?> </a><?php
						} else {
							$theme_class_name = $this->check_current_theme(); 
							$theme_class = $theme_class_name.'_tile_field'; ?>
							<div class="<?php echo $theme_class;?>">
								<div class = "add-address thwma-add-adr btn-checkout">
									<a  class="btn-add-adrs-checkout button primary is-outline" onclick = "thwma_show_custom_popup(event,'<?php echo $section_name; ?>', '<?php echo $mapped_section; ?>')"><?php esc_html_e('Choose Different Address', 'woocommerce-multiple-addresses-pro') ?>
										<!-- <button type="button"  class="btn-add-adrs-checkout" onclick = "thwma_show_custom_popup(event, '<?php echo $section_name; ?>', '<?php echo $mapped_section; ?>')">
										<?php //esc_html_e('Choose Different Address', 'woocommerce-multiple-addresses-pro') ?> -->
									</a>
									<!-- </button> -->
								</div>
							</div>
						<?php }
						$all_address = '';
						$html_address = $this->get_custom_tile_field($customer_id, $section, $mapped_section);
						$theme_class_name = $this->check_current_theme(); 
						$theme_class = $theme_class_name.'_tile_field';
						$all_address.= '<div id="thwma-custom-tile-field_'.$section_name.'" style="display:none;" class="'.$theme_class.'">'. $html_address.'</div>' ?>
						<div class="u-columns woocommerce-Addresses  addresses custom_section_address ">
							<?php echo $all_address; ?>
						</div>
					<?php } else {
						$section_name = $section->name;
						$custom_address = THWMA_Utils::get_custom_addresses($customer_id, $mapped_section);
						$options = array();
						$def_section_address =  $this->get_default_section_address($customer_id, $section_name);
								
						if($custom_address && is_array($custom_address)) {
							$address_count = count($custom_address);
							if($address_limit && ($address_count > 0)) {

								// Default address.
								foreach ($custom_address as $key => $address_values) {
									$adrsvalues_to_dd = array();
									if($key == $default_address) {	
										if(array_key_exists($mapped_section.'_heading',$address_values)) {							
											$heading = ($address_values[$mapped_section.'_heading'] != '') ? $address_values[$mapped_section.'_heading'] : esc_html__('', 'woocommerce-multiple-addresses-pro');
										}
										if(apply_filters('thwma_remove_dropdown_address_format', true)) {
											if(!empty($address_values) && is_array($address_values)) {
												foreach ($address_values as $adrs_key => $adrs_value) {
													if($adrs_key == $mapped_section.'_address_1' || $adrs_key == $mapped_section.'_address_2' || $adrs_key ==$mapped_section.'_city' || $adrs_key == $mapped_section.'_state' || $adrs_key == $mapped_section.'_postcode') {
														if($adrs_value) {
															$adrsvalues_to_dd[] = $adrs_value;
														}
													}
												}
											}
										} else {
											$type = $mapped_section;
											$separator = '</br>';
											$new_address = $custom_address[$def_section_address];
											$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
											$options_arr = WC()->countries->get_formatted_address($new_address_format);
											$adrsvalues_to_dd = explode('<br/>', $options_arr);
										}
									}
									$adrs_string = implode(', ', $adrsvalues_to_dd);
									//if((isset($heading)) && ($heading!= '') && (!is_array($heading))) {
									if(isset($heading) && $heading != '') {
										$options[$key] = $heading .' - '.$adrs_string;
									} else {
										$options[$key] = $adrs_string;
									}
									$options = array_filter($options);
								}

								// Custom addresses.
								$i = 0;

								$customer_id = get_current_user_id();
								$same_address = THWMA_Utils::is_same_address_exists($customer_id, $mapped_section);
								$get_default_address = $same_address;
								$no_of_tile_display = '';
								if(!empty($get_default_address)) {
									$no_of_tile_display = 1;
								} else {
									$no_of_tile_display = 0;
								}
								if($address_limit > $no_of_tile_display) {
									foreach ($custom_address as $key => $address_values) {
										if($key != $default_address) {
											if(isset($address_values[$mapped_section.'_heading'])) {
												if(array_key_exists($mapped_section.'_heading',$address_values)) {
													$heading = ($address_values[$mapped_section.'_heading'] != '') ? $address_values[$mapped_section.'_heading'] : esc_html__('', 'woocommerce-multiple-addresses-pro');
												}
											}
											$adrsvalues_to_dd = array();
											if(apply_filters('thwma_remove_dropdown_address_format', true)) {
												if(!empty($address_values) && is_array($address_values)){
													foreach ($address_values as $adrs_key => $adrs_value) {
														if($adrs_key == $mapped_section.'_address_1' || $adrs_key == $mapped_section.'_address_2' || $adrs_key ==$mapped_section.'_city' || $adrs_key == $mapped_section.'_state' || $adrs_key == $mapped_section.'_postcode') {
															if($adrs_value) {
																$adrsvalues_to_dd[] = $adrs_value;
															}
														}
													}
												}
											} else {
												$type = $mapped_section;
												$separator = '</br>';
												$new_address = $custom_address[$def_section_address];
												$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
												$options_arr = WC()->countries->get_formatted_address($new_address_format);
												$adrsvalues_to_dd = explode('<br/>', $options_arr);										
											}
											$adrs_string = implode(', ', $adrsvalues_to_dd);
											//if((isset($heading)) && ($heading != '') && (!is_array($heading))) {
											if(isset($heading) && $heading != '') {
												$options[$key] = $heading .' - '.$adrs_string;
											} else {
												$options[$key] = $adrs_string;
											}
											// if($i >= $address_limit-2) {
		         //                                break;
		         //                            }
											// custom field drop-down.
		                                    if(!empty($get_default_address)){
												if($i >= $address_limit-2) {
								                    break;
								                }
								            } else {
								            	if($i >= $address_limit-1) {
								                    break;
								                }
								            }
		                                    $i++;	
		                                }						
									}
								}
							}
						}
						if(is_array($def_section_address)) {
							if(array_filter($def_section_address)) {
								$options['section_address'] = esc_html__('Default Address','woocommerce-multiple-addresses-pro');
							}
						}

						$alt_field = array(
							'required' => false,
							'class'    => array('form-row form-row-wide enhanced_select select2-selection th-select'),
							'clear'    => true,
							'type'     => 'select',
							//'required' => 'true',
							//'label'    => 'Choose Address',
							//'placeholder' =>esc_html__('Choose an Address..', ''),
							
							'options'  => array('' => esc_html__('Choose saved address&hellip;','woocommerce-multiple-addresses-pro'))+$options,
						);
						$this->thwma_woocommerce_form_field('thwma_'.$section_name, $alt_field);
					}
				}
			}
		}

		/**
         * Function for get additional section tile fields (Additional sections-checkout page).
         * 
         * @param string $customer_id The setted delievery day.
         * @param string $section  Start date of the delivey.
         * @param string $maped_section  End date of the delivey.
         *
         * @return string.
         */
		public function get_custom_tile_field($customer_id, $section, $maped_section) {
			$custom_address = THWMA_Utils::get_custom_addresses($customer_id, $maped_section);
			$type = $maped_section;
			$section_name = $section->name;
			$section_address = array('section_address' => $this->get_default_section_address($customer_id, $section_name));		
			//$new_address_format = self::get_formated_address('billing', $section_address);
			$section_key_param = "'".$section_name."'";			
			$return_html = '';
			$add_class='';
			$address_type ="'". $maped_section."'";		
			$add_list_class  =  "thwma-thslider-list  "  .$section_name ; 			
			$all_addresses = 	$custom_address;

			$default_set_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_'.$type);	
			$same_address = THWMA_Utils::is_same_address_exists($customer_id, $type);
			$default_address = $default_set_address ? $default_set_address : $same_address;	
			$default_address = $same_address;	

			// Address limit.
			$address_limit = '';
            if($type) {
                $address_limit = THWMA_Utils::get_setting_value('settings_'.$type , $type.'_address_limit');
            }
            if (!is_numeric($address_limit)) {
                $address_limit = 0;
            }

	        if(is_array($custom_address)) {
	        	if($section_address['section_address'] != null && array_filter($section_address['section_address'])) {
	        		$all_addresses = array_merge($section_address, $custom_address);
	        	}
	        } else {
				$all_addresses = array();				
				if(array_filter($section_address['section_address'])) {
					$def_address = THWMA_Utils::get_default_address($customer_id, $type);
					if(array_filter($def_address) && (count(array_filter($def_address)) > 2)) {
						$all_addresses ['selected_address'] = $def_address;
					}
					$all_addresses = array_merge($section_address, $all_addresses);
					//$all_addresses  = $section_address;
				} 
				else {
					$def_address = THWMA_Utils::get_default_address($customer_id, $type);
					if(array_filter($def_address) && (count(array_filter($def_address)) > 2)) {
						$all_addresses ['selected_address'] = $def_address;
					}
				}
			}
			$address_count = is_array($all_addresses) ? count($all_addresses) : 0 ;
			//if($address_limit && ($address_count > 0)) {					
			$return_html .= '<div class="thwma-thslider" id="thslider-'.esc_attr($section_name).'">';
				if($address_limit && ($address_count >= 0)) {
					if($all_addresses && is_array($all_addresses)) {
			           	$return_html .= '<div class="thwma-thslider-box">';
				           	$return_html .= '<div  class = "thwma-thslider-viewport '.esc_attr($type).'" >';
					           	$return_html .= '<ul class=" '.esc_attr($add_list_class).'">';

					           		// Default address.
					           		$action_row_html = '';
									$new_address = isset($all_addresses[$default_address]) ? $all_addresses[$default_address] : '';
									$def_new_address = $new_address;
									if(!empty($def_new_address)) {
										$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
										$options_arr = WC()->countries->get_formatted_address($new_address_format);
										$address_key_param = "'".$default_address."'";
										if(isset($heading) && $heading != '') {
											$heading_css = '<div class="address-type-wrapper row"> 
												<div title="'.esc_attr($heading).'" class="address-type '.esc_attr($address_type_css).'">'.esc_attr($heading).'</div>  
												</div>       
												<div class="cus-adrr-text thwma-adr-text address-text address-wrapper">'.$options_arr.'</div>'; 
										} else {
											$heading_css = '<div class="cus-adrr-text thwma-adr-text address-text address-wrapper wrapper-only">'.$options_arr.'</div>'; 
										}
										$add_class  = "thwma-thslider-item_c $section_name " ;
										$return_html .= '<li class="'.esc_attr($add_class).'" value="'. esc_attr($default_address).'" >
											<div class="thwma-adr-box address-box" data-index="0" data-address-id=""> 
												<div class="thwma-main-content"> 
													<div class="complete-aaddress">  
														'.$heading_css.'

													</div> 
													<div class="btn-continue address-wrapper"> 
														<a class="th-btn '.esc_attr($default_address).' button primary is-outline" onclick="thwma_populate_selected_section_address(event, this, '.esc_attr($address_key_param).', '.esc_attr($section_key_param).', '.esc_attr($address_type).')"> 
															<span>'.esc_html__('Choose This Address', 'woocommerce-multiple-addresses-pro').'</span> 
														</a>  
													</div> 
												</div>'.$action_row_html.'</div>';
										$return_html .= '</li>';
									}	

									$i = 1;
									$no_of_tile_display = '';
									if(!empty($def_new_address)) {
										$no_of_tile_display = 1;
									} else {
										$no_of_tile_display = 0;
									}
									if($address_limit > $no_of_tile_display) {
										foreach ($all_addresses as $address_key => $value) {
											if($default_address) {
												$is_default = ($default_address == $address_key) ? true : false;
											} else {
												$is_default = false;
											}
											if(!$is_default) {	
												$new_address = $all_addresses[$address_key];
												$new_address_format = THWMA_Utils::get_formated_address($maped_section, $new_address);
												$options_arr = WC()->countries->get_formatted_address($new_address_format);
												$address_key_param = "'".$address_key."'";					
												$heading = !empty($new_address[$maped_section.'_heading']) ? $new_address[$maped_section.'_heading'] : esc_html__('', self::THWMA_TEXT_DOMAIN) ;
												$action_row_html = '';
												$address_type_css = '';					
												if($address_key == 'section_address') {
													$address_type_css = 'default';
													$heading = sprintf(esc_html__('Default ', 'woocommerce-multiple-addresses-pro'));
												}					
												$add_class  = "thwma-thslider-item_c  $section_name " ;
												$add_class .= $i == 0 ? ' first' : '';	
												//if((isset($heading)) && ($heading != '') && (!is_array($heading))) {
												if(isset($heading) && $heading != '') {
													$heading_css =	'<div class="address-type-wrapper row"> 
														<div title="'.esc_attr($heading).'" class="address-type  '.esc_attr($address_type_css).'">'.esc_attr($heading).'</div>  
													</div>       
													<div class="cus-adrr-text thwma-adr-text address-text address-wrapper">'.$options_arr.'</div>';
												} else {
													$heading_css =	'<div class="cus-adrr-text thwma-adr-text address-text address-wrapper wrapper-only">'.$options_arr.'</div>';
												}
													// <div class="th-btn '.$address_key.'" onclick="thwma_populate_selected_section_address(event, this, '.$address_key_param.', '.$section_key_param.', '.$address_type.')"> 
													// 	<span>'.esc_html__('Choose This Address', 'woocommerce-multiple-addresses-pro').'</span> 
													// </div> 		
												$return_html .= '<li class="'.esc_attr($add_class).'" value="'. esc_attr($address_key).'" >
													<div class="thwma-adr-box address-box" data-index="'.$i.'" data-address-id=""> 
														<div class="thwma-main-content"> 
															<div class="complete-aaddress">  
																'.$heading_css.'

															</div> 
															<div class="btn-continue address-wrapper"> 
																<a class="th-btn '.esc_attr($address_key).' button primary is-outline" onclick="thwma_populate_selected_section_address(event, this, '.esc_attr($address_key_param).', '.esc_attr($section_key_param).', '.esc_attr($address_type).')"> 
																	<span>'.esc_html__('Choose This Address', 'woocommerce-multiple-addresses-pro').'</span> 
																</a> 
															</div> 
														</div>'.$action_row_html.'
													</div>
												</li>';

												// if($i >= $address_limit-1) {
		          //                                   break;
		          //                               }

												// Custom address tile field.
												if($def_new_address) {
													if($i >= $address_limit-1) {
			                                            break;
			                                        }
			                                    } else {
			                                    	if($i >= $address_limit) {
			                                            break;
			                                        }
			                                    }
												$i++;
											}
										}
									}
								$return_html .= '</ul>';
							$return_html .= '</div>';
						$return_html .= '</div>';
						$return_html .= '<div class="control-buttons control-buttons-'.esc_attr($section_name).'">';	
							if($address_limit>2) {			
				            	$return_html .= '<div class="prev_'.esc_attr($section_name).' thwma-thslider-prev '.esc_attr($section_name).'"><i class="fa fa-angle-left fa-3x"></i></div>';
				            	$return_html .= '<div class="next_'.esc_attr($section_name).' thwma-thslider-next '.esc_attr($section_name).'"><i class="fa fa-angle-right fa-3x"></i></div>';	
				            }            
			            $return_html .= '</div>';
			        } else {
			        	$return_html .= '<div class="th-no-address-msg"  >	<span>'. esc_html__('No saved addresses found', 'woocommerce-multiple-addresses-pro').'</span>  </div>';
			        }	          
		        } else {
		        	$return_html .= '<div class="th-no-address-msg"  >	<span>'. esc_html__('No saved addresses found', 'woocommerce-multiple-addresses-pro').'</span>  </div>';
		        }	           
	        $return_html .= '</div>';	        
			return $return_html;
		}

		/**
         * Function for add billing address to dropdown(Checkout page - address display style(dropdown))
         * @return void
         */ 
		public function add_dd_to_checkout_billing() {
			$customer_id = get_current_user_id();
			$custom_addresses = THWMA_Utils::get_custom_addresses($customer_id, 'billing');
			$default_bil_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_billing');
			$same_address = THWMA_Utils::is_same_address_exists($customer_id, 'billing');
			$default_address = $default_bil_address ? $default_bil_address : $same_address ;
			$default_address = $same_address;

			// Address limit.
			$address_limit = THWMA_Utils::get_setting_value('settings_billing', 'billing_address_limit');
			if (!is_numeric($address_limit)) {
                $address_limit = 0;
            }

			$options = array();
			if(is_array($custom_addresses)) {
	        	$custom_address = $custom_addresses;
	        } else {
				$custom_address = array();
				$def_address = THWMA_Utils::get_default_address($customer_id, 'billing');				
				if(array_filter($def_address) && (count(array_filter($def_address)) > 2)) {
					$custom_address ['selected_address'] = $def_address;
				}
			}
			if($custom_address) {
				$address_count = count($custom_address);
				if($address_limit && ($address_count > 0)) {		
					$billing_heading = (isset($custom_address[$default_address]['billing_heading']) && $custom_address[$default_address]['billing_heading'] !='') ? $custom_address[$default_address]['billing_heading'] : esc_html__('', '');
					if($default_address) {
						if(isset($options[$default_address])) {
							$options[$default_address] = $billing_heading .'&nbsp - &nbsp'.$custom_address[$default_address]['billing_address_1'];
						}
					} else {
						$default_address = 'selected_address';
						$options[$default_address]  = esc_html__('Billing Address', 'woocommerce-multiple-addresses-pro');
					}

					if(is_array($custom_address)) {	

						// Default address.
						foreach ($custom_address as $key => $address_values) {
							$adrsvalues_to_dd = array();
							if($key == $default_address) {								
								$heading = (isset($address_values['billing_heading']) && $address_values['billing_heading'] != '') ? $address_values['billing_heading'] : esc_html__('', '');
								if(apply_filters('thwma_remove_dropdown_address_format', true)) {
									if(!empty($address_values) && is_array($address_values)) {
										foreach ($address_values as $adrs_key => $adrs_value) {
											if($adrs_key == 'billing_address_1' || $adrs_key =='billing_address_2' || $adrs_key =='billing_city' || $adrs_key =='billing_state' || $adrs_key =='billing_postcode') {
												if($adrs_value) {
													$adrsvalues_to_dd[] = $adrs_value;
												}
											}
										}
									}
								} else {
									$type = 'billing';
									$separator = '</br>';
									$new_address = $custom_address[$default_address];
									$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
									$options_arr = WC()->countries->get_formatted_address($new_address_format);
									$adrsvalues_to_dd = explode('<br/>', $options_arr);
								}
							}
							$adrs_string = implode(', ', $adrsvalues_to_dd);
							//if((isset($heading)) && ($heading!= '') && (!is_array($heading))) {
							if(isset($heading) && $heading != '') {
								$options[$key] = $heading .' - '.$adrs_string;
							} else {
								$options[$key] = $adrs_string;
							}
							$options = array_filter($options);
						}

						// Custom addresses.
						$i = 0;

						$default_bil_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_billing');
						$same_address = THWMA_Utils::is_same_address_exists($customer_id, 'billing');
						$get_default_address = $same_address ? $same_address : $default_bil_address ;
						$no_of_tile_display = '';
						if(!empty($get_default_address)) {
							$no_of_tile_display = 1;
						} else {
							$no_of_tile_display = 0;
						}
						if($address_limit > $no_of_tile_display) {		
							foreach ($custom_address as $key => $address_values) {
								if($key != $default_address) {
									$heading = (isset($address_values['billing_heading']) && $address_values['billing_heading'] != '') ? $address_values['billing_heading'] : esc_html__('', '');
									$adrsvalues_to_dd = array();
									// $dropdown_adr_ft = apply_filters('thwma_dropdown_address_format', false);
									if(apply_filters('thwma_remove_dropdown_address_format', true)) {
										if(!empty($address_values) && is_array($address_values)) {
											foreach ($address_values as $adrs_key => $adrs_value) {
												if($adrs_key == 'billing_address_1' || $adrs_key =='billing_address_2' || $adrs_key =='billing_city' || $adrs_key =='billing_state' || $adrs_key =='billing_postcode') {
													if($adrs_value) {
														$adrsvalues_to_dd[] = $adrs_value;
													}
												}
											}
										}
									} else {
										$type = 'billing';
										$separator = '</br>';
										$new_address = $custom_address[$key];
										$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
										$options_arr = WC()->countries->get_formatted_address($new_address_format);
										$adrsvalues_to_dd = explode('<br/>', $options_arr);
									}
									$adrs_string = implode(', ', $adrsvalues_to_dd);
									//if((isset($heading)) && ($heading!= '') && (!is_array($heading))) {
									if(isset($heading) && $heading != '') {
										$options[$key] = $heading .' - '.$adrs_string;
									} else {
										$options[$key] = $adrs_string;
									}
									// if($i >= $address_limit-2) {
		       //                          break;
		       //                      }

									// Billing dropdown.
									if(!empty($get_default_address)){
										if($i >= $address_limit-2) {
						                    break;
						                }
						            } else {
						            	if($i >= $address_limit-1) {
						                    break;
						                }
						            }	
		                            $i++;
		                        }					
							}
						}
					}
					$address_count = count($custom_address);
					$disable_adr_mngmnt = self::disable_address_mnagement();
					if($disable_adr_mngmnt != true){
						$disable_adr_mngmnt = apply_filters('disable_address_management_on_checkout', false);
					}
					if($disable_adr_mngmnt != true) {
						if(((int)($address_limit)) > $address_count) {
							$options['add_address'] = esc_html__('Add New Address', 'woocommerce-multiple-addresses-pro');
						}
					}
				}
			} else {
				$default_address = 'selected_address';
				$options[$default_address] = esc_html__('Billing Address', 'woocommerce-multiple-addresses-pro');
			}						
			$alt_field = array(
				'required' => false,
				'class'    => array('form-row form-row-wide enhanced_select select2-selection th-select'),
				'clear'    => true,
				'type'     => 'select',
				//'required' => 'true',
				'label'    => THWMA_Utils::get_setting_value('settings_billing','billing_display_text'),
				//'placeholder' =>esc_html__('Choose an Address..',''),
				'options'  => $options
			);
			//woocommerce_form_field(self::DEFAULT_BILLING_ADDRESS_KEY, $alt_field, $options[$default_address]);
			$this->thwma_woocommerce_form_field(self::DEFAULT_BILLING_ADDRESS_KEY, $alt_field, $default_address);	
		}

		/**
         * Function for add shipping address to dropdown(Checkout page - address display style(dropdown))
         *
         * @return void
         */ 
		public function add_dd_to_checkout_shipping() {
			$customer_id = get_current_user_id();
			$custom_addresses = THWMA_Utils::get_custom_addresses($customer_id, 'shipping');
			$default_ship_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_shipping');
			$same_address = THWMA_Utils::is_same_address_exists($customer_id, 'shipping');
			$default_address = $default_ship_address ? $default_ship_address : $same_address;
			$default_address = $same_address ? $same_address : $default_ship_address;

			// Address limit.
			$address_limit = THWMA_Utils::get_setting_value('settings_shipping', 'shipping_address_limit');
            if (!is_numeric($address_limit)) {
                $address_limit = 0;
            }

			$options = array();
			if(is_array($custom_addresses)) {
	        	$custom_address = $custom_addresses;
	        } else {
				$custom_address = array();
				$def_address = THWMA_Utils::get_default_address($customer_id, 'shipping');				
				if(array_filter($def_address) && (count(array_filter($def_address)) > 2)) {
					$custom_address ['selected_address'] = $def_address;
				}
			}
			if($custom_address) {	
				$address_count = count($custom_address);
				if($address_limit && ($address_count > 0)) {
					$shipping_heading = (isset($custom_address[$default_address]['shipping_heading']) && $custom_address[$default_address]['shipping_heading'] !='') ? $custom_address[$default_address]['shipping_heading'] : esc_html__('', '');
					if($default_address) {
						if(isset($options[$default_address])) {
							$options[$default_address] = $shipping_heading .' - '. $custom_address[$default_address]['shipping_address_1'];
						}
					} else {
						$default_address = 'selected_address';
						$options[$default_address] = esc_html__('Shipping Address', 'woocommerce-multiple-addresses-pro');
					}

					if(is_array($custom_address)) {	
						// Default address.	
						foreach ($custom_address as $key => $address_values) {
							$adrsvalues_to_dd = array();
							if($key == $default_address) {								
								$heading = (isset($address_values['shipping_heading']) && $address_values['shipping_heading'] != '') ? $address_values['shipping_heading'] : esc_html__('', '');
								if(apply_filters('thwma_remove_dropdown_address_format', true)) {
									if(!empty($address_values) && is_array($address_values)) {
										foreach ($address_values as $adrs_key => $adrs_value) {
											if($adrs_key == 'shipping_address_1' || $adrs_key =='shipping_address_2' || $adrs_key =='shipping_city' || $adrs_key =='shipping_state' || $adrs_key =='shipping_postcode') {
												if($adrs_value) {
													$adrsvalues_to_dd[] = $adrs_value;
												}
											}
										}
									}
								} else {
									$type = 'shipping';
									$separator = '</br>';
									$new_address = $custom_address[$default_address];
									$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
									$options_arr = WC()->countries->get_formatted_address($new_address_format);
									$adrsvalues_to_dd = explode('<br/>', $options_arr);
								}
							}
							$adrs_string = implode(', ', $adrsvalues_to_dd);
							//if((isset($heading)) && ($heading!= '') && (!is_array($heading))) {
							if(isset($heading) && $heading != '') {
								$options[$key] = $heading .' - '.$adrs_string;
							} else {
								$options[$key] = $adrs_string;
							}
							$options = array_filter($options);
						}

						// Custom addresses.
						$i = 0;
						$default_ship_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_shipping');
						$same_address = THWMA_Utils::is_same_address_exists($customer_id, 'shipping');
						$get_default_address = $same_address ? $same_address : $default_ship_address ;
						$no_of_tile_display = '';
						if(!empty($get_default_address)) {
							$no_of_tile_display = 1;
						} else {
							$no_of_tile_display = 0;
						}
						if($address_limit > $no_of_tile_display) {
							foreach ($custom_address as $key => $address_values) {
								if($key != $default_address) {
									$heading = (isset($address_values['shipping_heading']) && $address_values['shipping_heading'] != '') ? $address_values['shipping_heading'] : esc_html__('', 'woocommerce-multiple-addresses-pro');
									$adrsvalues_to_dd = array();
									if(apply_filters('thwma_remove_dropdown_address_format', true)) {
										if(!empty($address_values) && is_array($address_values)) {
											foreach ($address_values as $adrs_key => $adrs_value) {
												if($adrs_key == 'shipping_address_1' || $adrs_key =='shipping_address_2' || $adrs_key =='shipping_city' || $adrs_key =='shipping_state' || $adrs_key =='shipping_postcode') {
													if($adrs_value) {
														$adrsvalues_to_dd[] = $adrs_value;
													}
												}
											}
										}
									} else {
										$type = 'shipping';
										$separator = '</br>';
										$new_address = $custom_address[$key];
										$new_address_format = THWMA_Utils::get_formated_address($type,$new_address);
										$options_arr = WC()->countries->get_formatted_address($new_address_format);
										$adrsvalues_to_dd = explode('<br/>', $options_arr);
									}
									$adrs_string = implode(', ', $adrsvalues_to_dd);
									//if((isset($heading)) && ($heading != '') && (!is_array($heading))) {
									if(isset($heading) && $heading != '') {
										$options[$key] = $heading .' - '.$adrs_string;
									} else {
										$options[$key] = $adrs_string;
									}
									// if($i >= $address_limit-2) {
		       //                          break;
		       //                      }
									// Shipping dropdown.
									if(!empty($get_default_address)){
										if($i >= $address_limit-2) {
						                    break;
						                }
						            } else {
						            	if($i >= $address_limit-1) {
						                    break;
						                }
						            }	
		                            $i++;	
		                        }				
							}
						}
					}
					$address_count = count($custom_address);
					$disable_adr_mngmnt = self::disable_address_mnagement();
					if($disable_adr_mngmnt != true){
						$disable_adr_mngmnt = apply_filters('disable_address_management_on_checkout', false);
					}
					if($disable_adr_mngmnt != true) {
						if(((int)($address_limit)) > $address_count) {
							$options['add_address'] = esc_html__('Add New Address', 'woocommerce-multiple-addresses-pro');
						}
					}
				}
			} else {
				$default_address = 'selected_address';
				$options[$default_address] = esc_html__('Shipping Address', 'woocommerce-multiple-addresses-pro');
			}		
			$alt_field = array(
				'required' => false,
				'class'    => array('form-row form-row-wide enhanced_select', 'select2-selection'),
				'clear'    => true,
				'type'     => 'select',
				'label'    => THWMA_Utils::get_setting_value('settings_shipping', 'shipping_display_text'),
				//'placeholder' =>esc_html__('Choose an Address..', ''),
				'options'  => $options
			);
			//woocommerce_form_field(self::DEFAULT_SHIPPING_ADDRESS_KEY, $alt_field,$options[$default_address]);
			$this->thwma_woocommerce_form_field(self::DEFAULT_SHIPPING_ADDRESS_KEY, $alt_field,$default_address);
		}

		/**
         * Function for create address form field
         * 
         * @param string $key The key value
         * @param array $args The arguments of form field
         * @param string $value The field passing values
         * @param array $cart_item The cart item data
         *
         * @return string
         */ 
		public function thwma_woocommerce_form_field($key, $args, $value = null, $cart_item = null) {				
			$defaults = array(
				'type'              => '',
				'label'             => '',
				'description'       => '',
				'placeholder'       => '',
				'maxlength'         => false,
				'required'          => false,
				'autocomplete'      => false,
				'id'                => $key,
				'class'             => array(),
				'label_class'       => array(),
				'input_class'       => array(),
				'return'            => false,
				'options'           => array(),
				'custom_attributes' => array(),
				'validate'          => array(),
				'default'           => '',
				'autofocus'         => '',
				'priority'          => '',
			);
			$args = wp_parse_args($args, $defaults);
			$field           = '';
			$label_id        = $args['id'];
			$sort            = $args['priority'] ? $args['priority'] : '';
			$field_container = '<p class="form-row %1$s" id="%2$s" data-priority="' . esc_attr($sort) . '">%3$s</p>';
			$field   = '';
			$options = '';
			$custom_attributes= array();
			if(is_cart()) {
				$product_id = $cart_item["product_id"];
				$cart_key = $cart_item["key"];
			} else {
				$product_id = '';
			}
			if (! empty($args['options']) && is_array($args['options'])) {
				foreach ($args['options'] as $option_key => $option_text) {
					// If we have a blank option, select2 needs a placeholder.
					if (empty($args['placeholder'])) {
						$args['placeholder'] = $option_text ? $option_text : esc_html__('Choose an option','woocommerce-multiple-addresses-pro');
					}
					$custom_attributes[] = 'data-allow_clear="true"';
					// if(is_cart()) {
					// 	$shipping_addr = isset($cart_item['product_shipping_address']) ? $cart_item['product_shipping_address']: '';
					// 	$value = $shipping_addr;
					// 	$options .= '<option value="' . esc_attr($option_key) . '" ' . selected($value, $option_key, false) . ' >' . esc_attr($option_text) . '</option>';
					// } else {
					if($option_key);
					$options .= '<option value="' . esc_attr($option_key) . '" ' . selected($value, $option_key, false) . ' >' . esc_attr($option_text) . '</option>';
					// }
				}
				// if(is_cart()) {
				// 	$field .= '<select name="' . esc_attr($key) . '" id="' . esc_attr($args['id']) . '" class="thwma-cart-shipping-options select ' . esc_attr(implode(' ', $args['input_class'])) . '" ' . implode(' ', $custom_attributes) . ' data-placeholder="' . esc_attr($args['placeholder']) . '" data-product_id="'.$product_id.'" data-cart_key="'.$cart_key.'">' . $options . '</select>';
				// } else {
					$field .= '<select name="' . esc_attr($key) . '" id="' . esc_attr($args['id']) . '" class="select ' . esc_attr(implode(' ', $args['input_class'])) . '" ' . implode(' ', $custom_attributes) . ' data-placeholder="' . esc_attr($args['placeholder']) . '">' . $options . '</select>';
				// }
			}
			if (! empty($field)) {
				$field_html = '';
				if(!is_cart()) { // Check is Cart page.
					if ($args['label']) {
						$field_html .= '<label for="' . esc_attr($label_id) . '" class="' . esc_attr(implode(' ', $args['label_class'])) . '">' . esc_html__($args['label'], 'woocommerce-multiple-addresses-pro')  .'</label>';
					}
				}
				$field_html .= '<span class="woocommerce-input-wrapper">' . $field;
				if ($args['description']) {
					$field_html .= '<span class="description" id="' . esc_attr($args['id']) . '-description" aria-hidden="true">' . wp_kses_post($args['description']) . '</span>';
				}
				$field_html .= '</span>';
				$container_class = esc_attr(implode(' ', $args['class']));
				$container_id    = esc_attr($args['id']) . '_field';
				$field           = sprintf($field_container, $container_class, $container_id, $field_html);
			}
			echo $field; // WPCS: XSS ok.			
		}

		//////////Accordion Display////////////////
		
					
		/**
         * Function for add billing address to accordion(Checkout page - address display style(accordion))
         *
         * @return string
         */ 
		/*public function add_accordion_to_checkout_billing() {

			if (is_user_logged_in()) {
				$options_arr = array();
				$customer_id = get_current_user_id();
				$custom_address = THWMA_Utils::get_custom_addresses($customer_id, 'billing');
				$address_count = count($custom_address);
				$address_limit = THWMA_Utils::get_setting_value('settings_billing', 'billing_address_limit');
				$settings = THWMA_Utils::get_setting_value('settings_billing');
				$billing_display_text = htmlspecialchars($settings['billing_display_text']);
				if($settings['billing_display_title']=='button') {?>
				
					<button class = 'thwma_billing_toggle_accordion'><?php echo $billing_display_text; ?>
					</button> <?php
				} else { ?>
					<a href="#" class = 'thwma_billing_toggle_accordion'><?php echo $billing_display_text; ?></a> <?php
				} ?>
				<div id='thwma_billing_toggle_show' >
				<div id="thwma_billing_accordion" class="th_accordion"><?php
					if($custom_address)	{	
						foreach ($custom_address as $address_key => $value) {
							$new_address=$custom_address[$address_key];
							$new_address_format=THWMA_Utils::get_formated_address('billing', $new_address);
							$options_arr= WC()->countries->get_formatted_address($new_address_format);
							
							if(empty($new_address['billing_heading'])) {
								$heading=$new_address['billing_address_1'];
							}
							else {
								$heading=$new_address['billing_heading'];
							}
							?>
							
								<h4><?php echo $heading ?></h4>
								
								<div class='acordion_para'>
									<?php echo $options_arr; ?>
								
									
										<button class="thwma-acord-billing"  value=<?php echo $address_key ?> onclick="thwma_populate_selected_address(event, this, 'billing', '<?php echo $address_key ?>')">Billing with this Address</button>	
								</div>
							<?php	
						}
					}?>		 	
					</div> <?php
					if(((int)($address_limit)) > $address_count) { ?>
						<button class="thwma-add-new-address" id="thwma-billing_accordion_new_address" onclick="thwma_add_new_address(event, this, 'billing')">Billing with A New Address</button> <?php
					} ?>
				</div><?php
			}			
		}*/
		
		/**
         * Function for add shipping address to accordion(Checkout page - address display style(accordion))
         *
         * @return string
         */ 
		/*public function add_accordion_to_checkout_shipping() {
			if (is_user_logged_in()) {
				$options_arr = array();
				$customer_id=get_current_user_id();
				$custom_address=THWMA_Utils::get_custom_addresses($customer_id, 'shipping');
				$address_count=count($custom_address);
				$address_limit=THWMA_Utils::get_setting_value('settings_shipping', 'shipping_address_limit');
				$settings = THWMA_Utils::get_setting_value('settings_shipping');
				$shipping_display_text = htmlspecialchars($settings['shipping_display_text']);
				if($settings['shipping_display_title'] == 'button') {?>			
					<button class = 'thwma_shipping_toggle_accordion'><?php echo $shipping_display_text; ?></button> <?php
				} else { ?>
					<a href="#" class = 'thwma_shipping_toggle_accordion'><?php echo $shipping_display_text; ?></a> <?php
				} ?>
				<div id='thwma_shipping_toggle_show' >
				<div id="thwma_shipping_accordion" class="th_accordion" ><?php
				if($custom_address) {	
					foreach ($custom_address as $address_key => $value) {
						$new_address = $custom_address[$address_key];
						$new_address_format = THWMA_Utils::get_formated_address('shipping', $new_address);
						$options_arr = WC()->countries->get_formatted_address($new_address_format);
						
						$heading = !empty($new_address['shipping_heading']) ? $new_address['shipping_heading'] : $new_address['shipping_address_1'];
						
						?>
						
							<h4><?php echo $heading ?></h4>
							
							<div class='acordion_para'>
								<?php echo $options_arr; ?>
							
								
									<button class="button-shipping-address"  value=<?php echo $address_key ?> onclick="thwma_populate_selected_address(event,this,'shipping','<?php echo $address_key ?>')"><?php esc_html_e('Shipping with this Address','woocommerce-multiple-addresses-pro'); ?></button>	
							</div>
						<?php	
					}
				}?>		 	
				</div> <?php
				if(((int)($address_limit)) > $address_count) { ?>
					<button class="thwma-add-new-address" id="thwma-shipping_accordion_new_address" onclick="thwma_add_new_address(event, this, 'shipping')"><?php esc_html_e('Add New Address', 'woocommerce-multiple-addresses-pro');?></button><?php
				} ?>
				</div><?php
			}			
		}*/

		/**
         * Function for get address by id(ajax response-checkout page)
         *
         * @return void
         */ 
		public function get_addresses_by_id() {
			check_ajax_referer( 'get-address-with-id', 'security' );
			$address_key = isset($_POST['selected_address_id']) ? sanitize_key($_POST['selected_address_id']) : '';
			$type = isset($_POST['selected_type']) ? sanitize_key($_POST['selected_type']) : '';
			$section_name = isset($_POST['section_name']) ? sanitize_key($_POST['section_name']) : '';
			if(is_user_logged_in()) {			
				$customer_id = get_current_user_id();
				if(!empty($section_name) && $address_key == 'section_address') {
					$custom_address = $this->get_default_section_address($customer_id, $section_name);
				} else {
					if($address_key == 'selected_address') {
						$custom_address = THWMA_Utils::get_default_address($customer_id, $type);
					} else {
						$custom_address = THWMA_Utils::get_custom_addresses($customer_id, $type, $address_key);
					}				
				}
			} else {
				$custom_address = THWMA_Utils::get_custom_addresses_of_guest_user('shipping', $address_key);
			}
			if(!empty($custom_address) && is_array($custom_address)) {
				foreach ($custom_address as $key => $value) {
					if(is_array($value)) {
						$custom_address_data = $value;
					} else {
						$custom_address_data = $custom_address;
					}
				}
				$custom_address = $custom_address_data;
			}
			wp_send_json($custom_address);			
		}

		/**
         * Function for delete address from checkout(ajax response-checkout page)
         *
         * @return void
         */ 
		public function delete_address_from_checkout() {
			check_ajax_referer( 'delete-address-with-id', 'security' );
			$address_key = isset($_POST['selected_address_id']) ? sanitize_key($_POST['selected_address_id']) : '';
			$type = isset($_POST['selected_type']) ? sanitize_key($_POST['selected_type']) : '';			
			$customer_id = get_current_user_id();
			THWMA_Utils::delete($customer_id, $type, $address_key);
			$output_shipping = $this->get_tile_field($customer_id, 'shipping');
			$output_billing = $this->get_tile_field($customer_id, 'billing');
			$response = array(
				'result_billing' => $output_billing,
				'result_shipping' => $output_shipping,	
			);
			wp_send_json($response);
		}

		/**
         * Function for set default address from checkout page(ajax response-checkout page)
         *
         * @return void
         */ 
		public function default_address_from_checkout() {
			check_ajax_referer( 'set-default-address', 'security' );
			$address_key = isset($_POST['selected_address_id']) ? sanitize_key($_POST['selected_address_id']) : '';
			$type = isset($_POST['selected_type']) ? sanitize_key($_POST['selected_type']) : '';
			$user_id = get_current_user_id();
			$this->change_default_address($user_id, $type, $address_key);
			$output_shipping = $this->get_tile_field($user_id, 'shipping');
			$output_billing = $this->get_tile_field($user_id, 'billing');
			$response = array(
				'result_billing' => $output_billing,
				'result_shipping' => $output_shipping,	
			);
			wp_send_json($response);
		}

		/**
         * Function for get default section address from checkout page(checkout page)
         * 
         * @param string $user_id The User id
         * @param string $section_name The section name
         *
         * @return array
         */ 
		public function get_default_section_address($user_id, $section_name) {
			$section_fields  = THWMA_Utils::get_maped_sections_settings_value($section_name, 'map_fields');	
			$section_address = array();
			if(is_array($section_fields) && !empty($section_fields)) {
				foreach ($section_fields as $default_field => $custom_field) {
					$section_address[$default_field] = get_user_meta($user_id, $custom_field, true);
				}
				return $section_address ;
			}
		}

		/**
         * Function for update custom billing address from checkout page(checkout page)
         * 
         * @param integer $order_id The order id
         * @param array $posted_data The posted data info
         * @param array $order The order datas
         */ 
		public function update_custom_billing_address_from_checkout($order_id, $posted_data, $order) {
			if (is_user_logged_in()) {
				$address_key = isset($posted_data['thwma_hidden_field_billing']) ? $posted_data['thwma_hidden_field_billing'] : '';
				$user_id = get_current_user_id();
				$custom_key = THWMA_Utils::get_custom_addresses($user_id, 'default_billing');
				$same_address_key = THWMA_Utils::is_same_address_exists($user_id, 'billing');
				$default_key = ($custom_key) ? $custom_key : $same_address_key ;
				$this->update_address_from_checkout('billing', $address_key, $posted_data, $default_key);
				if($custom_key) {
					$modify = apply_filters('thwma_modify_billing_update_address', true);
					if($modify) {
						$this->change_default_address($user_id, 'billing', $default_key);
					} else {
						if ($address_key == 'add_address') {
							$new_key_id = (THWMA_Utils::get_new_custom_id($user_id, 'billing')) - 1;
							$new_key = 'address_'.$new_key_id;
							$this->change_default_address($user_id, 'billing', $new_key);
						} elseif(!empty($address_key)) {
							$this->change_default_address($user_id, 'billing', $address_key);
						}			
					}
				}		
			}
		}

		/**
         * Function for update custom shipping address from checkout page(checkout page)
         * 
         * @param integer $order_id The order id
         * @param array $posted_data The posted data info
         * @param array $order The order datas
         */ 
		public function update_custom_shipping_address_from_checkout($order_id, $posted_data, $order) {
			$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping']:'';
			$user_id = get_current_user_id();
	        $enable_multi_ship = '';

	        // Enable multi-shipping on checkout page.
	        if (is_user_logged_in()) {
	        	$enable_multi_ship = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
	        }
	        //$flag = false;
	  //       if($cart_shipping_enabled == 'yes') {
	  //       	if($enable_multi_ship != 'yes') {
			// 		$flag = true;
			// 	}
			// } else {
				$flag = true;
			// }

	        if($flag == true) {
				if (is_user_logged_in()) {
					$user_id = get_current_user_id();
					$custom_key = THWMA_Utils::get_custom_addresses($user_id, 'default_shipping');
					$same_address_key = THWMA_Utils::is_same_address_exists($user_id, 'shipping');
					$default_key = ($custom_key) ? $custom_key : $same_address_key ;				
					if (! wc_ship_to_billing_address_only() && wc_shipping_enabled()) {
						$address_key = isset($posted_data['thwma_hidden_field_shipping']) ? $posted_data['thwma_hidden_field_shipping'] : '';
						$ship_select = isset($posted_data['thwma_checkbox_shipping']) ? $posted_data['thwma_checkbox_shipping'] : '';
						if($ship_select == 'ship_select') {
							$this->update_address_from_checkout('shipping', $address_key, $posted_data, $default_key);
						} else {
							if(!$custom_key) {
								$this->update_address_from_checkout('shipping', $ship_select, $posted_data, $default_key);
								//$custom_address = self::get_custom_addresses($user_id, 'shipping', $ship_select);
								//$this->update_address_to_user($user_id, $custom_address, 'shipping', $ship_select);
							}
						}
					}
					if($custom_key) {
						$modify = apply_filters('thwma_modify_shipping_update_address', true);
						if($modify) {
							$this->change_default_address($user_id, 'shipping', $default_key);
						} else {
							if ($address_key == 'add_address') {
								$new_key_id = (THWMA_Utils::get_new_custom_id($user_id, 'shipping')) - 1;
								$new_key = 'address_'.$new_key_id;
								$this->change_default_address($user_id, 'shipping', $new_key);
							} elseif(!empty($address_key)) {
								$this->change_default_address($user_id, 'shipping', $address_key);
							}
						}
					}
				}
			}
		}

		/**
         * Function for update address from checkout(checkout page)
         * 
         * @param string $type The type of the address
         * @param string $address_key The address key value
         * @param array $posted_data The posted data info
         * @param string $default_key The passed default key
         */ 
		public function update_address_from_checkout($type, $address_key, $posted_data, $default_key) {
			$user_id = get_current_user_id();			
			$added_address = array();
			$added_address = $this->prepare_order_placed_address($user_id, $posted_data, $type);
			$heading = THWMA_Utils::get_custom_addresses($user_id, $type, $address_key, $type.'_heading');
			$added_address[$type.'_heading'] = ''; //$heading ? $heading : esc_html__('','woocommerce-multiple-addresses-pro');
			if($address_key == 'add_address') {				
				self::save_address_to_user_from_checkout($added_address, $type);
			}
			elseif(($default_key) && (empty($address_key)|| ($address_key == $default_key))) {	
				if(!apply_filters('disable_address_management_on_checkout', false)) {			
					THWMA_Utils::update_address_to_user($user_id, $added_address, $type, $default_key);
				}
			} elseif($address_key && ($address_key != 'selected_address')) {
				if(!apply_filters('disable_address_management_on_checkout', false)) {
					$this->update_address_to_user_from_checkout($user_id, $added_address, $type, $address_key);
				}
			}
		}

		/**
         * Function for save address to user from checkout(checkout page)
         * 
         * @param array $address The address information
         * @param string $type The type of the address
         */ 
		private function save_address_to_user_from_checkout($address, $type) {
			$user_id = get_current_user_id();
			$custom_addresses = get_user_meta($user_id,THWMA_Utils::ADDRESS_KEY, true);
			$custom_addresses = is_array($custom_addresses) ? $custom_addresses : array();
			$saved_address = THWMA_Utils::get_custom_addresses($user_id, $type);
			if(!is_array($saved_address)) {
				$custom_address = array();
				$default_address = THWMA_Utils::get_default_address($user_id, $type);
				if(!array_key_exists($type.'_heading', $default_address)) {
					$default_address[$type.'_heading'] = esc_html__('', 'woocommerce-multiple-addresses-pro');
				}
				$custom_address['address_0'] = $default_address;
				//$custom_address['address_1'] = $address;
				$custom_key = THWMA_Utils::get_custom_addresses($user_id, 'default_'.$type);
				$custom_addresses[$type] = $custom_address;				
			} else {
				if(is_array($saved_address)) {
					if(isset($custom_addresses[$type])) {
						$exist_custom = $custom_addresses[$type];
						$new_key_id = THWMA_Utils::get_new_custom_id($user_id, $type);
						$new_key = 'address_'.$new_key_id;
						$custom_address[$new_key] = $address; 
						$custom_key = THWMA_Utils::get_custom_addresses($user_id, 'default_'.$type);
						if(!$custom_key) {
							$custom_addresses['default_'.$type] = $new_key;
						}
						$custom_addresses[$type] = array_merge($exist_custom, $custom_address);		
					}
				}		
			}	
			
			update_user_meta($user_id,THWMA_Utils::ADDRESS_KEY, $custom_addresses);
		}

		/**
         * Function for update address to user from checkout(checkout page)
         * 
         * @param integer $user_id The user id
         * @param array $address The address information
         * @param string $type The type of the address
         * @param string $address_key The address key
         */ 
		private function update_address_to_user_from_checkout($user_id, $address, $type, $address_key) {
			$custom_addresses = get_user_meta($user_id,THWMA_Utils::ADDRESS_KEY, true);
			$exist_custom = isset($custom_addresses[$type]) ? $custom_addresses[$type] : array();
			$custom_address[$address_key] = $address;
			$custom_key = THWMA_Utils::get_custom_addresses($user_id, 'default_'.$type);
			if(!$custom_key) {
				$custom_addresses['default_'.$type] = $address_key;
			}
			$custom_addresses[$type] = array_merge($exist_custom, $custom_address);			
			update_user_meta($user_id,THWMA_Utils::ADDRESS_KEY, $custom_addresses);
		}

		/**
         * Function for add address from cehckout (for initial case-checkout page)
         * 
         * @param string $data The given item datas
         * @param string $errors The existing errors
         *
         * @return string
         */ 
		public function add_address_from_checkout($data, $errors) {
			$this->validate_multi_shipping_address_of_guest_users($data, $errors);

			$user_id = get_current_user_id();	
			if(empty($errors->get_error_messages() )) {
				if(isset($_POST['thwma_hidden_field_billing'])) {
					$checkout_bil_key = isset($_POST['thwma_hidden_field_billing']) ? $_POST['thwma_hidden_field_billing'] : '';				
					if($checkout_bil_key == 'add_address') {
						$this->set_first_address_from_checkout($user_id, 'billing');
					}
				}
				if (! wc_ship_to_billing_address_only() && wc_shipping_enabled()) {
					$checkout_ship_key = isset($_POST['thwma_hidden_field_shipping']) ? $_POST['thwma_hidden_field_shipping'] : '';
					if(!empty($checkout_ship_key)) {						
						if($checkout_ship_key == 'add_address') {
							$this->set_first_address_from_checkout($user_id, 'shipping');
						}
					}
				}
			}
		}

		/**
         * Function for multi-ship excluding produts lists.
         * 
         * @param string $data The given item datas
         * @param string $errors The existing errors
         *
         * @return string
         */ 
		public function multiship_excludeing_products() {
			$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$settings_guest_urs = THWMA_Utils::get_setting_value('settings_guest_users');			
			if($settings && !empty($settings)) {
				$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping'] : '';
				$enable_product_variation = isset($settings['enable_product_variation']) ? $settings['enable_product_variation'] : '';
				$enabled_guest_user = isset($settings_guest_urs['enable_guest_shipping']) ? $settings_guest_urs['enable_guest_shipping'] : '';
				$exclude_products = isset($settings['exclude_products']) ? $settings['exclude_products'] : '';
				$exclude_category = isset($settings['exclude_category']) ? $settings['exclude_category'] : '';
				$excl_pdt_ids = array();
				if(!empty($exclude_products) && is_array($exclude_products)) {
					foreach ($exclude_products as $ex_key => $ex_value) {
						$product_obj = get_page_by_path($ex_value, OBJECT, 'product');
						if($product_obj != '') {
							$excl_pdt_ids[] = $product_obj->ID;
						}
					}
				}
				$category_pdt_id = array();
				if(!empty($exclude_category) && is_array($exclude_category)) {
					foreach ($exclude_category as $ex_cat_key => $ex_cat_value) {
						$args = array('post_type' => 'product', 'product_cat' => $ex_cat_value, 'orderby' =>'rand');
						if(!empty($args)) {
							$loop = new WP_Query($args);
							while ($loop->have_posts()) : $loop->the_post();
							global $product; 
							$category_pdt_id[] = get_the_ID();							
							endwhile;wp_reset_query(); 
						}
					}
				}
				$ex_pdt_ids = array_merge($excl_pdt_ids, $category_pdt_id);
			}
			return $ex_pdt_ids;
		}


		/**
         * Function for validate multi-shiiping address fields of guest users (checkout page)
         * 
         * @param string $data The given item datas
         * @param string $errors The existing errors
         *
         * @return string
         */ 
		public function validate_multi_shipping_address_of_guest_users($data, $errors) {
			if(!is_user_logged_in()) {
				$settings = THWMA_Utils::get_setting_value('settings_shipping');
				if($settings && !empty($settings)) {

					// Check shipping is enabled.
					if(array_key_exists('enable_shipping', $settings)) {
						if($settings['enable_shipping'] == 'yes') {

							// Check Multi-shipping is enabled.
							$settings_multi_ship = THWMA_Utils::get_setting_value('settings_multiple_shipping');
							$multi_shipping_enabled = isset($settings_multi_ship['enable_cart_shipping']) ? $settings_multi_ship['enable_cart_shipping']:'';
							if($multi_shipping_enabled == 'yes') {
								$settings_guest_urs = THWMA_Utils::get_setting_value('settings_guest_users');
								$enabled_guest_user = isset($settings_guest_urs['enable_guest_shipping']) ? $settings_guest_urs['enable_guest_shipping']:'';
								$enable_product_variation = isset($settings_multi_ship['enable_product_variation']) ? $settings_multi_ship['enable_product_variation']:'';
								if($enabled_guest_user == 'yes') {

									// Enable multi-shipping on checkout page.
							        $enable_multi_ship = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]) ? $_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]:'';
							        
							        if($enable_multi_ship == 'yes') {

										// Case of shipping fields are not filled.
										if(!empty($errors->errors) && is_array($errors->errors)) {
											foreach ($errors->errors as $key => $value) {
												if(substr($key, 0, strlen('shipping')) == 'shipping') {
													unset($errors->errors[$key]);
												}
											}
										}

										if(!empty($errors->error_data) && is_array($errors->error_data)) {
											foreach ($errors->error_data as $key => $value) {
												if(substr($key, 0, strlen('shipping')) == 'shipping') {
													$errors->errors['multiple_shipping'] = array('0' =>'Addresses are not chosen for some of the products. Please choose an Address for all products.');
												}
											}
											return $errors;
										}

										// Case of shipping fields are filled.
										if(!empty(WC()->cart->get_cart()) && is_array(WC()->cart->get_cart())) {
											foreach ( WC()->cart->get_cart() as $cart_item ) {
												if(array_key_exists('variation_id', $cart_item)) {
													if($cart_item['variation_id'] !== 0){
														if($enable_product_variation == 'yes'){
														 	if(!array_key_exists('product_shipping_address', $cart_item)) {
														 		$errors->errors['multiple_shipping'] = array('0' =>'Addresses are not chosen for some of the products. Please choose an Address for all products.');
													        } else {
													        	$multi_shipping_adr = $cart_item['product_shipping_address'];
													        	if($multi_shipping_adr == null){
													        		$errors->errors['multiple_shipping'] = array('0' =>'Addresses are not chosen for some of the products. Please choose an Address for all products.');
																}
													        }
														} else {
													    }
													} else {
														$excluding_pdts = $this->multiship_excludeing_products();
														$product_id = isset($cart_item["product_id"]) ? $cart_item["product_id"] : '';
														if(!empty($excluding_pdts)) {
															if(!in_array($product_id, $excluding_pdts)) {	
																if(!array_key_exists('product_shipping_address', $cart_item)) {
																	$errors->errors['multiple_shipping'] = array('0' =>'Addresses are not chosen for some of the products. Please choose an Address for all products.');
																} else {
														        	$multi_shipping_adr = $cart_item['product_shipping_address'];
														        	if($multi_shipping_adr == null){
														        		$errors->errors['multiple_shipping'] = array('0' =>'Addresses are not chosen for some of the products. Please choose an Address for all products.');
																	}
														        }

															}
														} else {						        
													        if(!array_key_exists('product_shipping_address', $cart_item)) {
													        	$errors->errors['multiple_shipping'] = array('0' =>'Addresses are not chosen for some of the products. Please choose an Address for all products.');
															} else {
													        	$multi_shipping_adr = $cart_item['product_shipping_address'];
													        	if($multi_shipping_adr == null){
													        		$errors->errors['multiple_shipping'] = array('0' =>'Addresses are not chosen for some of the products. Please choose an Address for all products.');
																}
													        }
													    }
												    }
												}
											}
										    return $errors;
										}
									}
								}
							}
						}
					}
				}	
			}	
		}
		
		/**
         * Function for set first address from checkout(checkout page)
         * 
         * @param integer $user_id The user id
         * @param string $type The address type
         */ 
		public function set_first_address_from_checkout($user_id, $type) {
			$custom_addresses = get_user_meta($user_id, THWMA_Utils::ADDRESS_KEY, true);
			$custom_address = THWMA_Utils::get_custom_addresses($user_id, $type);
			$checkout_address_key = isset($_POST['thwma_hidden_field_'.$type]) ? sanitize_text_field($_POST['thwma_hidden_field_'.$type]) : '';
			if(!$custom_address && $checkout_address_key == 'add_address') {
				$custom_address = array();
				$custom_addresses = is_array($custom_addresses) ? $custom_addresses : array();
				$default_address = THWMA_Utils::get_default_address($user_id, $type);
				if(array_filter($default_address) && (count(array_filter($default_address)) > 2)) {
					if(!array_key_exists($type.'_heading', $default_address)) {
						$default_address[$type.'_heading'] = esc_html__('', 'woocommerce-multiple-addresses-pro');
					}	
					$custom_address['address_0'] = $default_address;
					$custom_addresses[$type] = $custom_address;
					update_user_meta($user_id, THWMA_Utils::ADDRESS_KEY, $custom_addresses);
				}
			}
		}

		/**
         * Function for prepare the address after place order process(checkout page)
         * 
         * @param integer $user_id The user id
         * @param string $posted_data The posted datas
         * @param string $type The address type
         */ 
		private function prepare_order_placed_address($user_id, $posted_data, $type) {
			$fields = THWMA_Utils::get_address_fields($type);
			$new_address = array();
			if(!empty($fields) && is_array($fields)) {
				foreach ($fields as $key) {
					if(isset($posted_data[$key])) {
						$new_address[$key] = is_array($posted_data[$key]) ? implode(', ', $posted_data[$key]) : $posted_data[$key];
					}
				}
			}
			return $new_address;
		}

		/**
         * Function for prepare address fields before billing (checkout page)
         * 
         * @param array $fields The field datas
         * @param string $country The country info
         *
         * @return array
         */ 
		public function prepare_address_fields_before_billing($fields, $country) {
			if(!empty($fields) && is_array($fields)) {
				foreach ($fields as $key => $value) {
					if ('billing_state' === $key) {
						if(!isset($fields[$key]['country_field'])) {
							$fields[$key]['country_field'] = 'billing_country';
						}
					}			
				}
			}
			return $fields;
		}

		/**
         * Function for prepare address fields before shipping (checkout page)
         * 
         * @param array $fields The field datas
         * @param string $country The country info
         *
         * @return array
         */ 
		public function prepare_address_fields_before_shipping($fields, $country) {
			if(!empty($fields) && is_array($fields)) {
				foreach ($fields as $key => $value) {
					if ('shipping_state' === $key) {
						if(!isset($fields[$key]['country_field'])) {
							$fields[$key]['country_field'] = 'shipping_country';
						}
					}		
				}
			}
			return $fields;
		}	

		/**
         * Localisation of address formats (address override)
         * 
         * @param array $formats The address format info
         *
         * @return array
         */ 
		public function localisation_address_formats($formats) {			
			$address_formats_str = THWMA_Utils:: get_advanced_settings_value('address_formats');
			$custom_formats = array();
			if(!empty($address_formats_str)) {
				$address_formats_arr = explode("|", $address_formats_str);
				if(is_array($address_formats_arr) && !empty($address_formats_arr)) {
					foreach($address_formats_arr as $address_format) {
						if(!empty($address_format)) {
							$format_arr = explode("=>", $address_format);
							if(is_array($format_arr) && count($format_arr) == 2) {
								$frmt = str_replace('\n', "\n", $format_arr[1]);
								$custom_formats[trim($format_arr[0])] = $frmt;
							}
						}
					}
				}
			}			
			if(is_array($formats) && $custom_formats && is_array($custom_formats)) {
				$formats = array_merge($formats, $custom_formats);
			}
			return $formats;
		}

		/**
         * Function for check current theme
         *
         * @return string
         */ 
		public function check_current_theme() {
			$current_theme = wp_get_theme();
		   	$current_theme_name = isset($current_theme['Template']) ? $current_theme['Template'] : '';
		   	$wrapper_class = '';
		  	if($current_theme_name) {
		   		$wrapper_class = str_replace(' ', '-', strtolower($current_theme_name));
		   		$theme_class_name = 'thwma_'.$wrapper_class;
		   	}
		   	return $theme_class_name;
		}

		/**
         * Function for set disable address management(Using user role)
         *
         * @return string
         */ 
		public static function disable_address_mnagement() {
			$settings = THWMA_Utils::get_advanced_settings();
			if(!empty($settings)) {
				$user_roles = array();
				$current_user = array();
				$user = wp_get_current_user();
				$disable_adr_mngmt = isset($settings['disable_address_management']) ? $settings['disable_address_management'] : '';
				$user_roles = isset($settings['select_user_role']) ? $settings['select_user_role'] : '';
				$userroles = explode(',', $user_roles);
				$current_user = $user->roles;
				
				if($disable_adr_mngmt == 'yes') {
					if(!empty($user_roles)) {
						if(!empty($current_user) && is_array($current_user)){
							foreach($current_user as $cur_user) {
								if (in_array($cur_user, $userroles, TRUE)) { 
									return true;
								}
							}
						}
					} else {
						return true;
					}
				}
			}
		}

		/*-- Multi-Shipping --*/

		/**
		 * Function for get tile addresses fields for multi-shipping addresses (checkout page).
         * Function for get tile field to cart page (cart page)
         * 
         * @param integer $customer_id The user id
         * @param string $type The address type
         *
         * @return string
         */ 
		public static function get_tile_field_to_cart($customer_id, $type) {
			$oldcols = 1;
			$cols    = 1;
			$disable_adr_mngmnt = self::disable_address_mnagement();
			if($disable_adr_mngmnt != true){
                $disable_adr_mngmnt = apply_filters('disable_address_management_on_checkout', false);
            }
			if($disable_adr_mngmnt == true) {
				$disable_mngt_class = 'thwma_disable_adr_mngt';
				$disable_acnt_sec = 'disable_acnt_sec';
			} else {
				$disable_mngt_class = '';
				$disable_acnt_sec = '';
			}
			$custom_address = THWMA_Utils::get_custom_addresses($customer_id, $type);
			$default_set_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_'.$type);
			$same_address = THWMA_Utils::is_same_address_exists($customer_id, $type);
			//$default_address = $default_set_address ? $default_set_address : $same_address;	
			$default_address = 	$same_address ? $same_address : $default_set_address;	
			$address_count = is_array($custom_address) ? count($custom_address) : 0 ;

			// Address limit.
			$address_limit = '';
            if($type) {
                $address_limit = THWMA_Utils::get_setting_value('settings_'.$type , $type.'_address_limit');
            }
            if (!is_numeric($address_limit)) {
                $address_limit = 0;
            }

			$return_html = '';
			$add_class='';
			$address_type = "'$type'";
			$add_list_class  = ($type == 'billing') ? " thwma-thslider-list-ms bill " : " thwma-thslider-list-ms ship"; 	
			$add_address_btn = '<div class="add-address thwma-add-adr '.esc_attr($disable_acnt_sec).'">
	            <button class="btn-add-address primary button '.esc_attr($disable_mngt_class).'" onclick="thwma_add_new_address(event, this, '.esc_attr($address_type).')">
	                <i class="fa fa-plus"></i> '.esc_html__(' Add new address', 'woocommerce-multiple-addresses-pro').'
	            </button>
	        </div>';
	        
	    	$add_address_btn = '<div class="add-address thwma-add-adr '.esc_attr($disable_acnt_sec).'">
	            <button class="btn-add-address primary button '.esc_attr($disable_mngt_class).'" onclick="thwma_add_new_shipping_address(event, this, '.esc_attr($address_type).')">
	                <i class="fa fa-plus"></i> '.esc_html__(' Add new address', 'woocommerce-multiple-addresses-pro').'
	            </button>
	        </div>';
	        if(is_array($custom_address)) {
	        	$all_addresses = $custom_address;
	        } else {
				$all_addresses = array();
				$def_address = THWMA_Utils::get_default_address($customer_id, $type);
				if(array_filter($def_address) && (count(array_filter($def_address)) > 2)) {
					$all_addresses ['selected_address'] = $def_address;
				}
			}
			$total_address_count = count($all_addresses);
			//if($address_limit && ($total_address_count > 1)) {
			$return_html .= '<div class="thwma-cart-modal-content">';
				$return_html .= '<div class="thwma-cart-modal-title-bar" >';
					$return_html .= '<span class="thwma-cart-modal-title" >Shipping Addresses</span>';
					//$return_html .= '<span class="thwma-cart-modal-close" onclick="thwma_close_cart_adr_list_modal(this)">&times;</span>';
					$return_html .= '<span class="thwma-cart-modal-close" onclick="thwma_close_cart_adr_list_modal(this)"><span class="dashicons dashicons-no"></span></span>';
				$return_html .= '</div>';
				$return_html .= '<div class="thwma-thslider">';
					if($address_limit && ($total_address_count >= 1)) {
						if($all_addresses && is_array($all_addresses)) {
				           	$return_html .= '<div class="thwma-thslider-box">';
					           	$return_html .= '<div class="thwma-thslider-viewport multi-'.esc_attr($type).'">';
						           	$return_html .= '<ul class=" '.esc_attr($add_list_class).'">';

						           	// Default address.
						           	$action_row_html = '';
									$new_address = isset($all_addresses[$default_address]) ? $all_addresses[$default_address] : '';
									$def_new_address = $new_address;
									if(!empty($def_new_address)) {
										$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
										$options_arr = WC()->countries->get_formatted_address($new_address_format);
										$address_key_param = "'".$default_address."'";	
										$address_type_css = 'default';
										$heading = sprintf(esc_html__('Default', 'woocommerce-multiple-addresses-pro'));
										$action_row_html .= '<div class="thwma-adr-footer address-footer '.$address_type_css.'"> 
											<div class="th-btn btn-delete '.esc_attr($disable_mngt_class).'"><span>'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'</span></div> 
											<div class="th-btn btn-default"><span>'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'</span></div>   
										</div>';
										if(isset($heading) && $heading != '') {
											$heading_css = '<div class="address-type-wrapper row"> 
												<div title="'.$heading.'" class="address-type '.esc_attr($address_type_css).'">'.esc_attr($heading).'</div>  
											</div>       
											<div class="tile-adrr-text thwma-adr-text address-text address-wrapper">'.$options_arr.'</div>'; 
										} else {
											$heading_css = '<div class="tile-adrr-text thwma-adr-text address-text address-wrapper wrapper-only">'.$options_arr.'</div>'; 
										}
										$add_class  = "thwma-thslider-item-ms $type" ;
										$return_html .= '<li class="'.esc_attr($add_class).'" value="'. esc_attr($default_address).'" >
										<div class="thwma-adr-box address-box" data-index="0" data-address-id=""> 
											<div class="thwma-main-content"> 
												<div class="complete-aaddress">  
													'.$heading_css.'
												</div> 
												<div class="btn-continue address-wrapper"> 
													<a class="th-btn button primary is-outline '.esc_attr($default_address).'" onclick="thwma_populate_selected_address(event, this, '.esc_attr($address_type).', '.esc_attr($address_key_param).')"> 
														<span>'.esc_html__('Choose This Address', 'woocommerce-multiple-addresses-pro').'</span> 
													</a> 
												</div> 
											</div>'.$action_row_html.'</div></li>';
									}

									$i = 1;	
									$no_of_tile_display = '';
									if(!empty($def_new_address)) {
										$no_of_tile_display = 1;
									} else {
										$no_of_tile_display = 0;
									}
									if($address_limit > $no_of_tile_display) {								
										foreach ($all_addresses as $address_key => $value) {
											// if(isset($value[$type.'_heading'])) {
											// 	if(!is_array($value[$type.'_heading'])) {
											// 		$new_address = $all_addresses[$address_key];
											// 		$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
											// 	}
											// }
											$new_address = $all_addresses[$address_key];
											$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
											$options_arr = WC()->countries->get_formatted_address($new_address_format);
											$address_key_param = "'".$address_key."'";									
											$heading = !empty($new_address[$type.'_heading']) ? $new_address[$type.'_heading'] : esc_html__('', 'woocommerce-multiple-addresses-pro') ;
											$action_row_html = '';
											if($default_address) {
												$is_default = ($default_address == $address_key) ? true : false;
											} else {
												$is_default = false;
											}
											$address_type_css = '';
											if(!$is_default || empty($def_new_address)) {						
												if($total_address_count>=1) {
													if(!empty($custom_address))	{						
														$action_row_html .= '<div class="thwma-adr-footer address-footer"> 
															<div class="btn-delete '.esc_attr($disable_mngt_class).'" data-index="0" data-address-id="" onclick="thwma_delete_selected_address_cart_page(this, '.esc_attr($address_type).', '.esc_attr($address_key_param).')" title="'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'"> 
																<span>'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'</span> 
															</div> 
															<div class="btn-default" data-index="0" data-address-id="" onclick="thwma_set_default_address_cart_page(this, '.esc_attr($address_type).', '.esc_attr($address_key_param).')" title="'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'"> 
																<span>'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'</span>
															</div>   
														</div>';
													}						
												} else {
													// // $address_type_css = 'default';
													// // $heading = sprintf(esc_html__('Default', 'woocommerce-multiple-addresses-pro'));						
													// $action_row_html .= '<div class="thwma-adr-footer address-footer '.$address_type_css.'"> 
													// 	<div class="th-btn btn-delete '.esc_attr($disable_mngt_class).'"><span>'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'</span></div> 
													// 	<div class="btn-default" data-index="0" data-address-id="" onclick="thwma_set_default_address_cart_page(this, '.esc_attr($address_type).', '.esc_attr($address_key_param).')" title="'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'"> 
													// 		<span>'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'</span>
													// 	</div>    
													// </div>';						
												}
												// } 
												// else 
												if(empty($def_new_address)) {
													if(empty($custom_address)) {
														$address_type_css = 'default';
														$heading = sprintf(esc_html__('Default', 'woocommerce-multiple-addresses-pro'));
														$action_row_html .= '<div class="thwma-adr-footer address-footer '.$address_type_css.'"> 
															<div class="th-btn btn-delete '.esc_attr($disable_mngt_class).'"><span>'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'</span></div> 
															<div class="th-btn btn-default"><span>'.esc_html__('Default', 'woocommerce-multiple-addresses-pro').'</span></div>   
														</div>';
													}
												}
												if(isset($heading) && $heading != '') {
													$heading_css = '<div class="address-type-wrapper row"> 
														<div title="'.$heading.'" class="address-type '.esc_attr($address_type_css).'">'.esc_attr($heading).'</div>  
													</div>       
													<div class="tile-adrr-text thwma-adr-text address-text address-wrapper">'.$options_arr.'</div>'; 
												} else {
													$heading_css = '<div class="tile-adrr-text thwma-adr-text address-text address-wrapper wrapper-only">'.$options_arr.'</div>'; 
												}
												$add_class  = "thwma-thslider-item-ms $type " ;
												$add_class .= $i == 0 ? ' first' : '';
												$return_html .= '<li class="'.esc_attr($add_class).'" value="'. esc_attr($address_key).'" >
													<div class="thwma-adr-box address-box" data-index="'.esc_attr($i).'" data-address-id=""> 
														<div class="thwma-main-content"> 
															<div class="complete-aaddress">  
																'.$heading_css.'
															</div> 
															<div class="btn-continue address-wrapper"> 
																<a class="th-btn button primary is-outline '.esc_attr($address_key).'" onclick="thwma_populate_selected_address(event, this, '.esc_attr($address_type).', '.esc_attr($address_key_param).')"> 
																	<span>'.esc_html__('Choose This Address', 'woocommerce-multiple-addresses-pro').'</span> 
																</a> 
															</div> 
														</div>'.$action_row_html.'</div></li>';
												// if($i >= $address_limit-1) {
		          //                                   break;
		          //                               }
												// multi-shipping tile.
												if($def_new_address) {
													if($i >= $address_limit-1) {
			                                            break;
			                                        }
			                                    } else {
			                                    	if($i >= $address_limit) {
			                                            break;
			                                        }
			                                    }
												$i++;
											}
										}
									}
									$return_html .= '</ul>';
								$return_html .= '</div>';
							$return_html .= '</div>';
							$return_html .= '<div class="control-buttons control-buttons-multi'.esc_attr($type).'">';
								if($address_count && $address_count > 3) {
									if($address_limit>3) {
						            	$return_html .= '<div class="prev thwma-thslider-prev multi-'.esc_attr($type).'"><i class="fa fa-angle-left fa-3x"></i></div>';
						            	$return_html .= '<div class="next thwma-thslider-next multi-'.esc_attr($type).'"><i class="fa fa-angle-right fa-3x"></i></div>';
						            }
					            }
				           	$return_html .= '</div>'; 
				           	if(((int)($address_limit)) > $address_count) {
				           		$return_html .= $add_address_btn;
				           	}
				        }
			        } else {
			        	$return_html .= '<div class="th-no-address-msg"  >	<span>'.esc_html__('No saved addresses found', 'woocommerce-multiple-addresses-pro').'</span>  </div>';
			        	//if(is_cart()) {
			        		$return_html .= $add_address_btn;
			        	//}
			        }	           
		        $return_html .= '</div>';
	        $return_html .= '</div>';
		    //}	        
			return $return_html;
		}

		/**
		 * Function enable or disable multi-shipping facility on checkout page(Ajax-response).
		 * function for saving the enabled ship to multiple address data.(ajax function- cart page)
		 */
		function enable_ship_to_multi_address() {
			check_ajax_referer( 'enable-ship-to-multi-address', 'security' );

				$value = isset($_POST['value']) ? sanitize_text_field($_POST['value']) : '';
				$settings_guest_urs = THWMA_Utils::get_setting_value('settings_guest_users');
				$enabled_guest_user = isset($settings_guest_urs['enable_guest_shipping']) ? $settings_guest_urs['enable_guest_shipping'] : '';
				if (is_user_logged_in()) {
					$user_id = get_current_user_id();
					update_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, $value);
				} else {
					if($enabled_guest_user == 'yes') {
						$expiration = self::expiration_time();
						setcookie(THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP, $value, time() + $expiration, "/");
					}
				}
				if($enabled_guest_user == 'yes') {
					$custom_addresses = THWMA_Utils::get_custom_addresses_of_guest_user('shipping');
				}
				if(!empty($custom_addresses)){
					echo true;
				} else {
					echo  false;
				}
				if($value == 'no') {
					//$this->thwma_get_shipping_change_to_packages_default($packages);
				}
			exit();
		}

		/**
         * Function for get saved custom addresses from db (cart page)
         *
         * @return array
         */ 
		public static function get_saved_custom_addresses_from_db() {
			$custom_addresses = array();
			$customer_id = get_current_user_id();
			$settings_guest_urs = THWMA_Utils::get_setting_value('settings_guest_users');
			$enabled_guest_user = isset($settings_guest_urs['enable_guest_shipping']) ? $settings_guest_urs['enable_guest_shipping']:'';
			if(is_user_logged_in()) {
				$default_ship_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_shipping');
				$same_address = THWMA_Utils::is_same_address_exists($customer_id, 'shipping');
				$default_address = $default_ship_address ? $default_ship_address : $same_address;
				$custom_addresses = THWMA_Utils::get_custom_addresses($customer_id, 'shipping');
			} else {
				if($enabled_guest_user == 'yes') {
					$custom_addresses = THWMA_Utils::get_custom_addresses_of_guest_user('shipping');
				}
			}
			if(is_array($custom_addresses)) {
	        	$custom_address = $custom_addresses;
	        } else {
				$custom_address = array();
				$def_address = THWMA_Utils::get_default_address($customer_id, 'shipping');								
				if(array_filter($def_address) && (count(array_filter($def_address)) > 2)) {
					$custom_address ['selected_address'] = $def_address;
				}	
			}
			return $custom_address;
		}

		/**
		 * Function for add new shipping addresses on multi-shipping section(checkout page).
         * Function for add new shipping address (cart page - ajax response).
         */ 
		public function add_new_shipping_address() {
			check_ajax_referer( 'add-new-shipping-address', 'security' );
			if(isset($_POST['thwma_save_address'])) {
				echo $this->thwma_save_address();
			}
			$load_address = sanitize_key('shipping');
			$country      = get_user_meta(get_current_user_id(), $load_address . '_country', true);
			if ('shipping' === $load_address) {
				$allowed_countries = WC()->countries->get_shipping_countries();
				if($country != null) {
					if (! array_key_exists($country, $allowed_countries)) {
						$country = current(array_keys($allowed_countries));
					}
				}
			}
			$address = WC()->countries->get_address_fields($country, $load_address . '_');
			if(!empty($address) && is_array($address)) {
				foreach ($address as $key => $field) {
					$value = get_user_meta(get_current_user_id(), $key, true);
					if (! $value) {
						switch ($key) {
							case 'billing_email':
							case 'shipping_email':
								$value = $current_user->user_email;
								break;
						}
					}
					$address[ $key ]['value'] = '';
				}
			} ?>
			<div class="thwma-cart-modal-content2">
				<div class="thwma-cart-modal-title-bar2" >
					<!-- <span class="thwma-cart-modal-title" >Shipping Address</span> -->
					<span class="thwma-cart-modal-close2" onclick="thwma_close_cart_add_adr_modal(event);">&times;</span>
				</div>
				<div class="thwma_hidden_error_mssgs"></div>
				<div id="cart_shipping_form_wrap">
				<form method="post" id="cart_shipping_form" name="thwma_cart_ship_form_action">
					<!-- <input type="hidden" name="cart_ship_form_action" value="<?php wp_create_nonce('thwma_cart_ship_form_action') ?>"> -->
					<?php echo '<input type="hidden" name="cart_ship_form_action" id="cart_ship_form_action" value="' . wp_create_nonce( 'thwma_cart_ship_form_action' ) . '" />';
					//$ajax_nonce = wp_create_nonce( "wpdocs-special-string" );
					//$nonce = wp_create_nonce('cart_ship_form_action'); ?>
					<div>
						<?php if(!empty($address) && is_array($address)) {
							foreach ($address as $key => $field) {
								woocommerce_form_field($key, $field, wc_get_post_data_by_key($key, $field['value']));
								// THWMA_Utils::thwma_cart_shipping_woocommerce_form_field($key, $field, wc_get_post_data_by_key($key, $field['value']));
							}
						}
						do_action("woocommerce_after_edit_address_form_{$load_address}");?>
					</div>
					<p>
						<button id="thwma_save_address_cart" type="submit" class="button form-row-odd" name="thwma_save_address" onclick="thwma_cart_save_address(event);" value="<?php esc_attr_e('Save address', 'woocommerce'); ?>"><?php esc_html_e('Save address', 'woocommerce'); ?></button>
					</p>
				</form>
				</div>
			</div>
			<?php exit();
		}

		/**
		 * Save new shipping address on multi-shipping section(checkout page).
		 * Save new shipping address from cart page(cart page).
		 * 
		 * @return void
		 */
		public static function thwma_save_address() {
			check_ajax_referer( 'cart_ship_form_action', 'security', false );
			$error_messgs = '';
			$user_id = get_current_user_id();
			// if ($user_id <= 0) {
			// 	return;
			// }
			$customer = new WC_Customer($user_id);
			// if (! $customer) {
			// 	return;
			// }
			$load_address = sanitize_key('shipping');
			$country      = get_user_meta(get_current_user_id(), $load_address . '_country', true);
			$cart_shipping = isset($_POST['cart_shipping']) ? $_POST['cart_shipping'] : '';
			$cart_shipping_data = self::thwma_unserializeForm($cart_shipping);
			$new_shipping_country = isset($cart_shipping_data['shipping_country']) ? $cart_shipping_data['shipping_country'] : '';
			$country = $new_shipping_country;
			$country_exist = 'true';
			if ('shipping' === $load_address) {
				$allowed_countries = WC()->countries->get_shipping_countries();

				if (! array_key_exists($country, $allowed_countries)) {
					$country_exist = 'false';
					$country = current(array_keys($allowed_countries));
				}
			}
			// $cart_shipping = $_POST['cart_shipping'];
			// $cart_shipping_data = self::thwma_unserializeForm($cart_shipping);
			// $new_shipping_country = isset($cart_shipping_data['shipping_country'])?$cart_shipping_data['shipping_country']:'';
			$address = WC()->countries->get_address_fields(wc_clean(wp_unslash($new_shipping_country)), $load_address . '_');			
			$address_data = array();
			$address_new = array();	
			if(!empty($address) && is_array($address)){		
				foreach ($address as $key => $field) {
					if(!empty($cart_shipping_data) && is_array($cart_shipping_data)){	
						foreach ($cart_shipping_data as $fkey => $value) {
							$address_data[$key] = array(
								'label' 		=> isset($field['label']) ? $field['label'] : '',
								'value' 		=> isset($cart_shipping_data[$key]) ? $cart_shipping_data[$key] : '',
								'required' 		=> isset($field['required']) ? $field['required'] : '',
								'type' 			=> isset($field['type']) ? $field['type'] : '',
								'validate' 		=> isset($field['validate']) ? $field['validate'] : ''
								);	
							//$address_new[$key] = isset($cart_shipping_data[$key]) ? $cart_shipping_data[$key] : '';
							$address_new[$key] = $cart_shipping_data[$key];	
						}
					}
				}
			}

			// Validate the address.
			$true_check = self::validate_cart_shipping_addr_data($cart_shipping_data, $load_address, $country, $address_data, $country_exist);

			$true_check_val = '';
			if($true_check ==  'true') {
				if ($user_id) {
					THWMA_Utils::save_address_to_user($user_id, $address_new, 'shipping');
					$true_check_val = 'true';
				}
			} else {
				$true_check_val = $true_check;	
			}

			// Create new dropdown list of newly added address.
			$adr_key = '';
			$custom_address = THWMA_Utils::get_custom_addresses($user_id,'shipping');
			if(!empty($custom_address) && is_array($custom_address)) {
				foreach ($custom_address as $a_key => $a_value) {
					$adr_key = $a_key;
				}
			}
			$address_dropdown = '';
			$opt_key = $adr_key;
			$value = '';

			$type  = 'shipping';
			$adrsvalues_to_dd = array();
			if(!empty($cart_shipping_data) && is_array($cart_shipping_data)) {
				foreach ($cart_shipping_data as $adrs_key => $adrs_value) {
					if($adrs_key == 'shipping_address_1' || $adrs_key =='shipping_address_2' || $adrs_key =='shipping_city' || $adrs_key =='shipping_state' || $adrs_key =='shipping_postcode') {
						if($adrs_value) {
							$adrsvalues_to_dd[] = $adrs_value;
						}
					}
				}
			}
			$new_adrs_string = implode(', ', $adrsvalues_to_dd);
			// $new_address = THWMA_Utils::get_formated_address('shipping', $cart_shipping_data);
			// $separator = ', ';
			// $new_option = WC()->countries->get_formatted_address($new_address, $separator);
			if($true_check_val == 'true') {
				$address_dropdown .= '<option value="' . esc_attr($opt_key) . '" ' . selected($value, $opt_key, false) . ' >' . esc_attr($new_adrs_string) . '</option>';
			}

			$customer_id = get_current_user_id();

			// Create new tile of shipping address.
			if($true_check_val == 'true') {
				$output_shipping = self::get_tile_field_to_cart($customer_id, 'shipping');
			} else {
				$output_shipping = '';
			}
			$total_address_count = THWMA_Utils::get_total_address_count();
			$output_table = self::multiple_address_management_form();
			$response = array(
				'result_shipping' => $output_shipping,
				'true_check' => $true_check_val,
				'address_dropdown' => $address_dropdown,
				'address_count' => $total_address_count,
				'output_table' => $output_table,
			);
			wp_send_json($response);	
			exit();
		}

		/**
		 * Function for validate multi shipping address form address data(checkout page).
         * Function for validate cart shipping address form address data(cart page).
         * 
         * @param array $cart_shipping The cart shipping details
         * @param array $load_address The loaded addresses
         * @param string $country The country name
         * @param array $address_data The address datas
         * @param string $country_exist  Check info of the country is exist
         *
         * @return string.
         */
		public static function validate_cart_shipping_addr_data($cart_shipping, $load_address, $country, $address_data, $country_exist) {
			$true_check = array();
			$error_check = '';
			if(!empty($address_data) && is_array($address_data)) {
				foreach($address_data as $dkey => $dvalue) {
					$value = $dvalue['value'];
					$required = $dvalue['required'];
					$ftype = $dvalue['type'];
					$validate = $dvalue['validate'];
					if (! isset($ftype)) {
						$type = 'text';
					}
					if (! empty($required) && empty($value)) {
						$error_check .= esc_html__($dvalue['label'].' is a required field.', 'woocommerce');
						$error_check .= '</br>';
						$true_check[] = false;
					} else {
						$true_check[] = true;
					}
					if (! empty($value)) {

						// Validation and formatting rules.
						if (! empty($validate) && is_array($validate)) {
							foreach ($validate as $rule) {
								if ($rule == 'postcode') {
									$country = isset($cart_shipping[$load_address . '_country']) ? $cart_shipping[$load_address . '_country']: '';
									if ($country_exist == 'false') {
										$error_check .= esc_html__('Please enter a valid country', 'woocommerce');
										$error_check .= '</br>';
										$true_check[] = false;
									} else {
										$true_check[] = true;
									}
									//$country = wc_clean(wp_unslash($_POST[ $load_address . '_country' ]));		
									$value   = wc_format_postcode($value, $country);
									if ('' !== $value && ! WC_Validation::is_postcode($value, $country)) {
										switch ($country) {
											case 'IE':									
												$postcode_validation_notice = esc_html__('Please enter a valid Eircode.', 'woocommerce');
												break;
											default:										
												$postcode_validation_notice = esc_html__('Please enter a valid postcode / ZIP.', 'woocommerce');
										}
										//wc_add_notice($postcode_validation_notice, 'error');
										$error_check .= $postcode_validation_notice;
										$true_check[] = false;
									} else {
										$true_check[] = true;
									}
								}
								if ($rule == 'phone') {
									if ('' !== $value && ! WC_Validation::is_phone($value)) {
										$error_check .=  esc_html__($dvalue['label'].' is not a valid phone number.', 'woocommerce');
										$true_check[] = false;
									} else {
										$true_check[] = true;
									}
								}
								if ($rule == 'email') {
									$value = strtolower($value);
									if (! is_email($value)) {
										$error_check .=  esc_html__($dvalue['label'].' is not a valid email address.', 'woocommerce');
										$true_check[] = false;
									} else {
										$true_check[] = true;
									}
								}
							}
						}
					}
				}
			} else {
				$error_check .=  esc_html__('Your address fields are empty', 'woocommerce');
				$true_check[] = false;
			}
			$true_chk = array_unique($true_check);
			if (in_array(false, $true_chk)) {
				return $error_check;
			} else {
				return 'true';
			}
			//return $true_check;
		}

		/**
		 * Function for unserialize address.
		 * 
         * @param string $str The given serialise address data
         *
         * @return array.
		 */
		public static function thwma_unserializeForm($str) {
		    $returndata = array();
		    $strArray = explode("&", $str);
		    $i = 0;
		    if(!empty($strArray) && is_array($strArray)) {
			    foreach ($strArray as $item) {
			        $array = explode("=", $item);
			        $returndata[$array[0]] = urldecode($array[1]);
			    }
			}
		    return $returndata;
		}

		/**
		 * Get current user saved addresses.
		 * 
		 * @param array $ship_addresses The shipping address info
		 *
		 * @return string
		 */
		public static function get_user_addresses($ship_addresses) {
	    	$user_id = get_current_user_id();
	    	$shipping_address = array();
	    	$default_shipping_address  = array();

	    	// Address data from user.
	    	$user_address_data = get_user_meta($user_id, THWMA_Utils::ADDRESS_KEY, true);

			// Address details.
	    	if(!empty($user_address_data) && is_array($user_address_data)) {
		    	foreach($user_address_data as $index => $ship_data) {
		    		$shipping_address = isset($user_address_data['shipping'])?$user_address_data['shipping']:'';
		    		$default_shipping_address = isset($user_address_data['default_shipping'])?$user_address_data['default_shipping'] : '';
		    	}
		    }
	    	if($shipping_address && is_array($shipping_address)) {
		    	foreach($shipping_address as $key => $value) {
		    		if($key == $ship_addresses) {
	    				return $value;
	    			}
		    	}
		    }
		    if($default_shipping_address && is_array($default_shipping_address)) {
		    	foreach($default_shipping_address as $key => $value) {
		    		if($key == $ship_addresses) {
	    				return $value;
	    			}
		    	}
		    }
		}

		/**
		 * Update order item meta.(for latest WC version).
		 * 
		 * @param integer $item_id The item id
		 * @param array $item The item details
		 * @param integer $order_id The sorder id
		 */
	   	public function thwma_add_addrs_to_new_order_item($item_id, $item, $order_id) {
	   		global $woocommerce,$wpdb;
	   		$order = wc_get_order( $order_id );
	   		
	   		$legacy_values = is_object($item) && isset($item->legacy_values) ? $item->legacy_values : false;
	   		if(!empty($legacy_values)) {

	   			// Check the product is vertual or downloadable.
	   			$product = wc_get_product( $legacy_values[ 'product_id' ] );
	   			if ((!$product->is_virtual()) && (!$product->is_downloadable('yes'))) {

					$product_shipping_address = isset($legacy_values['product_shipping_address']) ?  $legacy_values['product_shipping_address'] : '';

					$shipping_addr_info = array();	

					// Check is user logged in.			
			   		if (is_user_logged_in()) {
			   			if (isset($_COOKIE[THWMA_Utils::GUEST_USER_SHIPPING_ADDR])) {
							$shipping_address = $_COOKIE[THWMA_Utils::GUEST_USER_SHIPPING_ADDR];
							$shipping_address = preg_replace('!s:(\d+):"(.*?)";!', "'s:'.strlen('$2').':\"$2\";'", $shipping_address);
							$custom_address = unserialize(base64_decode($shipping_address));
							$shipping_addr_info = $this->thwma_add_order_item_datas($legacy_values,$custom_address);
							$user_id = get_current_user_id();
							update_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, 'yes');
						}
						if(empty($shipping_addr_info)) {
				   			$user_id = get_current_user_id();
							if(!empty($user_id)) {
								$custom_address  = get_user_meta($user_id , 'thwma_custom_address', true);
								$shipping_addr_info = $this->thwma_add_order_item_datas($legacy_values, $custom_address);
							}
						}
			   		} else {
			   			if (isset($_COOKIE[THWMA_Utils::GUEST_USER_SHIPPING_ADDR])) {
							$shipping_address = $_COOKIE[THWMA_Utils::GUEST_USER_SHIPPING_ADDR];
							$shipping_address = preg_replace('!s:(\d+):"(.*?)";!', "'s:'.strlen('$2').':\"$2\";'", $shipping_address);
							$custom_address = unserialize(base64_decode($shipping_address));
							$shipping_addr_info = $this->thwma_add_order_item_datas($legacy_values,$custom_address);
						}
			   		}

			   		// Shipping address name.
			   		if(!empty($shipping_addr_info)) {
			            wc_update_order_item_meta($item_id, THWMA_Utils::ORDER_KEY_SHIPPING_ADDR, $shipping_addr_info);
			        }
			        // Multi-shipping details.
			        $multi_ship_address = isset($legacy_values['multi_ship_address']) ?  $legacy_values['multi_ship_address'] : '';
			        if(!empty($multi_ship_address)) {
			        	wc_update_order_item_meta($item_id, THWMA_Utils::ORDER_KEY_SHIPPING_DATA, $multi_ship_address);
			        }

			        // $multi_ship_custom_fields = isset($legacy_values['thwma_custom_fields']) ?  $legacy_values['thwma_custom_fields'] : '';
			        // if(!empty($multi_ship_custom_fields)) {
			        // 	wc_update_order_item_meta($item_id, THWMA_Utils::ORDER_KEY_SHIPPING_CUS_FIELDS, $multi_ship_custom_fields);
			        // }

			        // Shipping method.
			        
			        $shipping_method_data = isset($legacy_values['thwma_shipping_methods']) ?  $legacy_values['thwma_shipping_methods'] : '';

			        if(!empty($shipping_method_data)) {
		            	wc_update_order_item_meta($item_id, THWMA_Utils::ORDER_KEY_SHIPPING_METHOD, $shipping_method_data);
			        }

		    	} // Check the product is vertual or downloadable.

		   	}
		   	
	   	}


		/**
		 * To save the item name on shipping method object(display item below shipping method on order edit page)
		 * @param $item
		 * @param $package_key
		 * @param $package
		 */
		function order_shipping_item( $item, $package_key, $package, $order ) {
			$note = [];
		    foreach ($package['contents'] as $key => $package_data) {
		    	$qunatity = $package_data['quantity'];
		    	$shipping_methods = '';
				foreach ($package_data as $pk_key => $package_value) {
		    		$shipping_methods = isset($package_data['thwma_shipping_methods']) ? $package_data['thwma_shipping_methods'] : '';
		    		$multi_ship_address = isset($package_data['multi_ship_address']) ? $package_data['multi_ship_address'] : '';
		    	}
		    	if(!empty($shipping_methods) && is_array($shipping_methods)) {
			    	foreach ($shipping_methods as $ship_key => $ship_value) {
			    		$note[] = $ship_value['item_name'].' × '.$qunatity;
			    	}
			    	
			    } else if(!empty($multi_ship_address) && is_array($multi_ship_address)) {
			    	$product_id = isset($multi_ship_address['product_id']) ? $multi_ship_address['product_id'] : '';
					$product = wc_get_product( $product_id );
					$product_name = $product->get_name();
					$note[] = $product_name.' × '.$qunatity;
			    }
		    }
		    if ( $note ) {
		    	if(isset($note[$package_key])) {
		    		$item->add_meta_data( 'Items', $note[$package_key], true );
		    	}
		    }

		}


	   	/**
         * Function for add order item datas
         * 
         * @param array $data The item datas
         * @param array $custom_address The custom addresses
         *
         * @return array
         */ 
		public function thwma_add_order_item_datas($data, $custom_address) {
	   		$unique_key = isset($data['unique_key']) ? $data['unique_key'] : '';
	   		if(is_array($data)) {
				if(!empty($data['product_shipping_address']) && isset($data['product_shipping_address'])) {
					$adrs_key = 'Shipping Address';
					$shipping_addr = isset($data['product_shipping_address']) ? $data['product_shipping_address']: '';
					// $custom_address  = get_user_meta($user_id , 'thwma_custom_address', true);
					$custom_address = $custom_address;
					$shipping_addr_info = array();
					if(!empty($custom_address)) {
						$ship_addrs = array(); 
						$dflt_ship_addrs = array();
						if(is_array($custom_address)){
							foreach ($custom_address as $key => $value) {
								$ship_addrs = isset($custom_address['shipping']) ? $custom_address['shipping']:'';
								$dflt_ship_addrs = isset($custom_address['default_shipping']) ? $custom_address['default_shipping']:'';
							}
						}
						$pdt_shipp_addr = array();
						if(!empty($ship_addrs) && is_array($ship_addrs)) {
							foreach ($ship_addrs as $adr_key => $adr_value) {
								if($adr_key == $shipping_addr) {
									$pdt_shipp_addr = $adr_value;
								}
							}
						}
						// $pdt_shipp_addr = implode(", ", $pdt_shipp_addr);
						//$shipping_addr_info = array();
						if(!empty($pdt_shipp_addr)) {
							$shipping_addr_info[$shipping_addr] = array(
								'product_id' => isset($data['product_id']) ? $data['product_id'] : '',
								'shipping_address' => $pdt_shipp_addr,
								'unique_key' => $unique_key,
							);
							return $shipping_addr_info;
						}
					}				
					return $shipping_addr_info;	
				}
			}
	   	}

		/**
		 * Update order item meta.(for below 3.0.0 WC version).
		 * 
		 * @param integer $item_id The item id
		 * @param array $values The values
		 * @param integer $cart_item_key The cart item key
		 */
		public function thwma_add_addrs_to_order_item_meta($item_id, $values, $cart_item_key) {
			global $woocommerce, $wpdb;
			if (is_user_logged_in()) {
				$current_user_id = get_current_user_id();
				if(!empty($current_user_id)) {
					$shipping_address_name = array();
					if(!empty($values) && is_array($values)) {

						// Check the product is vertual or downloadable.
			   			$product = wc_get_product( $values[ 'product_id' ] );
			   			if ((!$product->is_virtual()) && (!$product->is_downloadable('yes'))) {

						foreach($values as $key => $data) {
							// 	$shipping_address_name[] = isset($data['product_shipping_address']) ? $data['product_shipping_address']: '';
							// 	$shipping_address_name_1 = array_filter($shipping_address_name);
							// }
							if(is_array($data)) {
								if(!empty($data['product_shipping_address']) && isset($data['product_shipping_address'])) {
									$pdt_ship_adr = '';
									$pdt_ship_adr = isset($data['product_shipping_address']) ? $data['product_shipping_address'] : '';
									$adrs_key = 'Shipping Address';
									$shipping_addr = isset($data['product_shipping_address']) ? $data['product_shipping_address']: '';
									$custom_address  = get_user_meta($current_user_id , 'thwma_custom_address', true);	
									if(!empty($custom_address)) {
										$ship_addrs = array(); 
										$dflt_ship_addrs = array();
										if(!empty($custom_address) && is_array($custom_address)) {
											foreach ($custom_address as $key => $value) {
												$ship_addrs = isset($custom_address['shipping']) ? $custom_address['shipping']:'';
												$dflt_ship_addrs = isset($custom_address['default_shipping']) ? $custom_address['default_shipping']:'';
											}
										}
										$pdt_shipp_addr = array();
										if(!empty($ship_addrs) && is_array($ship_addrs)) {
											foreach ($ship_addrs as $adr_key => $adr_value) {
												if($adr_key == $shipping_addr) {
													$pdt_shipp_addr = $adr_value;
												}
											}
										}
										// $pdt_shipp_addr = implode(", ", $pdt_shipp_addr);
										$shipping_addr_info = array();
										if(!empty($pdt_shipp_addr)) {
											$shipping_addr_info[$pdt_ship_adr] = array(
												'Shipping Address' => $pdt_shipp_addr,
											);
										}
									}
									if(!empty($shipping_addr_info)) {
							            wc_update_order_item_meta($item_id, THWMA_Utils::ORDER_KEY_SHIPPING_ADDR, $shipping_addr_info);  
							        }
								}
							}
						}

						} // Check the product is vertual or downloadable.
					}
				}
			}
		}

		/**
		 * Display addresses on thankyou page, my-account, admin order edit page and admin order preview page.
		 * 
		 * @param array $formatted_meta The meta data info from ordered item
		 * @param array $order_item The order item details
         *
         * @return array.
		 */
		public function thwma_shipping_addresses_display_on_thankyou_page($formatted_meta, $order_item) {			
			$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping']:'';
			if($cart_shipping_enabled == 'yes') {
				if(method_exists($order_item, 'get_product_id')) {
					$order_item_id = $order_item->get_id();
					$item_id = $order_item_id;
					$product_id = $order_item->get_product_id();

					$product = wc_get_product( $product_id );
					if (($product->is_virtual()) || ($product->is_downloadable('yes'))) {
						return $formatted_meta;
					}

					$user_id = get_current_user_id();
					$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
					//$enable_multi_ship_data = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
					if (is_user_logged_in()) {
			        	$enable_multi_ship_data = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
			        } else {
			        	$enable_multi_ship_data = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP])?$_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]:'';
			        }					
					if($settings && !empty($settings)) {
						// if(is_admin()){
						// 	$formatted_meta = $this->thwma_shipping_thankyou_page($formatted_meta, $order_item);
						// } else {
							if($enable_multi_ship_data == 'yes') {
								$formatted_meta = $this->thwma_shipping_thankyou_page($formatted_meta, $order_item);
							}
						//}
					}					
					//return $formatted_meta;
				//} else {
					// $shipping_method_meta = $this->thwma_shipping_method_meta($formatted_meta, $order_item);
					// $formatted_meta = $shipping_method_meta;
					
					// return $formatted_meta;
				}
			}
			return $formatted_meta;
		}

		/**
		 * Core function to display thankyou page,my-account,admin order edit page and admin order preview page.
		 * 
		 * @param array $formatted_meta The meta data info from ordered item
		 * @param array $order_item The order item details
         *
         * @return array.
		 */
		public function thwma_shipping_thankyou_page($formatted_meta, $order_item) {
			$meta_data = $order_item->get_meta_data();	
			$meta_ship_adrsses = '';
			$user_id= get_current_user_id();
			$custom_fields = '';
			if(!empty($meta_data) && is_array($meta_data)) { 
				foreach ($meta_data as $id => $meta_array) {
					if('thwma_order_shipping_address' == $meta_array->key) {
						$meta_ship_adrsses = $meta_array->value;
					}
					$custom_fields = $this->get_custom_fields_to_display($custom_fields, $meta_ship_adrsses);
				}
			}

			if(!empty($meta_ship_adrsses) && is_array($meta_ship_adrsses)) {
				foreach($meta_ship_adrsses as $addr_key => $addr_data) {
					$addr_values = isset($addr_data["shipping_address"]) ? $addr_data["shipping_address"] : '';						
					$shipp_addr_format = THWMA_Utils::get_formated_address('shipping', $addr_values);
					if(apply_filters('thwma_inline_address_display', true)) {
						$separator = ', ';
						$pdt_shipp_addr_formated = WC()->countries->get_formatted_address($shipp_addr_format, $separator);
					} else {
						$pdt_shipp_addr_formated = WC()->countries->get_formatted_address($shipp_addr_format);
					}
					$addr_value = $pdt_shipp_addr_formated;
					$adrs_key = 'Shipping Address';

					// Set shipping address meta data.
					$formatted_meta = $this->set_formatted_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields);
				}

				// Custom fields display.
				$formatted_meta = $this->set_custom_fields_display($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields);
				return $formatted_meta;
			} else {
				if(is_user_logged_in()) {
					$default_address = THWMA_Utils::get_default_address($user_id, 'shipping');
					if (THWMA_Utils::check_thwcfe_plugin_is_active()) {

						// Custom fields.
						$custom_fields = $this->get_shipping_custom_fields_from_addresses($default_address);
					}
					$addr_values = $default_address;
					$shipp_addr_format = THWMA_Utils::get_formated_address('shipping', $addr_values);
					if(apply_filters('thwma_inline_address_display', true)) {
						$separator = ', ';
						$pdt_shipp_addr_formated = WC()->countries->get_formatted_address($shipp_addr_format, $separator);
					} else {
						$pdt_shipp_addr_formated = WC()->countries->get_formatted_address($shipp_addr_format);
					}
					$addr_key = 'default';
					$addr_value = $pdt_shipp_addr_formated;
					$adrs_key = 'Shipping Address';

					// Set default shipping address meta data.
					$formatted_meta = $this->set_formatted_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields);

				    // Custom fields for deafult address.
				    $formatted_meta = $this->set_custom_fields_display($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields);
				}
				return $formatted_meta;
			}
		}

		/**
		 * Function for get custom field to display.
		 * 
		 * @param array $custom_fields The custom fields
		 * @param array $meta_ship_adrsses The shipping address data.
         *
         * @return array.
		 */
		public function get_custom_fields_to_display($custom_fields, $meta_ship_adrsses) {
			if (THWMA_Utils::check_thwcfe_plugin_is_active()) {
				if(!empty($meta_ship_adrsses) && is_array($meta_ship_adrsses)) {
					foreach($meta_ship_adrsses as $addr_key => $addr_data) {
						$user_id = get_current_user_id();
						$shipping_adrs = THWMA_Utils::get_custom_addresses($user_id, 'shipping', $addr_key);
						$custom_fields = $this->get_shipping_custom_fields_from_addresses($shipping_adrs);
					}
				}
			}
			return $custom_fields;
		}

		/**
		 * Function for add custom field to meta data.
		 * 
		 * @param array $formatted_meta The existing formated meta data.
		 * @param array $addr_value The address values.
		 * @param array $adrs_key The address label.
		 * @param array $addr_key The address name.
		 * @param array $order_item The order details
		 * @param array $custom_fields The custom fields
         *
         * @return array.
		 */
		public function set_custom_fields_display($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields) {
			if(!empty($custom_fields) && is_array($custom_fields)) {
				$custom_field_data = array();
				unset( $custom_fields['heading'] );
				foreach($custom_fields as $custom_key => $custom_val) { 
					if(!empty($custom_fields[$custom_key])) {
						if(!is_array($custom_val)) {
							$custom_field_data[] = $custom_key.' : '.$custom_val;
						}
					}
				}
				if(apply_filters('thwma_inline_address_display', true)) {
					$custom_field_dt = implode(",  ", $custom_field_data);
				} else {
					$custom_field_dt = implode("<br/>  ", $custom_field_data);
				}
				$addr_key = 'custom_fields';	
				$addr_value = $custom_field_dt;
				$adrs_key = 'Custom fields';
				$formatted_meta = $this->set_formatted_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields);
			}
			return $formatted_meta;
		}

		/**
		 * Function for add address fields to meta data.
		 * 
		 * @param array $formatted_meta The existing formated meta data.
		 * @param array $addr_value The address values.
		 * @param array $adrs_key The address label.
		 * @param array $addr_key The address name.
		 * @param array $order_item The order details
		 * @param array $custom_fields The custom fields
         *
         * @return array.
		 */
		public function set_formatted_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields)  {
			if($adrs_key == 'Shipping Address') {
				if(!empty($formatted_meta) && is_array($formatted_meta)) {
					$formatted_meta = $this->update_formated_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields);
				} else {
					$formatted_meta = $this->prepare_formatted_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields);
				}
			} else if($adrs_key == 'Custom fields') {
				if(!empty($formatted_meta) && is_array($formatted_meta)) {
					$formatted_meta = $this->update_formated_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields);
				} else {
					$formatted_meta = $this->prepare_formatted_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields);
				}
			}
			return $formatted_meta;	
		}

		/**
		 * Function for update the existing meta data.
		 * 
		 * @param array $formatted_meta The existing formated meta data.
		 * @param array $addr_value The address values.
		 * @param array $adrs_key The address label.
		 * @param array $addr_key The address name.
		 * @param array $order_item The order details
		 * @param array $custom_fields The custom fields
         *
         * @return array.
		 */
		public function update_formated_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields) {
			if(!empty($formatted_meta) && is_array($formatted_meta)) {
				$data = array();
				foreach ($formatted_meta as $values) {
					$data[$values->key] = $values;
				}

				foreach ($formatted_meta as $key => $value) {
					if(!array_key_exists($adrs_key, $data)) {
						if($formatted_meta[$key]->key == $adrs_key) {
							$formatted_meta_data = $this->prepare_formatted_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields);
							$formatted_meta[$key] = $formatted_meta_data[$addr_key];
						} else {
							$formatted_meta = $this->prepare_formatted_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields);
						}
					}
				}
			}
			return $formatted_meta;
		}

		/**
		 * Function for prepare formated meta data.
		 * 
		 * @param array $formatted_meta The existing formated meta data.
		 * @param array $addr_value The address values.
		 * @param array $adrs_key The address label.
		 * @param array $addr_key The address name.
		 * @param array $order_item The order details
		 * @param array $custom_fields The custom fields
         *
         * @return array.
		 */
		public function prepare_formatted_meta_data($formatted_meta, $addr_value, $adrs_key, $addr_key, $order_item, $custom_fields) {
			$zeros_eliminated_value = ltrim($addr_value, '0');
			$addrs_value = $zeros_eliminated_value;
			$product = is_callable(array($this, 'get_product')) ? $this->get_product() : false;
			$display_key   = '<span class="thwma-thku-ship-adr-name">'.wc_attribute_label($adrs_key, $product).'<span>';
			$display_value = wp_kses_post($addrs_value);
			$display_value = '<span class="thwma-thku-ship-adr">'.$display_value.'<span>';

			if($adrs_key == 'Shipping Address') {
				if($addr_key) {
				    $formatted_meta[$addr_key] = (object) array(
						'key'           => $adrs_key,
						'value'         => $addr_value,
						'display_key'   => apply_filters('woocommerce_order_item_display_meta_key', $display_key, $addrs_value, $order_item),
						'display_value' => wpautop(make_clickable(apply_filters('woocommerce_order_item_display_meta_value', $display_value, $addrs_value, $order_item))),
					);
					
				}

			} else if($adrs_key == 'Custom fields') {
				if(count(array_filter($custom_fields)) != 0){
					if($addr_key) {
					    $formatted_meta[$addr_key] = (object) array(
							'key'           => $adrs_key,
							'value'         => $addr_value,
							'display_key'   => apply_filters('woocommerce_order_item_display_meta_key', $display_key, $addrs_value, $order_item),
							'display_value' => wpautop(make_clickable(apply_filters('woocommerce_order_item_display_meta_value', $display_value, $addrs_value, $order_item))),
						);
					}
				}
			}

			return $formatted_meta;
		}

		/**
         * Function for override shipping address display section on thankyou pageand order edit page(back-end)
         * 
         * @param array $raw_address The exsting shipping address
         * @param array $order_item The order item details
         *
         * @return array
         */ 
		public function thwma_overrides_shipping_address_section_on_thankyou_page($address, $raw_address, $order_item) {
			$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping'] : '';
			$user_id = get_current_user_id();
			if($cart_shipping_enabled == 'yes') {
				$enable_multi_ship_data = '';
		        if (is_user_logged_in()) {
		        	$enable_multi_ship_data = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
		        } else {
		        	$enable_multi_ship_data = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]) ? $_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP] : '';
		        }
		        if($enable_multi_ship_data == 'yes') {
		        	return esc_html__('Multiple shipping is enabled.', 'woocommerce-multiple-addresses-pro');
				} else {
					return $address;
				}
			} else {
				return $address;
			}
		}

		/**
         * Hide the shipping address updating feature on checkout page because cart page is already set shipping addresses
         * 
         * @param string $true The true or false string
         *
         * @return string
         */ 
		/*public function thwma_cart_have_multiple_shipping_address($true) {
			$arg = $true;
			global $woocommerce;
			$cart = $woocommerce->cart->cart_contents;
			$user_id = get_current_user_id();
			$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping']:'';

			if($cart_shipping_enabled == 'yes') {
		        $enable_multi_ship_data = '';
		        if (is_user_logged_in()) {
		        	$enable_multi_ship_data = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
		        } else {
		        	$enable_multi_ship_data = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP])?$_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]:'';
		        }
		        if($enable_multi_ship_data == 'yes') {
					foreach ($cart as $key => $item) {
						$shipping_address = isset($woocommerce->cart->cart_contents[$key]['product_shipping_address'])?$woocommerce->cart->cart_contents[$key]['product_shipping_address']:'';
						if(!empty($shipping_address)) {
							$true = 'false';
						} else {
							$true = 'false';
						}
					}
				} else {
					$true = $arg;
				}
			} else {
				$true = $arg;
			}
			return $true;
		}*/

		/**
		 * Get the persistent cart from the database.
		 *
		 * @since  3.5.0
		 * @return array
		 */
		private function get_saved_cart() {
			$saved_cart = array();

			if ( apply_filters( 'woocommerce_persistent_cart_enabled', true ) ) {
				$saved_cart_meta = get_user_meta( get_current_user_id(), '_woocommerce_persistent_cart_' . get_current_blog_id(), true );

				if ( isset( $saved_cart_meta['cart'] ) ) {
					$saved_cart = array_filter( (array) $saved_cart_meta['cart'] );
				}
			}

			return $saved_cart;
		}

		/**
         * Function for get cart contents(Set $package-shipping method)
         *
         * @return void
         */ 
		public function get_cart_contents() {
			$cart                = WC()->session->get('cart', null);
			$merge_saved_cart    = (bool) get_user_meta(get_current_user_id(), '_woocommerce_load_saved_cart_after_login', true);
			if (is_null($cart) || $merge_saved_cart) {
				$saved_cart          = $this->get_saved_cart();
				$cart                = is_null($cart) ? array() : $cart;
				$cart                = array_merge($saved_cart, $cart);
				$update_cart_session = true;
				delete_user_meta(get_current_user_id(), '_woocommerce_load_saved_cart_after_login');
			}
			$cart_contents = array();

			if(!empty($cart) && is_array($cart)){
				foreach ($cart as $key => $values) {
					$product = wc_get_product($values['variation_id'] ? $values['variation_id'] : $values['product_id']);
					$session_data = array_merge(
						$values,
						array(
							'data' => $product,
						)
					);
					$cart_contents[ $key ] = apply_filters('woocommerce_get_cart_item_from_session', $session_data, $values, $key);
				}
			}
			$this->cart_contents = (array) $cart_contents;
			return apply_filters('woocommerce_get_cart_contents', (array) $this->cart_contents);
		}
		/**
         * Function for get cart(Set $package-shipping method)
         *
         * @return void
         */ 
		public function get_cart() {
			if (! did_action('wp_loaded')) {
				wc_doing_it_wrong(__FUNCTION__, esc_html__('Get cart should not be called before the wp_loaded action.', 'woocommerce'), '2.3');
			}
			if (! did_action('woocommerce_load_cart_from_session')) {
				$this->session->get_cart_from_session();
			}
			return array_filter($this->get_cart_contents());
		}

		/**
         * Function for filter items needing shipping(Set $package-shipping method).
         *
         * @return void
         */ 
		protected function filter_items_needing_shipping($item) {
			$product = $item['data'];
			return $product && $product->needs_shipping();
		}

		/**
         * Function for get items needing shipping(Set $package-shipping method).
         *
         * @return void
         */ 
		protected function get_items_needing_shipping() {
			return array_filter($this->get_cart(), array($this, 'filter_items_needing_shipping'));
		}


		/**
         * Function for create item package array(Set $package-shipping method).
         * 
         * @param array $value The address values
         * @param array $cart_item The cart item details
         *
         * @return array
         */ 
		public function create_item_package_array($value, $cart_item) {
			$package = array();
			if($value) {
				$package = array(
			        'contents'        	=> $this->get_items_needing_shipping(),
			        'contents_cost'   	=> array_key_exists( 'line_total', $cart_item ) ? $cart_item['line_total'] : '',
			        'applied_coupons' 	=> WC()->cart->applied_coupons,
			        'destination'     	=> array(
			            'country'   	=> $value['country'],
			            'state'     	=> $value['shipping_state'],
			            'postcode'  	=> $value['shipping_postcode'],
			            'city'  		=> $value['shipping_city'],
			            'address'  		=> $value['shipping_address_1'],
			            'address1'  	=> $value['shipping_address_1'],
			            'address_2'  	=> $value['shipping_address_2']
			       )
				);
			}				
			return $package;		
		}


		/**
         * Function for sget shipping package(Set $package-shipping method).
         * 
         * @param array $packages The package details
         *
         * @return array
         */ 
		public function thwma_get_shipping_packages($packages) {
			$individual_details = array();
			$shipping_country = '';
			if(!empty(WC()->cart->get_cart()) && is_array(WC()->cart->get_cart())) {
				foreach (WC()->cart->get_cart() as $key => $cart_item) {
					$_product =  wc_get_product($cart_item['data']->get_id());

					// Check the product is vertual or downloadable.
					if ((!$_product->is_virtual()) && (!$_product->is_downloadable('yes'))) {

					$product_price = $_product->get_price();
					$product_name = $_product->get_title();
					$user_id= get_current_user_id();
					$values = array();

					if(isset($cart_item['product_shipping_address']) && !empty($cart_item['product_shipping_address'])) {
						$ship_addresses = isset($cart_item['product_shipping_address']) ? $cart_item['product_shipping_address'] : '';
						$settings_guest_usr = THWMA_Utils::get_setting_value('settings_guest_users');
						$enabled_guest_user = isset($settings_guest_usr['enable_guest_shipping']) ? $settings_guest_usr['enable_guest_shipping'] : '';
						$adr_data = '';
						if(is_user_logged_in()) {
							$adr_data = THWMA_Public::get_user_addresses($ship_addresses);	
						} else {
							if($enabled_guest_user == 'yes') {
								$adr_data = THWMA_Public::get_guest_user_addresses($ship_addresses);
							}
						}	
						
						if($adr_data && is_array($adr_data)) {
							$shipping_country = isset($adr_data['shipping_country']) ? esc_attr($adr_data['shipping_country']) : '';
							$shipping_state = isset($adr_data['shipping_state']) ? esc_attr($adr_data['shipping_state']) : '';
							$shipping_postcode = isset($adr_data['shipping_postcode']) ? esc_attr($adr_data['shipping_postcode']):'';
							$shipping_city = isset($adr_data['shipping_city'])?esc_attr($adr_data['shipping_city']) : '';
							$shipping_address_1 = isset($adr_data['shipping_address_1']) ? esc_attr($adr_data['shipping_address_1']) : '';
							$shipping_address_2 = isset($adr_data['shipping_address_2']) ? esc_attr($adr_data['shipping_address_2']) : '';							
						    $active_methods   = array();
						    $values = array (
						    	'country' => $shipping_country,
				                'amount'  => $product_price,
				                'shipping_state' => $shipping_state,
				                'shipping_postcode' => $shipping_postcode,
				                'shipping_city' => $shipping_city,
				                'shipping_address_1' => $shipping_address_1,
				                'shipping_address_2' => $shipping_address_2
				           );

						}
						
					} else {
						$default_address = THWMA_Utils::get_default_address($user_id, 'shipping');
						if($default_address && is_array($default_address)) {
							$shipping_country = isset($default_address['shipping_country']) ? esc_attr($default_address['shipping_country']) : '';
							$shipping_state = isset($default_address['shipping_state'])?esc_attr($default_address['shipping_state']) : '';
							$shipping_postcode = isset($default_address['shipping_postcode']) ? esc_attr($default_address['shipping_postcode']) : '';
							$shipping_city = isset($default_address['shipping_city']) ? esc_attr($default_address['shipping_city']) : '';
							$shipping_address_1 = isset($default_address['shipping_address_1']) ? esc_attr($default_address['shipping_address_1']) : '';
							$shipping_address_2 = isset($default_address['shipping_address_2']) ? esc_attr($default_address['shipping_address_2']) : '';							
						    $active_methods   = array();
						    $values = array (
						    	'country' => $shipping_country,
				                'amount'  => $product_price,
				                'shipping_state' => $shipping_state,
				                'shipping_postcode' => $shipping_postcode,
				                'shipping_city' => $shipping_city,
				                'shipping_address_1' => $shipping_address_1,
				                'shipping_address_2' => $shipping_address_2
				           );

						}
					}
					$individual_details[] = $this->create_item_package_array($values, $cart_item);

					} // Check the product is vertual or downloadable.

				}
			}
			if($shipping_country) {
				$packages = $individual_details;
			} else {
				$packages = $packages;
			}
	 		return $packages;
		}

		/**
         * Function for set shipping package(Set $package-shipping method)-(based on checkout page).
         * 
         * @param array $packages The package details
         *
         * @return array
         */ 
		/*public function thwma_get_shipping_packages($packages) {
			$individual_details = array();
			$shipping_country = '';
			if(!empty(WC()->cart->get_cart()) && is_array(WC()->cart->get_cart())) {
				foreach (WC()->cart->get_cart() as $key => $cart_item) {
					$_product =  wc_get_product($cart_item['data']->get_id());
					$product_price = $_product->get_price();
					$product_name = $_product->get_title();
					$user_id= get_current_user_id();
					$values = array();

					if(isset($cart_item['multi_ship_address']) && !empty($cart_item['multi_ship_address'])) {
						$ship_addresses_data = isset($cart_item['multi_ship_address']) ? $cart_item['multi_ship_address'] : '';
						if(!empty($ship_addresses_data) && is_array($ship_addresses_data)) {
							foreach ($ship_addresses_data as $sh_adr_key => $sh_adr_data) {
								$ship_addresses = isset($sh_adr_data['ship_address']) ? $sh_adr_data['ship_address'] : '';
								
								$settings_guest_usr = THWMA_Utils::get_setting_value('settings_guest_users');
								$enabled_guest_user = isset($settings_guest_usr['enable_guest_shipping']) ? $settings_guest_usr['enable_guest_shipping'] : '';
								
								if(is_user_logged_in()) {
									$adr_data = THWMA_Public::get_user_addresses($ship_addresses);	
								} else {
									if($enabled_guest_user == 'yes') {
										$adr_data = THWMA_Public::get_guest_user_addresses($ship_addresses);
									}
								}	

								if($adr_data && is_array($adr_data)) {
									$shipping_country = isset($adr_data['shipping_country']) ? esc_attr($adr_data['shipping_country']) : '';
									$shipping_state = isset($adr_data['shipping_state']) ? esc_attr($adr_data['shipping_state']) : '';
									$shipping_postcode = isset($adr_data['shipping_postcode']) ? esc_attr($adr_data['shipping_postcode']):'';
									$shipping_city = isset($adr_data['shipping_city'])?esc_attr($adr_data['shipping_city']) : '';
									$shipping_address_1 = isset($adr_data['shipping_address_1']) ? esc_attr($adr_data['shipping_address_1']) : '';
									$shipping_address_2 = isset($adr_data['shipping_address_2']) ? esc_attr($adr_data['shipping_address_2']) : '';							
								    $active_methods   = array();
								    $values = array (
								    	'country' => $shipping_country,
						                'amount'  => $product_price,
						                'shipping_state' => $shipping_state,
						                'shipping_postcode' => $shipping_postcode,
						                'shipping_city' => $shipping_city,
						                'shipping_address_1' => $shipping_address_1,
						                'shipping_address_2' => $shipping_address_2
						           );

								}
							}
						}
					} else {
						$default_address = THWMA_Utils::get_default_address($user_id, 'shipping');
						if($default_address && is_array($default_address)) {
							$shipping_country = isset($default_address['shipping_country']) ? esc_attr($default_address['shipping_country']) : '';
							$shipping_state = isset($default_address['shipping_state'])?esc_attr($default_address['shipping_state']) : '';
							$shipping_postcode = isset($default_address['shipping_postcode']) ? esc_attr($default_address['shipping_postcode']) : '';
							$shipping_city = isset($default_address['shipping_city']) ? esc_attr($default_address['shipping_city']) : '';
							$shipping_address_1 = isset($default_address['shipping_address_1']) ? esc_attr($default_address['shipping_address_1']) : '';
							$shipping_address_2 = isset($default_address['shipping_address_2']) ? esc_attr($default_address['shipping_address_2']) : '';							
						    $active_methods   = array();
						    $values = array (
						    	'country' => $shipping_country,
				                'amount'  => $product_price,
				                'shipping_state' => $shipping_state,
				                'shipping_postcode' => $shipping_postcode,
				                'shipping_city' => $shipping_city,
				                'shipping_address_1' => $shipping_address_1,
				                'shipping_address_2' => $shipping_address_2
				           );

						}
					}
					$individual_details[] = $this->create_item_package_array($values, $cart_item);
				}
			}
			if($shipping_country) {
				$packages = $individual_details;
			} else {
				$packages = $packages;
			}
	 		return $packages;
		}*/

		/**
         * Function for set defalut shipping package(Set $package-shipping method).
         * 
         * @param array $packages The package details
         *
         * @return string
         */ 
		public function thwma_get_shipping_packages_default($packages) {
			$packages = $this->thwma_get_shipping_change_to_packages_default($packages);
			return $packages;
		}
		/**
         * Core function for set defalut shipping package(Set $package-shipping method)
         * 
         * @param array $packages The package details
         *
         * @return array
         */ 
		public function thwma_get_shipping_change_to_packages_default($packages) {
			$packages = array(array(
				'contents'        => $this->get_items_needing_shipping(),
				'contents_cost'   => array_key_exists( 'line_total', array($this, 'filter_items_needing_shipping')) ?array_sum(wp_list_pluck($this->get_items_needing_shipping(), 'line_total')) : '',
				'applied_coupons' => WC()->cart->applied_coupons,
				'user'            => array(
					'ID' => get_current_user_id(),
				),
				'destination'     => array(
					'country'   => WC()->customer->get_shipping_country(),
					'state'     => WC()->customer->get_shipping_state(),
					'postcode'  => WC()->customer->get_shipping_postcode(),
					'city'      => WC()->customer->get_shipping_city(),
					'address'   => WC()->customer->get_shipping_address(),
					'address_1' => WC()->customer->get_shipping_address(), // Provide both address and address_1 for backwards compatibility.
					'address_2' => WC()->customer->get_shipping_address_2(),
				)
			));
			return $packages;
		}

		/**
         * Function for set woocommerce package rate.
         * 
         * @param array $rates The rate details
         * @param array $package The package details
         *
         * @return array
         */ 
		public function thwma_woocommerce_package_rates($rates, $package) {
			$individual_item = isset($package['individual_item']) ?  $package['individual_item'] : array();
			$new_rate = array();
			if(!empty($individual_item) && is_array($individual_item)) {
				foreach ($individual_item as $key => $item_pack) {
					if(!empty(WC()->shipping->load_shipping_methods($item_pack)) && is_array(WC()->shipping->load_shipping_methods($item_pack))) {
						foreach (WC()->shipping->load_shipping_methods($item_pack) as $shipping_method) {
							if (! $shipping_method->supports('shipping-zones') || $shipping_method->get_instance_id()) {
								$rates = $rates + $shipping_method->get_rates_for_package($package); // + instead of array_merge maintains numeric keys
							}
						}
					}
				}
			}
			$new_rate = $rates;
			return $rates;
		}

		/**
         * Function for set woocommerce package rate.
         * 
         * @param array $cart_item_data The cart item datas
         * @param integer $product_id The product id
         *
         * @return array
         */ 
		function thwma_namespace_force_individual_cart_items($cart_item_data, $product_id) {
			$unique_cart_item_key = md5(microtime() . rand());
			$created_time = date("h:i:sa");
			$cart_item_data['unique_key'] = $unique_cart_item_key;
			$cart_item_data['time'] = $created_time;
			return $cart_item_data;
		}

		/**
		 * Show all hooks.
		 * 
		 * @param string $tag The tag
		 */
		public function th_show_all_hooks($tag) { 
			if(!(is_admin())) { // Display Hooks in front end pages only 
				$debug_tags = array(); 
				global $debug_tags;
				if (in_array($tag, $debug_tags)) {
					return; 
				} 
				echo "<pre>" . $tag . "</pre>"; $debug_tags[] = $tag; 
			} 
		}

		/**
         * Function for woocommerce ajaxadd to cart.(ajax response)
         */
		/*function woocommerce_ajax_add_to_cart() {
			$cartitem = isset($_POST['cart_item']) ? $_POST['cart_item'] : '';
	        $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
	        $product           = wc_get_product($product_id);
	        $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
	        $variation_id = isset($_POST['variation_id']) ? absint($_POST['variation_id']) : '';
	        $variation         = array();
	        $variation    = isset($cartitem['variation']) ? $cartitem['variation'] : '';			
	        add_filter('woocommerce_add_cart_item_data', array($this,'thwma_namespace_force_individual_cart_items'), 10, 3);
	        $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $quantity);
	        $product_status = get_post_status($product_id);
	        if ($passed_validation && WC()->cart->add_to_cart($product_id, $quantity, $variation_id, $variation) && 'publish' === $product_status) {
				do_action('woocommerce_ajax_added_to_cart', $product_id);
	            if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
	                wc_add_to_cart_message(array($product_id => $quantity), true);
	            }
	            WC_AJAX :: get_refreshed_fragments();
	        } else {
	            $data = array(
	                'error' => true,
	                'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));
	            echo wp_send_json($data);
	        }
	        wp_die();
	    }*/

	    


		/*================= Multi-shipping on cart page - (Guest User) ==================*/
		
		/**
         * Function for get address by id(ajax response-checkout page)
         *
         * @return void
         */ 
		// public function get_guest_addresses_by_id() {
		// 	$address_key = isset($_POST['selected_address_id']) ? sanitize_key($_POST['selected_address_id']) : '';
		// 	$type = isset($_POST['selected_type']) ? sanitize_key($_POST['selected_type']) : '';
		// 	$section_name = isset($_POST['section_name']) ? sanitize_key($_POST['section_name']) : '';			
		// 	$customer_id = get_current_user_id();
		// 	if(!empty($section_name) && $address_key == 'section_address') {
		// 		$custom_address = $this->get_default_section_address($customer_id, $section_name);
		// 	} else {
		// 		if($address_key == 'selected_address') {
		// 			$custom_address = THWMA_Utils::get_default_address($customer_id, $type);
		// 		} else {
		// 			$custom_address = THWMA_Utils::get_custom_addresses($customer_id, $type, $address_key);
		// 		}				
		// 	}
		// 	wp_send_json($custom_address);			
		// }

		/**
         * The tile field creation function of guest users.(Multi-shipping on cart page - (Guest User))
         * 
         * @param string $type The address type
         *
         * @return array.
         */
		function get_not_loggedin_tile_field($type) {
			$oldcols = 1;
			$cols    = 1;
			$all_addresses = array();
			$disable_adr_mngmnt = self::disable_address_mnagement();
			if($disable_adr_mngmnt != true){
                $disable_adr_mngmnt = apply_filters('disable_address_management_on_checkout', false);
            }
			if($disable_adr_mngmnt == true) {
				$disable_mngt_class = 'thwma_disable_adr_mngt';
				$disable_acnt_sec = 'disable_acnt_sec';
			} else {
				$disable_mngt_class = '';
				$disable_acnt_sec = '';
			}
			$custom_address = THWMA_Utils::get_custom_addresses_of_guest_user($type);		
			$address_count = is_array($custom_address) ? count($custom_address) : 0 ;

			// Address limit.
			$address_limit = '';
            if($type) {
                $address_limit = THWMA_Utils::get_setting_value('settings_'.$type , $type.'_address_limit');
            }
            if (!is_numeric($address_limit)) {
                $address_limit = 0;
            }
			$return_html = '';
			$add_class='';
			$address_type = "'$type'";		
			$add_list_class  = ($type == 'billing') ? " thwma-thslider-list-ms bill " : " thwma-thslider-list-ms ship"; 
	    	$add_address_btn = '<div class="add-address thwma-add-adr '.esc_attr($disable_acnt_sec).'">
	            <button class="btn-add-address primary button '.esc_attr($disable_mngt_class).'" onclick="thwma_guest_users_add_new_shipping_address(event, this, '.esc_attr($address_type).')">
	                <i class="fa fa-plus"></i> '.esc_html__(' Add new address', 'woocommerce-multiple-addresses-pro').'
	            </button>
	        </div>';	        	
	        if(is_array($custom_address)) {
	        	$all_addresses = $custom_address;
	        }	
	        if($address_limit) {		
			$return_html .= '<div class="thwma-cart-modal-content">';
				$return_html .= '<div class="thwma-cart-modal-title-bar" >';
					$return_html .= '<span class="thwma-cart-modal-title" >Shipping Addresses</span>';
					//$return_html .= '<span class="thwma-cart-modal-close" onclick="thwma_close_cart_adr_list_modal(this)">&times;</span>';
					$return_html .= '<span class="thwma-cart-modal-close" onclick="thwma_close_cart_adr_list_modal(this)"><span class="dashicons dashicons-no"></span></span>';
				$return_html .= '</div>';
				$return_html .= '<div class="thwma-thslider thwma-thslider-guest-user">';
					if($all_addresses && is_array($all_addresses)) {
			           	$return_html .= '<div class="thwma-thslider-box">';
				           	$return_html .= '<div class="thwma-thslider-viewport '.esc_attr($type).'">';
					           	$return_html .= '<ul class=" '.esc_attr($add_list_class).'">';
									$i = 0;
									$total_address_count = count($all_addresses);

									foreach ($all_addresses as $address_key => $value) {
										$new_address = array_key_exists($address_key, $all_addresses) ? $all_addresses[$address_key] : array();
										$new_address_format = THWMA_Utils::get_formated_address($type,$new_address);
										$options_arr = WC()->countries->get_formatted_address($new_address_format);
										$address_key_param = "'".$address_key."'";
										$heading = !empty($new_address[$type.'_heading']) ? $new_address[$type.'_heading'] : esc_html__('', 'woocommerce-multiple-addresses-pro') ;
										$action_row_html = '';
										$address_type_css = '';
										$action_row_html .= '<div class="thwma-adr-footer address-footer"> 
											<div class="btn-delete '.esc_attr($disable_mngt_class).'" data-index="0" data-address-id="" onclick="thwma_guest_users_delete_selected_address_cart_page(this,'.esc_attr($address_type).','.esc_attr($address_key_param).')" title="'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'"> 
												<span>'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'</span> 
											</div>
										</div>';
										if(isset($heading) && $heading != '') {
											$heading_css = '<div class="address-type-wrapper row"> 
												<div title="'.$heading.'" class="address-type '.esc_attr($address_type_css).'">'.esc_html($heading).'</div>  
												</div>       
												<div class="tile-adrr-text thwma-adr-text address-text address-wrapper">'.$options_arr.'</div>'; 
										} else {
											$heading_css = '<div class="tile-adrr-text thwma-adr-text address-text address-wrapper wrapper-only">'.$options_arr.'</div>'; 
										}			
										$add_class  = "thwma-thslider-item-ms $type " ;
										$add_class .= $i == 0 ? ' first' : '';
										$return_html .= '<li class="'.$add_class.'" value="'. esc_attr($address_key).'" >
											<div class="thwma-adr-box address-box" data-index="'.esc_attr($i).'" data-address-id=""> 
												<div class="thwma-main-content"> 
													<div class="complete-aaddress">  
														'.$heading_css.'
													</div> 
													<div class="btn-continue address-wrapper"> 
														<a class="th-btn button primary is-outline '.esc_attr($address_key).'" onclick="thwma_populate_selected_address(event, this, '.esc_attr($address_type).', '.esc_attr($address_key_param).')"> 
															<span>'.esc_html__('Choose This Address', 'woocommerce-multiple-addresses-pro').'</span> 
														</a> 
													</div> 
												</div>'.$action_row_html.'</div></li>';

										// Guest shipping tile.
										if($i >= $address_limit-1) {
                                            break;
                                        }
										$i++;
									}
								$return_html .= '</ul>';
							$return_html .= '</div>';
						$return_html .= '</div>';
						$return_html .= '<div class="control-buttons control-buttons-'.esc_attr($type).'">';
						if($address_count && $address_count > 3) {
							if($address_limit>3) {
				            	$return_html .= '<div class="prev thwma-thslider-prev multi-'.esc_attr($type).'"><i class="fa fa-angle-left fa-3x"></i></div>';
				            	$return_html .= '<div class="next thwma-thslider-next multi-'.esc_attr($type).'"><i class="fa fa-angle-right fa-3x"></i></div>';
				            }
			            }
						$return_html .= '</div>'; 
			           		if(((int)($address_limit)) > $address_count) {
			           		$return_html .= $add_address_btn;
			           	}
			        } else {
			        	$return_html .= '<div class="th-no-address-msg"  >	<span>'.esc_html__('No saved addresses found', 'woocommerce-multiple-addresses-pro').'</span>  </div>';
			        	$return_html .= $add_address_btn;
			        }         
		        $return_html .= '</div>';
	        $return_html .= '</div>';	
	        }             
			return $return_html;
		}

		/**
		 * Add new shipping addresses on cart page - (Guest user-ajax response).
		 */
		public function guest_users_add_new_shipping_address() {
			check_ajax_referer( 'guest-users-add-new-shipping-address', 'security' );
			if(isset($_POST['thwma_save_guest_address'])) {
				echo $this->thwma_save_guest_address();
			}
			$load_address = sanitize_key('shipping');
			//$country      = get_user_meta(get_current_user_id(), $load_address . '_country', true);
			if ('shipping' === $load_address) {
				$allowed_countries = WC()->countries->get_shipping_countries();
				$country = current(array_keys($allowed_countries));
			}
			$address = WC()->countries->get_address_fields($country, $load_address . '_');
			if(!empty($address) && is_array($address)) {
				foreach ($address as $key => $field) {
					$value = get_user_meta(get_current_user_id(), $key, true);
					if (! $value) {
						switch ($key) {
							case 'billing_email':
							case 'shipping_email':
								$value = $current_user->user_email;
								break;
						}
					}
					$address[ $key ]['value'] = '';
				} 
			} ?>
			<div class="thwma-cart-modal-content2">
				<div class="thwma-cart-modal-title-bar2" >
					<!-- <span class="thwma-cart-modal-title" >Shipping Address</span> -->
					<span class="thwma-cart-modal-close2" onclick="thwma_close_cart_add_adr_modal(event);">&times;</span>
				</div>
				<div class="thwma_hidden_error_mssgs"></div>
				<div id="cart_shipping_form_wrap">
				<form method="post" id="cart_shipping_form" name="thwma_guest_cart_ship_form_action">
					<!-- <input type="hidden" name="guest_cart_ship_form_action" id="guest_cart_ship_form_action" value="<?php wp_create_nonce('quest_thwma_cart_ship_form_action') ?>"> -->
					<?php echo '<input type="hidden" name="guest_cart_ship_form_action" id="guest_cart_ship_form_action" value="' . wp_create_nonce( 'thwma_guest_cart_ship_form_action' ) . '" />'; ?>
					<div>
						<?php if(!empty($address) && is_array($address)){
							foreach ($address as $key => $field) {
								//woocommerce_form_field($key, $field, wc_get_post_data_by_key($key, $field['value']));
								THWMA_Utils::thwma_cart_shipping_woocommerce_form_field($key, $field, wc_get_post_data_by_key($key, $field['value']));
							}
						}
						do_action("woocommerce_after_edit_address_form_{$load_address}"); ?>
					</div>
					<p>
						<button id="thwma_save_guest_address_cart" type="submit" class="button" name="thwma_save_guest_address" onclick="thwma_cart_save_guest_address(event);" value="<?php esc_attr_e('Save address', 'woocommerce'); ?>"><?php esc_html_e('Save address', 'woocommerce'); ?></button>
					</p>
				</form>
				</div>
			</div>
			<?php exit();
		}

		/**
		 * Save new shipping address from cart page(Ajax function-guest user).
		 */
		public static function thwma_save_guest_address() {
			check_ajax_referer( 'thwma-save-guest-address', 'security' );
			$error_messgs = '';
			$index = 0;
			$load_address = sanitize_key('shipping');
			$cart_shipping = isset($_POST['cart_shipping']) ? $_POST['cart_shipping'] : '';
			
			$cart_shipping_data = self::thwma_unserializeForm($cart_shipping);
			
			$new_shipping_country = isset($cart_shipping_data['shipping_country']) ? $cart_shipping_data['shipping_country'] : '';
			$country = $new_shipping_country;
			$country_exist = 'true';
			if ('shipping' === $load_address) {
				$allowed_countries = WC()->countries->get_shipping_countries();
				if (! array_key_exists($country, $allowed_countries)) {
					$country_exist = 'false';
					$country = current(array_keys($allowed_countries));
				}
			}
			$address = WC()->countries->get_address_fields($country, $load_address . '_');
			$address_data = array();
			$address_new = array();
			if(!empty($address) && is_array($address)){
				foreach ($address as $key => $field) {
					if(!empty($cart_shipping_data) && is_array($cart_shipping_data)) {
						foreach ($cart_shipping_data as $fkey => $value) {
							$address_data[$key] = array(
								'label' =>isset($field['label']) ? $field['label'] : '',
								'value' => isset($cart_shipping_data[$key]) ? $cart_shipping_data[$key] : '',
								'required' => isset($field['required']) ? $field['required'] : '',
								'type' => isset($field['type']) ? $field['type'] : '',
								'validate' => isset($field['validate']) ? $field['validate'] : ''
								);	
							//$address_new[$key] = $cart_shipping_data[$key];	
							$address_new[$key] = isset($cart_shipping_data[$key]) ? $cart_shipping_data[$key] : '';	
						}
					}
				}
			}		
			$true_check = self::validate_cart_shipping_addr_data($cart_shipping_data, $load_address, $country, $address_data, $country_exist);
			$true_check_val = '';
			if($true_check == 'true') {	
				$settings_guest_usr = THWMA_Utils::get_setting_value('settings_guest_users');
				$settings = $settings_guest_usr;
				if($settings && !empty($settings)) {
					$enabled_guest_user = isset($settings['enable_guest_shipping']) ? $settings['enable_guest_shipping'] : '';
					if($enabled_guest_user == 'yes') {
						$expiration = self::expiration_time();
						THWMA_Utils::save_address_to_guest_user($address_new, 'shipping', $expiration);
						$true_check_val = 'true';
					} else {
						$true_check_val = 'error';
					}
				} else {
					$true_check_val = 'error';
				}				
			} else {
				$true_check_val = $true_check;				
			}

			// Create new tile field for append the existing address tiles.
			$custom_address = THWMA_Utils::get_custom_addresses_of_guest_user('shipping');
			if(is_array($custom_address)) {
				$adr_count = count($custom_address);
			} else {
				$adr_count = '';
			}
			$new_id = THWMA_Utils::get_guest_user_new_custom_id('shipping');
			// new.
			// if($new_id == ''){
			// 	$new_id = '1';
			// }
			$address_key = 'address_'.$new_id;	
			if($address_key == 'address_') {
				$address_key = 'address_1';
			}
			
			$add_list_class = 'thwma-thslider-list-ms ship';
			$new_adr_count = $new_id + 1;
			if(empty($adr_count)) {
				$adr_count = '';
				$return_adr_tile  = '';
				$return_adr_tile .= '<div class="thwma-thslider-box">';
		           	$return_adr_tile .= '<div class="thwma-thslider-viewport '.esc_attr('shipping').'">';
			           	$return_adr_tile .= '<ul class=" '.esc_attr($add_list_class).'">';
			           		$return_adr_tile .= self::create_a_new_adrs_tile_for_guest_user($cart_shipping_data, $address_key, $adr_count);

			           	$return_adr_tile .= '</ul>';
			        $return_adr_tile .= '</div>';
			    $return_adr_tile .= '</div>';
			} else {
				$return_adr_tile = self::create_a_new_adrs_tile_for_guest_user($cart_shipping_data, $address_key, $adr_count);
			}
			
			
			// Multi-shipping address table.
			$type  = 'shipping';
			$shipping_table = '';
			if(empty($adr_count)) {
				$shipping_table = self::guest_multiple_address_management_form();
			}

			// Create a new dropdown option.
			$opt_key = $address_key;
			$value = '';
			$address_dropdown = '';
			$adrsvalues_to_dd = array();
			if(!empty($cart_shipping_data) && is_array($cart_shipping_data)) {
				foreach ($cart_shipping_data as $adrs_key => $adrs_value) {
					if($adrs_key == 'shipping_address_1' || $adrs_key =='shipping_address_2' || $adrs_key =='shipping_city' || $adrs_key =='shipping_state' || $adrs_key =='shipping_postcode') {
						if($adrs_value) {
							$adrsvalues_to_dd[] = $adrs_value;
						}
					}
				}
			}

			// Set the Default address.
			// $default_addr = '';
			// 		if(!empty($cart_shipping_data) && is_array($cart_shipping_data)) {
			// 			foreach ($cart_shipping_data as $key => $value) {
			// 				$custom_adr[] = $value;
			// 			}
			// 		}
			// 		if(!empty($custom_adr)) {
			// 			$encoded_default_address = json_encode($custom_adr[0]);
			// 			$default_addr .= '<input type="hidden" name="ship_default_adr" value='.$encoded_default_address.'>';
			// 		}



			
			$new_adrs_string = implode(', ', $adrsvalues_to_dd);
			// $new_address = THWMA_Utils::get_formated_address('shipping', $cart_shipping_data);
			// $separator = ', ';
			// $new_option = WC()->countries->get_formatted_address($new_address, $separator);
			$address_dropdown .= '<option value="' . esc_attr($opt_key) . '" ' . selected($value, $opt_key, false) . ' >' . esc_attr($new_adrs_string) . '</option>';					
			
			// Slider arrows.
			$slider_arrows = '';
			$slider_arrows.= '<div class="control-buttons control-buttons-'.esc_attr($type).'">';
			if($adr_count && $adr_count >= 3) {
            	$slider_arrows .= '<div class="prev thwma-thslider-prev multi-'.esc_attr('shipping').'"><i class="fa fa-angle-left fa-3x"></i></div>';
            	$slider_arrows .= '<div class="next thwma-thslider-next multi-'.esc_attr('shipping').'"><i class="fa fa-angle-right fa-3x"></i></div>';
            }
            $slider_arrows.= '</div>';
            $total_address_count = '';
            $total_address_count = THWMA_Utils::get_total_address_count();

           	if(is_numeric( $total_address_count)) {
            	$total_address_count = $total_address_count + 1;
            } else if($total_address_count == null) {
            	$total_address_count = 1;
            }

            // Address limit.
			$address_limit = '';
            if($type) {
                $address_limit = THWMA_Utils::get_setting_value('settings_'.$type , $type.'_address_limit');
            }
            if (!is_numeric($address_limit)) {
                $address_limit = 0;
            }

            $response = array(
				//'result_shipping' => $output_shipping,
				'adr_count' => $adr_count,
				'true_check' => $true_check_val,
				'new_tile' => $return_adr_tile,
				'shipping_table' => $shipping_table,
				'address_dropdown' => $address_dropdown,
				'slider_arrows' => $slider_arrows,
				'default_address' => $opt_key,
				'address_count' => $total_address_count,
				'address_limit' => $address_limit,
			);
			wp_send_json($response);	
			exit();
		}

		public static function create_a_new_adrs_tile_for_guest_user($cart_shipping_data, $address_key, $adr_count) {
			if(!empty($adr_count)) {
				$i = $adr_count;
			} else{
				$i = '';
			}
			$action_row_html = '';
			$address_type_css = '';
			
			$disable_adr_mngmnt = self::disable_address_mnagement();
			if($disable_adr_mngmnt == true) {
				$disable_mngt_class = 'thwma_disable_adr_mngt';
				$disable_acnt_sec = 'disable_acnt_sec';
			} else {
				$disable_mngt_class = '';
				$disable_acnt_sec = '';
			}
			$type  = 'shipping';
			$new_address = THWMA_Utils::get_formated_address('shipping', $cart_shipping_data);
			$options_arr = WC()->countries->get_formatted_address($new_address);
			$address_key_param = "'".$address_key."'";
			$address_type = "'".$type."'";
			$heading = !empty($new_address[$type.'_heading']) ? $new_address[$type.'_heading'] : esc_html__('', 'woocommerce-multiple-addresses-pro') ;
			
			$action_row_html .= '<div class="thwma-adr-footer address-footer"> 
				<div class="btn-delete '.esc_attr($disable_mngt_class).'" data-index="0" data-address-id="" onclick="thwma_guest_users_delete_selected_address_cart_page(this,'.esc_attr($address_type).','.esc_attr($address_key_param).')" title="'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'"> 
					<span>'.esc_html__('Delete', 'woocommerce-multiple-addresses-pro').'</span> 
				</div>
			</div>';
			if(isset($heading) && $heading != '') {
				$heading_css = '<div class="address-type-wrapper row"> 
					<div title="'.$heading.'" class="address-type '.esc_attr($address_type_css).'">'.esc_html($heading).'</div>  
					</div>       
					<div class="tile-adrr-text thwma-adr-text address-text address-wrapper">'.$options_arr.'</div>'; 
			} else {
				$heading_css = '<div class="tile-adrr-text thwma-adr-text address-text address-wrapper wrapper-only">'.$options_arr.'</div>'; 
			}			
			$add_class  = "thwma-thslider-item-ms $type " ;
			$add_class .= $i == 0 ? ' first' : '';
			$return_html = '';
			$return_html .= '<li class="'.$add_class.'" value="'. esc_attr($address_key).'" >
				<div class="thwma-adr-box address-box" data-index="'.esc_attr($i).'" data-address-id=""> 
					<div class="thwma-main-content"> 
						<div class="complete-aaddress">  
							'.$heading_css.'
						</div> 
						<div class="btn-continue address-wrapper"> 
							<a class="th-btn button primary is-outline '.esc_attr($address_key).'" onclick="thwma_populate_selected_address(event, this, '.esc_attr($address_type).', '.esc_attr($address_key_param).')"> 
								<span>'.esc_html__('Choose This Address', 'woocommerce-multiple-addresses-pro').'</span> 
							</a> 
						</div> 
					</div>'.$action_row_html.'</div></li>';
			return $return_html;
		}

		/**
         * Delte guest user's addresses from cart page(Ajax respose -Guest users)
         */ 
		public function delete_guest_address_from_cart() {
			global $woocommerce;
			check_ajax_referer( 'delete-address-with-id-cart-guest', 'security' );
			$address_key = isset($_POST['selected_address_id']) ? $_POST['selected_address_id'] : '';
			$type = isset($_POST['selected_type']) ? $_POST['selected_type'] : '';			
			$customer_id = get_current_user_id();
			THWMA_Utils::delete($customer_id, $type, $address_key);
			$output_shipping = self::get_tile_field_to_cart($customer_id, 'shipping');
			$expiration = self::expiration_time();
			THWMA_Utils::delete_guest_address($type, $address_key, $expiration);

			if($address_key) {
				if (isset($_COOKIE[THWMA_Utils::GUEST_USER_SHIPPING_ADDR])) {
					$shipping_address = $_COOKIE[THWMA_Utils::GUEST_USER_SHIPPING_ADDR];
					$shipping_address = preg_replace('!s:(\d+):"(.*?)";!', "'s:'.strlen('$2').':\"$2\";'", $shipping_address);
					$custom_address = unserialize(base64_decode($shipping_address));

					setcookie(THWMA_Utils::GUEST_USER_SHIPPING_ADDR, '', time()-36000, '/');

					unset($custom_address[$type][$address_key]);

					$new_custom_address = $custom_address;
				   setcookie(THWMA_Utils::GUEST_USER_SHIPPING_ADDR, base64_encode(serialize($new_custom_address)), time() + $expiration, "/");

				    $shipping_address1 = $_COOKIE[THWMA_Utils::GUEST_USER_SHIPPING_ADDR];
					$shipping_address1 = preg_replace('!s:(\d+):"(.*?)";!', "'s:'.strlen('$2').':\"$2\";'", $shipping_address1);
					$custom_address1 = unserialize(base64_decode($shipping_address1));				
				}
				$output_shipping = $this->get_not_loggedin_tile_field('shipping');

				$total_address_count = THWMA_Utils::get_total_address_count();
				$total_address_count = $total_address_count - 1;


				// Address limit.
				$address_limit = '';
	            if($type) {
	                $address_limit = THWMA_Utils::get_setting_value('settings_'.$type , $type.'_address_limit');
	            }
	            if (!is_numeric($address_limit)) {
	                $address_limit = 0;
	            }

				$disable_adr_mngmnt = self::disable_address_mnagement();
				if($disable_adr_mngmnt != true){
	                $disable_adr_mngmnt = apply_filters('disable_address_management_on_checkout', false);
	            }
				if($disable_adr_mngmnt == true) {
					$disable_mngt_class = 'thwma_disable_adr_mngt';
					$disable_acnt_sec = 'disable_acnt_sec';
				} else {
					$disable_mngt_class = '';
					$disable_acnt_sec = '';
				}
				$type = 'shipping';
				$address_type = "'$type'";	
				$add_address_btn = '<div class="add-address thwma-add-adr '.esc_attr($disable_acnt_sec).'">
		            <button class="btn-add-address primary button '.esc_attr($disable_mngt_class).'" onclick="thwma_guest_users_add_new_shipping_address(event, this, '.esc_attr($address_type).')">
		                <i class="fa fa-plus"></i> '.esc_html__(' Add new address', 'woocommerce-multiple-addresses-pro').'
		            </button>
		        </div>';

		        if($address_limit-1 == $total_address_count) {
		        	$add_new_button =  $add_address_btn;
		        } else {
		        	$add_new_button =  '';
		        }

				$response = array(
					'result_shipping' => $output_shipping,				
					'address_key' => $address_key,
					'address_count' => $total_address_count,
					'add_new_button' =>  $add_new_button,
				);
				
				$cart = $woocommerce->cart->cart_contents;
				if(!empty($cart) && is_array($cart)) {	
					foreach ($cart as $key => $item) {
						// if(isset($cart[$key]['multi_ship_address'])) {	
						// 	$ship_adr_data = $cart[$key]['multi_ship_address'];
						// 	if(!empty($cart[$key]['multi_ship_address']) && is_array($cart[$key]['multi_ship_address'])) {
						// 		foreach ($cart[$key]['multi_ship_address'] as $ship_adr_key => $value) {	
						// 			if($value['ship_address'] == $address_key) {
						// 		        $woocommerce->cart->cart_contents[$key]['multi_ship_address'][$ship_adr_key]['ship_address'] = '';
						// 			}
						// 		}
						// 	}
						// }	
						if(isset($cart[$key]['product_shipping_address'])) {			
							if($cart[$key]['product_shipping_address'] == $address_key) {
						        $woocommerce->cart->cart_contents[$key]['product_shipping_address'] = '';
							}
						}
					}
				}
				$woocommerce->cart->set_session();
				wp_send_json($response);
			}
			exit();
		}

		/**
         * Function for set expiration time (Guest users)
         *
         * @return string
         */ 
		public static function expiration_time() {
			$settings_guest_usr = THWMA_Utils::get_setting_value('settings_guest_users');
			$settings = $settings_guest_usr;
			$expiration = '';
			if(isset($settings)) {
				$time_duration = isset($settings['set_time_duration']) ? $settings['set_time_duration']:'';
				$time_limit = isset($settings['time_limit']) ? $settings['time_limit']:'';
				if($time_limit == 'minute') {
					$expiration = $time_duration * MINUTE_IN_SECONDS;
				} else if($time_limit == 'hour') {
					$expiration = $time_duration * HOUR_IN_SECONDS;
				} else if($time_limit == 'day') {
					$expiration = $time_duration * DAY_IN_SECONDS;
				}
			}
			return $expiration;
		}

		/**
		 * Get current guest user saved addresses (Guest users).
		 *
		 * @param array ship_addresses The Guest user addresses
		 *
		 */
		public static function get_guest_user_addresses($ship_addresses) {
	    	$user_address_data = array();
			if(isset($_COOKIE[THWMA_Utils::GUEST_USER_SHIPPING_ADDR])) {
				$shipping_address = $_COOKIE[THWMA_Utils::GUEST_USER_SHIPPING_ADDR];
				$shipping_address = preg_replace('!s:(\d+):"(.*?)";!', "'s:'.strlen('$2').':\"$2\";'", $shipping_address);
				$user_address_data = unserialize(base64_decode($shipping_address));
			}

			// Address details.
	    	if($user_address_data) {
	    		if(is_array($user_address_data)) {
			    	foreach($user_address_data as $index => $ship_data) {
			    		$shipping_address = isset($user_address_data['shipping'])?$user_address_data['shipping']:'';
			    		$default_shipping_address = isset($user_address_data['default_shipping'])?$user_address_data['default_shipping'] : '';
			    	}
			    }
		    	if(!empty($shipping_address) && is_array($shipping_address)) {
			    	foreach($shipping_address as $key => $value) {
			    		if($key == $ship_addresses) {
		    				return $value;
		    			}
			    	}
			    }
			    if(!empty($default_shipping_address) && is_array($default_shipping_address)) {
			    	foreach($default_shipping_address as $key => $value) {
			    		if($key == $ship_addresses) {
		    				return $value;
		    			}
			    	}
			    }
		    }
		}

		/**
         * The order again set cart data(order again)
         * 
         * @param array $cart_item_data The cart item data
         * @param array $item The item details
         * @param array $order The order details
         *
         * @return array
         */ 
		public function thwma_filter_order_again_cart_item_data($cart_item_data, $item, $order) {	
			$item_id = $item->get_id();
			$meta_data = $item->get_meta_data();
			if(!empty($meta_data) && is_array($meta_data)) {
				foreach($meta_data as $key => $meta) {
					$shipping_data[] = $meta->value;
				}
			}
			$shipping_adrs = array();
			$unique_key = array();
			$time = '';
			
			if(!empty($shipping_data) && is_array($shipping_data)){
				foreach($shipping_data as $ship => $data) {
					$shipping_adrs = $data;
					if(is_array($shipping_adrs) && !empty($shipping_adrs)) {
						foreach ($shipping_adrs as $key => $value) {
							$unique_key[] = isset($value['cart_unique_key']) ? $value['cart_unique_key'] : '';
						}

					}
				}
			}
			$unique_key = array_filter($unique_key);
			if(!empty($unique_key) && is_array($unique_key)) {
				foreach ($unique_key as $key => $value) {
					$cart_item_data['unique_key'] = $value;
					$cart_item_data['time'] = $time;
				}
				
			}
			return $cart_item_data;
		}

		/**
         * Function for get shipping method details(Ajax response).
         */ 
		public function save_shipping_method_details() {
			global $woocommerce;
			check_ajax_referer( 'save-shipping-method-details', 'security' );
			// Update shipping package and Disable shipping calculator.
	    	$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping']:'';
			$user_id= get_current_user_id();
	        $enable_multi_ship = '';
	        if (is_user_logged_in()) {
	        	$enable_multi_ship = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
	        } else {
	        	$enable_multi_ship = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP])?$_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]:'';
	        }

	        if($enable_multi_ship == 'yes') {
				if($cart_shipping_enabled == 'yes') {
					$ship_method_arr = isset($_POST['ship_method_arr']) ? $_POST['ship_method_arr'] : '';
						if(!empty($ship_method_arr) && is_array($ship_method_arr)) {
						//foreach ($ship_method_arr as $ship_method_arr_data) {
						$cart_key = '';
						$shipping_methods = array();
						foreach ($ship_method_arr as $key => $value) {
							$id = isset($value['method_id']) ? $value['method_id'] : '';
							$method_id = substr($id, 0, strpos($id, ":"));

							$cart_key = isset($value['cart_key']) ? $value['cart_key'] : '';
							$cart_unique_key = isset($value['cart_unique_key']) ? $value['cart_unique_key'] : '';
							$product_name = isset($value['item_name']) ? rtrim ($value['item_name']) : '';
							$product_id = isset($value['product_id']) ? $value['product_id'] : '';
							$product_qty = isset($value['item_qty']) ? $value['item_qty'] : '';
							$address_name = isset($value['shipping_name']) ? $value['shipping_name'] : '';
							$shipping_adrs = isset($value['shipping_adrs']) ? json_decode(base64_decode($value['shipping_adrs'])) : '';
							$shipping_adrs = json_decode(json_encode($shipping_adrs), true);
							$custom_fields = $this->get_custom_fields_from_addresses($shipping_adrs);
							$shipping_methods[$id] = array(
								'method_id' 		=> $method_id,
								'product_id'		=> $product_id,
								'cart_key'			=> $cart_key,
								'cart_unique_key'	=> $cart_unique_key,
								'item_name' 		=> $product_name,					
								'item_quantity' 	=> $product_qty,
								'address_name'		=> $address_name,
								'shipping_address' 	=> $shipping_adrs,
								'custom_fields'		=> $custom_fields

							);
							$cart = $woocommerce->cart->cart_contents;
							$input_value = '';
							if(!empty($cart) && is_array($cart)){			
								foreach ($cart as $key => $item) {
									if(!empty($cart_key)) {
									    if($key == $cart_key) {									    	
									    	$woocommerce->cart->cart_contents[$key]['thwma_shipping_methods'] = $shipping_methods;

									        $shipping_methods = array();
										}
									}
								}
							}
							$woocommerce->cart->set_session();
						}											
					}
				}
			}
			//update_option(THWMA_Utils::OPTION_KEY_SHIPPING_METHOD, $shipping_methods);
			exit();
		}

		/**
         * Function for get custom fields from the address fields
         * 
         * @param obj $shipping_adrs The whole address fields including field values
         *
         * @return array
         */ 
		public function get_custom_fields_from_addresses($shipping_adrs) {			
			$default_fields = array(
				'first_name'=> '',
			    'last_name' => '',
			    'company' 	=> '',
			    'country' 	=> '',
			    'address_1' => '',
			    'address_2' => '',
			    'city' 		=> '',
			    'state' 	=> '',
			    'postcode' 	=> ''
			);
			$custom_fields = array();
			if(is_array($shipping_adrs)) {
				$custom_fields = array_diff_key($shipping_adrs,$default_fields);
			}
			return $custom_fields;
		}

		/**
         * Function for get custom fields from the address fields
         * 
         * @param obj $shipping_adrs The whole address fields including field values
         *
         * @return array
         */ 
		public function get_shipping_custom_fields_from_addresses($shipping_adrs) {
			$default_fields = array(
				'shipping_heading' => '',
				'shipping_first_name'=> '',
			    'shipping_last_name' => '',
			    'shipping_company' 	=> '',
			    'shipping_country' 	=> '',
			    'shipping_address_1' => '',
			    'shipping_address_2' => '',
			    'shipping_city' 		=> '',
			    'shipping_state' 	=> '',
			    'shipping_postcode' 	=> ''
			);
			$custom_fields = '';
			if(!empty($shipping_adrs)) {
				$custom_fields = array_diff_key($shipping_adrs,$default_fields);
			}
			return $custom_fields;
		}

		/*-- Multi Shipping on Checkout page --*/

		/**
		 * Function for delete addresses from shipping address list of multi-shipping section(ajax response- checkout page).
         * Function for delete address from cart page(ajax response-cart page)
         *
         * @return void
         */ 
		public function delete_address_from_cart() {
			global $woocommerce;
			check_ajax_referer( 'delete-address-with-id-cart', 'security' );
			$address_key = isset($_POST['selected_address_id']) ? sanitize_key($_POST['selected_address_id']) : '';
			$type = isset($_POST['selected_type']) ? sanitize_key($_POST['selected_type']) : '';			
			$customer_id = get_current_user_id();
			THWMA_Utils::delete($customer_id, $type, $address_key);
			$output_shipping = self::get_tile_field_to_cart($customer_id, 'shipping');
			$total_address_count = THWMA_Utils::get_total_address_count();

			$response = array(
				'result_shipping' => $output_shipping,
				'address_key' => $address_key,	
				'address_count' => $total_address_count,
			);
			$cart = $woocommerce->cart->cart_contents;
			if(!empty($cart) && is_array($cart)) {	
				foreach ($cart as $key => $item) {
					if(isset($cart[$key]['multi_ship_address'])) {	
						$ship_adr_data = $cart[$key]['multi_ship_address'];
						if(!empty($cart[$key]['multi_ship_address']) && is_array($cart[$key]['multi_ship_address'])) {
							foreach ($cart[$key]['multi_ship_address'] as $value) {	
								if(is_array($value)) {
									if (array_key_exists("ship_address",$value)) {
										if($value['ship_address'] == $address_key) {
									        $woocommerce->cart->cart_contents[$key]['multi_ship_address']['ship_address'] = '';
										}
									}
								}
							}
						}
					}
					if(isset($cart[$key]['product_shipping_address'])) {
						if($cart[$key]['product_shipping_address'] == $address_key) {
					        $woocommerce->cart->cart_contents[$key]['product_shipping_address'] = '';
						}
					}
				}
			}

			$woocommerce->cart->set_session();
			wp_send_json($response);
		}

		/**
		 * Function for set default address from shipping address list of multi-shipping section(ajax response- checkout page).
         * Function for set default address from cart page(ajax response-cart page)
         *
         * @return void
         */  
		public function default_address_from_cart() {
			check_ajax_referer( 'set-default-address-cart', 'security' );
			$address_key = isset($_POST['selected_address_id']) ? sanitize_key($_POST['selected_address_id']) : '';
			$type = isset($_POST['selected_type']) ? sanitize_key($_POST['selected_type']) : '';

			$user_id = get_current_user_id();
			$this->change_default_address($user_id, $type, $address_key);
			$output_shipping = self::get_tile_field_to_cart($user_id, 'shipping');
			$output_table = self::multiple_address_management_form();

			$user_id = get_current_user_id();
			$default_address = THWMA_Utils::get_default_address($user_id, 'shipping');
			$encoded_default_address = json_encode($default_address);

			$response = array(
				'result_shipping' => $output_shipping,	
				'output_table'    => $output_table,
				'default_address' => $encoded_default_address,
			);
			wp_send_json($response);
		}

		/**
         * Function for setting multi-shipping on checkout page( checkout page).
         */
		public function add_checkbox_for_set_multi_shipping(){			
			$settings_manage_style = THWMA_Utils::get_setting_value('settings_manage_style');
			$multi_address_button = isset($settings_manage_style['multi_address_button']) ? $settings_manage_style['multi_address_button']:'';
			$multi_shipping_checkbox_label = isset($settings_manage_style['multi_shipping_checkbox_label']) ? $settings_manage_style['multi_shipping_checkbox_label']:'';

			$time_duration = isset($settings['set_time_duration']) ? $settings['set_time_duration']:'';
			$user_id = get_current_user_id();
	        $enable_multi_ship_data = '';

	        // Check user is logged in.
	        if (is_user_logged_in()) {
	        	$enable_multi_ship_data = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
	        	$user_id = get_current_user_id();
				$default_address = THWMA_Utils::get_default_address($user_id, 'shipping');

				$encoded_default_address = json_encode($default_address);
				
				// echo '<input type="hidden" name="ship_default_adr" value="'.$encoded_default_address.'">';
				 echo "<input type='hidden' name='ship_default_adr' value='".$encoded_default_address."'>";

	        } else {
	        	$enable_multi_ship_data = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]) ? $_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP] : '';

	        	$custom_addresses = array();
	        	$settings_guest_urs = THWMA_Utils::get_setting_value('settings_guest_users');
				$enabled_guest_user = isset($settings_guest_urs['enable_guest_shipping']) ? $settings_guest_urs['enable_guest_shipping']:'';
				$custom_adr = array();
	        	if($enabled_guest_user == 'yes') {
					$custom_addresses = THWMA_Utils::get_custom_addresses_of_guest_user('shipping');
					if(!empty($custom_addresses) && is_array($custom_addresses)) {
						foreach ($custom_addresses as $key => $value) {
							$custom_adr[] = $value;
						}
					}
				}
				if(!empty($custom_adr)) {
					$encoded_default_address = json_encode($custom_adr[0]);
					echo "<input type='hidden' name='ship_default_adr' value='".$encoded_default_address."'>";
				}
	        }
	        

	        // Check multi-shipping checkbox is enabled on front end.
	        if(!empty($enable_multi_ship_data)) {
	        	$value = $enable_multi_ship_data;
		        if($enable_multi_ship_data == 'yes') {
		        	$checked = 'checked';
		        	$display_ppty = 'block';
		        } else {
		        	$checked = '';
		        	$display_ppty = 'none';
	        	}
	        } else {
	        	$value = 'no';
				$checked = '';
				$display_ppty = 'none';
	        }

	        $total_address_count = THWMA_Utils::get_total_address_count();

			echo '<input type="checkbox" name="ship_to_multi_address" value="'.$value.'" '.esc_attr__($checked).' data-address_count="'.$total_address_count.'"> '. esc_html__($multi_shipping_checkbox_label, "woocommerce-multiple-addresses-pro").'</br>';
			echo '<a onclick="thwma_cart_shipping_popup(event)" class=" thwma_cart_shipping_button" style="display:'.$display_ppty.'">'.esc_html__($multi_address_button, "woocommerce-multiple-addresses-pro").'</a>'; 
			$all_address='';

        	// Check user is logged in.
        	$html_address =	'';
			if (is_user_logged_in()) {				
				$html_address = self::get_tile_field_to_cart($user_id, 'shipping');
			} else {
				$html_address = $this->get_not_loggedin_tile_field('shipping');
			}
			$theme_class_name = $this->check_current_theme();
			$theme_class = $theme_class_name.'_tile_field';

			// List of shipping addresses-popup.
			$all_address.= '<div id="thwma-cart-shipping-tile-field" class="thwma-cart-modal thwma-cart-shipping-tile-field '.esc_attr__($theme_class).'">'. $html_address.'</div>';

			// Add address form - popup.
			$all_address.= '<div id="thwma-cart-shipping-form-section" class="thwma-cart-modal2 thwma-cart-shipping-form-section '.esc_attr__($theme_class).'"><div class="thwma_hidden_error_mssgs"></div></div>'; ?>
			<div class="u-columns woocommerce-Addresses col2-set addresses cart-shipping-addresses ">
				<?php echo $all_address; ?>
			</div> 
			<!-- <div id="multi-shipping-wrapper"> -->
				<div class="multi-shipping-wrapper">

				<?php
				echo self::multiple_address_management_form(); ?>
				</div>
			<!-- </div> -->
		<?php }	

		/**
         * Function for setting multi-shipping management form( checkout page).
         *
         * @return string
         */
		public static function multiple_address_management_form() {
			global $woocommerce;
			//$items = $woocommerce->cart->get_cart();
			$items = $woocommerce->cart->cart_contents;
			$customer_id = get_current_user_id();
			$def_address = THWMA_Utils::get_default_address($customer_id, 'shipping');
    		$custom_addresses = self::get_saved_custom_addresses_from_db();
    		$multi_ship_form = '';
			if($custom_addresses) {
				$multi_ship_form .= '<div class="multi-shipping-table-wrapper">';
					$multi_ship_form .= '<div class="multi-shipping-table-overlay"></div>';
					$multi_ship_form .= '<table class="multi-shipping-table">';
		    			$multi_ship_form .= '<tr>';
		    				$multi_ship_form .= '<th>Product</th>';
		    				$multi_ship_form .= '<th>Quantity</th>';
		    				$multi_ship_form .= '<th>Sent to:</th>';
		    			$multi_ship_form .= '</tr>';
		    			$i = 1;
		        		$ini_stage = 0;
		    			foreach ($items as $key => $value) {
		    				$multi_ship_id = 'multi_ship_'.$i; 
		    				$product = wc_get_product( $value[ 'product_id' ] );	

		    				

		    				//$cart = $woocommerce->cart->cart_contents;	
		    				$cart = $items;
		    				$qty = $value['quantity'];	
							$multi_ship_parent_id = '';
					    	$child_keys = '';
					    	if($qty <= 1) {
					    		$link_show = false;	
					    	} else {
					    		$link_show = true;
					    	}

					    	$cart_item = $items[$key];
					    	$cart_item = $value;
					    	$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $key );
					    	//$_product  =  $product;

					    	$multi_shipping_addresses = self::multi_shipping_addresses($value, $key, $link_show, $multi_ship_id);

					    	// Check the product is vertual or downloadable.
					    	if ((!$product->is_virtual()) && (!$product->is_downloadable('yes'))) {	    
			    				if(!empty($cart[$key]['multi_ship_address'])) {
			    					$multi_ship_content = $cart[$key]['multi_ship_address'];
			    					if(!empty($multi_ship_content) && is_array($multi_ship_content)) {
				    					foreach ( $multi_ship_content as $multi_ship_datas) {
				    						$multi_ship_parent_id = isset($multi_ship_content['multi_ship_parent_id']) ? $multi_ship_content['multi_ship_parent_id'] : '';
				    						$child_keys = isset($multi_ship_content['child_keys']) ? $multi_ship_content['child_keys'] : '';
				    					}
				    				}
				    				//$child_keys = $child_keys_data;
				    				// $child_keys_data = $child_keys;
				    				
									if($multi_ship_parent_id == null) {
										if($qty == 1) {
					        				$link_show = false;
					        			} else {
					        				$link_show = true;
					        			}
					        			$multi_shipping_addresses = self::multi_shipping_addresses($value, $key, $link_show, $multi_ship_id);
										$multi_ship_form .= '<tr class="main-pdct-tr">';
						    				$multi_ship_form .= '<td class="wmap-img-tr">';
						    					$multi_ship_form .= '<div class="checkout-thumbnail-img">';

							    					$product_permalink = $_product->is_visible() ? $_product->get_permalink( $cart_item ) : '';
								    				$thumbnail = $_product->get_image();

													if ( ! $product_permalink ) {
														echo $thumbnail; // PHPCS: XSS ok.
													} else {
														$multi_ship_form .= '<a href="'.esc_url( $product_permalink ).'">'.$thumbnail.'</a>';
													}
													//$multi_ship_form .= '<p class="product-thumb-name">'.$_product->get_name().'</p>';
													$multi_ship_form .= '<p class="product-thumb-name">';
														if ( ! $product_permalink ) {
															$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $key ) . '&nbsp;' );
														} else {
															
															$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $key ) );
														}
														//$multi_ship_form .= do_action( 'woocommerce_after_cart_item_name', $cart_item, $key );
														$multi_ship_form .=  wc_get_formatted_cart_item_data( $cart_item );
													$multi_ship_form .= '</p>';
													// Backorder notification.
													if ( $_product->backorders_require_notification() && $_product->is_on_backorder( $cart_item['quantity'] ) ) {
														$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_backorder_notification', '<p class="backorder_notification">' . esc_html__( 'Available on backorder', 'woocommerce' ) . '</p>', $product_id ) );
													}

													// $product_name = do_action( 'woocommerce_after_cart_item_name', $cart_item, $key );
								    	// 			$multi_ship_form .= '<p class="product-thumb-name">'.$product_name.'</p>';
								    				// do_action( 'woocommerce_after_cart_item_name', $cart_item, $key );
								    				//echo wc_get_formatted_cart_item_data( $items );




								    			$multi_ship_form .= '</div>';
						    				$multi_ship_form .= '</td>';
						    				$multi_ship_form .= '<td><input type="number" min="1" name="pdct-qty" value="'.$qty.'" class="multi-ship-pdct-qty pdct-qty main-pdct-qty pdct-qty-'.$key.'" data-cart_key="'.$key.'"></td>';
						    				$multi_ship_form .= '<td><input class="multi-ship-item" type="hidden" data-multi_ship_id="multi_ship_'.$i.'" data-multi_ship_parent_id="0" data-updated_qty="'.$qty.'" data-sub_row_stage="1">'.$multi_shipping_addresses.'</td>';
						    			$multi_ship_form .= '</tr>';

					    				if(!empty($child_keys) && is_array($child_keys)) {
					    					foreach ( $child_keys as $item_key) {
						    					if(!empty($cart[$item_key])) {
					    							$cart_data = $cart[$item_key];
					    							$child_qty = isset($cart[$item_key]['quantity']) ? $cart[$item_key]['quantity'] : '';
					    							$multi_ship_id = $cart[$item_key]['multi_ship_address']['multi_ship_id'];
													$link_show = false;
					    							$multi_shipping_addresses = self::multi_shipping_addresses($cart_data, $item_key, $link_show, $multi_ship_id);

					    							$multi_ship_form .= '<tr>';
									    				$multi_ship_form .= '<td>';
											    			// $multi_ship_form .= '<a id ="remove-multi-ship-tr" class="remove-multi-ship-tr"  onclick="thwma_remove_multi_shipping_tr(event,this)"><span class="dashicons dashicons-dismiss"></span></a>';
									    				$multi_ship_form .= '</td>';
									    				$multi_ship_form .= '<td>';
									    					$multi_ship_form .= '<input type="number" min="1" name="pdct-qty" value="'.$child_qty.'" class="multi-ship-pdct-qty pdct-qty sub-pdct-qty pdct-qty-'.$item_key.'" data-cart_key="'.$item_key.'">';
									    					$multi_ship_form .= '</td>';
									    				$multi_ship_form .= '<td>';
									    					$multi_ship_form .= '<input class="multi-ship-item" type="hidden" data-multi_ship_id="'.$multi_ship_id.'" data-multi_ship_parent_id="0" data-updated_qty="'.$child_qty.'" data-sub_row_stage="1">'.$multi_shipping_addresses;
									    					$multi_ship_form .= '<a id ="remove-multi-ship-tr" class="remove-multi-ship-tr"  onclick="thwma_remove_multi_shipping_tr(event,this)"><span class="dashicons dashicons-dismiss"></span></a>';
									    				$multi_ship_form .= '</td>';
									    			$multi_ship_form .= '</tr>';
					    						}
					    					}
					    				}			    				
					    			} else {
					    				$parent_cart_key = isset($multi_ship_content['parent_cart_key']) ? $multi_ship_content['parent_cart_key'] : '';
					    				$child_keys = isset($multi_ship_content['child_keys']) ? $multi_ship_content['child_keys'] : array();

					    				if (!array_key_exists($parent_cart_key, $items)){
						    				if($qty == 1) {
						        				$link_show = false;
						        			} else {
						        				$link_show = true;
						        			}
						        			$multi_shipping_addresses = self::multi_shipping_addresses($value, $key, $link_show, $multi_ship_id);
											$multi_ship_form .= '<tr class="main-pdct-tr">';
							    				$multi_ship_form .= '<td class="wmap-img-tr">';
							    					$multi_ship_form .= '<div class="checkout-thumbnail-img">';
								    					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $items ) : '', $items, $key );

									    				//$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $items, $key );
									    				$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $key );

														if ( ! $product_permalink ) {
															echo $thumbnail; // PHPCS: XSS ok.
														} else {
															$multi_ship_form .= '<a href="'.esc_url( $product_permalink ).'">'.$thumbnail.'</a>';
														}

									    				//$multi_ship_form .= '<p class="product-thumb-name">'.$_product->get_name().'</p>';
									    				$multi_ship_form .= '<p class="product-thumb-name">';
															if ( ! $product_permalink ) {
																$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $key ) . '&nbsp;' );
															} else {
																$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $key ) );
															}

															$multi_ship_form .=  wc_get_formatted_cart_item_data( $cart_item );
														$multi_ship_form .= '</p>';
										    			//do_action( 'woocommerce_after_cart_item_name', $cart_item, $key );

									    			$multi_ship_form .= '</div>';
							    				$multi_ship_form .= '</td>';
							    				$multi_ship_form .= '<td><input type="number" min="1" name="pdct-qty" value="'.$qty.'" class="multi-ship-pdct-qty pdct-qty main-pdct-qty pdct-qty-'.$key.'" data-cart_key="'.$key.'"></td>';
							    				$multi_ship_form .= '<td><input class="multi-ship-item" type="hidden" data-multi_ship_id="multi_ship_'.$i.'" data-multi_ship_parent_id="0" data-updated_qty="'.$qty.'" data-sub_row_stage="1">'.$multi_shipping_addresses.'</td>';
							    			$multi_ship_form .= '</tr>';

							    			// Update multi ship parent id.
							    			$woocommerce->cart->cart_contents[$key]['multi_ship_address']['multi_ship_parent_id'] = '0';
					    					$woocommerce->cart->set_session();
					    					if(!empty($child_keys) && is_array($child_keys)) {
						    					foreach ( $child_keys as $item_key) {
							    					if(!empty($cart[$item_key])) {
						    							$cart_data = $cart[$item_key];
						    							$child_qty = isset($cart[$item_key]['quantity']) ? $cart[$item_key]['quantity'] : '';
						    							$multi_ship_id = $cart[$item_key]['multi_ship_address']['multi_ship_id'];
														$link_show = false;
						    							$multi_shipping_addresses = self::multi_shipping_addresses($cart_data, $item_key, $link_show, $multi_ship_id);

						    							$multi_ship_form .= '<tr>';
										    				$multi_ship_form .= '<td>';
												    			// $multi_ship_form .= '<a id ="remove-multi-ship-tr" class="remove-multi-ship-tr"  onclick="thwma_remove_multi_shipping_tr(event,this)"><span class="dashicons dashicons-dismiss"></span></a>';
										    				$multi_ship_form .= '</td>';
										    				$multi_ship_form .= '<td>';
										    					$multi_ship_form .= '<input type="number" min="1" name="pdct-qty" value="'.$child_qty.'" class="multi-ship-pdct-qty pdct-qty sub-pdct-qty pdct-qty-'.$item_key.'" data-cart_key="'.$item_key.'">';
										    					$multi_ship_form .= '</td>';
										    				$multi_ship_form .= '<td>';
										    					$multi_ship_form .= '<input class="multi-ship-item" type="hidden" data-multi_ship_id="'.$multi_ship_id.'" data-multi_ship_parent_id="0" data-updated_qty="'.$child_qty.'" data-sub_row_stage="1">'.$multi_shipping_addresses;
										    					$multi_ship_form .= '<a id ="remove-multi-ship-tr" class="remove-multi-ship-tr"  onclick="thwma_remove_multi_shipping_tr(event,this)"><span class="dashicons dashicons-dismiss"></span></a>';
										    				$multi_ship_form .= '</td>';
										    			$multi_ship_form .= '</tr>';
						    						}
						    					}
						    				}
								    	}
					    			}
			    				} else {
				    				$multi_ship_form .= '<tr class="main-pdct-tr">';
					    				$multi_ship_form .= '<td class="wmap-img-tr">';
					    					$multi_ship_form .= '<div class="checkout-thumbnail-img">';
						    					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $items ) : '', $items, $key );
							    				//$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $product->get_image(), $items, $key );
							    				$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $key );

												if ( ! $product_permalink ) {
													echo $thumbnail; // PHPCS: XSS ok.
												} else {
													$multi_ship_form .= '<a href="'.esc_url( $product_permalink ).'">'.$thumbnail.'</a>';
												}
							    				//$multi_ship_form .= '<p class="product-thumb-name">'.$_product->get_name().'</p>';

							    				$multi_ship_form .= '<p class="product-thumb-name">';
													if ( ! $product_permalink ) {
														$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $key ) . '&nbsp;' );
													} else {
														$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $key ) );
													}

													$multi_ship_form .=  wc_get_formatted_cart_item_data( $cart_item );
												$multi_ship_form .= '</p>';
								    			//do_action( 'woocommerce_after_cart_item_name', $cart_item, $key );

							    			$multi_ship_form .= '</div>';
					    				$multi_ship_form .= '</td>';
					    				$multi_ship_form .= '<td><input type="number" min="1" name="pdct-qty" value="'.$qty.'" class="multi-ship-pdct-qty pdct-qty main-pdct-qty pdct-qty-'.$key.'" data-cart_key="'.$key.'"></td>';
					    				$multi_ship_form .= '<td><input class="multi-ship-item" type="hidden" data-multi_ship_id="multi_ship_'.$i.'" data-multi_ship_parent_id="0" data-updated_qty="'.$qty.'" data-sub_row_stage="1">'.$multi_shipping_addresses.'</td>';
					    			$multi_ship_form .= '</tr>';
					    			$product_id = $value[ 'product_id' ];
					    			$variation_id = $value[ 'variation_id' ];
					    			$quantity = $value['quantity'];
					    			//$updated_quantity = $quantity;
					    			$parent_item_id = 0;
					    			// $woocommerce->cart->cart_contents[$key]['multi_ship_address'] = array();
					    			// $woocommerce->cart->set_session();
						    		$woocommerce->cart->cart_contents[$key]['multi_ship_address'] = array(
										'product_id' => $product_id,
										'variation_id' => $variation_id,
										'quantity' => $quantity,
										//'updated_quantity' => $updated_quantity,
										'multi_ship_id' => $multi_ship_id,
										'multi_ship_parent_id' => $parent_item_id,
										'child_keys' => array(),
										'parent_cart_key' => '',
									);
									$woocommerce->cart->set_session();
								}
								$i++;
							}
			    		}
			    		// $multi_ship_form .= '<tr>';
			    		// 	$multi_ship_form .= '<td></td><td></td>';
			    		// 	$multi_ship_form .= '<td><div class="save-multi-ship-adr-btn button alt" onclick="thwma_save_multi_ship_adr_btn(event,this)">save shipping addresses</div></td>';
			    		// $multi_ship_form .= '</tr>';
		    		$multi_ship_form .= '</table>';

		    		$multi_shipping = array();
		    		foreach ($items as $key => $value) {
		    			$product_id = isset($value['product_id']) ? $value['product_id'] : '';
		    			$adr_name = isset($value['product_shipping_address']) ? $value['product_shipping_address'] : '';
		    			$cart_key = isset($value['key']) ? $value['key'] : '';
		    			$multi_shipping[$cart_key] = array(
		    				'product_id' => "$product_id",
		    				'address_name' => $adr_name,
		    			);		    			
		    		}
		    		$multi_shipping_data = $multi_shipping;
					$multi_shipping_info = json_encode($multi_shipping_data);

		    		$multi_ship_form .= '<input type="hidden" name="multi_shipping_adr_data" class="multi-shipping-adr-data" value='.$multi_shipping_info.'></input>';
		    	$multi_ship_form .= '</div>';
	    	}
			return $multi_ship_form;
		}	

		/**
		 * Add additional address and quantity fields on checkout page(checkout page-ajax response).
		 */
		function additional_address_management(){
			global $woocommerce;
			check_ajax_referer( 'additional-address-management', 'security' );
			$cartitem = isset($_POST['cart_item']) ? $_POST['cart_item'] : '';
	        $product_id = isset($_POST['product_id']) ? sanitize_key($_POST['product_id']) : '';
			$variation_id = isset($_POST['variation_id']) ? absint($_POST['variation_id']) : '';
			$cart_item_key = isset($_POST['cart_item_key']) ? $_POST['cart_item_key'] : '';
	        $quantity = empty($_POST['quantity']) ? 1 : wc_stock_amount($_POST['quantity']);
	        $parent_item_id = isset($_POST['parent_item_id']) ? sanitize_key($_POST['parent_item_id']) : '';
	        $row_stage = isset($_POST['sub_row_stage']) ? sanitize_key($_POST['sub_row_stage']) : '';
	        $updated_qty = isset($_POST['updated_qty']) ? sanitize_key($_POST['updated_qty']) : '';

	        // Reduce one qty from main item.
	        // $display_qty = $updated_qty - 1;
	        
	        $woocommerce->cart->set_quantity($cart_item_key, $updated_qty);

			// Update shipping package and Disable shipping calculator.
	    	$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping']:'';
			$user_id= get_current_user_id();
	        $enable_multi_ship = '';
	        if (is_user_logged_in()) {
	        	$enable_multi_ship = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
	        } else {
	        	$enable_multi_ship = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]) ? $_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP] : '';
	        }

	        // Check multi-shipping is enable.
	        if($enable_multi_ship == 'yes') {
				if($cart_shipping_enabled == 'yes') {
			       // $product_id = apply_filters('woocommerce_add_to_cart_product_id', absint($_POST['product_id']));
			        $product = wc_get_product($product_id);
			        $new_row_stage = '';
			        if(is_numeric ($row_stage)) {
			        	$new_row_stage = $row_stage - 1;
			        }

			        $items = $woocommerce->cart->get_cart();
			        $cart = $woocommerce->cart->cart_contents;			        
			       
			        //$crnt_updated_qty = isset($cart[$cart_item_key]['multi_ship_address']['updated_quantity']) ? $cart[$cart_item_key]['multi_ship_address']['updated_quantity'] : '';
			        if(array_key_exists($cart_item_key, $cart)) {
			       		$crnt_child_keys = isset($cart[$cart_item_key]['multi_ship_address']['child_keys']) ? $cart[$cart_item_key]['multi_ship_address']['child_keys'] : array();
			       	} else {
			       		$crnt_child_keys = array();
			       	}

					// Create new item.
			        $nw_quantity = 1;
					add_filter('woocommerce_add_cart_item_data', array($this,'thwma_namespace_force_individual_cart_items'), 10, 3);
			        $passed_validation = apply_filters('woocommerce_add_to_cart_validation', true, $product_id, $nw_quantity);
			        $product_status = get_post_status($product_id);
			        $variation    = array();
			        $variation    = isset($cartitem['variation']) ? $cartitem['variation'] : '';
			        $new_item_key = WC()->cart->add_to_cart($product_id, $nw_quantity, $variation_id, $variation);
			        if ($passed_validation && $new_item_key && 'publish' === $product_status) {
						do_action('woocommerce_ajax_added_to_cart', $product_id);
			            if ('yes' === get_option('woocommerce_cart_redirect_after_add')) {
			            	wc_add_to_cart_message(array($product_id => $nw_quantity), true);
			            }
			            //WC_AJAX :: get_refreshed_fragments();
			        } else {
			            $data = array(
			                'error' => true,
			                'product_url' => apply_filters('woocommerce_cart_redirect_after_error', get_permalink($product_id), $product_id));
			            echo wp_send_json($data);
			        }

			        // New item data saved to cart content.
			        $cart = $woocommerce->cart->cart_contents;
			        if(!empty($crnt_child_keys)) {
			        	$child_keys = $crnt_child_keys;
				       	$child_keys[] = $new_item_key;
				    } else {
				    	$child_keys = array($new_item_key);
				    }
				    $unique_key = rand(1000,9999);
			        $multi_ship_id = 'multi_ship_'.$unique_key;

			        if(array_key_exists($cart_item_key, $cart)) {
						$woocommerce->cart->cart_contents[$cart_item_key]['multi_ship_address']['child_keys'] = $child_keys;
					}
					if(array_key_exists($new_item_key, $cart)) {
				     	$woocommerce->cart->cart_contents[$new_item_key]['multi_ship_address'] = array(
							'product_id' => $product_id,
							'variation_id' => $variation_id,
							'quantity' => '1',
							//'updated_quantity' => $updated_quantity,
							'multi_ship_id' => $multi_ship_id,
							'multi_ship_parent_id' => $parent_item_id,
							'child_keys' => array(),
							'parent_cart_key' => $cart_item_key,
						);
						$woocommerce->cart->set_session();
					}
					
					// Create a new item display.
			        $cart_item_data = array();
			        foreach ($items as $key => $value) {
			        	$cart_item_data = isset($items[$new_item_key]) ? $items[$new_item_key] : '';
			        }
			        
			        $link_show = '0';
    				$multi_shipping_addresses = self::multi_shipping_addresses($cart_item_data, $new_item_key, $link_show, $multi_ship_id);
			        
					$multi_ship_form = '';
					$multi_ship_form .= '<tr class="multi-ship-tr">';
						$multi_ship_form .= '<td>';
							// $multi_ship_form .= '<a id ="remove-multi-ship-tr" class="remove-multi-ship-tr"  onclick="thwma_remove_multi_shipping_tr(event,this)"><span class="dashicons dashicons-dismiss"></span></a>';
						$multi_ship_form .= '</td>';
						$multi_ship_form .= '<td><input type="number" name="pdct-qty" value="1" min="1" class="multi-ship-pdct-qty pdct-qty sub-pdct-qty pdct-qty-'.$new_item_key.'" data-cart_key="'.$new_item_key.'"></td>';
						$multi_ship_form .= '<td><input class="multi-ship-item" type="hidden" data-multi_ship_id="'.$multi_ship_id.'" data-multi_ship_parent_id="'.$parent_item_id.'" data-sub_row_stage="1">'.$multi_shipping_addresses;
							$multi_ship_form .= '<a id ="remove-multi-ship-tr" class="remove-multi-ship-tr"  onclick="thwma_remove_multi_shipping_tr(event,this)"><span class="dashicons dashicons-dismiss"></span></a>';
						$multi_ship_form .= '</td>';
					$multi_ship_form .= '</tr>';
					// if(array_key_exists($parent_item_id, $cart[$cart_item_key]['multi_ship_address'])) {
					// 	//$woocommerce->cart->cart_contents[$cart_item_key]['multi_ship_address'][$parent_item_id]['updated_quantity'] = $updated_quantity;
					// 	$woocommerce->cart->cart_contents[$cart_item_key]['multi_ship_address']['updated_quantity'] = $updated_quantity;
					// 	$woocommerce->cart->set_session();
					// }
					echo $multi_ship_form;
				}
			}
			exit();
		}

		/**
		 * Remove multi-shipping row(checkout page-ajax response).
		 */
		public function remove_multi_shipping_row() {
			global $woocommerce;
			check_ajax_referer( 'remove-multi-shipping-row', 'security' );
			$product_id = isset($_POST['product_id']) ? sanitize_key($_POST['product_id']) : '';
			$cart_item_key = isset($_POST['cart_item_key']) ? sanitize_key($_POST['cart_item_key']) : '';
			$multi_ship_id = isset($_POST['multi_ship_id']) ? sanitize_key($_POST['multi_ship_id']) : '';
			$cart = $woocommerce->cart->cart_contents;
			if($cart) {
				$parent_cart_key = isset($cart[$cart_item_key]['multi_ship_address']['parent_cart_key']) ? $cart[$cart_item_key]['multi_ship_address']['parent_cart_key'] : '';

				if(!empty($parent_cart_key)) {
					$child_keys = $woocommerce->cart->cart_contents[$parent_cart_key]['multi_ship_address']['child_keys'];
					
					if(!empty($cart[$parent_cart_key]['multi_ship_address']['child_keys']) && is_array($cart[$parent_cart_key]['multi_ship_address']['child_keys'])) {
						foreach ($cart[$parent_cart_key]['multi_ship_address']['child_keys'] as $key => $value) {
							if($value == $cart_item_key) {
								unset($cart[$parent_cart_key]['multi_ship_address']['child_keys'][$key]);
							}
						}
							
						$woocommerce->cart->cart_contents[$parent_cart_key]['multi_ship_address']['child_keys'] = $cart[$parent_cart_key]['multi_ship_address']['child_keys'];
						$woocommerce->cart->set_session();
					}
				}
				WC()->cart->remove_cart_item( $cart_item_key );
				
				//$bool = WC_Cart::remove_cart_item( $cart_item_key );
				// if(!empty($cart[$cart_item_key]['multi_ship_address']) && is_array($cart[$cart_item_key]['multi_ship_address'])) {
				// 	foreach ($cart[$cart_item_key]['multi_ship_address'] as $key) {
				// 		unset($cart[$cart_item_key]['multi_ship_address'][$multi_ship_id]);
				// 	}
				// 	$woocommerce->cart->cart_contents[$cart_item_key]['multi_ship_address'] = $cart[$cart_item_key]['multi_ship_address'];
				// 	$woocommerce->cart->set_session();
				// }
			}
			exit();
		}

		/**
		 * update cart qty from multi-shipping address section(checkout page-ajax response).
		 */
		public function update_multi_shipping_qty_field() {
			global $woocommerce;
			check_ajax_referer( 'update-multi-shipping-qty-field', 'security' );
			$quantity = isset($_POST['value']) ? sanitize_key($_POST['value']) : '';
			$cart_item_key = isset($_POST['cart_key']) ? sanitize_key($_POST['cart_key']) : '';
			$woocommerce->cart->set_quantity($cart_item_key, $quantity);

			$settings_manage_style = THWMA_Utils::get_setting_value('settings_manage_style');
			$multi_address_url = isset($settings_manage_style['multi_address_url']) ? $settings_manage_style['multi_address_url']:'';
			if($quantity > 1) {	
				$disabled_class = 'link_enabled_class';
			} else {
				$disabled_class = 'link_disabled_class';
			}
			$display_ppty = 'block';
			$cart_item_data = array();
			$items = $woocommerce->cart->get_cart();
			foreach ($items as $key => $value) {
				$cart_item_data = isset($items[$cart_item_key]) ? $items[$cart_item_key] : '';
			}
			$link_html = '';
			if(!empty($cart_item_data) && is_array($cart_item_data)) {
				foreach($cart_item_data as $key => $datas) {			
					$cart_qty = isset($cart_item_data["quantity"]) ? $cart_item_data["quantity"] : '';
					$product_id = isset($cart_item_data["product_id"]) ? $cart_item_data["product_id"] : '';
					$variation_id = isset($cart_item_data["variation_id"]) ? $cart_item_data["variation_id"] : '';
				}			
				$cart_item_encoded = json_encode($cart_item_data);				
				$link_html .= '<a href="" class="ship_to_diff_adr add-dif-ship-adr '.$disabled_class.'" data-product_id ="'.esc_attr($product_id).'" data-cart_quantity ="'.esc_attr($cart_qty).'" data-variation_id="'.esc_attr($variation_id).'" data-cart_item ='.esc_attr($cart_item_encoded).'  data-cart_item_key = "'.esc_attr($cart_item_key).'" data-updated_qty="'.esc_attr($cart_qty).'" style="display:'.esc_attr($display_ppty).'">'.esc_html__($multi_address_url, "woocommerce-multiple-addresses-pro").'</a>';
			}
			echo $link_html;
			exit();
		}

		/**
		 * Custom addresses displayed in dropdown format on cart page(checkout page).
		 *
         * @param array $cart_item The cart item data
         * @param string $cart_item_key The cart item key
		 */
		public static function multi_shipping_addresses($cart_item, $cart_item_key, $link_show=false, $multi_ship_id=false) {
			$ex_pdt_ids = array();
			$cart_qty = '';
			$variation_id = '';
			$product_id = '';
			if(!empty($cart_item) && is_array($cart_item)) {
				foreach($cart_item as $key => $datas) {			
					$cart_qty = isset($cart_item["quantity"]) ? $cart_item["quantity"] : '';
					$product_id = isset($cart_item["product_id"]) ? $cart_item["product_id"] : '';
					$variation_id = isset($cart_item["variation_id"]) ? $cart_item["variation_id"] : '';
				}
			}
			$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$settings_guest_urs = THWMA_Utils::get_setting_value('settings_guest_users');			
			if($settings && !empty($settings)) {
				$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping'] : '';
				$enable_product_variation = isset($settings['enable_product_variation']) ? $settings['enable_product_variation'] : '';
				$enabled_guest_user = isset($settings_guest_urs['enable_guest_shipping']) ? $settings_guest_urs['enable_guest_shipping'] : '';
				$exclude_products = isset($settings['exclude_products']) ? $settings['exclude_products'] : '';
				$exclude_category = isset($settings['exclude_category']) ? $settings['exclude_category'] : '';
				$excl_pdt_ids = array();
				if(!empty($exclude_products) && is_array($exclude_products)) {
					foreach ($exclude_products as $ex_key => $ex_value) {
						$product_obj = get_page_by_path($ex_value, OBJECT, 'product');
						if($product_obj != '') {
							$excl_pdt_ids[] = $product_obj->ID;
						}
					}
				}
				$category_pdt_id = array();
				if(!empty($exclude_category) && is_array($exclude_category)) {
					foreach ($exclude_category as $ex_cat_key => $ex_cat_value) {
						$args = array('post_type' => 'product', 'product_cat' => $ex_cat_value, 'orderby' =>'rand');
						if(!empty($args)) {
							$loop = new WP_Query($args);
							while ($loop->have_posts()) : $loop->the_post();
							global $product; 
							$category_pdt_id[] = get_the_ID();							
							endwhile;wp_reset_query(); 
						}
					}
				}
				$ex_pdt_ids = array_merge($excl_pdt_ids, $category_pdt_id);
				$time_duration = isset($settings['set_time_duration']) ? $settings['set_time_duration']:'';
				$user_id = get_current_user_id();
		        $enable_multi_ship = '';
		        if (is_user_logged_in()) {
		        	$enable_multi_ship = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
		        } else {
		        	$enable_multi_ship = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP])?$_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]:'';
		        }
		        $multi_ship_dpdwn = '';
				if (is_user_logged_in()) {

					// Default address of logged in user.
					$user_id = get_current_user_id();
					$default_address = THWMA_Utils::get_default_address($user_id, 'shipping');
					$shipp_addr_format = THWMA_Utils::get_formated_address('shipping', $default_address);
					$default_shipp_addr = '';
					if(apply_filters('thwma_inline_address_display', true)) {
						$separator = ', ';
						$default_shipp_addr = WC()->countries->get_formatted_address($shipp_addr_format, $separator);
					} else {
						$default_shipp_addr = WC()->countries->get_formatted_address($shipp_addr_format);
					}

					if($cart_shipping_enabled == 'yes') {
						if($variation_id != '') {
							if($enable_product_variation == 'yes') {
								if(empty($ex_pdt_ids)) {
									$multi_ship_dpdwn = self::multi_shipping_dropdown_div($cart_item_key, $cart_item, $link_show, $multi_ship_id);
								} else if(!empty($ex_pdt_ids)) {
									if(!in_array($product_id, $ex_pdt_ids)) {
										$multi_ship_dpdwn = self::multi_shipping_dropdown_div($cart_item_key, $cart_item, $link_show, $multi_ship_id);
									} else {
										$multi_ship_dpdwn = '<p>'.$default_shipp_addr.'</p>';
									}
								}
							} else {
								$multi_ship_dpdwn = '<p>'.$default_shipp_addr.'</p>';
							}					
						} else {
							if(empty($ex_pdt_ids)) {
								$multi_ship_dpdwn = self::multi_shipping_dropdown_div($cart_item_key, $cart_item, $link_show, $multi_ship_id);
							} else if(!empty($ex_pdt_ids)) {
								if(!in_array($product_id, $ex_pdt_ids)) {
									$multi_ship_dpdwn = self::multi_shipping_dropdown_div($cart_item_key, $cart_item, $link_show, $multi_ship_id);
								} else {
									$multi_ship_dpdwn = '<p>'.$default_shipp_addr.'</p>';
								}
							}
						}
					}
				} else {			
					if($enabled_guest_user == 'yes') {
						if($cart_shipping_enabled == 'yes') {
							if($cart_shipping_enabled == 'yes') {
								if($variation_id != '') {
									if($enable_product_variation == 'yes') {
										if(empty($ex_pdt_ids)) {
											$multi_ship_dpdwn = self::multi_shipping_dropdown_div($cart_item_key, $cart_item, $link_show, $multi_ship_id);
										} else if(!empty($ex_pdt_ids)) {
											if(!in_array($product_id, $ex_pdt_ids)) {
												$multi_ship_dpdwn = self::multi_shipping_dropdown_div($cart_item_key, $cart_item, $link_show, $multi_ship_id);
											} else {
												$multi_ship_dpdwn = '<p>Multi shipping not available</p>';
											}
										}
									} else {
										$multi_ship_dpdwn = '<p>Multi shipping not available</p>';
									}					
								} else {
									if(empty($ex_pdt_ids)) {
										$multi_ship_dpdwn = self::multi_shipping_dropdown_div($cart_item_key, $cart_item,  $link_show, $multi_ship_id);
									} else if(!empty($ex_pdt_ids)) {
										if(!in_array($product_id, $ex_pdt_ids)) {
											$multi_ship_dpdwn = self::multi_shipping_dropdown_div($cart_item_key, $cart_item, $link_show, $multi_ship_id);
										} else {
											$multi_ship_dpdwn = '<p>Multi shipping not available</p>';
										}
									}
								}
							}
						}
					}
				}
				return $multi_ship_dpdwn;
			}
		}

		/**
         * Function for set multi shipping drop down div( checkout page)
         * 
         * @param array $cart_item The cart item data
         * @param string $cart_item_key The cart item key
         */ 
		public static function multi_shipping_dropdown_div($cart_item_key, $cart_item = null, $link_show=false, $multi_ship_id=false) {
			$cart_qty = '';
			$settings_manage_style = THWMA_Utils::get_setting_value('settings_manage_style');
			$multi_address_url = isset($settings_manage_style['multi_address_url']) ? $settings_manage_style['multi_address_url']:'';
			$product_id = '';
			$variation_id = '';
			if(!empty($cart_item) && is_array($cart_item)) {
				foreach($cart_item as $key => $datas) {			
					$cart_qty = isset($cart_item["quantity"]) ? $cart_item["quantity"] : '';
					$product_id = isset($cart_item["product_id"]) ? $cart_item["product_id"] : '';
					$variation_id = isset($cart_item["variation_id"]) ? $cart_item["variation_id"] : '';
				}
			}
			$user_id = get_current_user_id();
	        $enable_multi_ship = '';
	        if (is_user_logged_in()) {
	        	$enable_multi_ship = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
	        } else {
	        	$enable_multi_ship = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]) ? $_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP] : '';
	        }
	        if($enable_multi_ship == 'yes') {
	        	$display_ppty = 'block';
	        } else {
	        	$display_ppty = 'none';
	        }
	        $dpdwn_div = '';
			$dpdwn_div .= '<div id="thwma_cart_multi_shipping_display" class="thwma_cart_multi_shipping_display thwma_cart_multi_shipping_display_'.esc_attr($cart_item_key).'" style="display:'.esc_attr($display_ppty).'">';
				$dpdwn_div .= '<input type="hidden" name="hiden_qty_key" class="hiden_qty_key hiden_qty_key_1" value="4" data-field_name="cart['.esc_attr($cart_item_key).'][qty]_1" data-qty_hd_key="1">';
				$dpdwn_div .=  self::multi_shipping_dropdown_view($cart_item_key, $cart_item, $multi_ship_id);
			$dpdwn_div .= '</div>';
			$custom_addresses = self::get_saved_custom_addresses_from_db();
			if($custom_addresses) {
				$cart_item_encoded = json_encode($cart_item);
				$dpdwn_div .= '<input type="hidden" value="1" name="ship_to_diff_hidden" class="ship_to_diff_hidden">';

				$disabled_class = '';
				if($enable_multi_ship == 'yes') {
					if($link_show == false) {
						$disabled_class = 'link_disabled_class';
						$display_ppty = 'none';
					} else {
						$disabled_class = 'link_enabled_class';
						$display_ppty = 'block';
					}
				} else {
					$display_ppty = 'none';
				}
				$dpdwn_div .= '<a href="" class="ship_to_diff_adr add-dif-ship-adr '.$disabled_class.'" data-product_id="'.esc_attr($product_id).'" data-cart_quantity="'.esc_attr($cart_qty).'" data-variation_id="'.esc_attr($variation_id).'" data-cart_item='.esc_attr($cart_item_encoded).'  data-cart_item_key= "'.esc_attr($cart_item_key).'" data-updated_qty="'.esc_attr($cart_qty).'" style="display:'.esc_attr($display_ppty).'">'.esc_html__($multi_address_url, "woocommerce-multiple-addresses-pro").'</a>';
			} else {
				if (!is_user_logged_in()) {
					$cart_item_encoded = json_encode($cart_item);
					$dpdwn_div .= '<input type="hidden" value="1" name="ship_to_diff_hidden" class="ship_to_diff_hidden">';

					$disabled_class = '';
					if($enable_multi_ship == 'yes') {
						if($link_show == false) {
							$disabled_class = 'link_disabled_class';
							$display_ppty = 'none';
						} else {
							$disabled_class = 'link_enabled_class';
							$display_ppty = 'block';
						}
					} else {
						$display_ppty = 'none';
					}
					$dpdwn_div .= '<a href="" class="ship_to_diff_adr add-dif-ship-adr '.$disabled_class.'" data-product_id="'.esc_attr($product_id).'" data-cart_quantity="'.esc_attr($cart_qty).'" data-variation_id="'.esc_attr($variation_id).'" data-cart_item='.esc_attr($cart_item_encoded).'  data-cart_item_key= "'.esc_attr($cart_item_key).'" data-updated_qty="'.esc_attr($cart_qty).'" style="display:'.esc_attr($display_ppty).'">'.esc_html__($multi_address_url, "woocommerce-multiple-addresses-pro").'</a>';
				}
			}
			return $dpdwn_div;
		}

		/*
         * Function for set the dropdown div content( checkout page-multi shipping).
         *
         * @param array $cart_item The cart item data
         * @param int $multi_ship_id The multi shipping is
         * @param string $cart_item_key The cart item key
         */
		public static function multi_shipping_dropdown_view($cart_item_key, $cart_item = null, $multi_ship_id=false) {
			$multi_shipping_list = null;
			$data_field_name = null;
			$customer_id = get_current_user_id();			
			$default_ship_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_shipping');
			$same_address = THWMA_Utils::is_same_address_exists($customer_id, 'shipping');
			$default_address = $default_ship_address ? $default_ship_address : $same_address;
			$default_address = $same_address ? $same_address : $default_ship_address;
			$address_limit = THWMA_Utils::get_setting_value('settings_shipping', 'shipping_address_limit');
            if (!is_numeric($address_limit)) {
                $address_limit = 0;
            }

			$options = array();
			$settings_guest_urs = THWMA_Utils::get_setting_value('settings_guest_users');
			$enabled_guest_user = isset($settings_guest_urs['enable_guest_shipping']) ? $settings_guest_urs['enable_guest_shipping']:'';
			if(is_user_logged_in()) {
				$custom_addresses = THWMA_Utils::get_custom_addresses($customer_id, 'shipping');
			} else {
				if($enabled_guest_user == 'yes') {
					$custom_addresses = THWMA_Utils::get_custom_addresses_of_guest_user('shipping');
				}
			}
			if(is_array($custom_addresses)) {
	        	$custom_address = $custom_addresses;
	        } else {
				$custom_address = array();
				$def_address = THWMA_Utils::get_default_address($customer_id, 'shipping');				
				if(array_filter($def_address) && (count(array_filter($def_address)) > 2)) {
					$custom_address ['selected_address'] = $def_address;
				}
			}
			if($custom_address) {
				$address_count = count($custom_address);
				if($address_limit && ($address_count > 0)) { 	
					$shipping_heading = (isset($custom_address[$default_address]['shipping_heading']) && $custom_address[$default_address]['shipping_heading'] !='') ? $custom_address[$default_address]['shipping_heading'] : esc_html__('', '');
					if($default_address) {
						if(isset($options[$default_address])) {
							$options[$default_address] = $shipping_heading .' - '. $custom_address[$default_address]['shipping_address_1'];
						}
					} else {
						$default_address = 'selected_address';
						//$options[$default_address] = esc_html__('Shipping Address', 'woocommerce-multiple-addresses-pro');
					}
					if(is_array($custom_address)){

						// Default address.
						foreach ($custom_address as $key => $address_values) {
							$adrsvalues_to_dd = array();
							if($key == $default_address) {								
								$heading = (isset($address_values['shipping_heading']) && $address_values['shipping_heading'] != '') ? $address_values['shipping_heading'] : esc_html__('', '');
								if(apply_filters('thwma_remove_dropdown_address_format', true)) {
									if(!empty($address_values) && is_array($address_values)) {
										foreach ($address_values as $adrs_key => $adrs_value) {
											if($adrs_key == 'shipping_address_1' || $adrs_key =='shipping_address_2' || $adrs_key =='shipping_city' || $adrs_key =='shipping_state' || $adrs_key =='shipping_postcode') {
												if($adrs_value) {
													$adrsvalues_to_dd[] = $adrs_value;
												}
											}
										}
									}
								} else {
									$type = 'shipping';
									$separator = '</br>';
									$new_address = $custom_address[$default_address];
									$new_address_format = THWMA_Utils::get_formated_address($type, $new_address);
									$options_arr = WC()->countries->get_formatted_address($new_address_format);
									$adrsvalues_to_dd = explode('<br/>', $options_arr);
								}
							}
							$adrs_string = implode(', ', $adrsvalues_to_dd);
							//if((isset($heading)) && ($heading!= '') && (!is_array($heading))) {
							if((isset($heading)) && ($heading != '')  && (!is_array($heading))) {
								$options[$key] = $heading .' - '.$adrs_string;
							} else {
								$options[$key] = $adrs_string;
							}
							$options = array_filter($options);
						}

						// Custom addresses.
						$i = 0;

						$no_of_tile_display = '';							
						if(is_user_logged_in()) {
							$default_ship_address = THWMA_Utils::get_custom_addresses($customer_id, 'default_shipping');
							$same_address = THWMA_Utils::is_same_address_exists($customer_id, 'shipping');
							$get_default_address = $same_address ? $same_address : $default_ship_address ;
							if(!empty($get_default_address)) {
								$no_of_tile_display = 1;
							} else {
								$no_of_tile_display = 0;
							}
						}
						if(($address_limit > $no_of_tile_display) || (!is_user_logged_in())) {
							foreach ($custom_address as $key => $address_values) {
								if($key != $default_address) {
									$heading = isset($address_values['shipping_heading']) && $address_values['shipping_heading'] != '' ? $address_values['shipping_heading'] : esc_html__('', 'woocommerce-multiple-addresses-pro');
									$adrsvalues_to_dd = array();
									if(apply_filters('thwma_remove_dropdown_address_format', true)) {
										if(!empty($address_values) && is_array($address_values)) {
											foreach ($address_values as $adrs_key => $adrs_value) {
												if($adrs_key == 'shipping_address_1' || $adrs_key =='shipping_address_2' || $adrs_key =='shipping_city' || $adrs_key =='shipping_state' || $adrs_key =='shipping_postcode') {
													if($adrs_value) {
														$adrsvalues_to_dd[] = $adrs_value;
													}
												}
											}
										}
									} else {
										$type = 'shipping';
										$separator = '</br>';
										$new_address = array_key_exists($key, $custom_address) ? $custom_address[$key] : array();

										$new_address_format = THWMA_Utils::get_formated_address($type,$new_address);
										$options_arr = WC()->countries->get_formatted_address($new_address_format);
										$adrsvalues_to_dd = explode('<br/>', $options_arr);
									}
									$adrs_string = implode(',', $adrsvalues_to_dd);
									if((isset($heading)) && ($heading != '') && (!is_array($heading))) {
										$options[$key] = $heading .' - '.$adrs_string;
									} else {
										$options[$key] = $adrs_string;
									}

									// Multi-shipping drop-down.
									if(!is_user_logged_in()) {
										if($i >= $address_limit-1) {
			                                break;
			                            }
									} else {
										// if($i >= $address_limit-2) {
			       //                          break;
			       //                      }
			                            if(!empty($get_default_address)){
											if($i >= $address_limit-2) {
							                    break;
							                }
							            } else {
							            	if($i >= $address_limit-1) {
							                    break;
							                }
							            }
			                        }
		                            $i++;
		                        }					
							}
						}
					}
				}
				// $address_count = count($custom_address);
				// $address_limit = THWMA_Utils::get_setting_value('settings_shipping', 'shipping_address_limit');
				// $disable_adr_mngmnt = self::disable_address_mnagement();
				// if($disable_adr_mngmnt != true) {
				// 	if(((int)($address_limit)) > $address_count) {
				// 		$options['add_address'] = esc_html__('Add New Address', 'woocommerce-multiple-addresses-pro');
				// 	}
				// }
			} else {
				$default_address = 'selected_address';
				if (!is_user_logged_in()) {
					$options[] = '';
				}			
			}

			//$options = array_filter($options);
			$cart_shipping_class = 'cart_shipping_adr_slct';
			//$default_address = 'selected_address';
			$cart_item_data = $cart_item;
			$alt_field = array(
				'required' => false,
				'class'    => array('form-row form-row-wide enhanced_select', 'select2-selection',$cart_shipping_class),
				'clear'    => true,
				'type'     => 'select',
				'label'    => THWMA_Utils::get_setting_value('settings_shipping', 'shipping_display_text'),
				//'placeholder' =>esc_html__('Choose an Address..', ''),
				'options'  => $options
			);
			$dropdown_fields = self::thwma_shipping_dropdown_fields(self::DEFAULT_SHIPPING_ADDRESS_KEY, $alt_field,  $multi_ship_id, $cart_item_key, $default_address, $cart_item_data, $multi_shipping_list, $data_field_name);
			return $dropdown_fields;
		}

		/**
		 * Drowpdown core function on cart page multi shipping (cart page)
         * 
         * @param string $key The key value
         * @param array $args The argument data
         * @param string $value The field value
         * @param array $cart_item The cart item data
         * @param array $multi_shipping_list The shipping address list
         * @param string $data_field_name The data field name
		 * 
		 * @return void.
		 */
		public static function thwma_shipping_dropdown_fields($key, $args,  $multi_ship_id, $cart_item_key, $value = null, $cart_item = null, $multi_shipping_list = null, $data_field_name = null) {
			$defaults = array(
				'type'              => '',
				'label'             => '',
				'description'       => '',
				'placeholder'       => '',
				'maxlength'         => false,
				'required'          => false,
				'autocomplete'      => false,
				'id'                => $key,
				'class'             => array(),
				'label_class'       => array(),
				'input_class'       => array(),
				'return'            => false,
				'options'           => array(),
				'custom_attributes' => array(),
				'validate'          => array(),
				'default'           => '',
				'autofocus'         => '',
				'priority'          => '',
			);
			$args = wp_parse_args($args, $defaults);
			$field           = '';
			$label_id        = isset($args['id']) ? $args['id'] : '';
			$sort            = isset($args['priority']) ? $args['priority'] : '';
			$field_container = '<p class="form-row %1$s" id="%2$s-multi-ship" data-priority="' . esc_attr($sort) . '">%3$s</p>';
			$field   = '';
			$options = '';
			$optgroup = '';

			// Local Pickup
			// $settings = get_option(THWMA_Utils::OPTION_KEY_THWMA_SETTINGS);
			// if(!empty($settings)) {
			// 	$store_pickup = isset($settings['settings_store_pickup'])?$settings['settings_store_pickup']:array();
			// 	if(!empty($store_pickup)) {
			// 		$local_pickup_enabled = isset($store_pickup['enable_multi_store_pickup'])?$store_pickup['enable_multi_store_pickup']:array();
			// 		if($local_pickup_enabled == 'yes') {
			// 			$store_addresses = array();
			// 			for($i = 1; $i <=2; $i++) {
			// 				$store_addresses[] =$store_pickup['store_addresses'.$i];
			// 			}
			// 			
			// 		}
			// 	}
			// }

			$custom_attributes = array();
			$product_id = isset($cart_item["product_id"]) ? $cart_item["product_id"] : '';
			$cart_key = isset($cart_item["key"]) ? $cart_item["key"] : '' ;
			$cart_key = $cart_item_key;

			// Address limit.
			$address_limit = THWMA_Utils::get_setting_value('settings_shipping', 'shipping_address_limit');
            if (!is_numeric($address_limit)) {
                $address_limit = 0;
            }

			if(!empty($multi_shipping_list)) {
				$exist_multi_shipping_list = 'exist_multi_shipping_list';
				$key_multi_shipping_list = '';
			} else {
				$exist_multi_shipping_list = '';
				$key_multi_shipping_list = '';
			}								
			if (!empty($args['options']) && is_array($args['options']) ) {
				$new_options = array_filter($args['options']);
				foreach ($new_options as $option_key => $option_text) {	
					if (empty($args['placeholder'])) {
						$args['placeholder'] = $option_text ? $option_text : esc_html__('Choose an option', 'woocommerce-multiple-addresses-pro');
					}
					$custom_attributes[] = 'data-allow_clear="true"';
					$shipping_addr = isset($cart_item['product_shipping_address']) ? $cart_item['product_shipping_address']: '';

					if(!empty($shipping_addr)) {
						$value = $shipping_addr;
					}

					if(!empty($multi_shipping_list)) {
						if(!empty($multi_shipping_list['quantity_data']) && is_array($multi_shipping_list['quantity_data'])) {
							foreach ($multi_shipping_list['quantity_data'] as $shipping_key => $shipping_value) {
								if(isset($shipping_value[$data_field_name])) {
									$value = $shipping_value[$data_field_name]['shipping_address'];
								}
							}
						}
					}
					
					//if($option_key != null){
						$options .= '<option value="' . esc_attr($option_key) . '" ' . selected($value, $option_key, false) . ' >' . esc_attr($option_text) . '</option>';
					//}
				}
				if($address_limit) {
					$optgroup .= '<option value="" >'.esc_html__('Select Address', 'woocommerce-multiple-addresses-pro').'</option>';
				}
				$optgroup .= $options ;

				$field .= '<select name="' . esc_attr($key) . '" id="' . esc_attr($args['id'].'_'.$product_id.'_'.$cart_key) . '" class="thwma-cart-shipping-options select ' . esc_attr(implode(' ', $args['input_class'])) . '" ' . implode(' ', $custom_attributes) . ' data-placeholder="' . esc_attr($args['placeholder']) . '" data-product_id="'.esc_attr($product_id).'" data-cart_key="'.esc_attr($cart_key).'" data-exist_multi_adr = "'.esc_attr($exist_multi_shipping_list).'" data-key_multi_adr = "'.esc_attr($key_multi_shipping_list).'">' . $optgroup . '</select>';
			}
			if (! empty($field)) {
				$field_html = '';
				$field_html .= '<span class="woocommerce-input-wrapper">' . $field;
				if ($args['description']) {
					$field_html .= '<span class="description" id="' . esc_attr($args['id']) . '-description" aria-hidden="true">' . wp_kses_post($args['description']) . '</span>';
				}
				$field_html .= '</span>';
				$container_class = esc_attr(implode(' ', $args['class']));
				$container_id    = esc_attr($args['id']) . '_field';
				$field           = sprintf($field_container, $container_class, $container_id, $field_html);
			}
			return $field;
			//echo $field; // WPCS: XSS ok.			
		}

		/**
		 * Save selected multi shipping address on cart item data(checkout page - ajax response).
		 * 
		 * @return void
		 */	
		public function save_multi_selected_shipping() {
			global $woocommerce;
			check_ajax_referer( 'save-multi-selected-shipping', 'security' );
			$user_id = get_current_user_id();
	        $enable_multi_ship = '';
	        if (is_user_logged_in()) {
	        	$enable_multi_ship = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
	        } else {
	        	$enable_multi_ship = isset($_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP]) ? $_COOKIE[THWMA_Utils::GUEST_KEY_ENABLE_MULTI_SHIP] : '';
	        }
			$shipping_name = isset($_POST['value']) ? $_POST['value'] : '';
			$product_id = isset($_POST['product_id']) ? $_POST['product_id'] : '';
			$cart_key = isset($_POST['cart_key']) ? $_POST['cart_key'] : '';
			$multi_ship_id = isset($_POST['multi_ship_id']) ? $_POST['multi_ship_id'] : '';
			//$multi_ship_id = isset($_POST['multi_ship_id']) ? $_POST['multi_ship_id'] : '';

			// $this->save_multi_selected_shipping_addresses($cart_key);

			$cart = $woocommerce->cart->cart_contents;
			$input_value = '';	

			// Get shipping custom fields.
			// $shipping_adrs = THWMA_Utils::get_custom_addresses($user_id, 'shipping', $shipping_name);
			// $custom_fields = $this->get_shipping_custom_fields_from_addresses($shipping_adrs);
			// $custom_address = THWMA_Utils::get_custom_addresses_of_guest_user('shipping');	
			// if(count($custom_address) == 1) {
			// 	foreach ($custom_address as $key => $value) {
			// 		$first_custom_address = $value;
			// 	}
			// }
			// $new_address = $first_custom_address;

			// $shipp_addr_format = THWMA_Utils::get_formated_address('shipping', $new_address);
			// if(apply_filters('thwma_inline_address_display', true)) {
			// 	$separator = ', ';
			// 	$pdt_shipp_addr_formated = WC()->countries->get_formatted_address($shipp_addr_format, $separator);
			// } else {
			// 	$pdt_shipp_addr_formated = WC()->countries->get_formatted_address($shipp_addr_format);
			// }
			// $response = array(
			// 	'first_address' => $pdt_shipp_addr_formated,
			// );
			// wp_send_json($response);

			if(!empty($cart) && is_array($cart)){			
				foreach ($cart as $key => $item) {
				    if($key == $cart_key) {
				    	$woocommerce->cart->cart_contents[$key]['product_shipping_address'] = $shipping_name;
				        $woocommerce->cart->cart_contents[$key]['multi_ship_address']['ship_address'] = $shipping_name;
			        
				        //$woocommerce->cart->cart_contents[$key]['thwma_custom_fields'] = $custom_fields;
				        sleep(4);
					}
				}
				$woocommerce->cart->set_session();
			}
			exit();
		}

		/**
         * Function for guest- first multi-shipping management form( checkout page-qust add first address).
         *
         * @return string
         */
		public static function guest_multiple_address_management_form() {
			global $woocommerce;
			$items = $woocommerce->cart->get_cart();
    		$custom_addresses = self::get_saved_custom_addresses_from_db();
    		$multi_ship_form = '';
    		if(empty($custom_addresses)) {
    			$multi_ship_form .= '<div class="multi-shipping-table-wrapper">';
					$multi_ship_form .= '<div class="multi-shipping-table-overlay"></div>';
					$multi_ship_form .= '<table class="multi-shipping-table">';
		    			$multi_ship_form .= '<tr>';
		    				$multi_ship_form .= '<th>Product</th>';
		    				$multi_ship_form .= '<th>Quantity</th>';
		    				$multi_ship_form .= '<th>Sent to:</th>';
		    			$multi_ship_form .= '</tr>';
		    			$i = 1;
		        		$ini_stage = 0;
		    			foreach ($items as $key => $value) {
		    				$multi_ship_id = 'multi_ship_'.$i; 
		    				$product = wc_get_product( $value[ 'product_id' ] );
		    				
		    				$cart = $woocommerce->cart->cart_contents;	
		    				$qty = $value['quantity'];	
							$multi_ship_parent_id = '';

							$cart_item = $items[$key];							
					    	$_product   = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $key );

					    	$child_keys = '';
					    	if($qty <= 1) {
					    		$link_show = false;	
					    	} else {
					    		$link_show = true;
					    	}
					    	$multi_shipping_addresses = self::multi_shipping_addresses($value, $key, $link_show, $multi_ship_id);

					    	// Check the product is vertual or downloadable.
					    	if ((!$product->is_downloadable('yes'))) {		    
			    				if(!empty($cart[$key]['multi_ship_address'])) {
			    					$multi_ship_content = $cart[$key]['multi_ship_address'];
			    					if(!empty($multi_ship_content) && is_array($multi_ship_content)) {
				    					foreach ( $multi_ship_content as $multi_ship_datas) {
				    						$multi_ship_parent_id = $multi_ship_content['multi_ship_parent_id'];
				    						$child_keys = $multi_ship_content['child_keys'];
				    					}
				    				}
				    				//$child_keys = $child_keys_data;
				    				// $child_keys_data = $child_keys;
				    				
									if($multi_ship_parent_id == null) {
										if($qty == 1) {
					        				$link_show = false;
					        			} else {
					        				$link_show = true;
					        			}
					        			$multi_shipping_addresses = self::multi_shipping_addresses($value, $key, $link_show, $multi_ship_id);
										$multi_ship_form .= '<tr class="main-pdct-tr">';
						    				$multi_ship_form .= '<td class="wmap-img-tr">';
						    					$multi_ship_form .= '<div class="checkout-thumbnail-img">';
							    					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $items ) : '', $items, $key );
								    				//$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $items, $key );
								    				$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $key );

													if ( ! $product_permalink ) {
														echo $thumbnail; // PHPCS: XSS ok.
													} else {
														$multi_ship_form .= '<a href="'.esc_url( $product_permalink ).'">'.$thumbnail.'</a>';
													}
								    				//$multi_ship_form .= '<p class="product-thumb-name">'.$_product->get_name().'</p>';
								    				$multi_ship_form .= '<p class="product-thumb-name">';
														if ( ! $product_permalink ) {
															$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $key ) . '&nbsp;' );
														} else {
															$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $key ) );
														}

														$multi_ship_form .=  wc_get_formatted_cart_item_data( $cart_item );
													$multi_ship_form .= '</p>';
									    			//do_action( 'woocommerce_after_cart_item_name', $cart_item, $key );

								    			$multi_ship_form .= '</div>';
						    				$multi_ship_form .= '</td>';
						    				$multi_ship_form .= '<td><input type="number" min="1" name="pdct-qty" value="'.$qty.'" class="multi-ship-pdct-qty pdct-qty main-pdct-qty pdct-qty-'.$key.'" data-cart_key="'.$key.'"></td>';
						    				$multi_ship_form .= '<td><input class="multi-ship-item" type="hidden" data-multi_ship_id="multi_ship_'.$i.'" data-multi_ship_parent_id="0" data-updated_qty="'.$qty.'" data-sub_row_stage="1">'.$multi_shipping_addresses.'</td>';
						    			$multi_ship_form .= '</tr>';

					    				if(!empty($child_keys) && is_array($child_keys)) {
					    					foreach ( $child_keys as $item_key) {
						    					if(!empty($cart[$item_key])) {
					    							$cart_data = $cart[$item_key];
					    							$child_qty = isset($cart[$item_key]['quantity']) ? $cart[$item_key]['quantity'] : '';
					    							$multi_ship_id = $cart[$item_key]['multi_ship_address']['multi_ship_id'];
													$link_show = false;
					    							$multi_shipping_addresses = self::multi_shipping_addresses($cart_data, $item_key, $link_show, $multi_ship_id);

					    							$multi_ship_form .= '<tr>';
									    				$multi_ship_form .= '<td>';
											    			// $multi_ship_form .= '<a id ="remove-multi-ship-tr" class="remove-multi-ship-tr"  onclick="thwma_remove_multi_shipping_tr(event,this)"><span class="dashicons dashicons-dismiss"></span></a>';
									    				$multi_ship_form .= '</td>';
									    				$multi_ship_form .= '<td>';
									    					$multi_ship_form .= '<input type="number" min="1" name="pdct-qty" value="'.$child_qty.'" class="multi-ship-pdct-qty pdct-qty sub-pdct-qty pdct-qty-'.$item_key.'" data-cart_key="'.$item_key.'">';
									    					$multi_ship_form .= '</td>';
									    				$multi_ship_form .= '<td>';
									    					$multi_ship_form .= '<input class="multi-ship-item" type="hidden" data-multi_ship_id="'.$multi_ship_id.'" data-multi_ship_parent_id="0" data-updated_qty="'.$child_qty.'" data-sub_row_stage="1">'.$multi_shipping_addresses;
									    					$multi_ship_form .= '<a id ="remove-multi-ship-tr" class="remove-multi-ship-tr"  onclick="thwma_remove_multi_shipping_tr(event,this)"><span class="dashicons dashicons-dismiss"></span></a>';
									    				$multi_ship_form .= '</td>';
									    			$multi_ship_form .= '</tr>';
					    						}
					    					}
					    				}			    				
					    			} else {
					    				$parent_cart_key = isset($multi_ship_content['parent_cart_key']) ? $multi_ship_content['parent_cart_key'] : '';
					    				$child_keys = isset($multi_ship_content['child_keys']) ? $multi_ship_content['child_keys'] : array();

					    				if (!array_key_exists($parent_cart_key, $items)){
						    				if($qty == 1) {
						        				$link_show = false;
						        			} else {
						        				$link_show = true;
						        			}
						        			$multi_shipping_addresses = self::multi_shipping_addresses($value, $key, $link_show, $multi_ship_id);
											$multi_ship_form .= '<tr class="main-pdct-tr">';
							    				$multi_ship_form .= '<td class="wmap-img-tr">';
							    					$multi_ship_form .= '<div class="checkout-thumbnail-img">';
								    					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $items ) : '', $items, $key );
									    				//$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $items, $key );
									    				$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $key );

														if ( ! $product_permalink ) {
															echo $thumbnail; // PHPCS: XSS ok.
														} else {
															$multi_ship_form .= '<a href="'.esc_url( $product_permalink ).'">'.$thumbnail.'</a>';
														}
									    				//$multi_ship_form .= '<p class="product-thumb-name">'.$_product->get_name().'</p>';
									    				$multi_ship_form .= '<p class="product-thumb-name">';
															if ( ! $product_permalink ) {
																$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $key ) . '&nbsp;' );
															} else {
																$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $key ) );
															}

															$multi_ship_form .=  wc_get_formatted_cart_item_data( $cart_item );
														$multi_ship_form .= '</p>';
										    			//do_action( 'woocommerce_after_cart_item_name', $cart_item, $key );

									    			$multi_ship_form .= '</div>';
							    				$multi_ship_form .= '</td>';
							    				$multi_ship_form .= '<td><input type="number" min="1" name="pdct-qty" value="'.$qty.'" class="multi-ship-pdct-qty pdct-qty main-pdct-qty pdct-qty-'.$key.'" data-cart_key="'.$key.'"></td>';
							    				$multi_ship_form .= '<td><input class="multi-ship-item" type="hidden" data-multi_ship_id="multi_ship_'.$i.'" data-multi_ship_parent_id="0" data-updated_qty="'.$qty.'" data-sub_row_stage="1">'.$multi_shipping_addresses.'</td>';
							    			$multi_ship_form .= '</tr>';

							    			// Update multi ship parent id.
							    			$woocommerce->cart->cart_contents[$key]['multi_ship_address']['multi_ship_parent_id'] = '0';
					    					$woocommerce->cart->set_session();
					    					if(!empty($child_keys) && is_array($child_keys)) {
						    					foreach ( $child_keys as $item_key) {
							    					if(!empty($cart[$item_key])) {
						    							$cart_data = $cart[$item_key];
						    							$child_qty = isset($cart[$item_key]['quantity']) ? $cart[$item_key]['quantity'] : '';
						    							$multi_ship_id = $cart[$item_key]['multi_ship_address']['multi_ship_id'];
														$link_show = false;
						    							$multi_shipping_addresses = self::multi_shipping_addresses($cart_data, $item_key, $link_show, $multi_ship_id);

						    							$multi_ship_form .= '<tr>';
										    				$multi_ship_form .= '<td>';
												    			// $multi_ship_form .= '<a id ="remove-multi-ship-tr" class="remove-multi-ship-tr"  onclick="thwma_remove_multi_shipping_tr(event,this)"><span class="dashicons dashicons-dismiss"></span></a>';
										    				$multi_ship_form .= '</td>';
										    				$multi_ship_form .= '<td>';
										    					$multi_ship_form .= '<input type="number" min="1" name="pdct-qty" value="'.$child_qty.'" class="multi-ship-pdct-qty pdct-qty sub-pdct-qty pdct-qty-'.$item_key.'" data-cart_key="'.$item_key.'">';
										    					$multi_ship_form .= '</td>';
										    				$multi_ship_form .= '<td>';
										    					$multi_ship_form .= '<input class="multi-ship-item" type="hidden" data-multi_ship_id="'.$multi_ship_id.'" data-multi_ship_parent_id="0" data-updated_qty="'.$child_qty.'" data-sub_row_stage="1">'.$multi_shipping_addresses;
										    					$multi_ship_form .= '<a id ="remove-multi-ship-tr" class="remove-multi-ship-tr"  onclick="thwma_remove_multi_shipping_tr(event,this)"><span class="dashicons dashicons-dismiss"></span></a>';
										    				$multi_ship_form .= '</td>';
										    			$multi_ship_form .= '</tr>';
						    						}
						    					}
						    				}
								    	}
					    			}
			    				} else {
				    				$multi_ship_form .= '<tr class="main-pdct-tr">';
					    				$multi_ship_form .= '<td class="wmap-img-tr">';
					    					$multi_ship_form .= '<div class="checkout-thumbnail-img">';
						    					$product_permalink = apply_filters( 'woocommerce_cart_item_permalink', $_product->is_visible() ? $_product->get_permalink( $items ) : '', $items, $key );
							    				//$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $items, $key );
							    				$thumbnail = apply_filters( 'woocommerce_cart_item_thumbnail', $_product->get_image(), $cart_item, $key );

												if ( ! $product_permalink ) {
													echo $thumbnail; // PHPCS: XSS ok.
												} else {
													$multi_ship_form .= '<a href="'.esc_url( $product_permalink ).'">'.$thumbnail.'</a>';
												}
							    				//$multi_ship_form .= '<p class="product-thumb-name">'.$_product->get_name().'</p>';
							    				$multi_ship_form .= '<p class="product-thumb-name">';
													if ( ! $product_permalink ) {
														$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', $_product->get_name(), $cart_item, $key ) . '&nbsp;' );
													} else {
														$multi_ship_form .=  wp_kses_post( apply_filters( 'woocommerce_cart_item_name', sprintf( '<a href="%s">%s</a>', esc_url( $product_permalink ), $_product->get_name() ), $cart_item, $key ) );
													}

													$multi_ship_form .=  wc_get_formatted_cart_item_data( $cart_item );
												$multi_ship_form .= '</p>';
								    			//do_action( 'woocommerce_after_cart_item_name', $cart_item, $key );

							    			$multi_ship_form .= '</div>';
					    				$multi_ship_form .= '</td>';
					    				$multi_ship_form .= '<td><input type="number" min="1" name="pdct-qty" value="'.$qty.'" class="multi-ship-pdct-qty pdct-qty main-pdct-qty pdct-qty-'.$key.'" data-cart_key="'.$key.'"></td>';
					    				$multi_ship_form .= '<td><input class="multi-ship-item" type="hidden" data-multi_ship_id="multi_ship_'.$i.'" data-multi_ship_parent_id="0" data-updated_qty="'.$qty.'" data-sub_row_stage="1">'.$multi_shipping_addresses.'</td>';
					    			$multi_ship_form .= '</tr>';
					    			$product_id = $value[ 'product_id' ];
					    			$variation_id = $value[ 'variation_id' ];
					    			$quantity = $value['quantity'];
					    			//$updated_quantity = $quantity;
					    			$parent_item_id = 0;
					    			// $woocommerce->cart->cart_contents[$key]['multi_ship_address'] = array();
					    			// $woocommerce->cart->set_session();
						    		$woocommerce->cart->cart_contents[$key]['multi_ship_address'] = array(
										'product_id' => $product_id,
										'variation_id' => $variation_id,
										'quantity' => $quantity,
										//'updated_quantity' => $updated_quantity,
										'multi_ship_id' => $multi_ship_id,
										'multi_ship_parent_id' => $parent_item_id,
										'child_keys' => array(),
										'parent_cart_key' => '',
									);
									$woocommerce->cart->set_session();
								}
								$i++;
							}
			    		}
			    		// $multi_ship_form .= '<tr>';
			    		// 	$multi_ship_form .= '<td></td><td></td>';
			    		// 	$multi_ship_form .= '<td><div class="save-multi-ship-adr-btn button alt" onclick="thwma_save_multi_ship_adr_btn(event,this)">save shipping addresses</div></td>';
			    		// $multi_ship_form .= '</tr>';
		    		$multi_ship_form .= '</table>';
		    		$multi_ship_form .= '<input type="hidden" name="multi_shipping_adr_data" class="multi-shipping-adr-data" value=""></input>';
		    	$multi_ship_form .= '</div>';
			} else {
				$multi_ship_form = self::multiple_address_management_form();
	    	}
			return $multi_ship_form;
		}

		/**
		 * Filled the shipping fields by using default address.
		 * 
		 * @return void
		 */	
		function thwma_fill_shipping_fields_values($value, $input) {
			if(is_user_logged_in()) {
				$user_id = get_current_user_id();
				$default_address = THWMA_Utils::get_default_address($user_id, 'shipping');
				
				foreach ($default_address as $key => $data) {
					if($input == $key) {
						$value = $data;
					}
				}
			}
			return $value;
		}

		/**
		 * Function for get the posted values.
		 *
		 * @param string $key The key value
		 * 
		 * @return void
		 */	
		public static function get_posted_value($key){
			$value = isset($_POST[$key]) ? stripslashes($_POST[$key]) : '';
			if(!$value){
				$post_data = isset($_POST['post_data']) ? $_POST['post_data'] : '';			
				if($post_data){
					parse_str($post_data, $post_data_arr);
					$value = isset($post_data_arr[$key]) ? stripslashes($post_data_arr[$key]) : '';
				}
			}
			
			return $value;
		}

		/*--------------------- Create Multiple orders ---------------------*/

		/**
		 * Create a sub-orders call
		 * 
         * @param string $post_id The post id
		 *
		 */
		public function thwma_create_suborders_call_on_btn_click($post_id) {
			if($post_id != null) {
				$post = get_post($post_id);

				if ($post->post_type != 'shop_order') {
					return;
				}
				 if (filter_var(get_post_meta($post_id, THWMA_Utils::IS_SUB_ORDER, true), FILTER_VALIDATE_BOOLEAN)) {
					return;
				}
				$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
				$order_shipping_status = isset($settings['order_shipping_status']) ? $settings['order_shipping_status'] : '';
				$user_id = get_current_user_id();
				$enable_multi_ship = get_user_meta($user_id, THWMA_Utils::USER_META_ENABLE_MULTI_SHIP, true);
				$shipping_section = isset($_POST['ship_to_different_address']) ? sanitize_key($_POST['ship_to_different_address']) : '';
				if($shipping_section == true){
					if($order_shipping_status == 'no') {
						if($enable_multi_ship == 'yes') {
							$this->thwma_create_suborders($post_id);
						}
					}
				}
			}
		}

		/**
		 * Creates suborders from a main order.
		 *
		 * @param string $main_order_id The main order id
		 * @param array $args The araguments
		 * 
		 */
		public function thwma_create_suborders($main_order_id, $args = array()) {
			$args = wp_parse_args($args, array(
				'delete_prev_suborders' => false,
			));
			if (true !== filter_var(apply_filters('thwma_adrs_create_suborders', true, $main_order_id), FILTER_VALIDATE_BOOLEAN)) {
				return;
			}
			$main_order                 = wc_get_order($main_order_id);
			$order_number               = is_a($main_order, 'WC_Order_Refund') ? apply_filters('woocommerce_order_number', $main_order->get_id(), $main_order) : $main_order->get_order_number();
			$main_order_post            = get_post($main_order_id);
			$currentUser                = wp_get_current_user();
			$original_main_order_status = get_post_status($main_order_id);
			$consider_quantity = false;

			// Only create suborders if there is more than 1 item in order.
			if (! $consider_quantity && count($main_order->get_items()) <= 1) {

				// Creates a fake id for main order.
				update_post_meta($main_order_id, THWMA_Utils::SUB_ORDER_FAKE_ID, $order_number);
				return;
			} elseif ($consider_quantity && count($main_order->get_items()) == 1) {

				// @var WC_Order_Item_Product $item_data.
				if(!empty($main_order->get_items()) && is_array($main_order->get_items())){
					foreach ($main_order->get_items() as $item_id => $main_order_item) {
						if ($main_order_item->get_quantity() < get_option(THWMA_Utils::OPTION_QUANTITY_MIN, 2)) {
							return;
						}
					}
				}
			}
			self::$is_creating_suborder = true;

			// Get meta data from order
			$main_order_metadata = get_metadata('post', $main_order_id);
			$default_main_order_status = false;

			// Delete previous suborders
			if ($args['delete_prev_suborders']) {
				$this->delete_suborders_from_main_order($main_order_id);
			}

			// Gets fees and taxes
			$fees  = $main_order->get_fees();
			$taxes = $main_order->get_taxes();
			$line_items_shipping = $main_order->get_items( 'shipping' );
			$shipping_item = array();
			foreach ($line_items_shipping as $value) {
				$shipping_item[] = $value;
			}

			// Counter for creating fake suborders ids
			$last_suborder_id      = get_post_meta($main_order_id, THWMA_Utils::LAST_SUBORDER_SUB_ID, true);
			$order_counter         = $last_suborder_id ? $last_suborder_id + 1 : 1;
			$order_inverse_counter = $last_suborder_id ? 999-$last_suborder_id : 999;
			$suborders = array();

			// @var WC_Order_Item_Product $main_order_item.

			// $meta_data = $order_item->get_meta_data();	
			// $meta_ship_adrsses = '';
			// $user_id= get_current_user_id();

			// if(!empty($meta_data)) {
			// 	foreach ($meta_data as $id => $meta_array) {
			// 		if('thwma_order_shipping_address' == $meta_array->key) {
			// 			$meta_ship_adrsses = $meta_array->value;
			// 		}
			// 	}
			// }
			
			$i = 0;
			if(!empty($main_order->get_items()) && is_array($main_order->get_items())){
				foreach ($main_order->get_items() as $item_id => $main_order_item) {
					$prev_suborder_ids = wc_get_order_item_meta($item_id, THWMA_Utils::SUB_ORDER, ! $consider_quantity);
					if (! is_array($prev_suborder_ids)) {
						if ($prev_suborder_ids) {
							$prev_suborder_ids = array($prev_suborder_ids);
						} else {
							$prev_suborder_ids = array();
						}
					}
					$repetitions = 1;
					$product = $main_order_item->get_product();
					if (true !== filter_var(apply_filters('thwma_order_item_valid_as_suborder', true, $item_id, $main_order_item, $main_order), FILTER_VALIDATE_BOOLEAN)) {
						continue;
					}
					if ($consider_quantity) {
						if ($main_order_item->get_quantity() >= get_option(THWMA_Utils::OPTION_QUANTITY_MIN, 2)) {
							$repetitions = $main_order_item->get_quantity() - count($prev_suborder_ids);
							// prevent duplicates
							if ($repetitions < 1) {
								continue;
							}
						}
					} else {
						if (! empty($prev_suborder_ids)) {
							continue;
						}
					}
					for ($i = 0; $i < $repetitions; $i ++) {
						$fee_value        = 0;
						$suborder_status = '';
						// Suborder default status from admin settings.
						$suborder_status_from_admin_settings = false;
						if (empty($suborder_status_from_admin_settings)) {
							$suborder_status = $original_main_order_status;
						} else {
							if (empty($default_main_order_status)) {
								$suborder_status = $suborder_status_from_admin_settings == 'main_order' ? $original_main_order_status : $suborder_status_from_admin_settings;
							} else {
								$suborder_status = $suborder_status_from_admin_settings == 'main_order' ? $default_main_order_status : $suborder_status_from_admin_settings;
							}
						}
						$order_data = array(
							'post_type'     => 'shop_order',
							'post_title'    => $main_order_post->post_title,
							'post_status'   => $suborder_status,
							'ping_status'   => 'closed',
							'post_author'   => $currentUser->ID,
							'post_password' => $main_order_post->post_password,
							'meta_input'    => array(
								THWMA_Utils::IS_SUB_ORDER      => true,
								THWMA_Utils::PARENT_ORDER      => $main_order_id,
								THWMA_Utils::SUB_ORDER_SUB_ID  => $order_counter,
								THWMA_Utils::SUB_ORDER_FAKE_ID => $order_number . '-' . $order_counter,
								THWMA_Utils::SORT_ID           => $main_order->get_id() . $order_inverse_counter,
								THWMA_Utils::PARENT_ORDER_ITEM => $item_id,
							),
						);
						

						// Creates a fake id for main order.
						update_post_meta($main_order_id, THWMA_Utils::SUB_ORDER_FAKE_ID, $order_number);
						update_post_meta($main_order_id, THWMA_Utils::REMAINING, $main_order->get_total());

						// Create sub order.
						$suborder_id = wp_insert_post($order_data, true);				
						$suborders[] = $suborder_id;

						$meta_data = $main_order_item->get_meta_data();	
						$meta_ship_adrsses = '';
						$user_id= get_current_user_id();
						$custom_fields = '';
						$shipping_method_data = '';

						
						
						if(!empty($meta_data) && is_array($meta_data)) {
							foreach ($meta_data as $id => $meta_array) {
								if('thwma_order_shipping_method' == $meta_array->key) {
									$shipping_method_data = $meta_array->value;
								}
							}
						}

						// Delete previous association with suborder id.
						//wc_delete_order_item_meta($item_id, thwma_Order_Item_Metas::SUB_ORDER);

						// Clone order post metas into suborder.
						$exclude_post_metas = apply_filters('thwma_exclude_cloned_order_postmetas', array(
							THWMA_Utils::SUB_ORDERS,
							'_wcj_order_number',
							'has_sub_order'
						));
						$this->clone_order_postmetas($main_order_metadata, $suborder_id, $exclude_post_metas);

						// Adds line item in suborder.
						$suborder_item_id = $this->add_line_item_in_suborder($main_order_item, $item_id, $suborder_id);

						// Adds shipping method in suborder.
						//$suborder_shipping_mthd = $this->add_shipping_in_suborder($shipping_item, $suborder_id, $main_order, $i );
						$suborder_shipping_mthd = $this->add_shipping_in_suborder($line_items_shipping, $suborder_id, $main_order, $shipping_method_data );

						// Adds fees in suborder.
						$fee_value = $this->add_fees_in_suborder($fees, $suborder_id, $main_order);

						// Adds taxes in suborder.
						$this->add_taxes_in_suborder($taxes, $suborder_id, $main_order_item);

						// Updates suborder price.
						update_post_meta($suborder_id, '_order_total', $main_order_item->get_total_tax() + $main_order_item->get_total() + $fee_value);
						update_post_meta($suborder_id, '_order_tax', $main_order_item->get_total_tax());
						update_post_meta($suborder_id, THWMA_Utils::REMAINING, $main_order_item->get_total_tax() + $main_order_item->get_total() + $fee_value);

						//Update Suborder Discount.
						$discount = $this->calculate_order_item_product_discount($main_order_item);
						update_post_meta($suborder_id, '_cart_discount', $discount);
						if ($consider_quantity) {
							update_post_meta($suborder_id, '_order_total', $main_order_item->get_total_tax() + $product->get_price() + $fee_value);
							wc_update_order_item_meta($suborder_item_id, '_qty', 1);
							wc_update_order_item_meta($suborder_item_id, '_line_total', wc_get_price_excluding_tax($product));
							wc_update_order_item_meta($suborder_item_id, '_line_subtotal', wc_get_price_excluding_tax($product));
						}
						
						// Grab the order and recalculate taxes.
						$suborder = wc_get_order($suborder_id);
						$suborder->calculate_taxes();
						$suborder->calculate_totals(false);
						unset($suborder);

						// Updates main order meta regarding suborder
						add_post_meta($main_order_id, THWMA_Utils::SUB_ORDERS, $suborder_id, false);

						// Associate main order item with suborder id
						wc_add_order_item_meta($item_id, THWMA_Utils::SUB_ORDER, $suborder_id, ! $consider_quantity);

						// Saves last suborder sub id
						update_post_meta($main_order_id, THWMA_Utils::LAST_SUBORDER_SUB_ID, $order_counter);

						// Saves sort id
						update_post_meta($main_order_id, THWMA_Utils::SORT_ID, $main_order_id . '9999');

						// // Update status
						 do_action('wmap_after_insert_suborder', $suborder_id, $main_order_id);

						$order_counter ++;
						$order_inverse_counter --;
						//$this->delete_main_order($main_order_id);
					}
				}
				foreach ($suborders as $suborder) {
					update_post_meta($suborder, THWMA_Utils::SUB_ORDERS, $suborders);
					$cc = get_post_meta($suborder,THWMA_Utils::SUB_ORDERS);
				}

			}
		}

		/**
		 * Deletes previous suborders
		 *
		 * @param string  $main_order_id The main order id
		 *
		 */
		public function delete_suborders_from_main_order($main_order_id) {
			$prev_suborders = get_post_meta($main_order_id, THWMA_Utils::SUB_ORDERS);
			if(!empty($prev_suborders)){
				if (is_array($prev_suborders) && count($prev_suborders) > 0) {
					foreach ($prev_suborders as $prev_suborder_id) {
						wp_delete_post($prev_suborder_id, true);
					}
					delete_post_meta($main_order_id, THWMA_Utils::SUB_ORDERS);
				}
			}
		}

		/**
		 * Clones order postmetas
		 *
		 * @param array $main_order_metadata The main order metadata
		 * @param string $suborder_id The sub order id	 
		 * @param string $exclude The exclude id
		 */
		public function clone_order_postmetas($main_order_metadata, $suborder_id, $exclude = array()) {
			if(!empty($main_order_metadata) && is_array($main_order_metadata)) {
				foreach ($main_order_metadata as $index => $meta_value) {
					if(!empty($meta_value) && is_array($meta_value)) {
						foreach ($meta_value as $value) {
							if (! in_array($index, $exclude)) {
								add_post_meta($suborder_id, $index, $value);
							}
						}
					}
				}
			}
		}

		/**
		 * Adds line item in suborder
		 *
		 * @param array $main_order_item The main order item details
		 * @param string $item_id The item id info
		 * @param string $suborder_id The sub order id
		 *
		 * @return bool|int
		 */
		public function add_line_item_in_suborder($main_order_item, $item_id, $suborder_id) {
			$item_name        = $main_order_item['name'];
			$item_type        = $main_order_item->get_type();
			$suborder_item_id = wc_add_order_item($suborder_id, array(
				'order_item_name' => $item_name,
				'order_item_type' => $item_type,
			));

			// Clone order item metas
			$this->clone_order_itemmetas($item_id, $suborder_item_id, array(THWMA_Utils::SUB_ORDER));
			return $suborder_item_id;
		}		

		/**
		 * Add the shipping method data on the order edit page
		 *
		 * @param array $shippings The main order item details
		 * @param int $shippings The item id info
		 * @param string $main_order The sub order id
		 * @param array $shipping_method_data
		 *
		 * @return bool|int
		 */
		function add_shipping_in_suborder( $shippings, $suborder_id, $main_order, $shipping_method_data ) {
			$shipping_value_count = 0;
			/* @var WC_Order_Item_Shipping $shippings */
			if(!empty($shippings) && is_array($shippings)) {
				foreach ( $shippings as $shipping ) {
					if($shipping_value_count < 1) {
						$item_name           = $shipping->get_name();
						$item_type           = $shipping->get_type();
						$instance_id         = $shipping->get_instance_id();
						$method_id           = $shipping->get_method_id();
		
						// if(!empty($shipping) && is_array($shipping)) {
						// 	foreach($shipping as $key => $data) {
						// 	}
						// }
						$shipping_mthd_key = $method_id.':'.$instance_id;
						$ship_meta_data = $shipping->get_meta_data();
						$ship_data = $ship_meta_data[0]->get_data();
						$ship_meta_data = isset($ship_data['value']) ? $ship_data['value'] : '';

						if(!empty($shipping_method_data) && is_array($shipping_method_data)) {
							foreach ($shipping_method_data as $key => $value) {
								$ship_meta_value = $value['item_name'].' × '.$value['item_quantity'];
								if($key == $shipping_mthd_key) {
									if($ship_meta_data == $ship_meta_value) {
										//if(array_key_exists($shipping_mthd_key, $shipping_method_data)) {
										$suborder_new_shipping_id = wc_add_order_item( $suborder_id, array(
											'order_item_name' => $item_name,
											'order_item_type' => $item_type,
										) );

										$this->clone_order_itemmetas( $shipping->get_id(), $suborder_new_shipping_id );
										$shipping_value       = $shipping->get_total();
										//$shipping_value_count += $shipping_value;
										$shipping_value_count += 1;
										wc_update_order_item_meta( $suborder_new_shipping_id, 'cost', $shipping_value );
										
									}
								}
							} 
						} else {
							$item_name           = $shipping->get_name();
							$item_type           = $shipping->get_type();
							$suborder_new_shipping_id = wc_add_order_item( $suborder_id, array(
								'order_item_name' => $item_name,
								'order_item_type' => $item_type,
							) );
							$this->clone_order_itemmetas( $shipping->get_id(), $suborder_new_shipping_id );
							//$shipping_value       = $shipping->get_total() / $main_order->get_item_count();
							$shipping_value       = $shipping->get_total();
							$shipping_value_count += 1;
							wc_update_order_item_meta( $suborder_new_shipping_id, 'cost', $shipping_value );
						}
					}
				}
			}
			return $shipping_value_count;
		}

		/**
		 * Clones order item metas
		 *
		 * @param array $order_item_id The order item id
		 * @param string $target_order_id	 The target order id	 
		 * @param string $exclude The exclude ids
		 * @param string $method 'add' | 'update'
		 */
		public function clone_order_itemmetas($order_item_id, $target_order_id, $exclude = array(), $method = 'add') {
			$order_item_metas = wc_get_order_item_meta($order_item_id, '');
			if(!empty($order_item_metas) && is_array($order_item_metas)){
				foreach ($order_item_metas as $index => $meta_value) {
					if(!empty($meta_value) && is_array($meta_value)){
						foreach ($meta_value as $value) {
							if (! in_array($index, $exclude)) {
								if ($method == 'add') {
									wc_add_order_item_meta($target_order_id, $index, maybe_unserialize($value));
								} else if ($method == 'update') {
									wc_update_order_item_meta($target_order_id, $index, maybe_unserialize($value));
								}
							}
						}
					}
				}
			}
		}

		/**
		 * Adds fees in suborder
		 *
		 * @param array $fees The fees details
		 * @param string $suborder_id The sub order id
		 * @param array $main_order The main order details
		 *
		 * @return float|int
		 */
		public function add_fees_in_suborder($fees, $suborder_id, $main_order) {
			$fee_value_count = 0;
			// @var WC_Order_Item_Fee $fee.
			if(!empty($fees) && is_array($fees)){
				foreach ($fees as $fee) {
					$item_name           = $fee->get_name();
					$item_type           = $fee->get_type();
					$suborder_new_fee_id = wc_add_order_item($suborder_id, array(
						'order_item_name' => $item_name,
						'order_item_type' => $item_type,
					));
					$this->clone_order_itemmetas($fee->get_id(), $suborder_new_fee_id);
					$fee_value       = $fee->get_total() / $main_order->get_item_count();
					$fee_value_count += $fee_value;
					wc_update_order_item_meta($suborder_new_fee_id, '_line_total', $fee_value);
					wc_update_order_item_meta($suborder_new_fee_id, '_line_tax', 0);
					wc_update_order_item_meta($suborder_new_fee_id, '_line_tax_data', 0);
				}
			}
			return $fee_value_count;
		}

		/**
		 * Adds taxes in suborder
		 *
		 * @param array $taxes The tax details
		 * @param string $suborder_id The suborder id
		 */
		public function add_taxes_in_suborder($taxes, $suborder_id, $main_order_item) {
			// @var WC_Order_Item_Tax $tax.
			if(!empty($taxes) && is_array($taxes)){
				foreach ($taxes as $tax) {
					$item_name           = $tax->get_name();
					$item_type           = $tax->get_type();
					$suborder_new_tax_id = wc_add_order_item($suborder_id, array(
						'order_item_name' => $item_name,
						'order_item_type' => $item_type,
					));
					$this->clone_order_itemmetas($tax->get_id(), $suborder_new_tax_id);
					//wc_update_order_item_meta($suborder_new_tax_id, 'tax_amount', $main_order_item->get_total_tax());
				}
			}
		}

		/**
		 * Calculates order item product discount.
		 *
		 * @param WC_Order_Item_Product $order_item_product
		 *
		 * @return float|int
		 */
		function calculate_order_item_product_discount(WC_Order_Item_Product $order_item_product) {
			$subtotal = $order_item_product->get_subtotal();
			$total    = $order_item_product->get_total();
			$discount = $order_item_product->get_quantity() * ($subtotal - $total);
			return $discount;
		}

		/**
		 * Deletes suborder if correspondent item id is removed from main order
		 *
		 * @param $item_id
		 */
		public function thwma_remove_suborder_post_on_main_order_item_removal( $item_id ) {
			$suborder_id = wc_get_order_item_meta( $item_id, THWMA_Utils::SUB_ORDER, true );
			$suborder    = get_post( $suborder_id );
			if ( $suborder_id && $suborder ) {
				$main_order_id = get_post_meta( $suborder_id, THWMA_Utils::PARENT_ORDER, true );

				delete_post_meta( $main_order_id, THWMA_Utils::SUB_ORDERS, $suborder_id );
				delete_post_meta( $suborder_id, THWMA_Utils::PARENT_ORDER );
				$suborder = get_post( $suborder_id );
				if ( ! filter_var( get_post_meta( $suborder_id, THWMA_Utils::DELETING, true ), FILTER_VALIDATE_BOOLEAN ) ) {
					wp_delete_post( $suborder_id, true );
				}

				$main_order = wc_get_order( $main_order_id );
				if ( is_a( $main_order, 'WC_Order' ) ) {
					$main_order->calculate_totals();
				}
			}
		}

		/**
		 * Deletes suborder item from main order in case a suborder post is removed
		 *
		 * @param $post_id
		 */
		public function thwma_remove_suborder_item_on_suborder_post_removal( $post_id ) {
			$post_type = get_post_type( $post_id );

			if ( "shop_order" != $post_type ) {
				return;
			}

			$parent_order_id = get_post_meta( $post_id, THWMA_Utils::PARENT_ORDER, true );
			if ( empty( $parent_order_id ) ) {
				return;
			}

			$parent_order_item_id = get_post_meta( $post_id, THWMA_Utils::PARENT_ORDER_ITEM, true );
			delete_post_meta( $parent_order_id, THWMA_Utils::SUB_ORDERS, $post_id );
			update_post_meta( $post_id, THWMA_Utils::DELETING, true );
			wc_delete_order_item( $parent_order_item_id );
		}

		/**
		 * Set main order initial status
		 *
		 * @param $result
		 * @param $order_id
		 *
		 * @return mixed
		 */
		public function thwma_set_main_order_initial_status( $order_id ) {
			$default_main_order_status = get_option( THWMA_Utils::OPTION_DEFAULT_MAIN_ORDER_STATUS );
			if ( ! empty( $default_main_order_status ) ) {
				$suborders = get_post_meta( $order_id, THWMA_Utils::SUB_ORDERS );
				if ( is_array( $suborders ) && count( $suborders ) > 1 ) {
					wp_update_post( array(
						'ID'          => $order_id,
						'post_status' => $default_main_order_status,
					) );
				}
			}
		}

		/**
		 * List all orders on thank you page. 
		 *
		 * @param $result
		 * @param $order_id
		 *
		 * @return mixed
		 */
		public function thwma_before_thankyou_account($order) {
			$order_id = $order->get_id();
			$suborders = get_post_meta( $order_id, THWMA_Utils::SUB_ORDERS );
			$parent_order_id = get_post_meta( $order_id, THWMA_Utils::PARENT_ORDER );
			if(!empty($suborders)) {
				echo '<div class="suborder_list"><b>Related Orders:</b> ';
				if(array_key_exists(0, $suborders)) {
					if(!empty($suborders[0]) && is_array($suborders[0])) {
						foreach ($suborders[0] as $suborder) {
							$sub_order_data = wc_get_order( $suborder );
							echo '<a href="'.$sub_order_data->get_view_order_url().'">'.$suborder .'</a>, ';
							//if(!empty($suborder)) {
								//$parent_order_id = get_post_meta( $suborder, THWMA_Utils::PARENT_ORDER );
							//}
						}
					}
				}
				echo '<div>';
				if(!empty($parent_order_id) && is_array($parent_order_id)) {
					foreach ($parent_order_id as $parent_id) {
						//$this->thwma_remove_main_order($parent_id);
					}
				}
			}
		}

		/**
		 * Remove main order.
		 *
		 * @param $order_id
		 *
		 * @return mixed|void
		 */
		public function thwma_remove_main_order($order_id) {
			wp_delete_post ( $order_id, $force_delete = false );
		}

		public function admin_dashboard_subscriptions_filter_callback() {
			$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping']:'';
			$single_order = isset($settings['order_shipping_status']) ? $settings['order_shipping_status'] : '';	
			if($settings) {
				if($cart_shipping_enabled == 'yes') {
					if($single_order == 'no') {
					    global $pagenow, $post_type, $post;
					    $parent_order_id = '';

					    if(!empty($post)) {
							$last_order_id = $post->ID;
							$parent_order_id = get_post_meta( $last_order_id, THWMA_Utils::PARENT_ORDER );
						    if(!empty($parent_order_id) && is_array($parent_order_id)) {
						    	foreach ($parent_order_id as $parent_id) {
								    if ( $pagenow === 'edit.php') :
								    	if($post_type === 'shop_order'): ?>
									            <style>
									            .post-<?php echo $parent_id; ?> { display: none;}
									            </style>
									        <?php
							    			$this->thwma_remove_main_order($parent_id);
									    endif;
							    	endif;
								}
							} else {
								if ( $pagenow == 'edit.php') :
								    if($post_type == 'shop_order'): ?>
							            <style>
							            .post-<?php echo $last_order_id; ?> { display: none;}
							            </style>
							        	<?php
						    			$this->thwma_remove_main_order($last_order_id);
							        endif;
						    	endif;
							}
						}
					}
				}
			}
		}

		/**
		 * Set sub order id on thankyou page.
		 *
		 * @param $order_id
		 *
		 * @return mixed|void
		 */
		public function thwma_set_sub_order_id($order_id){
			$settings = THWMA_Utils::get_setting_value('settings_multiple_shipping');
			$cart_shipping_enabled = isset($settings['enable_cart_shipping']) ? $settings['enable_cart_shipping']:'';
			$single_order = isset($settings['order_shipping_status']) ? $settings['order_shipping_status'] : '';
			$suborders = get_post_meta( $order_id, THWMA_Utils::SUB_ORDERS );			
			if($settings) {
				if($cart_shipping_enabled == 'yes') {
					if($single_order == 'no') {
						if(!empty($suborders) && is_array($suborders)) {
							foreach ($suborders as $suborder_id) {
								$order_id = $suborder_id;
							}
						}
					}
				}
			}			
			return $order_id;
		}	
	}	
endif;