<?php
   /*
   Plugin Name: Stockists Import
   Plugin URI: 
   description: This plugin is used for import stockists data in our site.
   Version: 1.0
   Author: Mr. Geeks
   Author URI: 
   License: Free
   */
?>

<?php 

if ( ! function_exists( 'add_filter' ) ) {
  header( 'Status: 403 Forbidden' );
  header( 'HTTP/1.1 403 Forbidden' );
exit();
}

if ( ! defined( 'STOCKISTS_IMPORT' ) ) {
  define( 'STOCKISTS_IMPORT', __FILE__ );
}


if ( ! defined( 'STOCKISTS_IMPORT_PATH' ) ) {
  define( 'STOCKISTS_IMPORT_PATH', plugin_dir_path( STOCKISTS_IMPORT ) );
}

class Stockists {
    public function __construct(){

      add_action( 'admin_menu', array($this,'dbi_add_settings_page') );
      add_action( 'admin_post_nopriv_my_simple_form', array($this,'my_handle_form_submit') );
      add_action( 'admin_post_my_simple_form',  array($this,'my_handle_form_submit') );     
    }

    function dbi_add_settings_page() {
        add_options_page( 'Stockists', 'Stockists import', 'manage_options', 'stockists-import', array($this,'stockists_settings_page') );
    }
    
    function stockists_settings_page() { ?>
        <h2>Stockists import</h2>
          <form action="<?= admin_url( 'admin-post.php' ) ?>" method="POST" enctype="multipart/form-data" >
            <input type="hidden" name="action" value="my_simple_form">
            <button name="submit" class="button button-primary" type="submit"><?php esc_attr_e( 'Import' ); ?></button> 
          </form>
        <?php
    }


    

    
/*---------------------------------------------------SUBMIT FORM-----------------------------------------------------*/
    function removeDuplicateAddress($addresses){
      $uniqueAddrss = array();

      foreach ($addresses as $key => $address) {
        if(!in_array($address, $uniqueAddrss)){
          $uniqueAddrss[$key] = $address; 
        }
      }
      return $uniqueAddrss;

    }

    function updateShippingAddress($user_id, $addresses){
      echo $user_id."<br>";
      $addressBook['shipping'] = $this->removeDuplicateAddress($addresses );
      update_user_meta($user_id, 'thwma_custom_address', $addressBook);
    }

    
    function updateUserBrands($user_id, $brand_ids){
      update_user_meta($user_id, 'brands', array_unique($brand_ids));
    }

    function usersCoupon($user_id,$discount){
      
      global $woocommerce;
      echo $coupon_code = trim($discount).'-Off'; 
      $discount_type = 'percent'; 
      $coupon = new WC_Coupon($coupon_code);
      if(!$coupon->is_valid()){
        $param = array(
          'post_title' => $coupon_code,
          'post_content' => '',
          'post_status' => 'publish',
          'post_author' => 1,
          'post_type' => 'shop_coupon'
        );
        $coupon_id = wp_insert_post( $param );
      }else{
        $coupon_id = $coupon->get_code();
      }  

      $coupon_object = new WC_Coupon($coupon_id);
      $get_code = $coupon_object->get_code();
      // Update coupon meta
      update_post_meta( $coupon_id, 'discount_type', $discount_type );
      update_post_meta( $coupon_id, 'coupon_amount', $discount );
      update_user_meta( $user_id, 'user_coupon', $get_code );
    }
    
    function my_handle_form_submit() {
      $file = dirname( STOCKISTS_IMPORT ) . '/csv/stockist-data.csv';
      $stockists = $this->csvToArray($file);
      
      if(!empty($stockists)){
        foreach ($stockists as $email => $userData) {

          $addresses = array();
          $brand = array();
          foreach ($userData as $key => $user) {
           
              if($user['Democracy'] == 'TRUE'){
                $brand[] = 'democracy';
              }
              if($user['Classified'] == 'TRUE'){
                $brand[] = 'classified';
              }
           }

          if(in_array('democracy', $brand) && !in_array('classified', $brand)){
            $storeBrands = 'democracy';
          }else if(!in_array('democracy', $brand) && in_array('classified', $brand)){
             $storeBrands = 'classified';
          }else if(in_array('democracy', $brand) && in_array('classified', $brand)){
              $storeBrands = 'classified-democracy';
          }else{
            $storeBrands = "";
          }
        
          /*Add shipping address*/
          foreach ($userData as $key => $user) {
            if(!empty($user['EmailAddress'])){
              $user_object = get_user_by('email',$user['EmailAddress']);
              if(!$user_object){
                $password = $user['AccountNumber'].$user['PostalCode'];
                $user_id = wp_create_user ( $user['EmailAddress'], $password, $user['EmailAddress'] ); 
                if ( is_wp_error( $user_id ) ){
                  var_dump($user['EmailAddress']);
                  var_dump($user_id->get_error_message());die("ocean");
                }
                update_user_meta($user_id, 'password', $password);
              }else{
                $user_id = $user_object->ID;
              }
              /*Add discount coupon*/
              !empty($user['Discount'] && $user['Discount'] > 0) ? $this->usersCoupon($user_id , $user['Discount']) : "";
              
              /*Update users meta fields*/
              update_user_meta($user_id, 'first_name', $user['FirstName']);
              update_user_meta($user_id, 'last_name', $user['LastName']);

              $wp_user = new WP_User( $user_id );
              $wp_user->set_role( 'customer' );

              $addresses['address_'.$key]['shipping_first_name'] = $user['FirstName'];
              $addresses['address_'.$key]['shipping_last_name'] = $user['LastName'];
              $addresses['address_'.$key]['shipping_address_1'] = $user['AddressLine1'];
              $addresses['address_'.$key]['shipping_address_2'] = $user['AddressLine2'];
              $addresses['address_'.$key]['shipping_company'] = $user['CompanyName'];
              $addresses['address_'.$key]['shipping_country'] = 'NZ';
              $addresses['address_'.$key]['shipping_state'] = $user['RegionCode'];
              $addresses['address_'.$key]['shipping_city'] = $user['City'];
              $addresses['address_'.$key]['shipping_postcode'] = $user['PostalCode'];
              $addresses['address_'.$key]['shipping_email'] = $user['EmailAddress'];
              $addresses['address_'.$key]['shipping_phone'] = $user['ShopPhoneNumber'];
              $addresses['address_'.$key]['shipping_store_name'] = $user['StoreName'];
              $addresses['address_'.$key]['shipping_store_map'] = $user['MapAddress'];
              $addresses['address_'.$key]['shipping_brand'] = $storeBrands;
            }
          } 
          
          $this->updateShippingAddress($user_id,$addresses);
          // $this->updateUserBrands($user_id, $brand_ids);
        } 
      }
    }

    


    function csvToArray($file){
      $arrayData = [];
      if (($handle = fopen($file, "r")) !== false) {                
          if (($data = fgetcsv($handle, 1000, ",")) !== false) {         
              $keys = $data;                                             
          }
          while (($data = fgetcsv($handle, 1000, ",")) !== false) {  
           
              $arrayData[$data[6]][] = array_combine($keys, $data);              
          }
          fclose($handle);                                               
      }
      return $arrayData;
    }
      
}
 
$stockists = new Stockists();

?>