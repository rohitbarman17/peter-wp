<?php 
use Automattic\Jetpack\Constants;

add_action( 'admin_menu', 'register_product_import_submenu_page' );
function register_product_import_submenu_page() {
  add_submenu_page('woocommerce', 'Product import', 'Product Import', 'manage_woocommerce', 'product_import', 'product_import_callback');
}
    
function product_import_callback() {?>
    <script type="text/javascript">
       var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
    </script>
    <h3>Product Import</h3>
    <!-- action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>"  -->
    <form id="product_import" class="form-inline" name="import" method="post" enctype="multipart/form-data">
      <input type="hidden" name="action" value="product_import_submit">
      <input type="file" name="file" id="spreadsheet" accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel"/><br/>
      <button class="buttonload" type="submit">
        <span class="icon-spinner"><i class="fa fa-spinner fa-spin"></i></span>Upload file
      </button>
    </form>
    <ul class="message"></ul>
    <div class="tableHtml"></div>
    <?php
}

function export_csv_in_array($file){
    if ( $xlsx = SimpleXLSX::parse($file) ) {
        foreach ($xlsx->rows() as $key => $row) {
            if($key == 0){
                $headerKey = $row;
            }else{
              $data['products'][] = array_combine($headerKey, $row); 
            }
        }    
    }

    return $data;
}

 add_action( 'wp_ajax_product_import_submit','product_import_submit' );
// add_action( 'admin_post_product_import_submit','product_import_submit');
function product_import_submit(){

    if (isset($_FILES['file'])) {
        $html = "";
        if ( $xlsx = SimpleXLSX::parse( $_FILES['file']['tmp_name'] ) ) {

            $html .= '<div class="main">';
            $html .= '<h2>Parsing Result</h2>';
            $html .= '<button class="import-all btn-btn-primary">Import all</button>';
            $html .= '</div>';
            $html .= '<table border="1" cellpadding="3" style="border-collapse: collapse">';

            $dim = $xlsx->dimension();
            $cols = $dim[0];
            foreach ( $xlsx->rows() as $k => $r ) {
                $header = ($k == 0) ? "header" : "product-row";
                $html .= '<tr class="'.$header .'">';
                for ( $i = 0; $i < $cols; $i ++ ) {
                    $html .= '<td class="data">' . ( isset( $r[ $i ] ) ? $r[ $i ] : '&nbsp;' ) . '</td>';
                }

                $html .= ($k == 0) ?'<td>Action</td>' : '<td><button class="import-row"><span class="icon-spinner"><i class="fa fa-spinner fa-spin"></i></span>Import</button></td>';
                $html .= '</tr>';
            }
            $html .= '</table>';
            echo json_encode(array("status"=>"success", "html" => $html));

        } else {
            
            $html .= SimpleXLSX::parseError();
            echo json_encode(array("status"=>"error", "html" => $html));
        }

    }else{
        $html = "<p>File not found</p>";
        echo json_encode(array("status"=>"error", "html" => $html ));
    }

    exit();
}

add_action( 'wp_ajax_product_import_action','product_import_trigger' );
function product_import_trigger(){

    if(!empty($_POST) && array_key_exists("action", $_POST) && $_POST['action'] == "product_import_action"){
        $products = $_POST;
        
        $data['style_code'] = $products['StyleCode'];
        $data['title'] = $products['ProductName'];
        $data['regular_price'] = $products['RRP-Actual'];
        $data['stock'] = $products['Stock'];
        $data['gallery_ids'] = array();
        $data['fabrication'] = $products['Fabrication'];

        //SUPPLIERS
        $data['supplier'] = !empty($products['Supplier']) ? get_supplier_id($products['Supplier']) : '';
        $data['brand'] = !empty($products['Brand']) ? get_texonomy_by_name($products['Brand'], 'brands') : '';
        $data['collection'] = !empty($products['Collection']) ? get_texonomy_by_name($products['Collection'], 'collection') : '';

        //DIRECTORY PATH AND IMAGE URL
        $directory = $_SERVER['DOCUMENT_ROOT'].'/Collection-images/'.$products['Brand']."/".$products['Collection'];
        if(!file_exists($directory)){
            echo json_encode(array("status"=>"error" ,"message" => $directory." directory not exists on server!"));
            exit();
        }
        $image_directory_url = site_url().'/Collection-images/'.$products['Brand']."/".$products['Collection'];;

        //TYPE
        if(!empty($products['Type'])){
            $type = explode(",", $products['Type']);   
            foreach ($type as $value) {
                $product_type[] = get_texonomy_by_name($value ,'products_type');       
            }

           $data['type'] = $product_type;
        }

        //IMAGES
        if(!empty($products['Photo-Front'])){
            $image_array = get_image_from_directory($directory);
            if(!empty($image_array) && !empty($products['Photo-Front'])){
                if( in_array($products['Photo-Front'], $image_array) ){
                    $image_url = $image_directory_url."/".$products['Photo-Front'];
                    $attachment_id = media_sideload_image( $image_url, 0 , $products['Photo-Front'] , "id" );
                    if ( !is_wp_error( $attachment_id ) ){
                        $data['attach_id'] = $attachment_id;
                    }else{
                        echo json_encode(array("status"=>"error" ,"message" => $products['Photo-Front']." image not uploading!"));
                        exit();
                    }
                }else{
                    echo json_encode(array("status"=>"error" ,"message" => "<strong>".$products['Photo-Front']."</strong> image not found!"));
                        exit();
                }
            }
        }

        //COLOUR
        if(!empty($products['Colour-A'])){
            $data['attributes']['Colour'][] = $products['Colour-A'];   
        }

        if(!empty($products['Colour-B'])){
            $data['attributes']['Colour'][] = $products['Colour-B'];   
        }

        if(!empty($products['Colour-C'])){
            $data['attributes']['Colour'][] = $products['Colour-C'];   
        }

        //SIZE
        if(!empty($products['8/XS'])){
            $data['attributes']['Size'][] = '8/XS';    
        }

        if(!empty($products['10/S'])){
            $data['attributes']['Size'][] = '10/S';    
        }

        if(!empty($products['12/M'])){
            $data['attributes']['Size'][] = '12/M';    
        }

        if(!empty($products['14/L'])){
            $data['attributes']['Size'][] = '14/L';    
        }

        if(!empty($products['16/XL'])){
            $data['attributes']['Size'][] = '16/XL';    
        }

        if(!empty($products['18/XXL'])){
            $data['attributes']['Size'][] = '18/XXL';    
        }

        if(!empty($products['20/3XL'])){
            $data['attributes']['Size'][] = '20/3XL';    
        }

        create_variable_product($data);
    }else{
        echo json_encode(array("status"=>"error" ,"message" => "Data not found!"));
    }

    exit();
}
/**
 * Create a new variable product (with new attributes if they are).
 * (Needed functions:
 *
 * @since 3.0.0
 * @param array $data | The data to insert in the product.
 */

function create_variable_product( $data ){

    if( ! function_exists ('save_product_attribute_from_name') ) return;
    
    $postname = sanitize_title( $data['title'] );
    $productOject = get_page_by_title(trim($data['title']), OBJECT, 'product' );
    if($productOject == NULL){
        $post_data = array(
            'post_author'   => 1,
            'post_name'     => $postname,
            'post_title'    => $data['title'],
            'post_content'  => "empty",
            'post_status'   => 'publish',
            'ping_status'   => 'closed',
            'post_type'     => 'product',
            'guid'          => home_url( '/product/'.$postname.'/' ),
        );

        // Creating the product (post data)
        $product_id = wp_insert_post( $post_data );
    }else{
        $product_id = $productOject->ID;
    }

    // Get an instance of the WC_Product_Variable object and save it
    $product = new WC_Product_Variable( $product_id );
    $product->save();

    ## ---------------------- Other optional data  ---------------------- ##
    ##     (see WC_Product and WC_Product_Variable setters methods)

    // THE PRICES (No prices yet as we need to create product variations)

    //IMAGES GALLERY
   
    if( !empty( $data['attach_id'] ) ){
        set_post_thumbnail( $product_id, $data['attach_id'] );
    }

    //BRAND
    if( !empty( $data['brand'] ) && count( $data['brand'] ) > 0 )
        wp_set_object_terms( $product_id, $data['brand'], 'brands' );

    //COLLECTION
    if( !empty( $data['collection'] ) && count( $data['collection'] ) > 0 )
        wp_set_object_terms( $product_id, $data['collection'], 'collection' );

    //TYPE
    if( !empty( $data['type'] ) && count( $data['type'] ) > 0 )
        wp_set_object_terms( $product_id, $data['type'], 'products_type' );

    // SKU
    // if( ! empty( $data['sku'] ) )
    //     $product->set_sku( $data['sku'] );

    // STOCK (stock will be managed in variations)
    $product->set_stock_quantity( $data['stock'] ); // Set a minimal stock quantity
    $product->set_manage_stock(true);
    $product->set_stock_status('');
    $product->set_regular_price($data['regular_price']); 

    $product->validate_props(); // Check validation

    ## ---------------------- VARIATION ATTRIBUTES ---------------------- ##

    $product_attributes = array();

    foreach( $data['attributes'] as $key => $terms ){
        $term_name_array = array();
        $taxonomy = wc_attribute_taxonomy_name($key); // The taxonomy slug
        $attr_label = ucfirst($key); // attribute label name
        $attr_name = ( wc_sanitize_taxonomy_name($key)); // attribute slug

        // NEW Attributes: Register and save them
        if( ! taxonomy_exists( $taxonomy ) )
            save_product_attribute_from_name( $attr_name, $attr_label );

        $product_attributes[$taxonomy] = array (
            'name'         => $taxonomy,
            'value'        => '',
            'position'     => '',
            'is_visible'   => 0,
            'is_variation' => 1,
            'is_taxonomy'  => 1
        );

        foreach( $terms as $value ){
            $term_name = ucfirst($value);
            $term_slug = sanitize_title($value);

            // Check if the Term name exist and if not we create it.
            if( ! term_exists( $value, $taxonomy ) )
                wp_insert_term( $term_name, $taxonomy, array('slug' => $term_slug ) ); // Create the term

            //attribute add in array
            $term_name_array[] = $term_name;
        }

        // Set attribute values
        wp_set_post_terms( $product_id, $term_name_array, $taxonomy, false );

    }

    update_post_meta( $product_id, '_product_attributes', $product_attributes );
    update_post_meta( $product_id, 'supplier', $data['supplier'] );
    update_post_meta( $product_id, 'fabrication', $data['fabrication'] );
    update_post_meta( $product_id, 'style_code', $data['style_code'] );
    
    $product->save(); // Save the data

    $total_variation = create_variation_from_attributes($product_id);
    
    $product_object = wc_get_product($product_id);
    $current_products = $product_object->get_children();
    if(!empty($current_products)){
        foreach ($current_products as $variation_id) {
            set_price_to_variation($variation_id , $data['regular_price'] );
        }
    }

    if($total_variation >= 0){
        echo json_encode(array("status"=>"success" ,"message" => "Variations created successfully!"));
    }else{
        echo json_encode(array("status"=>"error" ,"message" => "Something went wrong"));
    }
    exit();    

}

function set_price_to_variation($var_id, $regular_price) {
    if ( !empty($var_id) ) {
        $variation = wc_get_product($var_id);
        $variation->set_regular_price($regular_price);
        // $variation->set_sale_price($sale);
        $variation->save();
    }
    return $variation;
}

//Generate variation from attribute of products
function create_variation_from_attributes( $product_id ){

    $product    = wc_get_product( $product_id );
    $data_store = $product->get_data_store();

    if ( ! is_callable( array( $data_store, 'create_all_product_variations' ) ) ) {
        echo json_encode(array("status"=>"error" ,"message" => "Something went wrong"));
      wp_die();
    }

    //https://woocommerce.wp-a2z.org/oik_api/wc_ajaxlink_all_variations/
    $total_variation = esc_html( $data_store->create_all_product_variations( $product, Constants::get_constant( 'WC_MAX_LINKED_VARIATIONS' ) ) );
    $data_store->sort_all_product_variations( $product->get_id() );
    
    return $total_variation;
}

//https://stackoverflow.com/questions/47518333/create-programmatically-a-variable-product-and-two-new-attributes-in-woocommerce
function save_product_attribute_from_name( $name, $label='', $set=true ){
    if( ! function_exists ('get_attribute_id_from_name') ) return;

    global $wpdb;

    $label = $label == '' ? ucfirst($name) : $label;
    $attribute_id = get_attribute_id_from_name( $name );

    if( empty($attribute_id) ){
        $attribute_id = NULL;
    } else {
        $set = false;
    }
    $args = array(
        'attribute_id'      => $attribute_id,
        'attribute_name'    => $name,
        'attribute_label'   => $label,
        'attribute_type'    => 'select',
        'attribute_orderby' => 'menu_order',
        'attribute_public'  => 0,
    );


    if( empty($attribute_id) ) {
        $wpdb->insert(  "{$wpdb->prefix}woocommerce_attribute_taxonomies", $args );
        set_transient( 'wc_attribute_taxonomies', false );
    }

    if( $set ){
        $attributes = wc_get_attribute_taxonomies();
        $args['attribute_id'] = get_attribute_id_from_name( $name );
        $attributes[] = (object) $args;
        //print_r($attributes);
        set_transient( 'wc_attribute_taxonomies', $attributes );
    } else {
        return;
    }
}

/**
 * Get the product attribute ID from the name.
 *
 * @since 3.0.0
 * @param string $name | The name (slug).
 */
function get_attribute_id_from_name( $name ){
    global $wpdb;
    $attribute_id = $wpdb->get_col("SELECT attribute_id
    FROM {$wpdb->prefix}woocommerce_attribute_taxonomies
    WHERE attribute_name LIKE '$name'");
    return reset($attribute_id);
}

function get_supplier_id($supplier_name){
    $data = get_post_by_name($supplier_name , "supplier");
    return ($data != null) ? $data->ID : '';
}



function get_post_by_name(string $name, string $post_type = "post") {
    $query = new WP_Query([
        "post_type" => $post_type,
        "name" => $name
    ]);

    return $query->have_posts() ? reset($query->posts) : null;
}

function get_texonomy_by_name($name , $texonomy){
    
    $get_texonomy = get_term_by("name" , $name, $texonomy);
    return ($get_texonomy) ? $get_texonomy->term_id : "";
}


//GET IMAGES OF DIRECTORY
function get_image_from_directory($dir){
    $ImagesArray = [];
    $file_display = [ 'jpg', 'jpeg', 'png', 'gif' ];

    if (file_exists($dir) == false) {
        return ["Directory \'', $dir, '\' not found!"];
    } else {
        $dir_contents = scandir($dir);
        foreach ($dir_contents as $file) {
            $file_type = pathinfo($file, PATHINFO_EXTENSION);
            if (in_array($file_type, $file_display) == true) {
                $ImagesArray[] = $file;
            }
        }
        return $ImagesArray;
    }
}

function array_search_partial($array, $keyword) {
    $matched_images = array();
    
    foreach($array as $index => $image_name) {
        if (strpos($image_name, $keyword) !== FALSE){
            $matched_images[] = $image_name;
        }
    }

    return $matched_images;
}

