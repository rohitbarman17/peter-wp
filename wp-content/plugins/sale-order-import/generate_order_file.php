<?php
add_action( 'wp_ajax_edit_spreadsheet_data','editSpreadsheetData');
     function editSpreadsheetData(){

      if(isset($_POST) && array_key_exists('action', $_POST) && $_POST['action'] == "edit_spreadsheet_data"){
        
        $user = wp_get_current_user();
        $customer_id = $user->ID;
        $customer_name = $user->user_nicename;
        $fname = ucfirst($customer_name)."-".date('Y-m-d-H-i-s').'.xlsx';
        $file = dirname( SALE_ORDER ) . '/order-formdata-peter-latest.xlsx';
        $filename = $_SERVER['DOCUMENT_ROOT'] . '/sale-orders/'.$fname; 
        $download_link = site_url().'/sale-orders/'.$fname; 
        
        //Generate date format
        $date = date("Y-m-d");
       	$currentMonth = date("F", strtotime ( $date )) ;
       	$nextMonth = date("F Y", strtotime ( '+1 month' , strtotime ( $date ) )) ;
       	
       	$deliveryData = $currentMonth."/".$nextMonth;
       	$cutOffDate = date("l mS F Y", strtotime ( $date )) ;

        //Generate file
        $custom_addresses_shipping  = THWMA_Utils::get_custom_addresses($customer_id, 'shipping');
        $collection = $_POST['collection'];
        $address_key = $_POST['address_key'];

        //EXPLODE COLLECTION
        $explode_data = explode("-", $collection);
        $selected_brands = $explode_data[0];
        $selected_collection = $explode_data[1];

        $address = $custom_addresses_shipping[$address_key];
        $contact_name = $address['shipping_first_name']." ".$address['shipping_last_name'];
        $store_name = $address['shipping_store_name'];
        $email = $address['shipping_email'];
        $phone = $address['shipping_phone'];
        $delivery_address = $address['shipping_address_1'].",".$address['shipping_address_2'].",".$address['shipping_city'].",".$address['shipping_state'].",".$address['shipping_postcode'];

        $spreadsheet = \PhpOffice\PhpSpreadsheet\IOFactory::load($file);
        $worksheet = $spreadsheet->getActiveSheet();

        $worksheet->getCell('B1')->setValue($collection." Order Form");
        $worksheet->getCell('B2')->setValue("Cut off for Orders ".$cutOffDate); 
        $worksheet->getCell('O2')->setValue("Delivery Date: ".$deliveryData); 
        $worksheet->getCell('D4')->setValue($contact_name); //CONTACT NAME
        $worksheet->getCell('j4')->setValue($phone); //PHONE
        $worksheet->getCell('D5')->setValue($email); //EMAIL
        $worksheet->getCell('D6')->setValue($delivery_address); //DELIVERY ADDRESS
        $worksheet->getCell('D8')->setValue($store_name); //STORENAME

        $worksheet->getStyle('B10:R10')->getFill()
          ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
          ->getStartColor()->setARGB(\PhpOffice\PhpSpreadsheet\Style\Color::COLOR_BLACK);

        $worksheet->getStyle('B10')->getFont()->setName('Arial')->setSize(9)->setBold(true);
        $worksheet->getStyle('C10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('D10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('E10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('F10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('G10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('H10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('I10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('J10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('K10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('L10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('M10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('N10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('O10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('P10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('Q10')->getFont()->getColor()->setRGB('FFFFFF');
        $worksheet->getStyle('R10')->getFont()->getColor()->setRGB('FFFFFF');

        $worksheet->getStyle('E:F')->getAlignment()->setHorizontal('center');
        $worksheet->getStyle('P:Q')->getAlignment()->setHorizontal('center');

        //GET PRODUCT LIST DATA
        $args = array(
          'post_type'      => 'product',
          'post_status'    => 'publish',
          'numberposts'   => -1,
          'tax_query' => array(
              'relation' => 'AND',
              array (
                  'taxonomy' => 'collection',
                  'field' => 'name',
                  'terms' => $selected_collection,
              ),
              array (
                  'taxonomy' => 'brands',
                  'field' => 'name',
                  'terms' => $selected_brands,
              )
          )
        );

        $products = get_posts($args);
        foreach ($products as $key => $product) {
          $_product = wc_get_product( $product->ID );
          $productData[$key]['styleCode'] = get_post_meta( $product->ID, 'style_code', true );

          $productData[$key]['ProductName'] = $product->post_title;
          $productData[$key]['Color'] = "Blue";
          $productData[$key]['Sizing'] = "8-20";
          $productData[$key]['RRPActual'] = $_product->get_price();
          $productData[$key]['DeliveryDate'] = $deliveryData;
        }

        $i = 13;
        foreach ($productData as $key => $value) {
          
          $worksheet->getStyle("D$i")->getAlignment()->setWrapText(true); 

          $worksheet->getCell("C$i")->setValue($value['styleCode']);
          $worksheet->getCell("D$i")->setValue($value['ProductName']);
          $worksheet->getCell("E$i")->setValue($value['Color']);
          $worksheet->getCell("F$i")->setValue($value['Sizing']);
          $worksheet->getCell("Q$i")->setValue($value['RRPActual']);
          $worksheet->getCell("R$i")->setValue($value['DeliveryDate']);
          $i++;
        }

        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save($filename);

        if(file_exists($filename)){
          echo json_encode(array("status"=>"success", "message" => $download_link )) ;
        }else{
          echo json_encode(array("status"=>"error", "message" => "something went wrong with file!" )) ;
        }

      }else{
          echo json_encode(array("status"=>"error", "message" => "something went wrong" )) ;
      }

      exit;
    }

    //My account page
    add_filter ( 'woocommerce_account_menu_items','misha_log_history_link', 40 );
    function misha_log_history_link( $menu_links ){
 
      $menu_links = array_slice( $menu_links, 0, 5, true ) 
        + array( 'sales-order-form' => 'Sales order form' )
        + array_slice( $menu_links, 5, NULL, true );
      
      return $menu_links;
    }

    add_action( 'init', 'misha_add_endpoint' );
    function misha_add_endpoint() {
      add_rewrite_endpoint( 'sales-order-form', EP_PAGES ); 
    }

    add_action( 'woocommerce_account_sales-order-form_endpoint', 'misha_my_account_endpoint_content');
    function misha_my_account_endpoint_content() {
     	
     	$active_season = WC_Admin_Settings::get_option( 'active_season' );
    	$brandsData = get_terms( 'brands', array(
        'hide_empty' => false,
    	));
    	$collection = array();
    	foreach ($brandsData as $data) {
      	if($data->slug == 'classified' || $data->slug =='democracy'){
          	$collection[ucfirst($data->name)."-".ucfirst($active_season)] = $data->name ."-".ucwords($active_season);
  		}
    	}
   
    	$customer_id = get_current_user_id();
    	$custom_addresses_shipping  = THWMA_Utils::get_custom_addresses($customer_id, 'shipping');
    	foreach ($custom_addresses_shipping as $key => $value) {
      	$addresses[] = array(
          "label" => $value["shipping_store_name"].",".$value['shipping_address_1'].",".$value['shipping_address_2'],
          "key" => $key
          ); 
    	}
        
      ?>
  		<div class="message"></div>
  		<form id="generateSheet" method="post">
    		<input type="hidden" name="action" value="edit_spreadsheet_data">
    		<div class="form-group">
      			<label>Collections</label>
      			<select class="form-control" id="collection" name="collection">
            		<option value="">Select Collection</option>
		            <?php
		              foreach ($collection as $key=>$data) {
		                echo "<option value='".$key."'>".$data."</option>"; 
		              }
		            ?>
      			</select>
    		</div>
    		<div class="form-group">
      			<label>Address</label>
  				<select class="form-control" name="address_key" id="address_key">
    				<option value="">Select Address</option>
		            <?php 
		              foreach ($addresses as $key => $address) {
		                echo "<option value='".$address["key"]."'>".$address['label']."</option>"; 
		              }
		            ?>
  				</select>
			</div>
	        <div class="form-group">
	            <button type="submit" id="generateBtn" class="btn btn-primary">Generate link</button>
	        </div>
   		</form>
    <?php
  	}
