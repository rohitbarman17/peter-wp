<?php
   /*
   Plugin Name: Sales order create
   Plugin URI: 
   description: This plugin is used for import sale order data in our site.
   Version: 1.0
   Author: Mr. Geeks
   Author URI: 
   License: Free
   */
?>

<?php 
  require_once __DIR__.'/generate_order_file.php';
  require_once __DIR__.'/simplexlsx/src/SimpleXLSX.php';
  //require_once __DIR__.'/simplexlsxgen/src/SimpleXLSXGen.php';
  require_once __DIR__.'/spreadsheet/vendor/autoload.php';

  use PhpOffice\PhpSpreadsheet\Spreadsheet;
  use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
  use PhpOffice\PhpSpreadsheet\IOFactory;
// Exit if accessed directly
if ( ! defined( 'ABSPATH' ) ){
    exit;
}
 
    if ( ! function_exists( 'add_filter' ) ) {
      header( 'Status: 403 Forbidden' );
      header( 'HTTP/1.1 403 Forbidden' );
    exit();
    }

    if ( ! defined( 'SALE_ORDER' ) ) {
      define( 'SALE_ORDER', __FILE__ );
    }


    if ( ! defined( 'SALE_ORDER_PATH' ) ) {
      define( 'SALE_ORDER_PATH', plugin_dir_path( SALE_ORDER ) );
    }

    $error = array();
    // add_action( 'wp_ajax_auto_create_sales_orders', array($this,'create_sales_orders_submit'));
    add_action('admin_enqueue_scripts','my_enqueue');
    function my_enqueue($hook) {
      wp_enqueue_script('custom_order', plugin_dir_url(__FILE__) . '/assets/order.js',false, '1.0.0');
      wp_enqueue_style('style', plugin_dir_url(__FILE__) . '/assets/style.css');
    }
    add_action('wp_enqueue_scripts', 'collectiveray_load_js_script');
    function collectiveray_load_js_script(){
        wp_enqueue_script('generateSheet', plugin_dir_url(__FILE__) . '/assets/generateSheet.js',false, '1.0.0', true);
    }

    add_action( 'admin_menu', 'register_my_custom_submenu_page' );
    function register_my_custom_submenu_page() {
      add_submenu_page('woocommerce', 'Create sales order', 'Create Sale Order', 'manage_woocommerce', 'auto-create-sales-orders', 'create_sales_order_callback');
    }
    
    function create_sales_order_callback() {?>
        <?php settings_errors(); ?>
        <script type="text/javascript">
        var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
        </script>

        <h3>Create Sale Order</h3>
        <!-- action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>"  -->
        <form id="saleOrderImport" class="form-inline" action="<?php echo esc_url( admin_url( 'admin-post.php' ) ); ?>" name="import" method="post" enctype="multipart/form-data">
            <!-- <input type="file" name="file" /><br /> -->
            <input type="hidden" name="action" value="auto_create_sales_orders">
            <button type="submit" name="submit">Submit</button>
        </form>
        <div>
          <ul class="error"></ul>
        </div>
        <?php
    }

    add_action( 'admin_post_auto_create_sales_orders','create_sales_orders_submit');
    function create_sales_orders_submit(){
      
      if(isset($_POST) && array_key_exists('action', $_POST) && $_POST['action'] == "auto_create_sales_orders"){
         
          $file = dirname( SALE_ORDER ) . '/order-formdata-peter.xlsx';
          $orderData = filterProductData($file);
          if(empty($orderData['user_email'])){
            $error[] = 'User email address is empty!';
          }else{
            $user_email = $orderData['user_email'];
            $user = get_user_by('email',$user_email);
            print_r($user);die;
            if(!$user){
              $error[] = 'Email address not found!';
            }
          }

          if(empty($orderData['brand'])){
            $error[] = 'Brands is empty!';
          }else{
            $term = get_term_by('name', $orderData['brand'], 'brands'); 
            if($term){
            $error[] = 'Invalid brand!';

            }
          }

          if(empty($orderData['season'])){
            $error[] = 'Season is empty!';
          }else{
            $term = get_term_by('name', $orderData['season'], 'collection'); 
            if($term){
            $error[] = 'Invalid brand!';

            }
          }

          if(empty($orderData['products'])){
            $error[] = 'Products are empty';
          }
          
          customOrder($orderData);
      }
     
    }

    function customOrder($orderData){
      $productData = array();
      $products = filterProductRow($orderData['products']);
      foreach ($products as $key => $product) {
        $attributes = array();
        $productOject = get_page_by_title(trim($product['Product Name']), OBJECT, 'product' );
        if(empty($productOject)){
          $error[] = $product['Product Name']. ' product not found';
        }else{
          $p_attributes = getProductAttributes($productOject->ID);
          die("ocean");
          if(!empty($product['Colour A'])){
            $colour = $product['Colour A'];
            if(strstr($colour, '/')){
              $colour = preg_replace('/\s+/', '', $colour);
            }
            $colorSlug = getColorSlug(trim($colour));
            $attributes['attribute_pa_colour' ]= $colorSlug;
            
          }
          
          $sizes = productSize($product);
          if(!emptyArray($sizes) && in_array('pa_size', $p_attributes)){
            foreach ($sizes as $sizeSlug => $quantity) {
              if(!empty($quantity)){
                $attributes['attribute_pa_size' ]= $sizeSlug;
                $product_id = find_matching_product_variation_id($productOject->ID,$attributes);
                if(empty($product_id)){
                  $error[] = $product['Product Name'] ."with colour not found"; 
                }else{
                  $productData[] = array("product_id" => $product_id , "quantity" => $quantity);
                }
              }
            }
          }else{
            $product_id = find_matching_product_variation_id($productOject->ID,$attributes);
            if(empty($product_id)){
              $error[] = $product['Product Name'] ."with colour not found"; 
            }else{
              $productData[] = array("product_id" => $product_id , "quantity" => 1);
            }
          }
        }
      }

      //ERROR IF EXSISTS
      if(empty($error)){
        createOrder($orderData, $productData);
      }else{
        echo json_encode(array("status" => "error", "message"=> array_unique($error)));
        exit();
      }

    }


    //https://codeblock.co.za/how-to-create-an-order-programmatically-in-woocommerce/
    function createOrder($orderData, $productData){
      $brand = $orderData['brand'];
      $season = $orderData['season'];

      $user_email = $orderData['user_email'];
      $user = get_user_by('email',$user_email);
      
      $user_id = $user->ID;
      $user_login = $user->user_login;
      // Create the order and assign it to the curr
      $new_order = wc_create_order(array('customer_id' => $user_id));
      foreach ($productData as $key => $product) {
        $new_order->add_product( wc_get_product($product['product_id']), $product['quantity'] );
      }
      
      $new_order->calculate_totals();
      // Update the status and add a note
      $new_order->update_status('pending', 'Order added programmatically!', true);
      // Save
      $new_order->save();
      $order_id = $new_order->get_ID();
      //UPDATE SALE ORDER NUMBER
      $fistBrand = mb_substr(ucfirst($brand), 0, 1);
      $collection = $fistBrand.$season;
      $sales_order_num = $user_login."-".$collection."-".$order_id; 
      update_post_meta($order_id, 'sales_order_number' , $sales_order_num );
      //UPDATE SALE ORDER SEASON
      $term = get_term_by('name', $season, 'collection'); 
      update_post_meta($order_id, 'season' , $term->term_id );
      
     
    }

    function getBillingAddress(){
      $billing_address = array(
         'first_name' => get_user_meta( $user_id, 'first_name', true ),
         'last_name'  => get_user_meta( $user_id, 'last_name', true ),
         'company'    => get_user_meta( $user_id, 'billing_company', true ),
         'email'      => get_user_meta( $user_id, 'billing_email', true ),
         'phone'      => get_user_meta($user_id, 'billing_phone', true ),
         'address_1'  => get_user_meta( $user_id, 'billing_address_1', true ),
         'address_2'  => get_user_meta( $user_id, 'billing_address_2', true ),
         'city'       => get_user_meta( $user_id, 'billing_city', true ),
         'state'      => get_user_meta( $user_id, 'billing_state', true ),
         'postcode'   => get_user_meta( $user_id, 'billing_postcode', true ),
         'country'    => get_user_meta( $user_id, 'billing_country', true )
     );
    }
    function productSize($product){
      foreach ($product as $key => $data) {
        switch ($key) {
          case '8/XS':
            $attribute_pa_size['8-xs'] = !empty($data) ? $data : Null;
          break;
          case '10/S':
            $attribute_pa_size['10-s'] = !empty($data) ? $data : Null;
          break;
          case '12/M':
            $attribute_pa_size['12-m'] = !empty($data) ? $data : Null;
          break;
          case '14/L':
            $attribute_pa_size['14-l'] = !empty($data) ? $data : Null;
          break;
          case '16/XL':
            $attribute_pa_size['16-xl'] = !empty($data) ? $data : Null;
          break;
          case '18/XXL':
            $attribute_pa_size['18-xxl'] = !empty($data) ? $data : Null;
          break;
          case '20/3XL':
            $attribute_pa_size['20-3xl'] = !empty($data) ? $data : Null;
          break;
        }
      }
      return $attribute_pa_size;
    }
/*----------------------------GET PRODUCT ATTRIBUTES-------------------------------------------------------------------------*/
     function getProductAttributes($productId){
       $attributes = get_post_meta( $productId , '_product_attributes', true );
       return array_keys($attributes);

    }

/*----------------------------GET PRODUCT COLOUR-------------------------------------------------------------------------*/
     function getColorSlug($colour){
      $colourSlug = get_term_by('name', $colour, 'pa_colour');
      if(empty($colourSlug)){
        $error[] = $colour ." colour not found in our system";
      }else{
        return $colourSlug->slug;
      }
    }
     function find_matching_product_variation_id($product_id, $attributes)
    {
        return (new \WC_Product_Data_Store_CPT())->find_matching_product_variation(
            new \WC_Product($product_id),
            $attributes
        );
    }

/*----------------------------FITER PRODUCT DATA-------------------------------------------------------------------------*/

     function filterProductData($file){
      $header = false;
      $endRow = false;
      $headerKey = [];
      if ( $xlsx = SimpleXLSX::parse($file) ) {
        foreach ($xlsx->rows() as $key => $row) {
          //GET PRODUCT BRAND AND SEASON
            if(strpos($row[1], 'Order Form') !== false){
              $string = preg_replace('/\s+/', ' ', $row[1]);
              $collection = explode(" ", $string);
              
              $data['brand'] = $collection[0];
              $data['season'] = $collection[1];
            }
          //GET STORE CONTACT
            if(strpos($row[1], 'Contact Email') !== false){
              $data['user_email'] = $row[3];
            }
          //GET PRODUCT STORE NAME
            if(strpos($row[1], 'Store Name') !== false){
              $data['store_name'] = $row[3];
            }
          //GET DELIVERY ADDRESS
            if(strpos($row[1], 'Delivery Address') !== false){
              $data['delivery_address'] = $row[3];
            }
          //GET PHONE NUMBER
            if(strpos($row[8], 'Phone') !== false){
              $data['phone'] = $row[9];
            }
          //GET PRODUCT DATA
            if($row[3] == 'Product Name'){
                $headerKey = $row;
                $header = true;
            }
            if(strpos($row[1], 'Totals') !== false){
              $endRow = true;
            }

            if($header && !$endRow){
              $data['products'][] = array_combine($headerKey, $row); 
            }
        }
        return $data;
       
      } else {
        return SimpleXLSX::parseError();
      }
    }

     function filterProductRow($products){
      $totalRowKey = count($products) - 1;
      unset($products[0]);
      return $products;
    }

    function emptyArray($array) {
      $empty = TRUE;
      if (is_array($array)) {
        foreach ($array as $value) {
          if (!emptyArray($value)) {
            $empty = FALSE;
          }
        }
      }
      elseif (!empty($array)) {
        $empty = FALSE;
      }
      return $empty;
    }

    
?>