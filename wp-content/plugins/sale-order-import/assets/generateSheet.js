jQuery(document).ready(function(){
	jQuery("#generateSheet").on("submit", function(e){
		e.preventDefault();
		var collection =  jQuery("#collection option:selected" ).val();
		if(!collection){
			alert("Please select collection");
			return false;
		}
		//Address value check
		var address_key = jQuery("#address_key option:selected" ).val();
		if(!address_key){
			alert("Please select address");
			return false;
		}
		
		jQuery.ajax({
			url:ajax_url,
			type:"POST",
			processData: false,
			contentType: false,
			data:  new FormData(this),
			success : function( response ){
				var object = JSON.parse(response);
				if(object.status == "success"){
					jQuery(".message").html('<div class="alert alert-success" role="alert">File is generated!! <a href="'+object.message+'">Click here</a> to download.</div>');
				}

				if(object.status == "error"){
					jQuery(".message").html('<div class="alert alert-danger" role="alert">'+object.message+'</div>');
				}
			},
		});
		

	});
});