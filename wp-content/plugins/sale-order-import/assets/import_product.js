jQuery(document).ready(function(){
	
	jQuery("#product_import").on("submit", function(e){
		e.preventDefault();
		var spinner = jQuery(".icon-spinner");
		var buttonload = jQuery(".buttonload");
		var spreadsheet =  jQuery("#spreadsheet").val();
		var tableHtml = jQuery(".tableHtml");
		if(!spreadsheet){
			alert("Please select a spreadsheet");
			return false;
		}
		spinner.fadeIn();
		buttonload.prop('disabled', true);
		jQuery.ajax({
			url:ajax_url,
			type:"POST",
			processData: false,
			contentType: false,
			data: new FormData(this),
			success : function( response ){
				spinner.fadeOut();
				buttonload.prop('disabled', false);
				var object = JSON.parse(response);
				var error = "";
				var message = "";
				if(object.status == "success"){
					message = "<li class='success'>"+object.html+"</li>";	
					jQuery(tableHtml).html(message);
				}

				if(object.status == "error"){
					error = "<li class='success'>"+object.html+"</li>";	
					jQuery(tableHtml).html(error);
				}
			},
		});
	});

	jQuery(document).on('click','.import-row',function(){

		var This = jQuery(this);
		var spinner = jQuery(this).find(".icon-spinner");
		var header = [];
		var data = [];
		var messageHtml = jQuery(".message"); 

		formData = new FormData();
		//HEADER DATA
		jQuery('tr.header').find('td.data').each(function(index, element) {
	        header.push(jQuery(element).html());
	    });

		//SELECTED ROW DATA
		jQuery(this).closest('tr').addClass("selectedRow");
		jQuery(this).closest('tr').find('td.data').each(function(index, element) {
	        data.push(jQuery(element).html());
	    });

		console.log("header length", header.length);
		console.log("data length", data.length);
	    
	    if(header.length == data.length){
	    	for (var i = 0; i < header.length; i++) {
			    formData.append(header[i], data[i]);
			}
	    }else{
	    	alert("file is not formated");
	    	return false;
	    }

		spinner.fadeIn();
		formData.append( 'action', 'product_import_action');
		jQuery.ajax({
			url:ajax_url,
			type:"POST",
			processData: false,
			contentType: false,
			data: formData,	
			success : function( response ){
				var object = JSON.parse(response);
				spinner.fadeOut();
				if(object.status == "error"){
					let error = "<li class='error'>"+object.message+"</li>";
					jQuery(messageHtml).html(error);
				}else{
					jQuery(This).css("background-color", "Green");
					jQuery(This).text("Done");
					jQuery(This).prop('disabled', true);
				}
				console.log("response", response);
			},
		});
	})

	jQuery(document).on('click','.import-all', function(){
		jQuery(document).find('tr.product-row').each(function(index, element) {
        	jQuery(element).find(".import-row").click();
	    });
	});		

	function toObject(header, data) {
	    var result = [];
	    for (var i = 0; i < header.length; i++)
	         result[header[i]] = data[i];
	    return result;
	}
})