jQuery(document).ready(function(){
	
	jQuery("#saleOrderImport").on("submit", function(e){
		e.preventDefault();
		var form = jQuery(this);
		var spinner = jQuery(".icon-spinner");
		var buttonload = jQuery(".buttonload");
		var spreadsheet =  jQuery("#spreadsheet").val();
		var messageHtml = jQuery("ul.message");
		jQuery(messageHtml).html("");
		if(!spreadsheet){
			alert("Please select a spreadsheet");
			return false;
		}
		spinner.fadeIn();
		buttonload.prop('disabled', true);
		jQuery.ajax({
			url:ajax_url,
			type:"POST",
			processData: false,
			contentType: false,
			data: new FormData(this),
			success : function( response ){
				spinner.fadeOut();
				buttonload.prop('disabled', false);
				var object = JSON.parse(response);
				var error = "";
				var message = "";
				console.log("status", object.status);
				if(object.status == "success"){
					message = "<li class='success'>"+object.message+"</li>";	
					jQuery(messageHtml).html(message);
					jQuery(form)[0].reset();
				}

				if(object.status == "error"){
					jQuery.each(object.message, function(key, value){
						error += "<li class='error'>"+value+"</li>";	
					})
					jQuery(messageHtml).html(error);
				}
			},
		});
	});
})