 <?php /* Template Name: Brand-Template */ ?>

<?php get_header();?>
	
  	  <!--About Start-->
	  <section class="about-w">
	    <div class="container">
	      <h2>About</h2>
	      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation </p>
	    </div>
	  </section>
	  <!--About End-->


	  <!--Current Collection Start-->
	  <section class="cc-w">
	    <div class="container">
	      <div class="row">
	        <div class="col-lg-5">
	          <div class="img-cc">
	            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/cc-img.jpg" alt="">
	          </div>
	        </div>
	        <div class="col-lg-6 offset-lg-1  d-flex align-items-center">
	          <div class="info-cc">
	            <h5>Current Collection - Summer '21'</h5>
	            <p>Our edit of what to pack will have your holiday ready in no time.</p>
	            <a href="#">Discover The Collection</a>
	          </div>

	        </div>
	      </div>
	    </div>
	  </section>
	  <!--Current Collection End-->

	  <section class="ss-2021">
	    <div class="container">
	      <h2>Previous Collections</h2>
	      <h3>Spring Summer 2021</h3>

	      <div class="cc-grid">
	        <div class="left-cc">
	          <div class="img-ss">
	            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/ss-img-1.jpg" alt="">
	          </div>
	          <div class="text-center">
	            <p>Title will come here</p>
	            <a href="#">Discover The Collection</a>
	          </div>
	        </div>
	        <div class="right-cc">
	          <div class="img-ss">
	            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/ss-img-1.jpg" alt="">
	          </div>

	        </div>
	      </div>
	    </div>
	  </section>

	  <section class="ss-2021">
	    <div class="container">

	      <h3>Spring Summer 2021</h3>

	      <div class="cc-grid">
	        <div class="left-cc">
	          <div class="img-ss">
	            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/ss-img-3.jpg" alt="">
	          </div>
	          <div class="text-center">
	            <p>Title will come here</p>
	            <a href="#">Discover The Collection</a>
	          </div>
	        </div>
	        <div class="right-cc">
	          <div class="img-ss">
	            <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/ss-img-4.jpg" alt="">
	          </div>

	        </div>
	      </div>
	    </div>
	  </section>


<?php get_footer(); ?>