<?php /* Template Name: Homepage-Template */ ?>

<?php get_header();?>
	
	  <!--About Start-->
	  <section class="about-w">
	    <div class="container">
	      <h2>About</h2>
	      <p>Democracy Ltd designs and supplies premium quality women's clothing brands Democracy and Classified, exclusively for the New Zealand market. Uniforms by Democracy Ltd serves schools, clubs and corporate clients with custom and standardised uniform needs. </p>

	      <p>The Democracy clothing brand is designed in New Zealand for the woman who loves wearing comfortable yet stylish lifestyle clothing of exceptional quality. Our Democracy lady loves textures and prints, making effortless choices knowing she is on trend.</p>

	      <p>The Classified clothing brand is inspired by garments from countries around the world encompassing new trends and prints that are timeless, powerful, with a touch of elegance. Classified is high quality clothing that is designed in New Zealand with a classic yet edgy touch, enabling women to feel confident in the way they dress.</p>
	    </div>
	  </section>
	  <!--About End-->

	  <!--View Our Brands Start-->
	  <section class="vob-w">
	    <div class="container">
	      <h2>View Our Brands</h2>
	      <div class="row">
	        <div class="col-lg-4">
	          <div class="img-vob">
	           <a href="/brands/classified/"> <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/vob-img-1.jpg" alt=""></a>
	          </div>
	          <div class="brand-vob">
	            <a href="/brands/classified/"> <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/brand-vob-1.jpg" alt=""></a>
	          </div>
	        </div>
	        <div class="col-lg-4 align-self-center">
	          <div class="img-vob">
	            <a href="/brands/uniforms/"> <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/vob-img-2.jpg" alt=""></a>
	          </div>
	          <div class="brand-vob">
	            <a href="/brands/uniforms/"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/brand-vob-2.jpg" alt=""></a>
	          </div>
	        </div>
	        <div class="col-lg-4">
	          <div class="img-vob">
	            <a href="/brands/democracy/"> <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/vob-img-3.jpg" alt=""></a>
	          </div>
	          <div class="brand-vob">
	            <a href="/brands/democracy/"> <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/brand-vob-3.jpg" alt=""></a>
	          </div>
	        </div>
	      </div>
	    </div>
	  </section>
	  <!--View Our Brands End-->


<?php get_footer(); ?>