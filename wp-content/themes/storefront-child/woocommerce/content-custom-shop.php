<?php
/**
 * The template for displaying product content within loops
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;

// Ensure visibility.
if ( empty( $product ) || ! $product->is_visible() ) {
	return;
}
?>
  <!--Lookbook Product Start-->
  <section class="lookbook-product-w">
    <div class="container">
      <h2>Democracy W21 Look Book</h2>
      <div class="select-category">
        <label>Select a category:</label>
        <select>
          <option>All</option>
        </select>
      </div>

      <div class="product-lookbook">
        <div class="row">
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4 col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
          <div class="col-lg-4  col-md-6">
            <div class="product-column">
              <a class="redirect" href="#"></a>
              <div class="img-product-lookbook">
                <img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/lookbook-thumbnail.jpg" alt="">
              </div>
              <h5>Alicia Side stripe pant</h5>
              <h6>RRP $139.90</h6>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!--Lookbook Product End-->
