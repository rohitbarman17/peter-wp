<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>

<style>
table.details,th.header,td.data{
  border: 1px solid black;
  border-collapse: collapse;
  width: 100%;
  text-align: center;
}

table.order-details,th.hd,td.dt{
  border: 1px solid black;
  border-collapse: collapse;
  width: 100%;
  text-align: center;
}

table.head.container {
	width: 100%;
}
td.shop-info {
    text-align: right;
}

h2.site-logo {
    font-size: 100px;
    font-family: 'Great Vibes', cursive;
	padding-top: 30px;
    color: #AC9F99;
    margin-bottom: 0px !important;
}

table.head.container td:first-child {
    text-align: center;
    width: auto;
}
table.head.container td {
    width: 33.33%;
}
table.head.container td:nth-child(2) {
    text-align: center;
    padding: 5px;
}
h1.document-type-label {
    padding-right: 0px;
    margin-bottom: 12px;
}

table.totals {
    width: 100%;
    text-align: right;
}

table.totals td.price {
    text-align: right;
}

</style>
<?php 
	
	function getProductName($itemName){
	    $name = explode("-",$itemName);
	    return $name[0];
	}

	$discount = 0;
	// Get the WC_Coupon object
	foreach( $this->order->get_coupon_codes() as $coupon_code ) {
	    
	    $coupon = new WC_Coupon($coupon_code);

	    $discount_type = $coupon->get_discount_type(); // Get coupon discount type
	    $coupon_amount = $coupon->get_amount(); // Get coupon amount
	    $discount += $coupon_amount;
	}

	//Collection name
	$season_id = $this->order->get_meta('season');
	$term = get_term_by('id', $season_id, 'collection'); 
	$shippingBrand = $this->order->get_meta('_shipping_brands');
	
	foreach ($shippingBrand as $key => $brand) {
		$string[] = ucfirst($brand) ." &nbsp;".$term->name;
	}

	$collection_name = implode("&nbsp;and&nbsp;", $string);
	//Sale order number
	$sales_order_num = $this->order->get_meta('sales_order_number');
	$custom_invoice_number = $this->order->get_meta('custom_invoice_number');
	
	$invoice_date = date('d-m-Y',strtotime($this->get_invoice_date()));
	$payment_due_date = date('d-m-Y', strtotime($invoice_date. ' +30 days'));
	
	$compactItems = [];
	$items = $this->get_order_items(); 
	if( sizeof( $items ) > 0 ){
		foreach( $items as $item_id => $item ){
			$price = get_post_meta($item['variation_id'], '_regular_price', true);
			$data['name'] = getProductName($item['name']); 
			$data['variation_id'] = $item['variation_id']; 
			$data['attibutes'] = wc_get_product_variation_attributes($item['variation_id']); 
			$data['quantity'] = $item['quantity']; 
			$data['single_price'] = trim(str_replace( '$', '', $price));

			$compactItems[$item['product_id']][] = $data;

 		}
	}

	$productData = array();
	$totalItems = 0;
	$shipping_date = $this->order->get_meta('shipping_date');
	$date = !empty($shipping_date) ? DateTime::createFromFormat('Ymd', $shipping_date) : '';
	
	if(!empty($compactItems)){
		foreach ($compactItems as $key => $product) {
			$quantity = 0; 
			$color = 'Null';
			foreach($product as $key2 => $value) {
				$totalItems += $value['quantity'];

				if($color != $value['attibutes']['attribute_pa_colour']){
					$quantity = 0;
				}
				$color = !empty($value['attibutes']['attribute_pa_colour']) ? $value['attibutes']['attribute_pa_colour'] : 'Null';
				$quantity += $value['quantity'];	
				$productData[$key][$color]['name'] = $value['name'];
				$productData[$key][$color]['color'] = $color;
				$productData[$key][$color]['quantity'] = $quantity;
				$productData[$key][$color]['single_price'] =$value['single_price'];
				if(!empty($value['attibutes']['attribute_pa_size'])){
					$productData[$key][$color]['size'][$value['attibutes']['attribute_pa_size']] = $value['quantity'];	
				}
			}
		}
	}


?>
<table class="head container">
	<tr>
		<td></td>
		<td>
			<?php 
				if($shippingBrand == 'democracy'){
					echo '<img src="'.site_url().'/wp-content/uploads/2021/11/logo-demo.png">';
				}
				if($shippingBrand == 'classified'){
					echo '<img src="'.site_url().'/wp-content/uploads/2021/11/Screenshot-1-e1636526436632.png">';
				}
			  ?>
			<h1 class="document-type-label">
			<?php if( $this->has_header_logo() ) echo $this->get_title(); ?>
			</h1>
		</td>
		<td class="shop-info">
			<?php
			if( $this->has_header_logo() ) {
				$this->header_logo();
			} else {
				echo $this->get_title();
			}
			do_action( 'wpo_wcpdf_before_shop_name', $this->type, $this->order ); ?>
			<!-- <div class="shop-name"><h3><?php $this->shop_name(); ?></h3></div> -->
			<?php do_action( 'wpo_wcpdf_after_shop_name', $this->type, $this->order ); ?>
			<?php do_action( 'wpo_wcpdf_before_shop_address', $this->type, $this->order ); ?>
			<div class="shop-address"><?php $this->shop_address(); ?></div>
			<?php do_action( 'wpo_wcpdf_after_shop_address', $this->type, $this->order ); ?>
		</td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>
<table class='details'>
<tr>
	<th class='header'><?php _e( 'Date','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Sales Order Number','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Collection Name','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Invoice items','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Shipping Date','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Invoice Number','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Payment Due Date','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
</tr>
<tr>
	<td class='data'><?=  $invoice_date; ?></td>
	<td class='data'><?= $sales_order_num; ?></td>
	<td class='data'><?= ucfirst($collection_name); ?></td>
	<td class='data'><?= $totalItems; ?></td>
	<td class='data'><?= !empty($date) ? $date->format('d-m-Y') : ''; ?></td>
	<td class='data'><?= $custom_invoice_number; ?></td>
	<td class='data'><?= $payment_due_date; ?></td>
</tr>
</table>
</br>
<h4 style="padding-top:15px;">REMARKS: &nbsp;This invoice relates to part of your order. The balance of your order will be invoiced separately.</h4>
<hr>
<table class="order-data-addresses">
	<tr>
		<td class="address billing-address">
			<h3><?php _e( 'Customer:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
			<?php do_action( 'wpo_wcpdf_before_billing_address', $this->type, $this->order ); ?>
			<?php $this->billing_address(); ?>
			<?php do_action( 'wpo_wcpdf_after_billing_address', $this->type, $this->order ); ?>
			<?php if ( isset($this->settings['display_email']) ) { ?>
			<div class="billing-email"><?php $this->billing_email(); ?></div>
			<?php } ?>
			<?php if ( isset($this->settings['display_phone']) ) { ?>
			<div class="billing-phone"><?php $this->billing_phone(); ?></div>
			<?php } ?>
		</td>
		<td class="address shipping-address">
			<?php if ( !empty($this->settings['display_shipping_address']) && ( $this->ships_to_different_address() || $this->settings['display_shipping_address'] == 'always' ) ) { ?>
			<h3><?php _e( 'Ship To:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
			<?php do_action( 'wpo_wcpdf_before_shipping_address', $this->type, $this->order ); ?>
			<?php $this->shipping_address(); ?>
			<?php do_action( 'wpo_wcpdf_after_shipping_address', $this->type, $this->order ); ?>
			<?php } ?>
		</td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>

<table class="order-details">
	<thead>
		<tr>
			<th class="hd styleNumber"><?php _e('Style Number', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="hd itemDescription"><?php _e('Item Description', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="hd colour"><?php _e('Colour', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="hd" colspan="7"></th>
			<!-- <th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th>
			<th></th> -->
			<th class="hd total"><?php _e('Total', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="hd unitPrice"><?php _e('Unit Price', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="hd price"><?php _e('Price', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
		</tr>
		<tr>
			<th class="hd styleNumber"></th>
			<th class="hd itemDescription"></th>
			<th class="hd colour"></th>
			<th class="hd">XS/8</th>
			<th class="hd">S/10</th>
			<th class="hd">M/12</th>
			<th class="hd">L/14</th>
			<th class="hd">XL/16</th>
			<th class="hd">XXL/18</th>
			<th class="hd">XXXL/20</th>
			<th class="hd total"></th>
			<th class="hd unitPrice"></th>
			<th class="hd price"></th>
		</tr>
	</thead>
	<tbody>
		<?php 
			if( sizeof( $productData ) > 0 ) { 
				foreach( $productData as $product_id => $product ){
					foreach ($product as $color => $item) {
						$total = number_format($item['single_price'] * $item['quantity'], 2, '.', '');
					 ?>
						<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', 'item-'.$item_id, $this->type, $this->order, $item_id ); ?>">
							<td class="dt"><?php echo $item['style_number']; ?></td>
							<td class="dt itemDescription">
								<?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); ?>
								<span class="dt item-name"><?php echo $item['name']; ?></span>
							</td>
							<td class="dt"><?php echo ucfirst($item['color']); ?></td>
							<td class="dt"><?php echo $item['size']['8-xs']; ?></td>
							<td class="dt"><?php echo $item['size']['10-s']; ?></td>
							<td class="dt"><?php echo $item['size']['12-m']; ?></td>
							<td class="dt"><?php echo $item['size']['14-l']; ?></td>
							<td class="dt"><?php echo $item['size']['16-xl']; ?></td>
							<td class="dt"><?php echo $item['size']['18-xxl']; ?></td>
							<td class="dt"><?php echo $item['size']['20-3xl']; ?></td>
							<td class="dt total"><?php echo $item['quantity']; ?></td>
							<td class="dt unitPrice"><?php echo $this->format_price($item['single_price']); ?></td>
							<td class="dt price"><?= $this->format_price($total); ?></td>
						</tr>
		<?php 		}
				}
			}
		?>
	</tbody>
	<tfoot>
		<tr class="no-borders">
			<td class="no-borders"></td>
			<td class="no-borders">
				<div class="document-notes">
					<?php do_action( 'wpo_wcpdf_before_document_notes', $this->type, $this->order ); ?>
					<?php if ( $this->get_document_notes() ) : ?>
						<h3><?php _e( 'Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
						<?php $this->document_notes(); ?>
					<?php endif; ?>
					<?php do_action( 'wpo_wcpdf_after_document_notes', $this->type, $this->order ); ?>
				</div>
				<div class="customer-notes">
					<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>
					<?php if ( $this->get_shipping_notes() ) : ?>
						<h3><?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
						<?php $this->shipping_notes(); ?>
					<?php endif; ?>
					<?php do_action( 'wpo_wcpdf_after_customer_notes', $this->type, $this->order ); ?>
				</div>				
			</td>
			<td class="no-borders"></td>
			<td class="no-borders"></td>
			<td class="no-borders"></td>
			<td class="no-borders"></td>
			<td class="no-borders"></td>
			<td class="no-borders"></td>
			<td class="no-borders"></td>
			<td class="no-borders"></td>
			<td class="no-borders" colspan="3">
				<table class="totals">
					<tfoot>
						<?php foreach( $this->get_woocommerce_totals() as $key => $total ) : ?>
						<tr class="<?php echo $key; ?>">
							<th class="description"><?= ($key == 'discount') ? $total['label']."(".$discount."%)" : $total['label']; ?></th>
							<td class="price"><span class="totals-price"><?php echo $total['value']; ?></span></td>
						</tr>
						<?php endforeach; ?>
					</tfoot>
				</table>
			</td>
		</tr>
	</tfoot>
</table>

<div class="bottom-spacer"></div>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	<!-- hook available: wpo_wcpdf_before_footer -->
	<?php $this->footer(); ?>
	<!-- hook available: wpo_wcpdf_after_footer -->
</div><!-- #letter-footer -->
<?php endif; ?>
<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); 
?>
