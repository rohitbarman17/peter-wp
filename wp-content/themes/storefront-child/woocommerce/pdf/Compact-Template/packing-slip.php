<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>
<style>
table.details,th.header,td.data{
  border: 1px solid black;
  border-collapse: collapse;
  width: 100%;
  text-align: center;
}

table.order-details,th.hd,td.dt{
  border: 1px solid black;
  border-collapse: collapse;
  width: 100%;
  text-align: center;
}

table.head.container {
	width: 100%;
}
td.shop-info {
    text-align: right;
}

h2.site-logo {
    font-size: 100px;
    font-family: 'Great Vibes', cursive;
	padding-top: 30px;
    color: #AC9F99;
    margin-bottom: 0px !important;
}

table.head.container td:first-child {
    text-align: center;
    width: auto;
}
table.head.container td {
    width: 33.33%;
}
table.head.container td:nth-child(2) {
    text-align: center;
    padding: 5px;
}
h1.document-type-label {
    padding-right: 0px;
    margin-bottom: 12px;
}

tfoot span.data{
	padding: 15px;
	display: flex;
}

tfoot>td{
	border: 1px solid black !important;
}

</style>
<?php 

	//Collection name
	$season_id = $this->order->get_meta('season');
	$term = get_term_by('id', $season_id, 'collection'); 
	$termName = substr($term->name, 0, 1);
	switch ($termName) {
	  	case 'S':
	    	$season_name = "Summer";
	    break;
	    case 'W':
	    	$season_name = "Winter";
	    break;
	  	default:
	    	$season_name = '';
	}

	$seasonYear = substr($term->name,1,3);
	$season = $season_name.'&nbsp;'.$seasonYear;
	$shippingBrand = $this->order->get_meta('_shipping_brands');
	foreach ($shippingBrand as $key => $brand) {
		$string[] = ucfirst($brand) ." High&nbsp;".$season;
	}

	$collection_name = implode(" and ", $string);

	$sales_order_num = strtoupper($this->order->get_meta('sales_order_number'));
	$fullName =  $this->order->get_shipping_first_name()."&nbsp;".$this->order->get_shipping_last_name();
	$shippingAddress =  $this->order->get_shipping_address_1()."<br>".$this->order->get_shipping_address_2();
	$storeName =  $this->order->get_meta('_shipping_store_name');
	$phoneNumber = $this->order->get_shipping_phone();
	//Shipping date
	$shipping_date = $this->order->get_meta('shipping_date');
	$date = !empty($shipping_date) ? DateTime::createFromFormat('Ymd', $shipping_date) : '';

	$compactItems = [];
	$items = $this->get_order_items(); 
	if( sizeof( $items ) > 0 ){
		foreach( $items as $item_id => $item ){
			$price = get_post_meta($item['variation_id'], '_regular_price', true);
			$data['name'] = $item['product_id']; 
			$data['name'] = getProductName($item['name']); 
			$data['variation_id'] = $item['variation_id']; 
			$data['attibutes'] = wc_get_product_variation_attributes($item['variation_id']); 
			$data['quantity'] = $item['quantity']; 
			$data['single_price'] = trim(str_replace( '$', '', $price));

			$compactItems[$item['product_id']][] = $data;

 		}
	}

	function getProductName($itemName){
	    $name = explode("-",$itemName);
	    return $name[0];
	}

	$productData = array();
	$totalItems = 0;
	$shipping_date = $this->order->get_meta('shipping_date');
	$date = !empty($shipping_date) ? DateTime::createFromFormat('Ymd', $shipping_date) : '';
	
	if(!empty($compactItems)){
		foreach ($compactItems as $key => $product) {
			$quantity = 0; 
			$color = 'Null';
			foreach($product as $key2 => $value) {
				$totalItems += $value['quantity'];

				if($color != $value['attibutes']['attribute_pa_colour']){
					$quantity = 0;
				}
				$color = !empty($value['attibutes']['attribute_pa_colour']) ? $value['attibutes']['attribute_pa_colour'] : 'Null';
				$quantity += $value['quantity'];	
				$productData[$key][$color]['name'] = $value['name'];
				$productData[$key][$color]['color'] = $color;
				$productData[$key][$color]['quantity'] = $quantity;
				$productData[$key][$color]['single_price'] =$value['single_price'];
				if(!empty($value['attibutes']['attribute_pa_size'])){
					$productData[$key][$color]['size'][$value['attibutes']['attribute_pa_size']] = $value['quantity'];	
				}
			}
		}
	}
?>
<table class="head container">
	<tr>
		<td></td>
		<td>
			<?php 
				if(in_array('democracy', $shippingBrand)){
					echo '<img src="'.site_url().'/wp-content/uploads/2021/11/logo-demo.png">';
				}
				if(in_array('classified', $shippingBrand)){
					echo '<img src="'.site_url().'/wp-content/uploads/2021/11/Screenshot-1-e1636526436632.png">';
				}
			  ?>
			<h1 class="document-type-label">
			<?php if( $this->has_header_logo() ) echo $this->get_title(); ?>
			</h1>
		</td>
		<td></td>
	</tr>
</table>
<table class="order-data-addresses">
	<tr>
		<td class="address shipping-address">
			<table>
				<tr class="contact-name">
					<th><?php _e( 'Contact Name:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>	
					<td><?php echo $fullName; ?></td>	
				</tr>
				<tr class="store-name">
					<th><?php _e( 'Store Name:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>	
					<td><?php echo $storeName; ?></td>		
				</tr>
				<tr class="delivery-address">
					<th><?php _e( 'Deilvery Address:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>	
					<td><?php echo $shippingAddress; ?></td>	
				</tr>
				<tr class="phone-number">
					<th><?php _e( 'Phone:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>	
					<td><?php echo $phoneNumber; ?></td>	
				</tr>
				
			</table>
		</td>
		
		<td class="order-data">
			<table>
				<?php do_action( 'wpo_wcpdf_before_order_data', $this->type, $this->order ); ?>
				<tr class="order-number">
					<th style="font-weight: bold;"><?php _e( 'Order Number:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?php echo $this->order_number()."&nbsp;/&nbsp;".$sales_order_num; ?></td>
				</tr>
				<tr class="shipping_date">
					<th style="font-weight: bold;"><?php _e( 'Delivery Date:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<td><?= !empty($date) ? $date->format('d-m-Y') : ''; ?></td>
				</tr>
				 <tr class="packing-slips">
					<th style="font-weight: bold;"><?php _e( 'Price Excludes GST', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
					<th></th>
				</tr>
				<tr class="enquiries">
					<th style="font-weight: bold;" colspan="2"><?php _e( 'Order and enquiries to:', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
				</tr> 
				<tr class="email">
					<td colspan="2"><a href="melanieb@compositeretail.coop"><?php echo "melanieb@compositeretail.coop"; ?></a></td>
				</tr> 
				<tr class="phone">
					<td colspan="2">melanie 021481925</td>
				</tr> 
				<?php do_action( 'wpo_wcpdf_after_order_data', $this->type, $this->order ); ?>
			</table>			
		</td>
	</tr>
</table>

<h3><?= $collection_name.'&nbsp;Order Panel Price'; ?></h3>
<table class="order-details">
	<thead>
		<tr>
			<th class="hd styleNumber"><?php _e('Style Number', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="hd itemDescription"><?php _e('Item Description', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="hd colour"><?php _e('Colour', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="hd">XS/8</th>
			<th class="hd">S/10</th>
			<th class="hd">M/12</th>
			<th class="hd">L/14</th>
			<th class="hd">XL/16</th>
			<th class="hd">XXL/18</th>
			<th class="hd">XXXL/20</th>
			<th class="hd total"><?php _e('Total', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="hd unitPrice"><?php _e('Cost', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="hd price"><?php _e('Price', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php
			$qntity_xs  = 0;
			$qntity_s  = 0;
			$qntity_m  = 0;
			$qntity_l  = 0;
			$qntity_xl  = 0;
			$qntity_xxl  = 0;
			$qntity_xxxl  = 0;

			if( sizeof( $productData ) > 0 ) { 
				foreach( $productData as $product_id => $product ){
					foreach ($product as $color => $item) {
						if(array_key_exists('size', $item)){
							foreach ($item['size'] as $size => $quantity) {
								switch ($size) {
									case '8-xs':
										$qntity_xs += $quantity;
										break;
									case '10-s':
										$qntity_s += $quantity;
										break;
									case '12-m':
										$qntity_m += $quantity;
										break;
									case '14-l':
										$qntity_l += $quantity;
										break;
									case '16-xl':
										$qntity_xl += $quantity;
										break;
									case '18-xxl':
										$qntity_xxl += $quantity;
										break;
									case '20-xxxl':
										$qntity_xxxl += $quantity;
										break;
								}
							}
						}
					}
				}
				//ITEMS DATA SHOW	
				foreach( $productData as $product_id => $product ){
					foreach ($product as $color => $item) {
						$total = number_format($item['single_price'] * $item['quantity'], 2, '.', '');
					?>
						<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', 'item-'.$item_id, $this->type, $this->order, $item_id ); ?>">
							<td class="dt"><?php echo $item['style_number']; ?></td>
							<td class="dt itemDescription">
								<?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); ?>
								<span class="dt item-name"><?php echo $item['name']; ?></span>
							</td>
							<td class="dt"><?php echo ucfirst($item['color']); ?></td>
							<td class="dt"><?php echo $item['size']['8-xs']; ?></td>
							<td class="dt"><?php echo $item['size']['10-s']; ?></td>
							<td class="dt"><?php echo $item['size']['12-m']; ?></td>
							<td class="dt"><?php echo $item['size']['14-l']; ?></td>
							<td class="dt"><?php echo $item['size']['16-xl']; ?></td>
							<td class="dt"><?php echo $item['size']['18-xxl']; ?></td>
							<td class="dt"><?php echo $item['size']['20-xxxl']; ?></td>
							<td class="dt total"><?php echo $item['quantity']; ?></td>
							<td class="dt unitPrice"><?php echo $this->format_price($item['single_price']); ?></td>
							<td class="dt price"><?= $this->format_price($total); ?></td>
						</tr>
		<?php 		}
				}
			}
		?>
	</tbody>
	<tfoot style="text-align:center; height:50px;">
		<tr class="no-borders">
			<td class="data"><span class="data"><?php _e('Totals:', 'woocommerce-pdf-invoices-packing-slips' ); ?></span></td>
			<td class="data"></td>
			<td class="data"></td>
			<td class="data"><span class="data"><?= ($qntity_xs != 0) ? $qntity_xs : ''; ?></span></td>
			<td class="data"><span class="data"><?= ($qntity_s != 0) ? $qntity_s : ''; ?></span></td>
			<td class="data"><span class="data"><?= ($qntity_m != 0) ? $qntity_m : ''; ?></span></td>
			<td class="data"><span class="data"><?= ($qntity_l != 0) ? $qntity_l : ''; ?></span></td>
			<td class="data"><span class="data"><?= ($qntity_xl != 0) ? $qntity_xl :'' ; ?></span></td>
			<td class="data"><span class="data"><?= ($qntity_xxl != 0) ? $qntity_xxl : ''; ?></span></td>
			<td class="data"><span class="data"><?= ($qntity_xxxl != 0) ? $qntity_xxxl : ''; ?></span></td>
			<td class="data"><span class="data"><?php echo $totalItems; ?></span></td>
			<td class="data"></td>
			<td class="data"><span style="padding: 15px"><?php echo $this->format_price($this->order->get_total()); ?></span></td>
		</tr>
	</tfoot>
</table>

<div class="bottom-spacer"></div>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>

<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>
<div class="customer-notes">
	<?php if ( $this->get_shipping_notes() ) : ?>
		<h3><?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
		<?php $this->shipping_notes(); ?>
	<?php endif; ?>
</div>
<?php do_action( 'wpo_wcpdf_after_customer_notes', $this->type, $this->order ); ?>

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	<!-- hook available: wpo_wcpdf_before_footer -->
	<?php $this->footer(); ?>
	<!-- hook available: wpo_wcpdf_after_footer -->
</div><!-- #letter-footer -->
<?php endif; ?>

<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order );

?>