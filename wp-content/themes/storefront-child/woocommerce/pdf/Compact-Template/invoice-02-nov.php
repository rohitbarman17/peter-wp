<?php if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly ?>
<?php do_action( 'wpo_wcpdf_before_document', $this->type, $this->order ); ?>
<style>
table.details,th.header,td.data{
  border: 1px solid black;
  border-collapse: collapse;
  width: 100%;
  text-align: center;
}

table.head.container {
	width: 100%;
}
td.shop-info {
    text-align: right;
}
table.head.container td {
    width: 50%;
}
table.head.container td:first-child {
    text-align: right;
}

h2.site-logo {
    font-size: 100px;
    font-family: 'Great Vibes', cursive;
	padding-top: 30px;
    color: #AC9F99;
    margin-bottom: 0px !important;
}

h1.document-type-label {
    padding-right: 20px;
}
table.head.container td:first-child {
    text-align: center;
    width: 100%;
}
</style>
<?php 
	$quantity = 0;
	$items = $this->get_order_items(); 
	if( sizeof( $items ) > 0 ) : 
		foreach( $items as $item_id => $item ):
 			$quantity += $item['quantity'];
 		endforeach; 
	endif;
?>
<table class="head container">
	<tr>
		<td>
			<!-- <h2 class="site-logo">Democracy</h2> -->
			<img src="http://democracy.fcdtest.com/wp-content/uploads/2021/11/logo-demo.png">
			<h1 class="document-type-label">
			<?php if( $this->has_header_logo() ) echo $this->get_title(); ?>
			</h1>
		</td>
		<td class="shop-info">
			<?php
			if( $this->has_header_logo() ) {
				$this->header_logo();
			} else {
				echo $this->get_title();
			}
			do_action( 'wpo_wcpdf_before_shop_name', $this->type, $this->order ); ?>
			<!-- <div class="shop-name"><h3><?php $this->shop_name(); ?></h3></div> -->
			<?php do_action( 'wpo_wcpdf_after_shop_name', $this->type, $this->order ); ?>
			<?php do_action( 'wpo_wcpdf_before_shop_address', $this->type, $this->order ); ?>
			<div class="shop-address"><?php $this->shop_address(); ?></div>
			<?php do_action( 'wpo_wcpdf_after_shop_address', $this->type, $this->order ); ?>
		</td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_after_document_label', $this->type, $this->order ); ?>
<table class='details'>
<tr>
	<th class='header'><?php _e( 'Date','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Sales Order Number','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Collection Name','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Invoice items','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Shipping Date','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Invoice Number','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
	<th class='header'><?php _e( 'Payment Due Date','woocommerce-pdf-invoices-packing-slips' ); ?> </th>
</tr>
<tr>
	<td class='data'><?=  $this->invoice_date();; ?></td>
	<td class='data'><?= $this->order_number(); ?></td>
	<td class='data'>------</td>
	<td class='data'><?= $quantity; ?></td>
	<td class='data'>------</td>
	<td class='data'><?= $this->invoice_number(); ?></td>
	<td class='data'>------</td>
</tr>
</table>
</br>
<h4 style="padding-top:15px;">REMARKS: &nbsp;This invoice relates to part of your order. The balance of your order will be invoiced separately.</h4>
<hr>
<table class="order-data-addresses">
	<tr>
		<td class="address billing-address">
			<h3><?php _e( 'Customer:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
			<?php do_action( 'wpo_wcpdf_before_billing_address', $this->type, $this->order ); ?>
			<?php $this->billing_address(); ?>
			<?php do_action( 'wpo_wcpdf_after_billing_address', $this->type, $this->order ); ?>
			<?php if ( isset($this->settings['display_email']) ) { ?>
			<div class="billing-email"><?php $this->billing_email(); ?></div>
			<?php } ?>
			<?php if ( isset($this->settings['display_phone']) ) { ?>
			<div class="billing-phone"><?php $this->billing_phone(); ?></div>
			<?php } ?>
		</td>
		<td class="address shipping-address">
			<?php if ( !empty($this->settings['display_shipping_address']) && ( $this->ships_to_different_address() || $this->settings['display_shipping_address'] == 'always' ) ) { ?>
			<h3><?php _e( 'Ship To:', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
			<?php do_action( 'wpo_wcpdf_before_shipping_address', $this->type, $this->order ); ?>
			<?php $this->shipping_address(); ?>
			<?php do_action( 'wpo_wcpdf_after_shipping_address', $this->type, $this->order ); ?>
			<?php } ?>
		</td>
	</tr>
</table>

<?php do_action( 'wpo_wcpdf_before_order_details', $this->type, $this->order ); ?>

<table class="order-details">
	<thead>
		<tr>
			<th class="product"><?php _e('Product', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="quantity"><?php _e('Quantity', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="quantity"><?php _e('Unit Price', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
			<th class="price"><?php _e('Price', 'woocommerce-pdf-invoices-packing-slips' ); ?></th>
		</tr>
	</thead>
	<tbody>
		<?php $items = $this->get_order_items(); if( sizeof( $items ) > 0 ) : foreach( $items as $item_id => $item )
		 :?>
		<tr class="<?php echo apply_filters( 'wpo_wcpdf_item_row_class', 'item-'.$item_id, $this->type, $this->order, $item_id ); ?>">
			<td class="product">
				<?php $description_label = __( 'Description', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
				<span class="item-name"><?php echo $item['name']; ?></span>
				<?php do_action( 'wpo_wcpdf_before_item_meta', $this->type, $item, $this->order  ); ?>
				<span class="item-meta"><?php echo $item['meta']; ?></span>
				<dl class="meta">
					<?php $description_label = __( 'SKU', 'woocommerce-pdf-invoices-packing-slips' ); // registering alternate label translation ?>
					<?php if( !empty( $item['sku'] ) ) : ?><dt class="sku"><?php _e( 'SKU:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="sku"><?php echo $item['sku']; ?></dd><?php endif; ?>
					<?php if( !empty( $item['weight'] ) ) : ?><dt class="weight"><?php _e( 'Weight:', 'woocommerce-pdf-invoices-packing-slips' ); ?></dt><dd class="weight"><?php echo $item['weight']; ?><?php echo get_option('woocommerce_weight_unit'); ?></dd><?php endif; ?>
				</dl>
				<?php do_action( 'wpo_wcpdf_after_item_meta', $this->type, $item, $this->order  ); ?>
			</td>
			<td class="quantity"><?php echo $item['quantity']; ?></td>
			<td class="price"><?php echo $item['single_price']; ?></td>
			<td class="price"><?php echo $item['order_price']; ?></td>
		</tr>
		<?php endforeach; endif; ?>
	</tbody>
	<tfoot>
		<tr class="no-borders">
			<td class="no-borders"></td>
			<td class="no-borders">
				<div class="document-notes">
					<?php do_action( 'wpo_wcpdf_before_document_notes', $this->type, $this->order ); ?>
					<?php if ( $this->get_document_notes() ) : ?>
						<h3><?php _e( 'Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
						<?php $this->document_notes(); ?>
					<?php endif; ?>
					<?php do_action( 'wpo_wcpdf_after_document_notes', $this->type, $this->order ); ?>
				</div>
				<div class="customer-notes">
					<?php do_action( 'wpo_wcpdf_before_customer_notes', $this->type, $this->order ); ?>
					<?php if ( $this->get_shipping_notes() ) : ?>
						<h3><?php _e( 'Customer Notes', 'woocommerce-pdf-invoices-packing-slips' ); ?></h3>
						<?php $this->shipping_notes(); ?>
					<?php endif; ?>
					<?php do_action( 'wpo_wcpdf_after_customer_notes', $this->type, $this->order ); ?>
				</div>				
			</td>
			<td class="no-borders" colspan="2">
				<table class="totals">
					<tfoot>
						<?php foreach( $this->get_woocommerce_totals() as $key => $total ) : ?>
						<tr class="<?php echo $key; ?>">
							<th class="description"><?php echo $total['label']; ?></th>
							<td class="price"><span class="totals-price"><?php echo $total['value']; ?></span></td>
						</tr>
						<?php endforeach; ?>
					</tfoot>
				</table>
			</td>
		</tr>
	</tfoot>
</table>

<div class="bottom-spacer"></div>

<?php do_action( 'wpo_wcpdf_after_order_details', $this->type, $this->order ); ?>

<?php if ( $this->get_footer() ): ?>
<div id="footer">
	<!-- hook available: wpo_wcpdf_before_footer -->
	<?php $this->footer(); ?>
	<!-- hook available: wpo_wcpdf_after_footer -->
</div><!-- #letter-footer -->
<?php endif; ?>
<?php do_action( 'wpo_wcpdf_after_document', $this->type, $this->order ); 
?>
