 <?php /* Template Name: About-Template */ ?>

<?php get_header();?>
	
  	  <!--About Start-->
	  <section class="about-w">
	    <div class="container">
	      <h2>About Us</h2>
	      	<p>Democracy is designed in New Zealand for the woman who loves wearing comfortable yet stylish lifestyle clothing.  Our Democracy lady loves textures and prints making effortless choices knowing she is on trend.</p>
            <p>Democracy is only sold through independent retailers throughout New Zealand.  Our current range can be found in 50 stores and many have online shopping available.  Click on Stockists to find a store close to you.</p>
            <p>Democracy is owned by Democracy Ltd. For further information please contact Melanie Brown at <u><a href="mailto:melanie@democracyltd.co.nz">Contact Us</a></u></p>
	    </div>
	  </section>
	  <!--About End-->

<?php get_footer(); ?>