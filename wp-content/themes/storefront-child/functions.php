<?php
// Exit if accessed directly
if ( !defined( 'ABSPATH' ) ) exit;

// BEGIN ENQUEUE PARENT ACTION
// AUTO GENERATED - Do not modify or remove comment markers above or below:

if ( !function_exists( 'chld_thm_cfg_locale_css' ) ):
    function chld_thm_cfg_locale_css( $uri ){
        if ( empty( $uri ) && is_rtl() && file_exists( get_template_directory() . '/rtl.css' ) )
            $uri = get_template_directory_uri() . '/rtl.css';
        return $uri;
    }
endif;
add_filter( 'locale_stylesheet_uri', 'chld_thm_cfg_locale_css' );

// END ENQUEUE PARENT ACTION
/*-----------------------------CUSTOM CSS ADDING -------------------------------------------------------------------------------*/

add_action( 'wp_enqueue_scripts', 'wpdocs_theme_name_scripts' );
function wpdocs_theme_name_scripts(){
    wp_enqueue_style('bootstrap.min.css', get_stylesheet_directory_uri() . '/assets/css/bootstrap.min.css', array(), time(), false);
    wp_enqueue_style('font-awesome.css', get_stylesheet_directory_uri() . '/assets/css/font-awesome.css', array(), time(), false);
    wp_enqueue_style('slick.css', get_stylesheet_directory_uri() . '/assets/css/slick.css', array(), time(), false);
    wp_enqueue_style('slick-theme.css', get_stylesheet_directory_uri() . '/assets/css/slick-theme.css', array(), time(), false);
    wp_enqueue_style('base', get_stylesheet_directory_uri() . '/assets/css/base.css', array(), time(), false);
    wp_enqueue_style('main', get_stylesheet_directory_uri() . '/assets/css/main.css', array(), time(), false);
    wp_enqueue_style('responsive', get_stylesheet_directory_uri() . '/assets/css/responsive.css', array(), time(), false);
    wp_enqueue_style('mob-nav', get_stylesheet_directory_uri() . '/assets/SlickNav/mob-nav.css', array(), time(), false);
    wp_enqueue_style('style', get_stylesheet_directory_uri() . '/style.css', array(), time(), false);

    /*SCRIPT*/
    wp_enqueue_script( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js');
    wp_enqueue_script( 'slicknav', get_stylesheet_directory_uri() . '/assets/SlickNav/jquery.slicknav.js', array( 'jquery' ) , time(), false );
    wp_enqueue_script( 'slick', get_stylesheet_directory_uri() . '/assets/js/slick.js', array( 'jquery' ) , time(), false );
    wp_enqueue_script( 'infiniteslidev2', get_stylesheet_directory_uri() . '/assets/js/infiniteslidev2.js', array( 'jquery' ) , time(), false );
    wp_enqueue_script( 'custom-js', get_stylesheet_directory_uri() . '/assets/js/custom.js', array( 'jquery' ) , time(), true );
}

/*---------------------------------------SALE TEXT REMOVE FROM PRODUCTS----------------------------------------------------------------*/

add_filter('woocommerce_sale_flash', 'lw_hide_sale_flash');
function lw_hide_sale_flash()
{
return false;
}

/*---------------------------------------PRODUCT TAB REMOVES FROM SINGLE PRODUCT PAGE----------------------------------------------------------------*/

add_filter( 'woocommerce_product_tabs', 'woo_remove_product_tabs', 98 );
function woo_remove_product_tabs( $tabs ) {

    unset( $tabs['additional_information'] );      
    unset( $tabs['reviews'] );      
    unset( $tabs['description'] );      

    return $tabs;
}/*--------------------------------------- PAGE----------------------------------------------------------------*/

function getBrandsBanner($brand_slug,$brand_name){
    $html = "";
    switch ($brand_slug) {
        case "classified":
            $html .= '<section class="hdr-classified">';
            $html .= '<h2>'.$brand_name.'</h2>';
            $html .= '</section>';
        break;
        case "democracy":
            $html .= '<section class="hdr-democracy">';
            $html .= '<h2>'.$brand_name.'</h2>';
            $html .= '</section>';
        break;
        case "uniforms":
            $html .= '<section class="hdr-uniforms">';
            $html .= '<h2>'.$brand_name.'</h2>';
            $html .= '</section>';
        break;
        default:
            $html .= '<section class="">';
            $html .= '<h2>'.$brand_name.'</h2>';
            $html .= '</section>';
    }

    return $html;
}
/*--------------------------------------------PRODUCT PAGE VIDEO PRODUCT----------------------------------------------------------------*/

add_action( 'woocommerce_after_single_product_summary',  'evolve_woocommerce_after_single_product_summary',  15 );
function evolve_woocommerce_after_single_product_summary(){
    echo '<div class="video-product">
          <img src="http://democracy.fcdtest.com/wp-content/uploads/2021/09/video-img.jpg" alt="video-product">
        </div>';
} 

/*--------------------------------------------PRODUCT PAGE TITLE CHANGE ( https://stackoverflow.com/questions/39063958/woocommerce-action-hooks-and-overriding-templates )----------------------------------------------------------------*/
https://stackoverflow.com/questions/39063958/woocommerce-action-hooks-and-overriding-templates

remove_action( 'woocommerce_single_product_summary','woocommerce_template_single_title', 5 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_price', 10 );
// remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_meta', 40 );

add_action( 'woocommerce_single_product_summary', 'sgl_template_single_title', 5 );
function sgl_template_single_title() {
    
    $product_id = get_the_ID();
    $product = wc_get_product( $product_id );
    $fabrication = get_field('fabrication', $product_id);

    echo '<h2>'.$product->get_name().'</h2>';
    echo '<h6>W21D900 - BLACK</h6>';
    echo '<span>Fabrication :'.$fabrication.'</span>';
    
} 


add_action( 'woocommerce_single_variation', 'page_content_mix' );

function page_content_mix() {

    $product_id = get_the_ID();
    $product = wc_get_product( $product_id );
    echo '<h5>RRP $'.$product->get_price().'</h5>';
    echo '<h5>Description</h5>';  
    echo '<div>'.$product->get_description().'</div>';   

}

add_filter( 'woocommerce_add_to_cart_fragments', 'woo_cart_but_count' );
function woo_cart_but_count( $fragments ) {
 
    ob_start();
    
    $cart_count = WC()->cart->cart_contents_count;
    $cart_url = wc_get_cart_url();
    
    ?>
    <a class="cart-contents menu-item" href="<?php echo $cart_url; ?>" title="<?php _e( 'View your shopping cart' ); ?>">
    <?php
        if ( $cart_count > 0 ) {
            echo '<span class="cart-contents-count">'.$cart_count.'</span>';
        }?>
    </a>
    <?php
 
    // $fragments['a.cart-contents'] = ob_get_clean();
     
    return $fragments;

}

/*-----------------------------HEADER MINI CART -------------------------------------------------------------------------------*/
add_shortcode( 'custom-techno-mini-cart', 'custom_mini_cart' );

function custom_mini_cart() { 

    if(WC()->cart->get_cart_contents_count() > 0){
       
        echo '<a href="#min-cart"  data-toggle="collapse"> ';
        echo '<img src="'.get_stylesheet_directory_uri().'/assets/images/icon-shopping-cart.svg" alt="">';
        echo '<div class="basket-item-count" style="display: inline;">';
            echo '<span class="cart-items-count count">';
                echo WC()->cart->get_cart_contents_count();
            echo '</span>';
        echo '</div>';
        echo '</a>';
        echo '<ul id="min-cart" class="dropdown-menu dropdown-menu-mini-cart collapse">';
        echo '<li> <div class="widget_shopping_cart_content">';
                woocommerce_mini_cart();
        echo '</div></li></ul>';
    }else{
        echo '<a href="javascript:void(0)">';
        echo '<img src="'.get_stylesheet_directory_uri().'/assets/images/icon-shopping-cart.svg" alt="">';
        echo '</a>';
    }

}
/*-----------------------------CHANGE TITLE IN SHOP PAGE -------------------------------------------------------------------------------*/
add_filter( 'woocommerce_page_title', 'woo_shop_page_title');
function woo_shop_page_title( $page_title ) {
    if( 'Shopx' == $page_title) {
        return "<h2>Democracy W21 Look Book</h2>";
    }
}

/*-----------------------------REMOVE SIDEBAR FROM ALL PAGES -------------------------------------------------------------------------------*/
function remove_wc_sidebar_always( $array ) {
  return false;
}
// add_filter( 'is_active_sidebar', 'remove_wc_sidebar_always', 10, 2 );

/*-----------------------------CHANGE PRICE HTML IN SHOP PAGE -------------------------------------------------------------------------------*/

add_filter( 'woocommerce_get_price_html', 'wpa83367_price_html', 100, 2 );
function wpa83367_price_html( $price, $product ){
    return '<h6>RRP ' . str_replace( '<ins>', ' Now:<ins>', $price ).'</h6>';
}
remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 10 );

/*--------------------------------REMOVE RELATED PRODUCT FROM PRODUCT PAGE---------------------------------------------------------------*/

remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_output_related_products', 20 );

function admin_js() { ?>
    <style type="text/css">
        a.edit_address {display: block !important;}
    </style>
<?php }

add_action('admin_head', 'admin_js');

/*----------------------------- ----------------ADD PRODUCT COLUMN IN LIST---------------------------------------------------------------*/

// Add product new column in administration
add_filter( 'manage_edit-product_columns', 'woo_product_weight_column', 20 );
function woo_product_weight_column( $columns ) {

    $columns['brand'] = esc_html__( 'Brand', 'woocommerce' );
        return $columns;

}

// Populate weight column
add_action( 'manage_product_posts_custom_column', 'woo_product_weight_column_data', 2 );
function woo_product_weight_column_data( $column ) {
    global $post;

    if ( $column == 'brand' ) {
        $brand = get_the_terms($post->ID , 'brands' );
        echo (!empty($brand)) ? $brand[0]->name : '';
    }
}


add_action( 'woocommerce_single_product_summary', 'remove_add_to_cart_button', 1 );
function remove_add_to_cart_button() {
    // Only for logged in users
    if(  is_user_logged_in() ) return;

    global $product;

    // For variable product types (keeping attribute select fields)
    if( $product->is_type( 'variable' ) ) {
        remove_action( 'woocommerce_single_variation', 'woocommerce_single_variation_add_to_cart_button', 20 );
    }
    // For all other product types
    else {
        remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_add_to_cart', 30 );
    }
}

/*--------------------------------------------USER BRANDS---------------------------------------------------*/ 

function userBrands($user_id){
    $bndArray = array();
    $brand_ids = get_user_meta($user_id , 'brands' , true);

    if(!empty($brand_ids)){
        if(is_array($brand_ids)){
            foreach ($brand_ids as $key => $id) {
                $terms = get_term_by('id', $id , 'brands');
                ($terms) ? array_push($bndArray, $terms->name) : ''; 
            }
            return implode(", ",$bndArray);
        }else if(!is_array($brand_ids)){
            $terms = get_term_by('id', $brand_ids , 'brands');
            $brandName = ($terms) ? $terms->name : '';  
            return $brandName;
        }
    }else{
        return '';
    }
    
}
/*--------------------------------------------REMOVE USER COLUMNS---------------------------------------------------*/ 

function new_modify_user_table( $column ) {
    unset( $column['posts'] );
    return $column;
}
add_filter( 'manage_users_columns', 'new_modify_user_table' );

/*--------------------------------------------Ship to a different address?---------------------------------------------------*/ 

add_filter( 'woocommerce_order_needs_shipping_address', 'maybe_display_shipping_address', 10, 3);
function maybe_display_shipping_address( $needs_shipping, $hide, $order ) {
return true;
}
add_filter( 'woocommerce_product_needs_shipping', 'maybe_each_product_needs_shipping', 10, 2);
function maybe_each_product_needs_shipping( $is_virtual, $product ){
return true;
}
add_filter( 'woocommerce_cart_needs_shipping_address', 'maybe_cart_needs_shipping_address', 10, 1);
function maybe_cart_needs_shipping_address( $needs_shipping_address ){
return true;
}

/*--------------------------------------------DEFAULT BILLING AND SHIPPING COUNTRY SET---------------------------------------------------*/ 

add_filter( 'default_checkout_billing_country', 'change_default_checkout_country' );
add_filter( 'default_checkout_shipping_country', 'change_default_checkout_country' );
function change_default_checkout_country(){
    return 'NZ';
}

// add_filter('woocommerce_shipping_fields', 'custom_woocommerce_shipping_fields');

// function custom_woocommerce_shipping_fields($fields)
// {

//     $fields['shipping_options'] = array(
//         'label' => __('NIF', 'woocommerce'), // Add custom field label
//         'placeholder' => _x('Your NIF here....', 'placeholder', 'woocommerce'), // Add custom field placeholder
//         'required' => false, // if field is required or not
//         'clear' => false, // add clear or not
//         'type' => 'text', // add field type
//         'class' => array('my-css')    // add class name
//     );

//     return $fields;
// }

// add the filter 
// add_filter( 'woocommerce_customer_meta_fields', 'house_number_customer_meta_fields', 10, 1 );
function house_number_customer_meta_fields( $fields ) {
    
    $fields['shipping']['fields']['shipping_country'] = array(

        'label'       => __( 'Store country', 'woocommerce' ),
        'description' => '',
        'class'       => 'js_field-country',
        'type'        => 'select',
        'options'     => array( __( 'Select a country', 'woocommerce' ) ) + WC()->countries->get_allowed_countries(),
    );

    $fields['shipping']['fields']['shipping_state'] = array(

        'label'       => __( 'Store state', 'woocommerce' ),
        'description' => '',
        'class'       => 'js_field-state',
    );

    if ( current_user_can( 'manage_options' ) ) {
        //Something in the case of admin
        $fields['shipping']['fields']['shipping_store_map'] =array(
            'type'        => 'textarea',
            'label'       => __( 'Store map', 'woocommerce' ),
            'description' => ''
        );
    }

    $fields['shipping']['fields']['shipping_store_name'] = array(
        'label'       => __( 'Store name', 'woocommerce' ),
        'description' => ''
    );

    $fields['shipping']['fields']['shipping_email'] =array(
        'type'        => 'email',
        'label'       => __( 'Store email', 'woocommerce' ),
        'description' => ''
    );

    $fields['shipping']['fields']['shipping_phone'] = array(

        'label'       => __( 'Store phone', 'woocommerce' ),
        'description' => ''
    );

    $fields['shipping']['fields']['shipping_brand'] = array(
        'description' => '',
        'type'      => 'select',
        'label'     => __('Brands', 'woocommerce'),
        'class'     => 'form-row-wide',
        'options' => array( '' => __( 'Select brand', 'woocommerce' ) ) +getRegisterdBrands()
    );

    $fields['shipping']['fields']['shipping_address_approve_by'] = array(
        'description' => '',
        'type'      => 'select',
        'label'     => __('Approve by', 'woocommerce'),
        'class'     => 'form-row-wide',
        'required'     => true,
        'options' => array( '' => __( 'Select', 'woocommerce' ) ) +array("customer"=>"Customer","admin"=>"Admin")
    );     

    
    return $fields;
}

// add_filter( 'woocommerce_default_address_fields', 'misha_add_field' );
function misha_add_field( $fields ) {

    if ( current_user_can( 'manage_options' ) ) {
        $fields['store_map']   = array(
            'type' => 'textarea',
            'label'        => __( 'Store map', 'woocommerce' ),
            'required'     => false,
            'class'        => array( 'form-row-wide' ),
            'priority'     => 20,
            'placeholder'  => '',
        );
    }

    $fields['store_name']   = array(
        'label'        => __( 'Store name', 'woocommerce' ),
        'required'     => true,
        'class'        => array( 'form-row-wide'),
        'priority'     => 20,
        'placeholder'  => '',
    );

    $fields['email']   = array(
        'type'         => 'email',
        'label'        => __( 'Store email', 'woocommerce' ),
        'required'     => true,
        'class'        => array( 'form-row-wide'),
        'priority'     => 20,
        'placeholder'  => '',
    );

    $fields['phone']   = array(
        'label'        => __( 'Store phone', 'woocommerce' ),
        'required'     => true,
        'class'        => array( 'form-row-wide'),
        'priority'     => 20,
        'placeholder'  => '',
    );

    $fields['address_approve_by']   = array(
        'label'        => __( 'Approve by', 'woocommerce' ),
        'type'          => 'select',
        'class'        => array( 'form-row-wide content-hide'),
        'priority'     => 20,
        'options' => array( '' => __( 'Select', 'woocommerce' ) ) +array("customer"=>"Customer","admin"=>"Admin")
    );

    $fields['brand'] = array(
        'label'     => __('Brands', 'woocommerce'),
        'class'     => array( 'form-row-wide content-hide'),
        'type' => 'select',
        'options' => array( '' => __( 'Select brand', 'woocommerce' ) ) +getRegisterdBrands()

    );

    // $fields['country']['required'] = false;
    return $fields;

}

function getAllStoresLocations(){
    $stores = array();
    $users = get_users(array('role'=>'customer'));
    foreach ($users as $key => $user) {
        $user_id = $user->data->ID;
        $userAddress = get_user_meta($user->ID, 'thwma_custom_address', true);
        
        foreach($userAddress['shipping'] as $address){
            $stores[] = $address;   
        }
    }
    return $stores;
}
/*GET REGISTERED BRANDS*/
function getRegisterdBrands(){

    $brands = array();
    $brandsData = get_terms( 'brands', array(
        'hide_empty' => false,
    ));

    foreach ($brandsData as $data) {
        if($data->slug == 'classified' || $data->slug =='democracy'){
            $brands[$data->slug] = $data->name; 
        }
    }
    $brands['classified-democracy'] = "Classified & Democracy" ;
    return $brands;
}

/*--------------------------------------------STOCKISTS PAGE HTML---------------------------------------------------*/ 

function stockistHtml($address){

    $html = "";
    foreach ($address as $key => $data) {
        
        $storeName = $data['shipping_store_name'];
        $storeMap = $data['shipping_store_map'];
        $phoneNumber = $data['shipping_phone'];
        $brand = $data['shipping_brand'];
        $region = $data['shipping_state'];
        $address1 = $data['shipping_address_1'];
        $address2 = $data['shipping_address_2'];
        $addrss = $address1 .' '. $address2;
        if($key%2 == 0){
            $html .= '<div class="row">
                        <div class="col-sm-6">'.$storeMap.'</div>
                        <div class="col-sm-6">
                           <div class="ml-4 dp-table">
                                <div class="equal-height-column">
                                    <div class="team-name">'.$storeName.'</div>
                                    <div class="team-position"><a href="tel:'.$phoneNumber.'"><i class="fa fa-phone-square"></i>&nbsp;'.$phoneNumber.'</a></div>
                                    <div class="address-list mb-2"><i class="fa fa-building"></i> &nbsp; '.$addrss.'</div>
                                </div>
                            </div>
                        </div>
                    </div>';
        }else{
            $html .= '<div class="row">
                <div class="col-sm-6">
                   <div class="ml-4 dp-table">
                        <div class="equal-height-column">
                            <div class="team-name">'.$storeName.'</div>
                            <div class="team-position"><a href="tel:'.$phoneNumber.'"><i class="fa fa-phone-square"></i>&nbsp;'.$phoneNumber.'</a></div>
                            <div class="address-list mb-2"><i class="fa fa-building"></i> &nbsp;'.$addrss.'</div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6">'.$storeMap.'</div>
            </div>';
        } 
    }

    return $html;
}

/*--------------------------------------------ADD BRAND IN CART ITEMS---------------------------------------------------*/ 

add_filter( 'woocommerce_cart_item_name', 'customizing_cart_item_data', 10, 3);
function customizing_cart_item_data( $item_name, $cart_item, $cart_item_key ) {
    $term_names = array();

    // Get product brand
    $item_name .= '<ul>';
    $terms = wp_get_post_terms( $cart_item['product_id'], 'brands' );
    if( count($terms) > 0 ){
        foreach( $terms as $term ) $term_names[] = $term->name;

        $item_name .= '<li class="item-brand" style="margin:12px 0 0; font-size: .875em;">
            <strong class="label">' . _n( 'Brand', 'brands', count($terms), 'woocommerce' ) . ': </strong>
            <span class="brandName">' . implode( ', ', $term_names ) . '</span>
        </li>';
    }

    // Get product collection(seasons)
    $collection_terms = wp_get_post_terms( $cart_item['product_id'], 'collection' );
    if( count($collection_terms) > 0 ){
        foreach( $collection_terms as $collection ) $collection_names[] = $collection->name;

        $item_name .= '<li class="item-brand" style="margin:12px 0 0; font-size: .875em;">
            <strong class="label">' . _n( 'Season', 'seasons', count($collection_terms), 'woocommerce' ) . ': </strong>
            <span class="values">' . implode( ', ', $collection_names ) . '</span>
        </li>';
    }

    $item_name .= '</ul>';

    return $item_name;
}

/*--------------------------------------------GET PRODUCTS BRANDS-----------------------------------------------------------------*/

add_action( 'woocommerce_checkout_create_order_line_item', 'custom_checkout_create_order_line_item', 20, 4 );
function custom_checkout_create_order_line_item( $item, $cart_item_key, $values, $order ) {
    
    $brands = wp_get_post_terms( $item->get_product_id(), 'brands' );
    if( count($brands) > 0 ){
        foreach( $brands as $brand ) $brand_names[] = $brand->name;
        $item->update_meta_data( _n( 'Brand', 'brands', count($brands), 'woocommerce' ), implode( ', ', $brand_names) );
    }

    $collections = wp_get_post_terms( $item->get_product_id(), 'collection' );
    
    if( count($collections) > 0 ){
        foreach( $collections as $collection ) $collection_names[] = $collection->name;
        $item->update_meta_data( _n( 'Season', 'seasons', count($collections), 'woocommerce' ), implode( ', ', $collection_names) );
    }
}

/*----------------------GET PRODUCTS BRAND BY PRODUCT ID-----------------------------------------------------------------*/

function getProductsBrand($product_id){
    $brands = wp_get_post_terms( $product_id, 'brands' );
    $brandSlug = !empty($brands) ? $brands[0]->slug : '';
    return $brandSlug;
}


/*---------------------------------CART AND CHECKOUT BUTTON TEXT CHANGE--------------------------------------------------*/

add_filter( 'woocommerce_order_button_text', 'misha_custom_button_text' );
 
function misha_custom_button_text( $button_text ) {
   return 'Save Order'; // new text is here 
}

function woocommerce_button_proceed_to_checkout() { ?>
 <a href="<?php echo esc_url( wc_get_checkout_url() ); ?>" class="checkout-button button alt wc-forward">
 <?php esc_html_e( 'Proceed to save order', 'woocommerce' ); ?>
 </a>
 <?php
}

/*--------------------------------ADD COUPON IN USERS PROFLE AND SAVE-----------------------------------------------------------------*/

add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields( $user ) { 
    global $wpdb;

    $user_coupon = get_user_meta($user->ID , "user_coupon", true);
    // Get an array of all existing coupon codes
    $coupon_codes = $wpdb->get_col("SELECT  post_title FROM $wpdb->posts WHERE post_type = 'shop_coupon' AND post_status = 'publish' ORDER BY post_name ASC");
    
    ?>
    <h3><?php _e("Assign Coupon", "blank"); ?></h3>

    <table class="form-table">
        <tr>
            <th><label for="coupon_code"><?php _e("Coupon code"); ?></label></th>
            <td>
                <select class="regular-text" name="coupon_code">
                    <option>Select</option>
                    <?php 
                        if(!empty($coupon_codes)){
                            foreach ($coupon_codes as $couponCode) {
                                $coupon_object = new WC_Coupon($couponCode);
                                $code = $coupon_object->get_code();
                                $selected = ($code == $user_coupon) ? "selected" : "";
                                echo "<option ".$selected." value='".$code."'>".$couponCode."</option>";
                            }
                        }
                    ?>
                </select>
            </td>
        </tr>
    </table>
<?php }

add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );
function save_extra_user_profile_fields( $user_id ) {
    if ( empty( $_POST['_wpnonce'] ) || ! wp_verify_nonce( $_POST['_wpnonce'], 'update-user_' . $user_id ) ) {
        return;
    }
    
    if ( !current_user_can( 'edit_user', $user_id ) ) { 
        return false; 
    }
    update_user_meta( $user_id, 'user_coupon', $_POST['coupon_code'] );

}

/*---------------------------------SHOW ERROR NON SHIPPING BRANDS-----------------------------------------------------------------*/

add_filter( 'woocommerce_checkout_create_order', 'create_order_for_shipping', 10, 2 );
function create_order_for_shipping ($order,$data) {
    
    $approve_by = $order->get_meta("_shipping_address_approve_by"); 
    if( empty($approve_by) || $approve_by == 'customer'){
        $error_text = __("This address is not approve by the admin!", "wocommerce" );
        throw new Exception( $error_text );
    }
    
    if( is_user_logged_in() ) {
        $user = wp_get_current_user();
        $roles = ( array ) $user->roles;
        $brand = $data['shipping_brands'];
        
        if( empty($brand)){
            $error_text1 = __("Shipping brand is empty, please add brand in shipping address!", "wocommerce" );
            throw new Exception( $error_text1 );
        }

        if(!empty($brand) && in_array('customer', $roles) ){
            $shipping_brands = explode("-", $brand);
            foreach ( $order->get_items() as $item_id => $item ) {
                // Get the product object
                $productBrand = getProductsBrand($item->get_product_id());
                if(!in_array($productBrand, $shipping_brands)){
                    $error_text = __("The delivery location chosen is for ".ucfirst($shipping_brands[0])." products only. Please remove any non ".ucfirst($shipping_brands[0])." products or chose a different delivery location.", "wocommerce" );
                    throw new Exception( $error_text );
                }

            }
        }
    }
    return $order;
}

/*------------------------REMOVE PRODUCT BY PRODUCTS BRANDS FROM ORDERS-----------------------------------------------------------------*/

// https://stackoverflow.com/questions/60758640/remove-product-after-an-order-has-been-placed-in-woocommerce
//https://stackoverflow.com/questions/48188567/applying-programmatically-a-coupon-to-an-order-in-woocommerce3
function getBrandText($brands){
    
    foreach ($brands as $key => $brand) {
       $fistBrand[] = mb_substr(ucfirst($brand), 0, 1);    
    }
    return implode("", $fistBrand);
}

add_action( 'woocommerce_checkout_order_processed', 'custom_process_order', 10, 1 );
function custom_process_order( $order_id ) {
    if( ! $order_id ) return;

    if( is_user_logged_in() ) {
        $user = wp_get_current_user();
        $roles = ( array ) $user->roles;
        // get order object
            $order = new WC_Order( $order_id );
            $user_id = $order->get_user_id();
        // //Update season in orders
        //     $active_season = WC_Admin_Settings::get_option( 'active_season' );
        //     $term = get_term_by('slug', $active_season, 'collection'); 
        //     update_post_meta($order_id, 'season' , $term->term_id );
        

        // //Update sales order number : customerid + collection + ordernum?
            
        //     $customerId = $user->user_login;
        //     $fistBrand = getBrandText($brand);
        //     $collection = ucfirst($fistBrand).$term->name;
        //     $sales_order_num = $customerId."-".$collection."-".$order_id; 
        //     update_post_meta($order_id, 'sales_order_number' , $sales_order_num );

        
        //get customer coupon
        $coupon_code = get_user_meta($user_id , "user_coupon", true);
        //get customer shipping store brand
        $shipping_brands = !empty($brand) ? explode("-", $brand) : ''; 
        if(!empty($shipping_brands) && in_array('customer', $roles)){
            foreach ($order->get_items() as $item ) {
                $product = wc_get_product( $item['product_id'] );
                $productBrand = getProductsBrand($product->get_id());
                if(!in_array($productBrand, $shipping_brands)){
                    $order->remove_item( $item->get_id() );
                }
            }
        }
        // Apply coupon of the user
        if(!empty($coupon_code)){
            $order->apply_coupon($coupon_code);
        }
        // Calculate
        $order->calculate_totals();
    }
    // Save
    $order->save();
}

/*--------------------------------------------UPDATE NEW ORDER STATUS-----------------------------------------------------------------*/

add_action( 'woocommerce_thankyou', 'woocommerce_thankyou_change_order_status', 10, 1 );
function woocommerce_thankyou_change_order_status( $order_id ){
    if( ! $order_id ) return;

    $order = wc_get_order( $order_id );
    if( $order->get_status() == 'processing' ){
        $order->update_status( 'pending' );
    }

    $user = wp_get_current_user();
    $user_id = $order->get_user_id();
    
    $invoice_number = get_post_meta($order_id,'_wcpdf_invoice_number', true);
    $orderDate = gmdate( 'Y-m-d H:i:s', $order->get_date_created()->getOffsetTimestamp() );
    //Update custom invoice number  2108037-1
        $year_code = date('y',strtotime($orderDate));
        $month_code = date('m',strtotime($orderDate));
        $custom_invoice_number = $year_code.$month_code.$user_id."-".$invoice_number;
        update_post_meta($order_id, 'custom_invoice_number' , $custom_invoice_number );

    //Update season in orders
        $active_season = WC_Admin_Settings::get_option( 'active_season' );
        $term = get_term_by('slug', $active_season, 'collection'); 
        update_post_meta($order_id, 'season' , $term->term_id );
        

    //Update sales order number : customerid + collection + ordernum?
        $brands = $order->get_meta('_shipping_brands');
        $customerId = $user->user_login;
        $fistBrand = getBrandText($brands);
        $collection = $fistBrand.$term->name;
        $sales_order_num = $customerId."-".$collection."-".$order_id; 
        update_post_meta($order_id, 'sales_order_number' , $sales_order_num );


}

/*-------------------------------------hide coupon field on the car and checkout page--------------------------------------------*/ 

function disable_coupon_field_on_checkout( $enabled ) {
    if ( is_cart() ) {
        $enabled = false;
    }
    if ( is_checkout() ) {
        $enabled = false;
    }
    return $enabled;
}
add_filter( 'woocommerce_coupons_enabled', 'disable_coupon_field_on_checkout' );

/*------------------------------------------- ADD REMOVE BUTTON IN CHECKOUT PAGE ---------------------------------------------*/ 

function filter_woocommerce_checkout_cart_item_quantity( $product_quantity, $cart_item, $cart_item_key ) { 
    // make filter magic happen here...
    $_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
    $product_id = apply_filters( 'woocommerce_cart_item_product_id', $cart_item['product_id'], $cart_item, $cart_item_key );

    $remove_link = apply_filters( 'woocommerce_cart_item_remove_link', sprintf(
        '<a href="%s" class="link" aria-label="%s" data-product_id="%s" data-product_sku="%s">Remove from Cart</a>',
        esc_url( wc_get_cart_remove_url( $cart_item_key ) ),
        __( 'Remove this item', 'woocommerce' ),
        esc_attr( $product_id ),
        esc_attr( $_product->get_sku() )
    ), $cart_item_key ); 

    return $product_quantity . '&nbsp;<span>'.$remove_link .'</span>'; 
}; 
         
// add the filter 
add_filter( 'woocommerce_checkout_cart_item_quantity', 'filter_woocommerce_checkout_cart_item_quantity', 10, 3 ); 


// add_filter( 'woocommerce_checkout_fields', 'readonly_billing_account_fields', 25, 1 );
function readonly_billing_account_fields ( $fields ) {
    // Only my account billing address for logged in users
    if( (is_user_logged_in() && is_account_page()) || is_checkout()){

        $array = [ 'hidden', 'country', 'state'];
        $disabled = ['disabled' => 'true'];
        $readonly = ['readonly' => 'readonly'];
        foreach ($fields['billing'] as $key => $field) {
            
            if(isset($field['type']) && in_array($field['type'], $array) ){
                $fields['billing'][$key]['custom_attributes'] = $disabled;
            }else{
                $fields['billing'][$key]['custom_attributes'] = $readonly;
            }
        }

        foreach ($fields['shipping'] as $key => $field) {

            if(isset($field['type']) && in_array($field['type'], $array) ){
                $fields['shipping'][$key]['custom_attributes'] = $disabled;
            }else{
                $fields['shipping'][$key]['custom_attributes'] = $readonly;
            }
        }
    }

    return $fields;
}


/*--------------------------ADD TO CART VALIDATION FOR ACTIVE COLLECTIONS-------------------------------------------------------*/

// add_filter('woocommerce_is_purchasable', 'filter_is_purchasable', 10, 2);
// function filter_is_purchasable($is_purchasable, $product ) {

//     $productFromActiveSeason = productBelogsToActiveSeason($product->get_id());
    
//     if(!$productFromActiveSeason ){
//     return false;
//     }
//     return $is_purchasable;
// }


add_filter( 'woocommerce_add_to_cart_validation', 'logged_in_customers_validation', 10, 3 );
function logged_in_customers_validation( $passed, $product_id, $quantity) {
    
    $active_season = WC_Admin_Settings::get_option( 'active_season' );
    if(!productBelogsToActiveSeason($product_id)){
        $message = __("You can add only ".ucfirst($active_season)." collection products into the cart!", "woocommerce");
        wc_add_notice( $message, 'error' );

        return false;
    }
    
    return $passed;
}


function productBelogsToActiveSeason($product_id){
    
    $collection_slug = [];
    $collection_terms = wp_get_post_terms( $product_id, 'collection');
    if( count($collection_terms) > 0 ){
        foreach( $collection_terms as $collection ) $collection_slug[] = $collection->slug;
    }

    $active_season = WC_Admin_Settings::get_option( 'active_season' );
    
    if(empty($collection_slug) || !in_array($active_season, $collection_slug) ){

        return false;
    }

    return true;    
}
/*--------------------------------------- GET PRODUCT COLLECTIONS-----------------------------------------------------------*/

function getCollections(){
    $collections = [];

    $collection = get_terms( 'collection', array(
        'hide_empty' => false,
    ));
    foreach ($collection as $data) {
        $collections[$data->slug] = $data->name;
    }

    return $collections;

}

/*---------------------------------------ACTIVATE SEASON SETTINGS IN WC-------------------------------------------------------*/

add_filter( 'woocommerce_get_sections_general' , 'add_settings_tab' );

function add_settings_tab( $settings_tab ){
     $settings_tab['activate_season'] = __( 'Activate season' );
     return $settings_tab;
}

add_filter( 'woocommerce_get_settings_general' , 'general_settings' , 10, 2 );

function general_settings( $settings, $current_section ) {
    $custom_settings = array();

    if( 'activate_season' == $current_section ) {

        $custom_settings =  array(
            array(
                    'name' => __( '' ),
                    'type' => 'radio',
                    'desc' => __( '' ),
                    'id'   => 'active_season',
                    'options' => getCollections()
            )

        );

        return $custom_settings;
   } else {
        return $settings;
   }

}

/*---------------------------------------------ADD COLUMN IN ORDER TABLE IN ADMIN------------------------------------------*/
add_filter( 'manage_edit-shop_order_columns', 'custom_shop_order_column', 20 );
function custom_shop_order_column($columns){
    
    foreach( $columns as $key => $column){
        $reordered_columns[$key] = $column;
        if( $key ==  'shipping_address' ){ 
            $reordered_columns['sales_order_number'] = esc_html__( 'Sales order number','woocommerce');
            $reordered_columns['invoice_number'] = esc_html__( 'Invoice number','woocommerce');
        }
    }
    return $reordered_columns;
}

add_action( 'manage_shop_order_posts_custom_column' , 'custom_orders_list_column_content', 20, 2 );
function custom_orders_list_column_content( $column, $post_id ){
    switch ( $column )
    {
        case 'sales_order_number' :
            // Get custom post meta data
            $sales_order_number = get_post_meta( $post_id, 'sales_order_number', true );
            if(!empty($sales_order_number))
                echo $sales_order_number;

            // Testing (to be removed) - Empty value case
            else
                echo '<small>(<em>no value</em>)</small>';

            break;

        case 'invoice_number' :
            // Get custom post meta data
            $invoice_number = get_post_meta( $post_id, 'custom_invoice_number', true );
            if(!empty($invoice_number))
                echo $invoice_number;

            // Testing (to be removed) - Empty value case
            else
                echo '<small>(<em>no value</em>)</small>';

            break;
    }
}

/*---------------------------------------------SALE ORDER AND INVOICE NUMBER COLUMN SEARCHABLE------------------------------------------*/

add_filter( 'woocommerce_shop_order_search_fields', 'filter_woocommerce_shop_order_search_fields', 10, 1 ); 
function filter_woocommerce_shop_order_search_fields( $array ) { 
    array_push($array, 'sales_order_number');
    array_push($array, 'custom_invoice_number');
    return $array;
}; 

add_filter( 'wpo_wcpdf_paper_format', 'wcpdf_custom_inch_page_size', 10, 2 );
function wcpdf_custom_inch_page_size($paper_format, $template_type) {
    // change the values below
    $width = 10.5; //inches!
    $height = 12.5; //inches!
 
    //convert inches to points
    $paper_format = array( 0, 0, $width * 72, $height * 72 );
 
    return $paper_format;
}

add_filter( 'wpo_wcpdf_paper_orientation', 'wcpdf_landscape', 10, 2 );
function wcpdf_landscape($paper_orientation, $template_type) {
    // use $template type ( 'invoice' or 'packing-slip') to set paper oriention for only one document type.
    $paper_orientation = 'landscape';
    return $paper_orientation;
} 

add_filter('autoptimize_filter_cachecheck_maxsize','increase_ao_cachesize');
function increase_ao_cachesize() {
    return 1024 * 1024 * 1024;
}

// add_action("init", "initialization");
function initialization(){
    

}
