 <?php /* Template Name: StockList-Template */ ?>
<!-- https://www.maps.ie/create-google-map/ -->
<?php get_header();?>
<?php 
	global $woocommerce;
  	$countries_obj   = new WC_Countries();
  	$regionsList =  $countries_obj->get_states('NZ');

  	$stockists = array();
  	$brands = array("classified" => "Classified","democracy" => "Democracy");
  	$stores = getAllStoresLocations();

  	if(isset($_GET) && !empty($_GET)){
	/*GET STORE BY BRANDS*/  		
  		if($_GET['brand'] !='all'){
	  		foreach ($stores as $key => $store) {
	  			if(strpos($store['shipping_brand'], $_GET['brand']) !== false){
					$brandFiter[] = $store;
				}
			}
			$fiterdStores = $brandFiter;
		}else{
			$fiterdStores = $stores;
		}

	/*GET STORE BY REGIONS*/
		if($_GET['region'] !='all'){
	  		foreach ($fiterdStores as $key => $store1) {
	  			if(strpos($store1['shipping_state'], $_GET['region']) !== false){
	  				
					$regionFiter[] = $store1;
				}
			}
			$fiterdStores = $regionFiter;
		}
	}else{
  		$fiterdStores = $stores;
  	}
	
?>
<style type="text/css">
	@import url('https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap');

	.col-full .container {
	    padding-top: 25px;
	}
	.team-name {
	    color: #555;
	    display: block;
	    font-size: 18px;
	    margin-bottom: 5px;
	    text-transform: uppercase;
	}
	.team-position a {
	    display: block;
	    color: #72c02c;
	    font-style: normal;
	    margin-bottom: 15px;
        text-decoration: none;
	}
	.fb-link{
	    color: #a3d8dc;
	    padding: 5px;
	}
	.dp-table {
	    display: table;
	    height: 100%;
	    min-height: 280px;
	}
	.equal-height-column {
	    display: table-cell;
	    height: 100%;
	    vertical-align: middle;
	}
</style>
<div class="container">
  	<form id="fiterForm" method="get">	
  		<strong>Total results : <?= count($fiterdStores);?></strong><br>
  		<label>Select a Region</label>
  		<select class="stockList" name="region">
  			<?php
  				echo "<option value='all'>All</option>";
  				foreach ($regionsList as $regionKey => $region) {
  					$seltedRegion = (isset($_GET['region']) && $_GET['region']== $regionKey) ? 'selected' : '';
  					echo "<option ".$seltedRegion." value='".$regionKey."'>".$region."</option>";
  				}
  			?>
  		</select>
  		<label>Select a Brand</label>
  		<select class="stockList" name="brand">
  			<?php
  				echo "<option value='all'>All</option>";
  				foreach ($brands as $key => $brand) {
  					$seltedBrnd = (isset($_GET['brand']) && $_GET['brand']== $key) ? 'selected' : '';
  					echo "<option ".$seltedBrnd." value='".$key."'>".$brand."</option>";
  				}
  			?>
  		</select>
  	</form>
  	<?php 
  		echo stockistHtml($fiterdStores);
	?>
</div>
<?php get_footer(); ?>
<script>
	jQuery(document).ready(function(){
		jQuery(".stockList").on("change", function(){
			jQuery("#fiterForm").submit();
		});
	})
</script>