<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<!--Footer Start-->
	  <footer>
	    <div class="container">
	      <div class="sm-icon">
	        <ul>
	          <li><a href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/sm-icon-1.svg" alt=""></a></li>
	          <li><a href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/sm-icon-2.svg" alt=""></a></li>
	        </ul>
	      </div>
	      	<?php
				wp_nav_menu( array( 
			    'menu' => 'footer-menu', 
			    'menu_class' => '',
			    'container_class' => 'f-links' ) ); 
			?>
	      <p>© Copyright 2021. Democracy Ltd. All rights reserved.</p>
	    </div>
	  </footer>
	  <!--Footer End-->
		<!--Slicknav -->
	
	  <script type="text/javascript">
	    jQuery(document).ready(function () {
	      jQuery('#menu').slicknav({
	        closedSymbol: '<i class="fa fa-chevron-right" aria-hidden="true"></i>',
	        openedSymbol: '<i class="fa fa-chevron-down" aria-hidden="true"></i>',
	      });
	      jQuery('html').addClass('js');
	    });
	  </script>
	  <script>
	    jQuery(window).scroll(function () {
	      if (jQuery(this).scrollTop() > 1) {
	        jQuery('.topheader').addClass("stickyhead");
	        //jQuery('body').addClass("body-top-margin");
	      }
	      else {
	        jQuery('.topheader').removeClass("stickyhead");
	        //jQuery('body').removeClass("body-top-margin");
	      }
	    });
	  </script>
	  <script>
	    jQuery('.slider-testimonial').slick({
	      infinite: true,
	      slidesToShow: 1,
	      arrows: false,
	      autoplay: true,
	      autoplaySpeed: 5000,
	      dots: true,
	      slidesToScroll: 1
	    });
	  </script>
	 
	  <script type="text/javascript">
	    jQuery(function () {
	      jQuery('.scroll1').infiniteslide({
	        speed: 50,
	        pauseonhover: false,
	      });
	    });
	  </script>

	  <!--Slider Pagination Location Change-->
	  <script>
	    const mq = window.matchMedia("(min-width: 991px)");
	    if (mq.matches) {
	      jQuery("ul.slick-dots").insertAfter(".testionial-txt");
	    }
	    else {
	      //Default Position Pagination//    
	    }
	  </script>
	  <script>
	    jQuery(".btm-w h6").click(function () {
	      jQuery(this).parent(".nav-btm").toggleClass("open");
	    });
	  </script>

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
