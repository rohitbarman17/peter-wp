 <?php /* Template Name: Contact-Template */ ?>

<?php get_header();?>
<!--About Start-->
<section class="about-w">
	<div class="container">
		<div class="text-center"><div class="site-logo"><img src="images/democracy_logo.png" alt=""></div>
	    	<h5><strong>Melanie Brown - Director</strong></h5>
	    	<p>Phone: 021 481 925
	      	<br>
	    	<a href="mailto:melanie@democracyltd.co.nz" target="_blank">melanie@democracyltd.co.nz</a> </p>
	  	</div>
     	<p>Democracy Ltd designs and supplies premium quality women’s clothing brands Democracy and Classified, exclusively for the New Zealand market. Uniforms by Democracy Ltd serves schools, clubs and corporate clients with custom and standardised uniform needs.&nbsp; </p>
      	<p>The Democracy clothing brand is designed in New Zealand for the woman who loves wearing comfortable yet stylish lifestyle clothing of exceptional quality. Our Democracy lady loves textures and prints, making effortless choices knowing she is on trend. </p>
      	<p>The Classified clothing brand is inspired by garments from countries around the world encompassing new trends and prints that are timeless, powerful, with a touch of elegance. Classified is high quality clothing that is designed in New Zealand with a classic yet edgy touch, enabling women to feel confident in the way they dress. </p>
      	<p>Democracy and Classified are sold by independent retailers throughout New Zealand and many have online shopping available.&nbsp; </p>
      	<p><a href="http://democracyclothing.co.nz/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://Democracyclothing.co.nz&amp;source=gmail&amp;ust=1617614191994000&amp;usg=AFQjCNEz30O5PBEhSDBDvjWwQoIRmE0vzw">Democracyclothing.co.nz</a>&nbsp;can also be found on Facebook at&nbsp;<a href="https://www.facebook.com/democracyclothing" rel="nofollow" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.facebook.com/democracyclothing&amp;source=gmail&amp;ust=1617614191994000&amp;usg=AFQjCNFbaHqnX9KBriaNXF4wURQYGmAteA">https://www.facebook.com/democracyclothing</a>&nbsp;and Instagram at&nbsp;<a href="https://www.instagram.com/democracy.clothing/?igshid=19uj5qw4nujd" rel="nofollow" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.instagram.com/democracy.clothing/?igshid%3D19uj5qw4nujd&amp;source=gmail&amp;ust=1617614191995000&amp;usg=AFQjCNH8aE-qH5QFETO3z12dXRqfu1sHUA">https://www.instagram.com/democracy.clothing/?igshid=19uj5qw4nujd</a>.&nbsp;<a href="http://classifiedclothing.co.nz/" target="_blank" data-saferedirecturl="https://www.google.com/url?q=http://Classifiedclothing.co.nz&amp;source=gmail&amp;ust=1617614191995000&amp;usg=AFQjCNEWiGonsDwyxycN4K-sy_LmCUC3Nw">Classifiedclothing.co.nz</a>&nbsp;can be found on Facebook&nbsp;at&nbsp;<a href="https://www.facebook.com/Classified-Clothing-376896269256834/" rel="nofollow" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.facebook.com/Classified-Clothing-376896269256834/&amp;source=gmail&amp;ust=1617614191995000&amp;usg=AFQjCNEiGsfLdTjfA_Ct9HtENkKXZZPvRA">https://www.facebook.com/Classified-Clothing-376896269256834/</a>&nbsp;and Instagram at&nbsp;<a href="https://www.instagram.com/classified_apparel/?igshid=g9ijn8nuwokr" rel="nofollow" target="_blank" data-saferedirecturl="https://www.google.com/url?q=https://www.instagram.com/classified_apparel/?igshid%3Dg9ijn8nuwokr&amp;source=gmail&amp;ust=1617614191995000&amp;usg=AFQjCNHGwCTU4-IBIFaoYJw09vQxmw5LDg">https://www.instagram.com/classified_apparel/?igshid=g9ijn8nuwokr</a>t&nbsp;&nbsp;  </p>
      	<p>Democracy clothing and Classified clothing are registered trademarks of Democracy Limited. </p>
      	<p>&nbsp;</p>
   		<br>
	    <div class="row">
		    <div class="col-md-6 form-left-content"><p><strong>Please select from our brands below</strong></p>
		      	<div class="logo">
			      	<a href="/brands/classified/" target="_blank"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/brand-vob-1.jpg" alt="" class="img-fluid"></a> 
			      	<a href="/brands/uniforms/" target="_blank"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/brand-vob-2.jpg" alt="" class="img-fluid"></a> 
			      	<a href="/brands/democracy/"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/brand-vob-3.jpg" alt="" class="img-fluid" style="margin-bottom: 10px">
			      	<p>Please contact us for further information regarding uniforms</p></a>
		      	</div>
		    </div>
	    	<div class="col-md-6">
		      	<div class="form-c"><p><strong>Contact Form</strong></p>
			        <?php echo do_shortcode('[contact-form-7 id="1289" title="Contact form 1"]'); ?>
		      	</div>
	    	</div>
	  	</div>
	</div>
</section>
	  <!--About End-->
<?php get_footer(); ?>