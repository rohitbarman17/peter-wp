<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package storefront
 */

?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>
<script type="text/javascript">
	var ajax_url = "<?php echo admin_url('admin-ajax.php'); ?>";
</script>
<body <?php body_class(); ?>>

<?php wp_body_open(); ?>

<?php do_action( 'storefront_before_site' ); ?>

<div id="page" class="hfeed site">
	<?php do_action( 'storefront_before_header' ); ?>

	<div class="desktop-delimiter"></div>
	  <!--Header Start-->
	  <header class="topheader">
	    <div class="container">
	      <div class="logofrm">
	        <div class="logo"> <a href="/"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/logo.jpg" alt="log"></a> </div>
	        <div class="user-cart">
	        	<?php if(is_user_logged_in()){ ?>
		        	<ul class="icon-class">
		            	<li><a href="/my-account"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/icon-user.svg" alt="user-icon"></a></li>
		            	<li> <?php echo do_shortcode("[custom-techno-mini-cart]"); ?></li>
		          	</ul>

	          	<?php }else{ ?>
		          	<ul class="social-icon">
		          		<li><a href="/my-account">Retail Login</a></li>
			            <li><a href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/facebook.svg" alt="facebook-icon"></a></li>
			            <li><a href="#"><img src="<?= get_stylesheet_directory_uri(); ?>/assets/images/instagram.svg" alt="instagram-icon"></a></li>
		          	</ul>
	          <?php } ?>
	          <!-- <div class="widget_shopping_cart_content"><?php woocommerce_mini_cart(); ?></div> -->

	         <?php //echo do_shortcode("[custom-techno-mini-cart]"); ?>
	        </div>
	        <div class="rt-bar">
	          	<?php
				wp_nav_menu( array( 
				    'menu' => 'main-menu', 
				    'container_class' => 'toplinks',
				    'menu_id' => 'menu' ) ); 
				?>
	        </div>
	      </div>
	    </div>
	    <?php 
	    	// do_action( 'storefront_header_cart' );
	    ?>
	  </header>
	  <!--Header End-->

	<?php
	/**
	 * Functions hooked in to storefront_before_content
	 *
	 * @hooked storefront_header_widget_region - 10
	 * @hooked woocommerce_breadcrumb - 10
	 */
	// do_action( 'storefront_before_content' );
	global $template;
	?>
	<section template="<?= $template ?>"></section>
	<!--Banner Start-->
	<?php 
	if($post){
		
		if(is_page() && $post->post_name != 'stockist'){

				$backgroundImg = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), 'full' );
				echo !empty($backgroundImg) ? '<section class="banner-w" style=" background:url('.$backgroundImg[0].') no-repeat top; background-size:cover"></section>' : '';	
			// }
			
		}		
	}
	?>
	<!--Banner End-->
	<div id="content" class="site-content" tabindex="-1">
		
		<?php
		if ( is_product()){
			$product = wc_get_product();
			$terms = get_the_terms($product->get_id(), 'brands');
			if(!empty($terms)){
				$current_brand = $terms[0];
				$brand_slug = $current_brand->slug;
	  			$brand_name = $current_brand->name;
				echo getBrandsBanner($brand_slug,$brand_name);
			}

		} else if(is_shop()){

			echo '<section class="hdr-democracy">';
			echo '<h2>'.get_the_title( get_option( 'woocommerce_shop_page_id' ) ).'</h2>';
			echo '</section>';

		} else if(is_product_category()){ 

			echo '<section class="hdr-democracy">';
			echo '<h2>Democracy</h2>';
			echo '</section>';

	  	}else if(is_tax()){
	  		$current_post = get_queried_object();
	  		$brand_slug = $current_post->slug;
	  		$brand_name = $current_post->name;
	  		echo getBrandsBanner($brand_slug,$brand_name);

	  	}else if(isset($post) && $post->post_name == 'stockist'){

	  		if(  isset($_GET['brand']) && $_GET['brand'] == 'democracy'){
				
				$brand_slug = 'democracy';
				$brand_name = 'Democracy';
				echo getBrandsBanner($brand_slug,$brand_name);
			
			}else if(  isset($_GET['brand']) && $_GET['brand'] == 'classified'){

				$brand_slug = 'classified';
				$brand_name = 'Classified';
				echo getBrandsBanner($brand_slug,$brand_name);
			
			}else{
				
				echo '<section class="hdr-democracy">';
				echo '<h2>'.$post->post_title.'</h2>';
				echo '</section>';
			}

	  	} ?>
  	
		<div class="col-full">
		<?php
		do_action( 'storefront_content_top' );
	?>
