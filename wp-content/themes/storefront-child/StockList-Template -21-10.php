 <?php /* Template Name: StockList-Template */ ?>
<!-- https://www.maps.ie/create-google-map/ -->
<?php get_header();?>
<?php 
	$brands = array(
				'democracy' => 'Democracy',
				'classified' => 'Classified'
				);

	$regionsList = get_terms( array(
	    'taxonomy' => 'regions',
	    'hide_empty' => false,
	) );
	

	$args = array('role' => 'customer' , 'meta_query' => array());

	if(isset($_GET['region']) && $_GET['region'] != 'all'){

		// $regionTerms = get_term_by('slug' , $_GET['region'] , 'regions');
		$region =  array(
	            'key'     => 'shipping_state',
	            'value'   => $_GET['region'],
	             'compare' => 'LIKE'
	    );
		array_push( $args['meta_query'], $region); 

	}

	if(isset($_GET['brand']) && $_GET['brand'] != 'all'){

		$brandTerms = get_term_by('slug' , $_GET['brand'] , 'brands');
		$brand =	array(
	        'key'     => 'brands',
	        'value'   => $brandTerms->term_id,
	        'compare' => 'LIKE'
	    );
		array_push( $args['meta_query'], $brand); 

	}

	if( (isset($_GET['region']) && $_GET['region'] != 'all') && (isset($_GET['brand']) && $_GET['brand'] != 'all')  ){
		$args['meta_query']['relation'] = 'AND';
	}

	$user_query = new WP_User_Query( $args );
	$usersData = $user_query->get_results();
	
?>
<style type="text/css">
	@import url('https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800&display=swap');

	.col-full .container {
	    padding-top: 25px;
	}
	.team-name {
	    color: #555;
	    display: block;
	    font-size: 18px;
	    margin-bottom: 5px;
	    text-transform: uppercase;
	}
	.team-position a {
	    display: block;
	    color: #72c02c;
	    font-style: normal;
	    margin-bottom: 15px;
        text-decoration: none;
	}
	.fb-link{
	    color: #a3d8dc;
	    padding: 5px;
	}
	.dp-table {
	    display: table;
	    height: 100%;
	    min-height: 280px;
	}
	.equal-height-column {
	    display: table-cell;
	    height: 100%;
	    vertical-align: middle;
	}
</style>
<div class="container">
  	<form id="fiterForm" method="get">	
  		<label>Select a Region</label>
  		<select class="stockList" name="region">
  			<?php
  				echo "<option value='all'>All</option>";
  				foreach ($regionsList as $key => $region) {
  					$seltedRegion = (isset($_GET['region']) && $_GET['region']== $region->slug) ? 'selected' : '';
  					echo "<option ".$seltedRegion." value='".$region->slug."'>".$region->name."</option>";
  				}
  			?>
  		</select>
  		<label>Select a Brand</label>
  		<select class="stockList" name="brand">
  			<?php
  				echo "<option value='all'>All</option>";
  				foreach ($brands as $key => $brand) {
  					$seltedBrnd = (isset($_GET['brand']) && $_GET['brand']== $key) ? 'selected' : '';
  					echo "<option ".$seltedBrnd." value='".$key."'>".$brand."</option>";
  				}
  			?>
  		</select>
  	</form>
  	<?php 
  	foreach ($usersData as $key => $user) {
  		$user_id = $user->ID;
  		$storeName = get_field('store_name', 'user_'. $user_id );
  		$storeMap = get_field('store_map', 'user_'. $user_id );
  		$phoneNumber = get_user_meta($user_id, 'shipping_phone', true);
  		$address1 = get_user_meta($user_id, 'shipping_address_1', true);
  		$address2 = get_user_meta($user_id, 'shipping_address_2', true);
  		$address = $address1 .' '. $address2;

  		if($key%2 == 0){?>
  			<div class="row">
		        <div class="col-sm-6"><?= $storeMap; ?></div>
		        <div class="col-sm-6">
		           <div class="ml-4 dp-table">
		                <div class="equal-height-column">
		                    <div class="team-name"><?= $storeName; ?></div>
		                    <div class="team-position"><a href="tel:<?= $phoneNumber; ?>"><i class="fa fa-phone-square"></i>&nbsp; <?= $phoneNumber; ?></a></div>
		                    <div class="address-list mb-2"><i class="fa fa-building"></i> &nbsp; <?= $address; ?></div>
		                    <div class="info">
		                    	<?php
		                    		$website = get_field('website', 'user_'. $user_id );
		                    		echo (!empty($website)) ? '<a class="fb-link" href="'.$website.'" target="_blank"><i class="fa fa-2x fa-globe"></i></a>' : '';
		                    		$facebook_store = get_field('facebook_store', 'user_'. $user_id ); 
		                    		echo (!empty($facebook_store)) ? '<a class="fb-link" href="http://www.abc.com" target="_blank"><i class="fab fa-2x  fa-facebook-square"></i></a>' : '';
		                        ?>
		                    </div>
		                </div>
		            </div>
		        </div>
			</div>
		<?php }else{ ?>
			<div class="row">
		        <div class="col-sm-6">
		           <div class="ml-4 dp-table">
		                <div class="equal-height-column">
		                    <div class="team-name"><?= $storeName; ?></div>
		                    <div class="team-position"><a href="tel:<?= $phoneNumber; ?>"><i class="fa fa-phone-square"></i>&nbsp; <?= $phoneNumber; ?></a></div>
		                    <div class="address-list mb-2"><i class="fa fa-building"></i> &nbsp; <?= $address; ?></div>
		                    <div class="info">
		                        <?php
		                    		$website = get_field('website', 'user_'. $user_id );
		                    		echo (!empty($website)) ? '<a class="fb-link" href="'.$website.'" target="_blank"><i class="fa fa-2x fa-globe"></i></a>' : '';
		                    		$facebook_store = get_field('facebook_store', 'user_'. $user_id ); 
		                    		echo (!empty($facebook_store)) ? '<a class="fb-link" href="http://www.abc.com" target="_blank"><i class="fab fa-2x  fa-facebook-square"></i></a>' : '';
		                        ?>
		                    </div>
		                </div>
		            </div>
		        </div>
		        <div class="col-sm-6"><?= $storeMap; ?></div>
		    </div>
	<?php } 
	}
	?>
</div>
<?php get_footer(); ?>
<script>
	jQuery(document).ready(function(){
		jQuery(".stockList").on("change", function(){
			jQuery("#fiterForm").submit();
		});
	})
</script>